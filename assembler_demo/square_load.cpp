#include<hip/hip_runtime.h>
#include<hip/hip_runtime_api.h>
#include<iostream>
#include<fstream>
#include<vector>

#define LEN 64
#define SIZE LEN<<2

#define LOAD_FROM_HSACO


#ifdef LOAD_FROM_HSACO
#define fileName "square.hsaco"
#define kernel_name "_ZZ4mainEN67HIP_kernel_functor_name_begin_unnamed_HIP_kernel_functor_name_end_319__cxxamp_trampolineEPfS0_"
#endif

#ifdef LOAD_FROM_COBA
#include "kernel.h"
#define kernel_name "_ZZ4mainEN67HIP_kernel_functor_name_begin_unnamed_HIP_kernel_functor_name_end_319__cxxamp_trampolineEPfS0_"
#endif

int main(){
    float *A, *B;
    hipDeviceptr_t Ad, Bd;
    A = new float[LEN];
    B = new float[LEN];

    for(uint32_t i=0;i<LEN;i++){
        A[i] = i*1.0f;
        B[i] = 0.0f;
        std::cout<<A[i] << " "<<B[i]<<std::endl;
    }

    hipError_t res;
    res = hipMalloc((void**)&Ad, SIZE);
    hipMalloc((void**)&Bd, SIZE);

    hipMemcpyHtoD(Ad, A, SIZE);
    hipMemcpyHtoD(Bd, B, SIZE);
    hipModule_t Module;
    hipFunction_t Function;
    #ifdef LOAD_FROM_HSACO
    res = hipModuleLoad(&Module, fileName);
    #endif
    #ifdef LOAD_FROM_COBA
    res = hipModuleLoadData(&Module, square_kernel);
    #endif
    std::cout<<"Load Data  " << hipGetErrorString(res) << std::endl;
    res = hipModuleGetFunction(&Function, Module, kernel_name);
    std::cout<<"Load Func  "<< kernel_name << " "<< hipGetErrorString(res) << std::endl;
    //void *args[2]={&Bd, &Ad};
    struct {
        float * dataA;
        float * dataB;
    } hipFunctionArgs;
    hipFunctionArgs.dataA = (float *)Bd;
    hipFunctionArgs.dataB = (float *)Ad;
    size_t hipFunctionArgsSize = sizeof(hipFunctionArgs);
    void *hipLaunchParams[] = {HIP_LAUNCH_PARAM_BUFFER_POINTER, &hipFunctionArgs, HIP_LAUNCH_PARAM_BUFFER_SIZE, &hipFunctionArgsSize, HIP_LAUNCH_PARAM_END};
    res = hipModuleLaunchKernel(Function, 1, 1, 1, LEN, 1, 1, 0, 0, NULL, (void **)hipLaunchParams);
    std::cout<<"Launch Kernel   "<<hipGetErrorString(res) << std::endl;
    hipMemcpyDtoH(B, Bd, SIZE);
    hipMemcpyDtoH(A, Ad, SIZE);
    for(uint32_t i=0;i<LEN;i++){
        std::cout<<A[i]<<" - "<<B[i]<<std::endl;
    }

    return 0;
}
