#!/bin/bash

TEST=$1

grep -R -n "TFLOPS" ${TEST}.log | grep "direct" | cut -d "-" -f 2 | cut -d " " -f 7 > direct.txt
grep -R -n "TFLOPS" ${TEST}.log | grep "gemm" | cut -d "-" -f 2 | cut -d " " -f 7 > gemm.txt

python plot.py ${TEST}

rm direct.txt gemm.txt
