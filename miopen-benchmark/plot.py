#!/usr/bin/python2
# encoding: utf-8       
import matplotlib  
import matplotlib.pyplot as plt 
import sys

def readFromFile():
    y_direct = []
    count_direct = 0
    with open("direct.txt", "r") as direct:
        for line in direct:
            y_direct.append(float(line))
            count_direct += 1
    y_gemm = []
    count_gemm = 0
    with open("gemm.txt", "r") as gemm:
        for line in gemm:
            y_gemm.append(float(line))
            count_gemm += 1
    assert count_direct == count_gemm
    return y_direct, y_gemm, count_direct

y_direct, y_gemm, count = readFromFile()
x= range(1, 1+count)
ax = plt.subplot(1,1,1)
plot_direct, = ax.plot(x, y_direct, 'b')
plot_gemm, = ax.plot(x, y_gemm, 'r')
ax.legend((plot_direct,plot_gemm,), ('direct', 'gemm',))
plt.xlabel('conv')
plt.ylabel("TFLOPS")
plt.title(sys.argv[1])
plt.show()
