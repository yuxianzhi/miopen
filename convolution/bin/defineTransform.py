#!/usr/bin/python
import argparse
import os

def define_transform(line):
    define_array = line.split(" ")
    output_content = ""
    validate_content = ""
    for element in define_array:
        element_length = len(element)
        if element_length > 1:
            index = element.index("=")
            if index == -1:
                index = element_length
            key = element[2:index]
            value = ""
            if not index == element_length:
                value = element[index+1 : element_length]
            output_content += "#define " + key + " " + value + "\n"
            validate_content += " -D" + key + "=" + value

    if validate_content.strip() == line.strip():
        print output_content
    else:
        print("ERROR: ")
        print("Input: %s" % line)
        print("Output: %s" % validate_content)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Transform define")
    parser.add_argument('-i','--input', required=True, help='Input define file', type=str)
    args = parser.parse_args()
    if not os.path.exists(args.input):
        print("[ERROR] Please Make sure input file [%s] exists!!!" % args.input)
        exit(0)
    inputFile = open(args.input, "r")
    inputFileContent = ""
    for line in inputFile:
        inputFileContent += line[0:len(line)-1]
    inputFile.close()
    define_transform(inputFileContent)    
