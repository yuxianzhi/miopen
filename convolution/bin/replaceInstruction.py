#!/usr/bin/python
import os
import argparse
import re

def replaceInstruction(isaFileName, outFileName, debug):
    isaFileContent = open(isaFileName, "r");
    outIsaFileContent = ""
    for line in isaFileContent:
        matchResult = re.findall(r"\\s{2,}|\tv_fma_f32 (.*), (.*), (.*), (.*)", line)
        if len(matchResult) > 0:
            dest = matchResult[0][0]
            src1 = matchResult[0][1]
            src2 = matchResult[0][2]
            src3 = matchResult[0][3]
            if dest == src3:
                output = "        v_mac_f32 " + dest + ", " + src1 + ", " + src2 + "\n"
                if debug:
                    print("In: %s Out: %s" % (line, output))
                outIsaFileContent += output
            else:
                if debug:
                    print("In: %s Out: mismatch src3" % (line))
                outIsaFileContent += line
        else:
            if debug:
                print("In: %s Out: mismatch I" % (line))
            outIsaFileContent += line

    isaFileContent.close()
    outIsaFile = open(outFileName, "w+")
    outIsaFile.write(outIsaFileContent)
    outIsaFile.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Replace instruction in .isa file ")
    parser.add_argument('-i','--input', required=True, help='Input .isa file, eg. dump-gfx900.isa', type=str)  
    parser.add_argument('-o','--output', default='result.isa', help='Output .isa file', type=str)  
    parser.add_argument('-d','--debug', default=False, help='Debug', type=bool)  
    args = parser.parse_args()
    if not os.path.exists(args.input):
        print("[ERROR] Please Make sure input file [%s] exists!!!" % args.input)
        exit(0)
    replaceInstruction(args.input, args.output, args.debug)
