#!/usr/bin/python
import os
import argparse
import re

def replaceKernelName(isaFileName, kernelNameNew, outFileName):
    isaFileContent = open(isaFileName, "r");
    outIsaFileContent = ""
    kernelNameOld = ""
    """
        .weak   _ZZ13gpuDataRandomIfEvPT_ibEN67HIP_kernel_functor_name_begin_unnamed_HIP_kernel_functor_name_end_419__cxxamp_trampolineEPfic ; -- Begin function _ZZ13gpuDataRandomIfEvPT_ibEN67HIP_kernel_functor_name_begin_unnamed_HIP_kernel_functor_name_end_419__cxxamp_trampolineEPfic
        .type   _ZZ13gpuDataRandomIfEvPT_ibEN67HIP_kernel_functor_name_begin_unnamed_HIP_kernel_functor_name_end_419__cxxamp_trampolineEPfic,@function
        .amdgpu_hsa_kernel _ZZ13gpuDataRandomIfEvPT_ibEN67HIP_kernel_functor_name_begin_unnamed_HIP_kernel_functor_name_end_419__cxxamp_trampolineEPfic
_ZZ13gpuDataRandomIfEvPT_ibEN67HIP_kernel_functor_name_begin_unnamed_HIP_kernel_functor_name_end_419__cxxamp_trampolineEPfic: ; @_ZZ13gpuDataRandomIfEvPT_ibEN67HIP_kernel_functor_name_begin_unnamed_HIP_kernel_functor_name_end_419__cxxamp_trampolineEPfic
.Lfunc_begin0:
        .size   _ZZ13gpuDataRandomIfEvPT_ibEN67HIP_kernel_functor_name_begin_unnamed_HIP_kernel_functor_name_end_419__cxxamp_trampolineEPfic, .Lfunc_end0-_ZZ13gpuDataRandomIfEvPT_ibEN67HIP_kernel_functor_name_begin_unnamed_HIP_kernel_functor_name_end_419__cxxamp_trampolineEPfic
                                        ; -- End function
  - Name:            _ZZ13gpuDataRandomIfEvPT_ibEN67HIP_kernel_functor_name_begin_unnamed_HIP_kernel_functor_name_end_419__cxxamp_trampolineEPfic
    """
    codeTemplate_0 = "\t.weak\t"
    codeTemplate_1 = "\t.type\t"
    codeTemplate_2 = "\t.amdgpu_hsa_kernel "
    codeTemplate_3 = "\t.size\t"
    codeTemplate_4 = "  - Name:            "
    for line in isaFileContent:
        if line.startswith(codeTemplate_0):
            kernelNameOld = re.findall(r"\t.weak\t(.*) ; -- Begin function .*", line)
            if len(kernelNameOld) > 0:
                kernelNameOld = kernelNameOld[0]
            outIsaFileContent = outIsaFileContent + codeTemplate_0 + kernelNameNew + " ; -- Begin function " + kernelNameNew + "\n"
            continue
        if line.startswith(codeTemplate_1+kernelNameOld):
            outIsaFileContent = outIsaFileContent + codeTemplate_1 + kernelNameNew + ",@function\n"
            continue
        if line.startswith(codeTemplate_2+kernelNameOld):
            outIsaFileContent = outIsaFileContent + codeTemplate_2 + kernelNameNew + "\n"
            continue
        if line == (kernelNameOld + ": ; @" + kernelNameOld + "\n"):
            outIsaFileContent = outIsaFileContent + kernelNameNew + ": ; @" + kernelNameNew + "\n"
            continue
        if line.startswith(codeTemplate_3+kernelNameOld):
            outIsaFileContent = outIsaFileContent + codeTemplate_3 + kernelNameNew + ", .Lfunc_end0-" + kernelNameNew + "\n"
            continue
        if line.startswith(codeTemplate_4+kernelNameOld):
            outIsaFileContent = outIsaFileContent + codeTemplate_4 + kernelNameNew + "\n"
            continue

        if (not kernelNameOld == "") and line.find(kernelNameOld) > -1:
            print("[ERROR] %s" % line)
        outIsaFileContent += line 

    outIsaFile = open(outFileName, "w+")
    outIsaFile.write(outIsaFileContent)
    outIsaFile.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Replace kernel name in .isa file ")
    parser.add_argument('-i','--input', required=True, help='Input .isa file, eg. dump-gfx900.isa', type=str)  
    parser.add_argument('-n','--name', required=True, help='New kernel name', type=str)  
    parser.add_argument('-o','--output', help='Output file', default="result.isa", type=str)
    args = parser.parse_args()
    if not os.path.exists(args.input):
        print("[ERROR] Please Make sure input file [%s] exists!!!" % args.input)
        exit(0)
    replaceKernelName(args.input, args.name, args.output)
