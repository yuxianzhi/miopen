#!/bin/bash


HSACO_INFILE=$1
ISA_OUTFILE=$2
basepath=$(cd `dirname $0`; pwd)

if [ ! -f "$HSACO_INFILE" ];
then
    echo "Please Make sure $HSACO_INFILE is exists!!!"
    exit 0
fi
if [ $ISA_OUTFILE == "" ];
then
    echo "Please add second parameter to specify the out file"
    exit 0
fi

OPTION=" -disassemble -mcpu=fiji"
llvm-objdump $OPTION $HSACO_INFILE > $ISA_OUTFILE

${basepath}/removeCommentAndStrip.py -i $ISA_OUTFILE -s "//"
