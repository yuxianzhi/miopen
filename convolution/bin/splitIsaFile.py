#!/usr/bin/python
import os
import argparse
import re

def splitIsaFile(isaFileName, outDir='isa'):
    isaFileContent = open(isaFileName, "r");
    firstLine = "	.text\n"
    isFirstFile = True
    subIsaFileName = None
    subIsaFileContent = ""
    kernel_name = None
    kernel_code = True
    kernel_common_header = ""
    kernel_header = ""
    kernel_common_tail = ""
    kernel_count_1 = 0
    kernel_count_2 = 0
    kernelFileList = []
    section = 0
    for line in isaFileContent:
        if line == "\n" :
            continue

        if line.startswith('\t.ident'):
            if (not subIsaFileContent == "") and (subIsaFileName is not None):
                subIsaFile = open(subIsaFileName, "w+")
                subIsaFile.write(subIsaFileContent)
                subIsaFile.close()
            section = 1

        if line.startswith('...'):
            if (not kernel_header == "") and (subIsaFileName is not None):
                subIsaFile = open(subIsaFileName, "a")
                subIsaFile.write(kernel_header)
                subIsaFile.close()
            section = 2

        if section == 0:
            if line == firstLine:
                kernel_count_1 += 1
                if not isFirstFile:
                    if subIsaFileName is not None:
                        subIsaFile = open(subIsaFileName, "w+")
                        subIsaFile.write(subIsaFileContent)
                        subIsaFile.close()
                        subIsaFileContent = ""
                        kernel_name = None
                        subIsaFileName = None
                    else:
                        print("[ERROR] Recognized file content failed!!!")
                isFirstFile = False
            subIsaFileContent = subIsaFileContent + line
            
            kernel_name = re.findall(r"\t.amdgpu_hsa_kernel (.*)", line)
            if len(kernel_name) > 0:
                subIsaFileName = outDir + "/" + kernel_name[0] + ".isa"
                kernelFileList.append(subIsaFileName)
        if section == 1:
            if line.startswith('  - Name:            '):
                # write last kernel header
                if not kernel_count_2 == 0:
                    subIsaFile = open(subIsaFileName, "a")
                    subIsaFile.write(kernel_header)
                    subIsaFile.close()
                kernel_count_2 += 1
                kernel_header = kernel_common_header + line
                kernel_name = re.findall(r"  - Name:            (.*)", line)
                subIsaFileName = outDir + "/" + kernel_name[0] + ".isa"
                if subIsaFileName not in kernelFileList:
                    print("[ERROR] Kernel [%s] only have header, doesn't have code!!!" % kernel_name[0])
            else:
                if kernel_count_2 == 0:
                    kernel_common_header += line
                else:
                    kernel_header += line

        if section == 2:
            kernel_common_tail += line

    for subIsaFileName in kernelFileList:
        subIsaFile = open(subIsaFileName, "a")
        subIsaFile.write(kernel_common_tail)
        subIsaFile.close()
    
    isaFileContent.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Split dump-gfx*.isa file to many subFiles")
    parser.add_argument('-i','--input', required=True, help='Input .isa file, eg. dump-gfx900.isa', type=str)  
    parser.add_argument('-o','--output', help='Output file directory', default="kernel_isa", type=str)  
    args = parser.parse_args()
    if not os.path.exists(args.input):
        print("[ERROR] Please Make sure input file [%s] exists!!!" % args.input)
        exit(0)
    if not os.path.isdir(args.output):
        os.makedirs(args.output)
    splitIsaFile(args.input, args.output)
