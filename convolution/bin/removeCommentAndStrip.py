#!/usr/bin/python
import os
import argparse
import re

def removeCommentAndStrip(fileName, commentSymbol):
    fileContent = open(fileName, "r")
    fileContent_new = ""
    for line in fileContent:
        index = line.find(commentSymbol)
        if index > -1:
            line = line[:index]
            line = line.rstrip()
            fileContent_new += "\n" + line
        else:
            fileContent_new += "\n" + line.rstrip() 
    fileContent.close()
    os.remove(fileName)
    
    # write out
    file_new = open(fileName, "w")
    file_new.write(fileContent_new)
    file_new.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Remove code note and strip")
    parser.add_argument('-i','--input', required=True, help='Input file', type=str)  
    parser.add_argument('-s','--symbol', help='Comment symbol', default="//", type=str)
    args = parser.parse_args()
    if not os.path.exists(args.input):
        print("[ERROR] Please Make sure input file [%s] exists!!!" % args.input)
        exit(0)
    removeCommentAndStrip(args.input, args.symbol)
