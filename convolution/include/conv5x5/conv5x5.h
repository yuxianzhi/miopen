#ifndef __CONV5X5_H__
#define __CONV5X5_H__

#include "hip_fundamental.h"
#include "tensor.h"
#include "convolution_cpu.h"
#include "conv5x5/conv5x5_kernel_parameter.h"

#define LOAD_FROM_HSACO 1
#define LOAD_FROM_BYTESARRAY 1

#define ITER 20


// CPU conv5x5, no bias, no stride, padding
void conv5x5_cpu(Tensor input, Tensor weight, Tensor stride, int *padding, Tensor output){
    //std::cout << input.data << std::endl;
    double conv_cpu_time = 0;
    for (int iter=0; iter<1; iter++){
        conv_cpu_time += convolution_cpu(input,
                                           weight,
                                           output,
                                           stride,
                                           padding,
                                           0,
                                           NULL,
                                           NULL);
    }
    //conv_cpu_time /= ITER;
    double conv_cpu_tflops = conv_TFLOPS(input, weight, output, conv_cpu_time);
    std::cout << "[INFO] Convolution 5x5 CPU Time: " << conv_cpu_time << " sec " << std::endl;
    std::cout << "[INFO] Convolution 5x5 CPU Perf: " << conv_cpu_tflops << " TFLOPS " << std::endl;
}


#ifdef LOAD_FROM_HSACO
// GPU conv5x5, no bias, no stride, padding
// hsaco kernel
void conv5x5_gpu_load_from_hsaco(Tensor input, Tensor weight, Tensor stride, int *padding, Tensor output){
    Tensor input_d = gpuTensor(input);
    Tensor weight_d = gpuTensor(weight);
    Tensor output_d = gpuTensor(output, false);

    const char *hsacoFileName = "conv5x5.hsaco";
    const char *kernelName = "MIOpenConvUniC";
    hipFunction_t kernel_function = loadKernelFromHsaco(hsacoFileName, kernelName);
    struct {
        const FLOAT* in_ptr;
        const FLOAT* wei_ptr;
#if MLO_CONV_BIAS
        const FLOAT* bias;
#endif
        FLOAT* out_ptr;
        int padding_val;
    } hipFunctionArgs;
    hipFunctionArgs.in_ptr = (const FLOAT *)input_d.data;
    hipFunctionArgs.wei_ptr = (const FLOAT *)weight_d.data;
#if MLO_CONV_BIAS
    hipFunctionArgs.bias = NULL;
#endif
    hipFunctionArgs.out_ptr = (FLOAT *)output_d.data;
    hipFunctionArgs.padding_val = 0;
    size_t hipFunctionArgsSize = sizeof(hipFunctionArgs);
    void *hipLaunchParams[] = {HIP_LAUNCH_PARAM_BUFFER_POINTER, &hipFunctionArgs, HIP_LAUNCH_PARAM_BUFFER_SIZE, &hipFunctionArgsSize, HIP_LAUNCH_PARAM_END};

    double conv_gpu_time = 0;
    for(int iter=0; iter<ITER; iter++){
        gpu_time_tic();
        GPU_CHECK(hipModuleLaunchKernel(kernel_function,
                                        1, 14, 128,
                                        256, 1, 1,
                                        0, 0, NULL,
                                        (void **)hipLaunchParams));
        conv_gpu_time += gpu_time_toc();
    }
    conv_gpu_time /= ITER;
    double conv_gpu_tflops = conv_TFLOPS(input_d, weight_d, output_d, conv_gpu_time);
    std::cout << "[INFO] Convolution 5x5 GPU Time: " << conv_gpu_time << " sec " << std::endl;
    std::cout << "[INFO] Convolution 5x5 GPU Perf: " << conv_gpu_tflops << " TFLOPS " << std::endl;
    std::cout << "[INFO] Convolution 5x5 CPU&GPU Verify: " << cpuTensor_gpuTensor_verify(output, output_d) << std::endl;
}
#endif

#ifdef LOAD_FROM_BYTESARRAY
#include "conv5x5/conv5x5_opencl_kernel_bytesArray.h"
// GPU conv5x5, no bias, no stride, padding
// code bytes array kernel
void conv5x5_gpu_load_from_bytesArray(Tensor input, Tensor weight, Tensor stride, int *padding, Tensor output){
    Tensor input_d = gpuTensor(input);
    Tensor weight_d = gpuTensor(weight);
    Tensor output_d = gpuTensor(output, false);

    const char *kernelName = "MIOpenConvUniC";
    hipFunction_t kernel_function = loadKernelFromBytesArray(_conv5x5_HSA_CodeObjMem, kernelName);
    struct {
        const FLOAT* in_ptr;
        const FLOAT* wei_ptr;
#if MLO_CONV_BIAS
        const FLOAT* bias;
#endif
        FLOAT* out_ptr;
        int padding_val;
    } hipFunctionArgs;
    hipFunctionArgs.in_ptr = (const FLOAT *)input_d.data;
    hipFunctionArgs.wei_ptr = (const FLOAT *)weight_d.data;
#if MLO_CONV_BIAS
    hipFunctionArgs.bias = NULL;
#endif
    hipFunctionArgs.out_ptr = (FLOAT *)output_d.data;
    hipFunctionArgs.padding_val = 0;
    size_t hipFunctionArgsSize = sizeof(hipFunctionArgs);
    void *hipLaunchParams[] = {HIP_LAUNCH_PARAM_BUFFER_POINTER, &hipFunctionArgs, HIP_LAUNCH_PARAM_BUFFER_SIZE, &hipFunctionArgsSize, HIP_LAUNCH_PARAM_END};

    double conv_gpu_time = 0;
    for(int i=0; i<ITER; i++){
        gpu_time_tic();
        GPU_CHECK(hipModuleLaunchKernel(kernel_function,
                                        1, 14, 128,
                                        256, 1, 1,
                                        0, 0, NULL,
                                        (void **)hipLaunchParams));
        conv_gpu_time += gpu_time_toc();
    }
    conv_gpu_time /= ITER;
    double conv_gpu_tflops = conv_TFLOPS(input_d, weight_d, output_d, conv_gpu_time);
    std::cout << "[INFO] Convolution 5x5 GPU Time: " << conv_gpu_time << " sec " << std::endl;
    std::cout << "[INFO] Convolution 5x5 GPU Perf: " << conv_gpu_tflops << " TFLOPS " << std::endl;
    //std::cout << "[INFO] Convolution 5x5 CPU&GPU Verify: " << cpuTensor_gpuTensor_verify(output, output_d) << std::endl;
    gpu2cpu(output_d.data, output.data, sizeof(FLOAT)*output.n*output.c*output.h*output.w);
}
#endif


void conv5x5(){
    const int batch = MLO_BATCH_SZ;
    const int in_channel = MLO_N_INPUTS;
    const int out_channel = MLO_N_OUTPUTS;
    const int input_n = batch, output_n = batch;
    const int input_c = in_channel, weight_c = in_channel;
    const int input_h = MLO_IN_HEIGHT;
    const int input_w = MLO_IN_WIDTH;
    const int weight_h = 5;
    const int weight_w = 5;
    const int weight_n = out_channel, output_c = out_channel;
    const int padding_h = 2;
    const int padding_w = 2;
    const int stride_h = 1;
    const int stride_w = 1;
    const int output_h = MLO_OUT_HEIGHT;
    const int output_w = MLO_OUT_WIDTH;

    Tensor input = cpuTensor(input_n, input_c, input_h, input_w, true, true);
    Tensor weight = cpuTensor(weight_n, weight_c, weight_h, weight_w, true, false);
    Tensor output = cpuTensor(output_n, output_c, output_h, output_w, true, true);
    Tensor stride = cpuTensor(1, 1, stride_h, stride_w, false, false);
    int padding[2] = {padding_h, padding_w};
    
    // CPU
    //conv5x5_cpu(input, weight, stride, padding, output);
   
    // GPU
#if LOAD_FROM_BYTESARRAY
    conv5x5_gpu_load_from_bytesArray(input, weight, stride, padding, output);
#endif
#if LOAD_FROM_HSACO
    conv5x5_gpu_load_from_hsaco(input, weight, stride, padding, output);
#endif
}

#endif

