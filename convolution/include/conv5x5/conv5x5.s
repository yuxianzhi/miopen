	.text
	.hsa_code_object_version 2,1
	.hsa_code_object_isa 9,0,0,"AMD","AMDGPU"
	.globl	MIOpenConvUniC          ; -- Begin function MIOpenConvUniC
	.p2align	8
	.type	MIOpenConvUniC,@function
	.amdgpu_hsa_kernel MIOpenConvUniC
MIOpenConvUniC:                         ; @MIOpenConvUniC
	.amd_kernel_code_t
		amd_code_version_major = 1
		amd_code_version_minor = 1
		amd_machine_kind = 1
		amd_machine_version_major = 9
		amd_machine_version_minor = 0
		amd_machine_version_stepping = 0
		kernel_code_entry_byte_offset = 256
		kernel_code_prefetch_byte_size = 0
		max_scratch_backing_memory_byte_size = 0
		granulated_workitem_vgpr_count = 25
		granulated_wavefront_sgpr_count = 3
		priority = 0
		float_mode = 192
		priv = 0
		enable_dx10_clamp = 1
		debug_mode = 0
		enable_ieee_mode = 1
		enable_sgpr_private_segment_wave_byte_offset = 0
		user_sgpr_count = 6
		enable_trap_handler = 1
		enable_sgpr_workgroup_id_x = 1
		enable_sgpr_workgroup_id_y = 1
		enable_sgpr_workgroup_id_z = 1
		enable_sgpr_workgroup_info = 0
		enable_vgpr_workitem_id = 0
		enable_exception_msb = 0
		granulated_lds_size = 0
		enable_exception = 0
		enable_sgpr_private_segment_buffer = 1
		enable_sgpr_dispatch_ptr = 0
		enable_sgpr_queue_ptr = 0
		enable_sgpr_kernarg_segment_ptr = 1
		enable_sgpr_dispatch_id = 0
		enable_sgpr_flat_scratch_init = 0
		enable_sgpr_private_segment_size = 0
		enable_sgpr_grid_workgroup_count_x = 0
		enable_sgpr_grid_workgroup_count_y = 0
		enable_sgpr_grid_workgroup_count_z = 0
		enable_ordered_append_gds = 0
		private_element_size = 1
		is_ptr64 = 1
		is_dynamic_callstack = 0
		is_debug_enabled = 0
		is_xnack_enabled = 0
		workitem_private_segment_byte_size = 0
		workgroup_group_segment_byte_size = 10488
		gds_segment_byte_size = 0
		kernarg_segment_byte_size = 64
		workgroup_fbarrier_count = 0
		wavefront_sgpr_count = 28
		workitem_vgpr_count = 102
		reserved_vgpr_first = 0
		reserved_vgpr_count = 0
		reserved_sgpr_first = 0
		reserved_sgpr_count = 0
		debug_wavefront_private_segment_offset_sgpr = 0
		debug_private_segment_buffer_sgpr = 0
		kernarg_segment_alignment = 4
		group_segment_alignment = 4
		private_segment_alignment = 4
		wavefront_size = 6
		call_convention = -1
		runtime_loader_kernel_symbol = 0
	.end_amd_kernel_code_t
; BB#0:                                 ; %.lr.ph292.preheader
	v_cvt_f32_ubyte0_e32 v2, v0
	v_mov_b32_e32 v1, 0x3727c5ac
	s_mov_b32 s0, 0xffffff
	s_movk_i32 s1, 0xff04
	v_mac_f32_e32 v1, 0x3b820821, v2
	v_cvt_u32_f32_e32 v33, v1
	v_mov_b32_e32 v1, 0x3727c5ac
	s_movk_i32 s2, 0xff82
	v_mov_b32_e32 v7, 0x640
	v_and_b32_e32 v3, s0, v33
	v_mul_lo_i32 v3, v3, s1
	s_movk_i32 s1, 0x782
	v_mul_lo_i32 v5, v33, s1
	s_mul_i32 s1, s7, 14
	v_add_i32_e32 v6, vcc, v0, v3
	v_cvt_f32_u32_e32 v3, v6
	v_mul_u32_u24_e32 v37, s1, v7
	v_mov_b32_e32 v4, 0x3727c5ac
	s_load_dwordx2 s[12:13], s[4:5], 0x0
	v_mac_f32_e32 v1, 0x3c020821, v3
	v_cvt_u32_f32_e32 v1, v1
	s_load_dwordx2 s[14:15], s[4:5], 0x8
	s_load_dwordx2 s[10:11], s[4:5], 0x10
	s_mul_i32 s9, s6, 27
	v_and_b32_e32 v3, s0, v1
	v_mul_lo_i32 v7, v3, s2
	v_mul_lo_i32 v3, v1, 7
	s_mul_i32 s7, s8, 0xb640
	s_mov_b64 s[2:3], 0
	v_add_i32_e32 v6, vcc, v7, v6
	v_cvt_f32_u32_e32 v7, v6
	v_add_i32_e32 v1, vcc, s1, v3
	s_mov_b32 m0, -1
	v_mac_f32_e32 v4, 0x3de38e39, v7
	v_cvt_u32_f32_e32 v4, v4
	v_and_b32_e32 v7, s0, v4
	v_mul_lo_i32 v7, v7, -9
	v_mul_lo_i32 v8, v4, 62
	v_lshlrev_b32_e32 v31, 1, v4
	v_add_i32_e32 v6, vcc, v7, v6
	v_mul_lo_i32 v34, v6, 3
	v_add_i32_e32 v4, vcc, v8, v5
	v_mov_b32_e32 v5, v0
	v_add_i32_e32 v41, vcc, v4, v34
	v_lshlrev_b32_e32 v4, 2, v0
BB0_1:                                  ; %.lr.ph292
                                        ; =>This Inner Loop Header: Depth=1
	v_add_i32_e32 v5, vcc, 0x100, v5
	v_mov_b32_e32 v6, 0x781
	v_cmp_gt_u32_e64 s[0:1], v5, v6
	v_mov_b32_e32 v6, 0
	ds_write_b32 v4, v6
	v_add_i32_e32 v4, vcc, 0x400, v4
	s_or_b64 s[2:3], s[0:1], s[2:3]
	s_andn2_b64 exec, exec, s[2:3]
	s_cbranch_execnz BB0_1
; BB#2:                                 ; %.preheader277.preheader
	s_or_b64 exec, exec, s[2:3]
	v_mov_b32_e32 v6, 0x3ca3d70a
	v_madak_f32 v2, v2, v6, 0x3727c5ac
	v_cvt_u32_f32_e32 v2, v2
	v_mov_b32_e32 v4, 0xfc
	v_cmp_lt_u32_e64 s[0:1], v0, v4
	v_sub_i32_e32 v4, vcc, 0x2bb, v0
	v_lshrrev_b32_e32 v5, 8, v4
	v_bfe_u32 v4, v4, 8, 1
	s_movk_i32 s16, 0x640
	v_cmp_eq_u32_e64 s[2:3], 0, v4
	v_mul_lo_i32 v4, v2, s16
	v_and_b32_e32 v2, 0xffffff, v2
	s_movk_i32 s19, 0xffce
	v_mul_lo_i32 v2, v2, s19
	s_movk_i32 s19, 0x2d9
	s_mul_i32 s6, s6, s19
	s_movk_i32 s18, 0x100
	v_add_i32_e32 v4, vcc, v4, v0
	s_add_i32 s6, s7, s6
	v_lshlrev_b32_e32 v6, 2, v0
	s_movk_i32 s17, 0x1e08
	v_add_i32_e32 v49, vcc, v2, v4
	v_add_i32_e32 v47, vcc, s17, v6
	v_cmp_ne_u32_e64 s[4:5], 0, v5
	v_mul_u32_u24_e32 v50, 50, v3
	v_add_i32_e32 v45, vcc, 31, v41
	v_or_b32_e32 v48, s18, v0
	v_mov_b32_e32 v2, 0
	v_mov_b32_e32 v52, s6
	v_mov_b32_e32 v53, 0
	s_movk_i32 s20, 0x3c1
	v_mov_b32_e32 v3, 0
	v_mov_b32_e32 v4, 0
	v_mov_b32_e32 v5, 0
	v_mov_b32_e32 v6, 0
	v_mov_b32_e32 v7, 0
	v_mov_b32_e32 v8, 0
	v_mov_b32_e32 v9, 0
	v_mov_b32_e32 v10, 0
	v_mov_b32_e32 v11, 0
	v_mov_b32_e32 v12, 0
	v_mov_b32_e32 v13, 0
	v_mov_b32_e32 v14, 0
	v_mov_b32_e32 v15, 0
	v_mov_b32_e32 v16, 0
	v_mov_b32_e32 v17, 0
	v_mov_b32_e32 v18, 0
	v_mov_b32_e32 v19, 0
	v_mov_b32_e32 v20, 0
	v_mov_b32_e32 v21, 0
	v_mov_b32_e32 v22, 0
	v_mov_b32_e32 v23, 0
	v_mov_b32_e32 v24, 0
	v_mov_b32_e32 v25, 0
	v_mov_b32_e32 v26, 0
	v_mov_b32_e32 v27, 0
	v_mov_b32_e32 v28, 0
	v_mov_b32_e32 v29, 0
	v_mov_b32_e32 v30, 0
	v_mov_b32_e32 v32, 0
	v_mov_b32_e32 v35, 0
	v_mov_b32_e32 v36, 0
	v_mov_b32_e32 v38, 0
	v_mov_b32_e32 v39, 0
	v_mov_b32_e32 v40, 0
	v_mov_b32_e32 v42, 0
	v_mov_b32_e32 v43, 0
	v_mov_b32_e32 v44, 0
	v_mov_b32_e32 v46, 0
	v_mov_b32_e32 v51, 0
	v_mov_b32_e32 v54, 0
	v_mov_b32_e32 v55, 0
BB0_3:                                  ; %.lr.ph.i.preheader
                                        ; =>This Loop Header: Depth=1
                                        ;     Child Loop BB0_4 Depth 2
                                        ;     Child Loop BB0_6 Depth 2
                                        ;     Child Loop BB0_11 Depth 2
                                        ;     Child Loop BB0_15 Depth 2
	s_waitcnt lgkmcnt(0)
	s_barrier
	s_mov_b64 s[6:7], 0
	v_mov_b32_e32 v56, v0
BB0_4:                                  ; %.lr.ph.i
                                        ;   Parent Loop BB0_3 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_cvt_f32_u32_e32 v57, v56
	v_mov_b32_e32 v58, 0x3d17b426
	v_mov_b32_e32 v61, s13
	s_mov_b32 m0, -1
	v_madak_f32 v57, v57, v58, 0x3727c5ac
	v_cvt_u32_f32_e32 v59, v57
	v_add_i32_e32 v57, vcc, v52, v56
	v_mov_b32_e32 v58, 0
	v_lshlrev_b64 v[57:58], 2, v[57:58]
	v_add_i32_e32 v60, vcc, 2, v59
	v_add_i32_e32 v57, vcc, s12, v57
	v_addc_u32_e32 v58, vcc, v61, v58, vcc
	flat_load_dword v57, v[57:58]
	v_mul_lo_i32 v58, v59, 27
	v_mul_u32_u24_e32 v59, 31, v60
	v_sub_i32_e32 v58, vcc, v59, v58
	v_add_i32_e32 v58, vcc, v58, v56
	v_mov_b32_e32 v59, 0x2d8
	v_add_i32_e32 v56, vcc, s18, v56
	v_cmp_gt_u32_e32 vcc, v56, v59
	v_lshlrev_b32_e32 v58, 2, v58
	s_or_b64 s[6:7], vcc, s[6:7]
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write_b32 v58, v57 offset:8
	s_andn2_b64 exec, exec, s[6:7]
	s_cbranch_execnz BB0_4
; BB#5:                                 ; %.lr.ph.i.1.preheader
                                        ;   in Loop: Header=BB0_3 Depth=1
	s_or_b64 exec, exec, s[6:7]
	s_mov_b64 s[6:7], 0
	v_mov_b32_e32 v56, v0
BB0_6:                                  ; %.lr.ph.i.1
                                        ;   Parent Loop BB0_3 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_cvt_f32_u32_e32 v57, v56
	v_mov_b32_e32 v58, 0x3d17b426
	v_mov_b32_e32 v60, s13
	v_mov_b32_e32 v61, 0x2d8
	v_madak_f32 v57, v57, v58, 0x3727c5ac
	v_cvt_u32_f32_e32 v59, v57
	v_add_i32_e32 v57, vcc, v52, v56
	v_mov_b32_e32 v58, 0
	v_add_i32_e32 v57, vcc, s19, v57
	v_lshlrev_b64 v[57:58], 2, v[57:58]
	v_add_i32_e32 v62, vcc, 2, v59
	v_add_i32_e32 v57, vcc, s12, v57
	v_addc_u32_e32 v58, vcc, v60, v58, vcc
	flat_load_dword v57, v[57:58]
	v_mul_lo_i32 v58, v59, 27
	v_mul_u32_u24_e32 v59, 31, v62
	s_mov_b32 m0, -1
	v_sub_i32_e32 v58, vcc, v59, v58
	v_add_i32_e32 v58, vcc, v58, v56
	v_add_i32_e32 v56, vcc, s18, v56
	v_cmp_gt_u32_e32 vcc, v56, v61
	v_lshlrev_b32_e32 v58, 2, v58
	s_or_b64 s[6:7], vcc, s[6:7]
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write_b32 v58, v57 offset:3852
	s_andn2_b64 exec, exec, s[6:7]
	s_cbranch_execnz BB0_6
; BB#7:                                 ; %.lr.ph.preheader
                                        ;   in Loop: Header=BB0_3 Depth=1
	s_or_b64 exec, exec, s[6:7]
	v_mov_b32_e32 v56, v0
	s_and_saveexec_b64 s[6:7], s[2:3]
	s_xor_b64 s[22:23], exec, s[6:7]
	; mask branch BB0_9
	s_cbranch_execz BB0_9
BB0_8:                                  ; %.lr.ph.prol.preheader
                                        ;   in Loop: Header=BB0_3 Depth=1
	v_add_i32_e32 v56, vcc, v37, v49
	v_mov_b32_e32 v57, 0x4b000
	v_cmp_lt_u32_e64 s[6:7], v56, v57
	v_mov_b32_e32 v57, 0
	v_lshlrev_b64 v[56:57], 2, v[56:57]
	v_add_i32_e32 v56, vcc, s14, v56
	v_mov_b32_e32 v58, s15
	v_addc_u32_e32 v57, vcc, v58, v57, vcc
	v_cndmask_b32_e64 v57, v58, v57, s[6:7]
	v_mov_b32_e32 v58, s14
	v_cndmask_b32_e64 v56, v58, v56, s[6:7]
	flat_load_dword v56, v[56:57]
	s_mov_b32 m0, -1
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write_b32 v47, v56
	v_mov_b32_e32 v56, v48
BB0_9:                                  ; %.lr.ph.prol.loopexit.unr-lcssa
                                        ;   in Loop: Header=BB0_3 Depth=1
	s_or_b64 exec, exec, s[22:23]
	s_and_saveexec_b64 s[6:7], s[4:5]
	s_xor_b64 s[22:23], exec, s[6:7]
	; mask branch BB0_13
	s_cbranch_execz BB0_13
BB0_10:                                 ; %.lr.ph.preheader366
                                        ;   in Loop: Header=BB0_3 Depth=1
	v_lshlrev_b32_e32 v57, 2, v56
	v_add_i32_e32 v57, vcc, s17, v57
	s_mov_b64 s[24:25], 0
BB0_11:                                 ; %.lr.ph
                                        ;   Parent Loop BB0_3 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_cvt_f32_u32_e32 v58, v56
	v_mov_b32_e32 v62, 0x3727c5ac
	v_mov_b32_e32 v63, 0x3ca3d70a
	v_add_i32_e32 v64, vcc, v37, v56
	v_mad_f32 v58, v63, v58, v62
	v_cvt_u32_f32_e32 v58, v58
	v_mov_b32_e32 v66, 0x4b000
	v_mov_b32_e32 v65, s15
	v_mov_b32_e32 v67, s14
	v_mul_lo_i32 v59, v58, s16
	v_mul_u32_u24_e32 v58, 50, v58
	s_mov_b32 m0, -1
	v_subrev_i32_e32 v58, vcc, v58, v59
	v_add_i32_e32 v58, vcc, v58, v64
	v_mov_b32_e32 v59, 0
	v_lshlrev_b64 v[60:61], 2, v[58:59]
	v_cmp_lt_u32_e64 s[6:7], v58, v66
	v_add_i32_e32 v58, vcc, s14, v60
	v_addc_u32_e32 v61, vcc, v65, v61, vcc
	v_cndmask_b32_e64 v60, v67, v58, s[6:7]
	v_add_i32_e32 v58, vcc, s18, v56
	v_cvt_f32_u32_e32 v58, v58
	v_cndmask_b32_e64 v61, v65, v61, s[6:7]
	flat_load_dword v60, v[60:61]
	v_mac_f32_e32 v62, v63, v58
	v_cvt_u32_f32_e32 v58, v62
	v_mul_lo_i32 v61, v58, s16
	v_mul_u32_u24_e32 v58, 50, v58
	v_subrev_i32_e32 v58, vcc, v58, v61
	v_add_i32_e32 v58, vcc, v58, v64
	v_add_i32_e32 v58, vcc, s18, v58
	v_cmp_lt_u32_e64 s[6:7], v58, v66
	v_lshlrev_b64 v[58:59], 2, v[58:59]
	v_add_i32_e32 v58, vcc, s14, v58
	v_addc_u32_e32 v59, vcc, v65, v59, vcc
	v_cndmask_b32_e64 v58, v67, v58, s[6:7]
	v_cndmask_b32_e64 v59, v65, v59, s[6:7]
	flat_load_dword v58, v[58:59]
	v_mov_b32_e32 v59, 0x2bb
	v_add_i32_e32 v56, vcc, 0x200, v56
	v_cmp_gt_u32_e32 vcc, v56, v59
	s_or_b64 s[24:25], vcc, s[24:25]
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write2st64_b32 v57, v60, v58 offset1:4
	v_add_i32_e32 v57, vcc, 0x800, v57
	s_andn2_b64 exec, exec, s[24:25]
	s_cbranch_execnz BB0_11
; BB#12:                                ; %Flow406
                                        ;   in Loop: Header=BB0_3 Depth=1
	s_or_b64 exec, exec, s[24:25]
BB0_13:                                 ; %Flow407
                                        ;   in Loop: Header=BB0_3 Depth=1
	s_or_b64 exec, exec, s[22:23]
	s_waitcnt lgkmcnt(0)
	s_barrier
	s_and_saveexec_b64 s[6:7], s[0:1]
	s_xor_b64 s[6:7], exec, s[6:7]
	; mask branch BB0_16
	s_cbranch_execz BB0_16
BB0_14:                                 ; %.preheader7.i.preheader
                                        ;   in Loop: Header=BB0_3 Depth=1
	v_mov_b32_e32 v56, 0
	v_mov_b32_e32 v57, v45
	v_mov_b32_e32 v58, v41
BB0_15:                                 ; %.preheader7.i
                                        ;   Parent Loop BB0_3 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_mul_lo_i32 v59, v56, 25
	v_lshlrev_b32_e32 v100, 2, v58
	s_mov_b32 m0, -1
	v_lshlrev_b32_e32 v101, 2, v57
	v_add_i32_e32 v59, vcc, v59, v50
	v_lshlrev_b32_e32 v59, 2, v59
	v_add_i32_e32 v60, vcc, s17, v59
	v_add_i32_e32 v62, vcc, 0x1ed0, v59
	v_add_i32_e32 v64, vcc, 0x1f98, v59
	v_add_i32_e32 v70, vcc, 0x21f0, v59
	ds_read2_b32 v[60:61], v60 offset1:1
	ds_read2_b32 v[88:89], v100 offset1:1
	ds_read2_b32 v[94:95], v101 offset1:1
	ds_read2_b32 v[90:91], v100 offset0:2 offset1:3
	ds_read2_b32 v[96:97], v101 offset0:2 offset1:3
	ds_read2_b32 v[62:63], v62 offset1:1
	ds_read2_b32 v[64:65], v64 offset1:1
	ds_read2_b32 v[70:71], v70 offset1:1
	v_add_i32_e32 v74, vcc, 0x1e10, v59
	v_add_i32_e32 v66, vcc, 0x2060, v59
	v_add_i32_e32 v76, vcc, 0x1ed8, v59
	v_add_i32_e32 v78, vcc, 0x1fa0, v59
	v_add_i32_e32 v84, vcc, 0x21f8, v59
	ds_read2_b32 v[74:75], v74 offset1:1
	ds_read2_b32 v[92:93], v100 offset0:4 offset1:5
	ds_read2_b32 v[98:99], v101 offset0:4 offset1:5
	ds_read2_b32 v[66:67], v66 offset1:1
	ds_read2_b32 v[76:77], v76 offset1:1
	ds_read2_b32 v[78:79], v78 offset1:1
	ds_read2_b32 v[84:85], v84 offset1:1
	s_waitcnt lgkmcnt(13)
	v_mac_f32_e32 v55, v60, v88
	v_mac_f32_e32 v54, v60, v89
	s_waitcnt lgkmcnt(11)
	v_mac_f32_e32 v51, v60, v90
	v_mac_f32_e32 v46, v60, v94
	v_mac_f32_e32 v44, v60, v95
	s_waitcnt lgkmcnt(10)
	v_mac_f32_e32 v43, v60, v96
	v_add_i32_e32 v72, vcc, 0x22b8, v59
	v_add_i32_e32 v80, vcc, 0x2068, v59
	v_mac_f32_e32 v55, v61, v89
	v_mac_f32_e32 v54, v61, v90
	v_mac_f32_e32 v51, v61, v91
	v_mac_f32_e32 v46, v61, v95
	v_mac_f32_e32 v44, v61, v96
	v_mac_f32_e32 v43, v61, v97
	s_waitcnt lgkmcnt(9)
	v_mac_f32_e32 v42, v62, v88
	v_mac_f32_e32 v40, v62, v89
	v_mac_f32_e32 v39, v62, v90
	v_mac_f32_e32 v38, v62, v94
	v_mac_f32_e32 v36, v62, v95
	v_mac_f32_e32 v35, v62, v96
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v32, v64, v88
	v_mac_f32_e32 v30, v64, v89
	v_mac_f32_e32 v29, v64, v90
	v_mac_f32_e32 v28, v64, v94
	v_mac_f32_e32 v27, v64, v95
	v_mac_f32_e32 v26, v64, v96
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v13, v70, v88
	v_mac_f32_e32 v12, v70, v89
	v_mac_f32_e32 v11, v70, v90
	v_mac_f32_e32 v10, v70, v94
	v_mac_f32_e32 v9, v70, v95
	v_mac_f32_e32 v8, v70, v96
	ds_read2_b32 v[72:73], v72 offset1:1
	v_mac_f32_e32 v42, v63, v89
	v_mac_f32_e32 v40, v63, v90
	v_mac_f32_e32 v39, v63, v91
	v_mac_f32_e32 v38, v63, v95
	v_mac_f32_e32 v36, v63, v96
	v_mac_f32_e32 v35, v63, v97
	v_mac_f32_e32 v32, v65, v89
	v_mac_f32_e32 v30, v65, v90
	v_mac_f32_e32 v29, v65, v91
	v_mac_f32_e32 v28, v65, v95
	v_mac_f32_e32 v27, v65, v96
	v_mac_f32_e32 v26, v65, v97
	v_mac_f32_e32 v13, v71, v89
	v_mac_f32_e32 v12, v71, v90
	v_mac_f32_e32 v11, v71, v91
	v_mac_f32_e32 v10, v71, v95
	v_mac_f32_e32 v9, v71, v96
	v_mac_f32_e32 v8, v71, v97
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v55, v74, v90
	v_mac_f32_e32 v54, v74, v91
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v51, v74, v92
	v_mac_f32_e32 v46, v74, v96
	v_mac_f32_e32 v44, v74, v97
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v43, v74, v98
	v_add_i32_e32 v60, vcc, 0x1e18, v59
	ds_read2_b32 v[80:81], v80 offset1:1
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v25, v66, v88
	v_mac_f32_e32 v24, v66, v89
	v_mac_f32_e32 v23, v66, v90
	v_mac_f32_e32 v22, v66, v94
	v_mac_f32_e32 v21, v66, v95
	v_mac_f32_e32 v20, v66, v96
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v42, v76, v90
	v_mac_f32_e32 v40, v76, v91
	v_mac_f32_e32 v39, v76, v92
	v_mac_f32_e32 v38, v76, v96
	v_mac_f32_e32 v36, v76, v97
	v_mac_f32_e32 v35, v76, v98
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v32, v78, v90
	v_mac_f32_e32 v30, v78, v91
	v_mac_f32_e32 v29, v78, v92
	v_mac_f32_e32 v28, v78, v96
	v_mac_f32_e32 v27, v78, v97
	v_mac_f32_e32 v26, v78, v98
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v13, v84, v90
	v_mac_f32_e32 v12, v84, v91
	v_mac_f32_e32 v11, v84, v92
	v_mac_f32_e32 v10, v84, v96
	v_mac_f32_e32 v9, v84, v97
	v_mac_f32_e32 v8, v84, v98
	v_add_i32_e32 v64, vcc, 0x1fa8, v59
	v_mac_f32_e32 v55, v75, v91
	ds_read2_b32 v[60:61], v60 offset1:1
	v_mac_f32_e32 v54, v75, v92
	v_mac_f32_e32 v46, v75, v97
	v_mac_f32_e32 v44, v75, v98
	v_mac_f32_e32 v51, v75, v93
	v_mac_f32_e32 v43, v75, v99
	ds_read_b32 v84, v100 offset:24
	ds_read2_b32 v[74:75], v101 offset0:6 offset1:37
	v_add_i32_e32 v66, vcc, 0x2070, v59
	v_mac_f32_e32 v25, v67, v89
	v_mac_f32_e32 v24, v67, v90
	v_mac_f32_e32 v23, v67, v91
	v_mac_f32_e32 v22, v67, v95
	v_mac_f32_e32 v21, v67, v96
	v_mac_f32_e32 v20, v67, v97
	v_mac_f32_e32 v42, v77, v91
	v_mac_f32_e32 v40, v77, v92
	v_mac_f32_e32 v39, v77, v93
	v_mac_f32_e32 v38, v77, v97
	v_mac_f32_e32 v36, v77, v98
	v_mac_f32_e32 v35, v77, v99
	v_mac_f32_e32 v32, v79, v91
	ds_read2_b32 v[64:65], v64 offset1:1
	v_mac_f32_e32 v30, v79, v92
	v_mac_f32_e32 v29, v79, v93
	v_mac_f32_e32 v28, v79, v97
	v_mac_f32_e32 v27, v79, v98
	v_mac_f32_e32 v26, v79, v99
	ds_read2_b32 v[66:67], v66 offset1:1
	ds_read2_b32 v[76:77], v101 offset0:31 offset1:32
	ds_read2_b32 v[78:79], v101 offset0:33 offset1:34
	v_add_i32_e32 v86, vcc, 0x22c0, v59
	ds_read2_b32 v[86:87], v86 offset1:1
	s_waitcnt lgkmcnt(9)
	v_mac_f32_e32 v7, v72, v88
	v_mac_f32_e32 v6, v72, v89
	v_mac_f32_e32 v5, v72, v90
	v_mac_f32_e32 v4, v72, v94
	v_mac_f32_e32 v3, v72, v95
	v_mac_f32_e32 v2, v72, v96
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v25, v80, v90
	v_mac_f32_e32 v24, v80, v91
	v_mac_f32_e32 v23, v80, v92
	v_mac_f32_e32 v22, v80, v96
	v_mac_f32_e32 v21, v80, v97
	v_mac_f32_e32 v20, v80, v98
	v_add_i32_e32 v62, vcc, 0x1ee0, v59
	v_add_i32_e32 v72, vcc, 0x2210, v59
	ds_read2_b32 v[62:63], v62 offset1:1
	v_mac_f32_e32 v7, v73, v89
	v_mac_f32_e32 v6, v73, v90
	v_mac_f32_e32 v5, v73, v91
	v_mac_f32_e32 v4, v73, v95
	v_mac_f32_e32 v3, v73, v96
	v_mac_f32_e32 v2, v73, v97
	v_mac_f32_e32 v25, v81, v91
	v_mac_f32_e32 v24, v81, v92
	v_mac_f32_e32 v23, v81, v93
	v_mac_f32_e32 v22, v81, v97
	v_mac_f32_e32 v21, v81, v98
	v_mac_f32_e32 v20, v81, v99
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v55, v60, v92
	v_mac_f32_e32 v54, v60, v93
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v51, v60, v84
	v_mac_f32_e32 v46, v60, v98
	v_mac_f32_e32 v44, v60, v99
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v43, v60, v74
	ds_read2_b32 v[72:73], v72 offset1:46
	v_add_i32_e32 v60, vcc, 0x22cc, v59
	v_add_i32_e32 v68, vcc, 0x2128, v59
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v32, v64, v92
	v_mac_f32_e32 v30, v64, v93
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v25, v66, v92
	v_mac_f32_e32 v24, v66, v93
	v_mac_f32_e32 v29, v64, v84
	v_mac_f32_e32 v28, v64, v98
	v_mac_f32_e32 v27, v64, v99
	v_mac_f32_e32 v26, v64, v74
	v_mac_f32_e32 v23, v66, v84
	v_mac_f32_e32 v22, v66, v98
	v_mac_f32_e32 v21, v66, v99
	v_mac_f32_e32 v20, v66, v74
	v_mac_f32_e32 v55, v61, v94
	v_mac_f32_e32 v54, v61, v95
	v_mac_f32_e32 v51, v61, v96
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v46, v61, v76
	v_mac_f32_e32 v44, v61, v77
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v43, v61, v78
	ds_read2_b32 v[60:61], v60 offset1:1
	v_add_i32_e32 v64, vcc, 0x1ee8, v59
	v_add_i32_e32 v66, vcc, 0x1fb0, v59
	ds_read2_b32 v[68:69], v68 offset1:1
	v_mac_f32_e32 v32, v65, v94
	v_mac_f32_e32 v30, v65, v95
	v_mac_f32_e32 v29, v65, v96
	v_mac_f32_e32 v28, v65, v76
	v_mac_f32_e32 v27, v65, v77
	v_mac_f32_e32 v26, v65, v78
	v_mac_f32_e32 v25, v67, v94
	v_mac_f32_e32 v24, v67, v95
	v_mac_f32_e32 v23, v67, v96
	v_mac_f32_e32 v22, v67, v76
	v_mac_f32_e32 v21, v67, v77
	v_mac_f32_e32 v20, v67, v78
	ds_read2_b32 v[66:67], v66 offset1:1
	ds_read2_b32 v[64:65], v64 offset1:1
	v_add_i32_e32 v82, vcc, 0x2130, v59
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v7, v86, v90
	v_mac_f32_e32 v6, v86, v91
	v_mac_f32_e32 v5, v86, v92
	v_mac_f32_e32 v4, v86, v96
	v_mac_f32_e32 v3, v86, v97
	v_mac_f32_e32 v2, v86, v98
	ds_read2_b32 v[80:81], v101 offset0:35 offset1:36
	v_mac_f32_e32 v7, v87, v91
	v_mac_f32_e32 v6, v87, v92
	v_mac_f32_e32 v5, v87, v93
	v_mac_f32_e32 v4, v87, v97
	v_mac_f32_e32 v3, v87, v98
	v_mac_f32_e32 v2, v87, v99
	ds_read2_b32 v[82:83], v82 offset1:1
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v42, v62, v92
	v_mac_f32_e32 v40, v62, v93
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v7, v73, v92
	v_mac_f32_e32 v6, v73, v93
	v_mac_f32_e32 v39, v62, v84
	v_mac_f32_e32 v38, v62, v98
	v_mac_f32_e32 v36, v62, v99
	v_mac_f32_e32 v35, v62, v74
	v_mac_f32_e32 v5, v73, v84
	v_mac_f32_e32 v4, v73, v98
	v_mac_f32_e32 v3, v73, v99
	v_mac_f32_e32 v2, v73, v74
	v_mac_f32_e32 v42, v63, v94
	v_mac_f32_e32 v40, v63, v95
	v_mac_f32_e32 v39, v63, v96
	v_mac_f32_e32 v38, v63, v76
	v_mac_f32_e32 v36, v63, v77
	v_mac_f32_e32 v35, v63, v78
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v7, v60, v94
	v_mac_f32_e32 v6, v60, v95
	v_mac_f32_e32 v5, v60, v96
	v_mac_f32_e32 v4, v60, v76
	v_mac_f32_e32 v3, v60, v77
	v_mac_f32_e32 v2, v60, v78
	v_add_i32_e32 v60, vcc, 0x22d4, v59
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v19, v68, v88
	v_mac_f32_e32 v18, v68, v89
	v_mac_f32_e32 v17, v68, v90
	v_mac_f32_e32 v16, v68, v94
	v_mac_f32_e32 v15, v68, v95
	v_mac_f32_e32 v14, v68, v96
	v_add_i32_e32 v70, vcc, 0x2200, v59
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v42, v64, v95
	v_mac_f32_e32 v32, v66, v95
	v_mac_f32_e32 v40, v64, v96
	v_mac_f32_e32 v39, v64, v97
	v_mac_f32_e32 v38, v64, v77
	v_mac_f32_e32 v36, v64, v78
	v_mac_f32_e32 v35, v64, v79
	v_mac_f32_e32 v30, v66, v96
	v_mac_f32_e32 v29, v66, v97
	v_mac_f32_e32 v28, v66, v77
	v_mac_f32_e32 v27, v66, v78
	v_mac_f32_e32 v26, v66, v79
	v_mac_f32_e32 v7, v61, v95
	v_mac_f32_e32 v6, v61, v96
	v_mac_f32_e32 v5, v61, v97
	v_mac_f32_e32 v4, v61, v77
	v_mac_f32_e32 v3, v61, v78
	v_mac_f32_e32 v2, v61, v79
	ds_read2_b32 v[60:61], v60 offset1:1
	v_add_i32_e32 v64, vcc, 0x1ef0, v59
	v_add_i32_e32 v66, vcc, 0x1fb8, v59
	v_mac_f32_e32 v19, v69, v89
	v_mac_f32_e32 v18, v69, v90
	v_mac_f32_e32 v17, v69, v91
	v_mac_f32_e32 v16, v69, v95
	v_mac_f32_e32 v15, v69, v96
	v_mac_f32_e32 v14, v69, v97
	ds_read2_b32 v[70:71], v70 offset1:1
	v_mac_f32_e32 v42, v65, v96
	v_mac_f32_e32 v40, v65, v97
	v_mac_f32_e32 v39, v65, v98
	v_mac_f32_e32 v38, v65, v78
	v_mac_f32_e32 v36, v65, v79
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v35, v65, v80
	ds_read2_b32 v[64:65], v64 offset1:1
	v_mac_f32_e32 v32, v67, v96
	v_mac_f32_e32 v30, v67, v97
	v_mac_f32_e32 v29, v67, v98
	v_mac_f32_e32 v28, v67, v78
	v_mac_f32_e32 v27, v67, v79
	v_mac_f32_e32 v26, v67, v80
	ds_read2_b32 v[66:67], v66 offset1:1
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v19, v82, v90
	v_mac_f32_e32 v18, v82, v91
	v_mac_f32_e32 v17, v82, v92
	v_mac_f32_e32 v16, v82, v96
	v_mac_f32_e32 v15, v82, v97
	v_mac_f32_e32 v14, v82, v98
	v_add_i32_e32 v82, vcc, 0x2208, v59
	v_add_i32_e32 v62, vcc, 0x1e20, v59
	v_mac_f32_e32 v19, v83, v91
	v_mac_f32_e32 v18, v83, v92
	v_mac_f32_e32 v17, v83, v93
	v_mac_f32_e32 v16, v83, v97
	v_mac_f32_e32 v15, v83, v98
	v_mac_f32_e32 v14, v83, v99
	ds_read2_b32 v[82:83], v82 offset1:1
	ds_read2_b32 v[62:63], v62 offset1:1
	v_mac_f32_e32 v13, v85, v91
	v_mac_f32_e32 v12, v85, v92
	v_mac_f32_e32 v11, v85, v93
	v_mac_f32_e32 v10, v85, v97
	v_mac_f32_e32 v9, v85, v98
	v_mac_f32_e32 v8, v85, v99
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v7, v60, v96
	v_mac_f32_e32 v6, v60, v97
	v_mac_f32_e32 v5, v60, v98
	v_mac_f32_e32 v4, v60, v78
	v_mac_f32_e32 v3, v60, v79
	v_mac_f32_e32 v2, v60, v80
	v_add_i32_e32 v60, vcc, 0x2214, v59
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v13, v70, v92
	v_mac_f32_e32 v12, v70, v93
	v_mac_f32_e32 v11, v70, v84
	v_mac_f32_e32 v10, v70, v98
	v_mac_f32_e32 v9, v70, v99
	v_mac_f32_e32 v8, v70, v74
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v42, v64, v97
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v32, v66, v97
	v_mac_f32_e32 v40, v64, v98
	v_mac_f32_e32 v39, v64, v99
	v_mac_f32_e32 v38, v64, v79
	v_mac_f32_e32 v36, v64, v80
	v_mac_f32_e32 v35, v64, v81
	v_mac_f32_e32 v30, v66, v98
	v_mac_f32_e32 v29, v66, v99
	v_mac_f32_e32 v28, v66, v79
	v_mac_f32_e32 v27, v66, v80
	v_mac_f32_e32 v26, v66, v81
	v_mac_f32_e32 v7, v61, v97
	v_mac_f32_e32 v6, v61, v98
	v_mac_f32_e32 v5, v61, v99
	v_mac_f32_e32 v4, v61, v79
	v_mac_f32_e32 v3, v61, v80
	v_mac_f32_e32 v2, v61, v81
	ds_read2_b32 v[60:61], v60 offset1:1
	v_mac_f32_e32 v13, v71, v94
	v_mac_f32_e32 v12, v71, v95
	v_mac_f32_e32 v11, v71, v96
	v_mac_f32_e32 v10, v71, v76
	v_mac_f32_e32 v9, v71, v77
	v_mac_f32_e32 v8, v71, v78
	v_mac_f32_e32 v42, v65, v98
	v_mac_f32_e32 v40, v65, v99
	v_mac_f32_e32 v39, v65, v74
	v_mac_f32_e32 v38, v65, v80
	v_mac_f32_e32 v36, v65, v81
	v_mac_f32_e32 v35, v65, v75
	v_mac_f32_e32 v32, v67, v98
	v_mac_f32_e32 v30, v67, v99
	v_mac_f32_e32 v29, v67, v74
	v_mac_f32_e32 v28, v67, v80
	v_mac_f32_e32 v27, v67, v81
	v_mac_f32_e32 v26, v67, v75
	ds_read2_b32 v[64:65], v101 offset0:62 offset1:63
	ds_read2_b32 v[66:67], v101 offset0:64 offset1:65
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v13, v82, v95
	v_mac_f32_e32 v12, v82, v96
	v_mac_f32_e32 v11, v82, v97
	v_mac_f32_e32 v10, v82, v77
	v_mac_f32_e32 v9, v82, v78
	v_mac_f32_e32 v8, v82, v79
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v55, v62, v95
	v_mac_f32_e32 v54, v62, v96
	v_mac_f32_e32 v51, v62, v97
	v_mac_f32_e32 v46, v62, v77
	v_mac_f32_e32 v44, v62, v78
	v_mac_f32_e32 v43, v62, v79
	v_mac_f32_e32 v13, v83, v96
	v_mac_f32_e32 v12, v83, v97
	v_mac_f32_e32 v11, v83, v98
	v_mac_f32_e32 v10, v83, v78
	v_mac_f32_e32 v9, v83, v79
	v_mac_f32_e32 v8, v83, v80
	v_add_i32_e32 v62, vcc, 0x1e28, v59
	v_mac_f32_e32 v13, v72, v97
	v_mac_f32_e32 v12, v72, v98
	v_mac_f32_e32 v11, v72, v99
	v_mac_f32_e32 v10, v72, v79
	v_mac_f32_e32 v9, v72, v80
	v_mac_f32_e32 v8, v72, v81
	v_mac_f32_e32 v55, v63, v96
	v_mac_f32_e32 v54, v63, v97
	v_mac_f32_e32 v51, v63, v98
	v_mac_f32_e32 v46, v63, v78
	v_mac_f32_e32 v44, v63, v79
	v_mac_f32_e32 v43, v63, v80
	ds_read2_b32 v[62:63], v62 offset1:1
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v13, v60, v98
	v_mac_f32_e32 v12, v60, v99
	v_mac_f32_e32 v11, v60, v74
	v_mac_f32_e32 v10, v60, v80
	v_mac_f32_e32 v9, v60, v81
	v_mac_f32_e32 v8, v60, v75
	v_add_i32_e32 v60, vcc, 0x1e30, v59
	v_mac_f32_e32 v13, v61, v76
	v_mac_f32_e32 v12, v61, v77
	v_mac_f32_e32 v11, v61, v78
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v10, v61, v64
	v_mac_f32_e32 v9, v61, v65
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v8, v61, v66
	ds_read2_b32 v[60:61], v60 offset1:1
	v_add_i32_e32 v68, vcc, 0x2138, v59
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v55, v62, v97
	v_mac_f32_e32 v54, v62, v98
	v_mac_f32_e32 v51, v62, v99
	v_mac_f32_e32 v46, v62, v79
	v_mac_f32_e32 v44, v62, v80
	v_mac_f32_e32 v43, v62, v81
	ds_read2_b32 v[68:69], v68 offset1:1
	v_mac_f32_e32 v55, v63, v98
	v_mac_f32_e32 v54, v63, v99
	v_mac_f32_e32 v51, v63, v74
	v_mac_f32_e32 v46, v63, v80
	v_mac_f32_e32 v44, v63, v81
	v_mac_f32_e32 v43, v63, v75
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v55, v60, v76
	v_mac_f32_e32 v54, v60, v77
	v_mac_f32_e32 v51, v60, v78
	v_mac_f32_e32 v46, v60, v64
	v_mac_f32_e32 v44, v60, v65
	v_mac_f32_e32 v43, v60, v66
	v_add_i32_e32 v60, vcc, 0x1ef8, v59
	v_mac_f32_e32 v55, v61, v77
	v_mac_f32_e32 v54, v61, v78
	v_mac_f32_e32 v51, v61, v79
	v_mac_f32_e32 v46, v61, v65
	v_mac_f32_e32 v44, v61, v66
	v_mac_f32_e32 v43, v61, v67
	ds_read2_b32 v[60:61], v60 offset1:1
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v19, v68, v92
	v_mac_f32_e32 v18, v68, v93
	v_mac_f32_e32 v17, v68, v84
	v_mac_f32_e32 v16, v68, v98
	v_mac_f32_e32 v15, v68, v99
	v_mac_f32_e32 v14, v68, v74
	v_add_i32_e32 v68, vcc, 0x2078, v59
	v_mac_f32_e32 v19, v69, v94
	v_mac_f32_e32 v18, v69, v95
	v_mac_f32_e32 v17, v69, v96
	v_mac_f32_e32 v16, v69, v76
	v_mac_f32_e32 v15, v69, v77
	v_mac_f32_e32 v14, v69, v78
	ds_read2_b32 v[68:69], v68 offset1:1
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v42, v60, v76
	v_mac_f32_e32 v40, v60, v77
	v_mac_f32_e32 v39, v60, v78
	v_mac_f32_e32 v38, v60, v64
	v_mac_f32_e32 v36, v60, v65
	v_mac_f32_e32 v35, v60, v66
	v_add_i32_e32 v60, vcc, 0x1fc0, v59
	v_mac_f32_e32 v42, v61, v77
	v_mac_f32_e32 v40, v61, v78
	v_mac_f32_e32 v39, v61, v79
	v_mac_f32_e32 v38, v61, v65
	v_mac_f32_e32 v36, v61, v66
	v_mac_f32_e32 v35, v61, v67
	ds_read2_b32 v[60:61], v60 offset1:1
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v25, v68, v95
	v_add_i32_e32 v70, vcc, 0x2140, v59
	v_mac_f32_e32 v24, v68, v96
	v_mac_f32_e32 v23, v68, v97
	v_mac_f32_e32 v22, v68, v77
	v_mac_f32_e32 v21, v68, v78
	v_mac_f32_e32 v20, v68, v79
	v_add_i32_e32 v68, vcc, 0x2080, v59
	ds_read2_b32 v[70:71], v70 offset1:1
	v_mac_f32_e32 v25, v69, v96
	v_mac_f32_e32 v24, v69, v97
	v_mac_f32_e32 v23, v69, v98
	v_mac_f32_e32 v22, v69, v78
	v_mac_f32_e32 v21, v69, v79
	v_mac_f32_e32 v20, v69, v80
	ds_read2_b32 v[68:69], v68 offset1:1
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v32, v60, v76
	v_mac_f32_e32 v30, v60, v77
	v_mac_f32_e32 v29, v60, v78
	v_mac_f32_e32 v28, v60, v64
	v_mac_f32_e32 v27, v60, v65
	v_mac_f32_e32 v26, v60, v66
	v_add_i32_e32 v60, vcc, 0x2088, v59
	v_mac_f32_e32 v32, v61, v77
	v_mac_f32_e32 v30, v61, v78
	v_mac_f32_e32 v29, v61, v79
	v_mac_f32_e32 v28, v61, v65
	v_mac_f32_e32 v27, v61, v66
	v_mac_f32_e32 v26, v61, v67
	ds_read2_b32 v[60:61], v60 offset1:1
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v19, v70, v95
	v_mac_f32_e32 v18, v70, v96
	v_mac_f32_e32 v17, v70, v97
	v_mac_f32_e32 v16, v70, v77
	v_mac_f32_e32 v15, v70, v78
	v_mac_f32_e32 v14, v70, v79
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v25, v68, v97
	v_mac_f32_e32 v24, v68, v98
	v_mac_f32_e32 v23, v68, v99
	v_mac_f32_e32 v22, v68, v79
	v_mac_f32_e32 v21, v68, v80
	v_mac_f32_e32 v20, v68, v81
	v_add_i32_e32 v70, vcc, 0x2148, v59
	v_mac_f32_e32 v25, v69, v98
	v_mac_f32_e32 v24, v69, v99
	v_mac_f32_e32 v23, v69, v74
	v_mac_f32_e32 v22, v69, v80
	v_mac_f32_e32 v21, v69, v81
	v_mac_f32_e32 v20, v69, v75
	v_mac_f32_e32 v19, v71, v96
	v_mac_f32_e32 v18, v71, v97
	v_mac_f32_e32 v17, v71, v98
	v_mac_f32_e32 v16, v71, v78
	v_mac_f32_e32 v15, v71, v79
	v_mac_f32_e32 v14, v71, v80
	ds_read2_b32 v[70:71], v70 offset1:1
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v25, v60, v76
	v_mac_f32_e32 v24, v60, v77
	v_mac_f32_e32 v23, v60, v78
	v_mac_f32_e32 v22, v60, v64
	v_mac_f32_e32 v21, v60, v65
	v_mac_f32_e32 v20, v60, v66
	v_add_i32_e32 v60, vcc, 0x2150, v59
	v_mac_f32_e32 v25, v61, v77
	v_mac_f32_e32 v24, v61, v78
	v_mac_f32_e32 v23, v61, v79
	v_mac_f32_e32 v22, v61, v65
	v_mac_f32_e32 v21, v61, v66
	v_mac_f32_e32 v20, v61, v67
	ds_read2_b32 v[60:61], v60 offset1:1
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v19, v70, v97
	v_mac_f32_e32 v18, v70, v98
	v_mac_f32_e32 v17, v70, v99
	v_mac_f32_e32 v16, v70, v79
	v_mac_f32_e32 v15, v70, v80
	v_mac_f32_e32 v14, v70, v81
	v_mac_f32_e32 v19, v71, v98
	v_mac_f32_e32 v18, v71, v99
	v_mac_f32_e32 v17, v71, v74
	v_mac_f32_e32 v16, v71, v80
	v_mac_f32_e32 v15, v71, v81
	v_mac_f32_e32 v14, v71, v75
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v19, v60, v76
	v_mac_f32_e32 v18, v60, v77
	v_mac_f32_e32 v17, v60, v78
	v_mac_f32_e32 v16, v60, v64
	v_mac_f32_e32 v15, v60, v65
	v_mac_f32_e32 v14, v60, v66
	v_add_i32_e32 v60, vcc, 0x221c, v59
	v_mac_f32_e32 v19, v61, v77
	v_mac_f32_e32 v18, v61, v78
	v_mac_f32_e32 v17, v61, v79
	v_mac_f32_e32 v16, v61, v65
	v_mac_f32_e32 v15, v61, v66
	v_mac_f32_e32 v14, v61, v67
	ds_read2_b32 v[60:61], v60 offset1:1
	ds_read2_b32 v[68:69], v101 offset0:66 offset1:67
	v_add_i32_e32 v62, vcc, 0x22dc, v59
	ds_read2_b32 v[62:63], v62 offset1:1
	v_add_i32_e32 v70, vcc, 0x1e40, v59
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v13, v60, v77
	v_mac_f32_e32 v12, v60, v78
	v_mac_f32_e32 v11, v60, v79
	v_mac_f32_e32 v10, v60, v65
	v_mac_f32_e32 v9, v60, v66
	v_mac_f32_e32 v8, v60, v67
	v_add_i32_e32 v60, vcc, 0x1e38, v59
	v_mac_f32_e32 v13, v61, v78
	v_mac_f32_e32 v12, v61, v79
	v_mac_f32_e32 v11, v61, v80
	v_mac_f32_e32 v10, v61, v66
	v_mac_f32_e32 v9, v61, v67
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v8, v61, v68
	ds_read2_b32 v[60:61], v60 offset1:1
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v5, v62, v74
	v_mac_f32_e32 v7, v62, v98
	v_mac_f32_e32 v6, v62, v99
	v_mac_f32_e32 v4, v62, v80
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v55, v60, v78
	v_mac_f32_e32 v54, v60, v79
	v_mac_f32_e32 v51, v60, v80
	v_mac_f32_e32 v46, v60, v66
	v_mac_f32_e32 v44, v60, v67
	v_mac_f32_e32 v43, v60, v68
	v_add_i32_e32 v60, vcc, 0x1f00, v59
	v_mac_f32_e32 v55, v61, v79
	v_mac_f32_e32 v54, v61, v80
	v_mac_f32_e32 v51, v61, v81
	v_mac_f32_e32 v46, v61, v67
	v_mac_f32_e32 v44, v61, v68
	v_mac_f32_e32 v43, v61, v69
	ds_read2_b32 v[60:61], v60 offset1:1
	v_mac_f32_e32 v3, v62, v81
	v_mac_f32_e32 v2, v62, v75
	v_add_i32_e32 v62, vcc, 0x22e4, v59
	v_mac_f32_e32 v7, v63, v76
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v42, v60, v78
	v_mac_f32_e32 v40, v60, v79
	v_mac_f32_e32 v39, v60, v80
	v_mac_f32_e32 v38, v60, v66
	v_mac_f32_e32 v36, v60, v67
	v_mac_f32_e32 v35, v60, v68
	v_add_i32_e32 v60, vcc, 0x1fc8, v59
	v_mac_f32_e32 v42, v61, v79
	v_mac_f32_e32 v40, v61, v80
	v_mac_f32_e32 v39, v61, v81
	v_mac_f32_e32 v38, v61, v67
	v_mac_f32_e32 v36, v61, v68
	v_mac_f32_e32 v35, v61, v69
	ds_read2_b32 v[60:61], v60 offset1:1
	v_mac_f32_e32 v6, v63, v77
	v_mac_f32_e32 v5, v63, v78
	v_mac_f32_e32 v4, v63, v64
	v_mac_f32_e32 v3, v63, v65
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v32, v60, v78
	v_mac_f32_e32 v30, v60, v79
	v_mac_f32_e32 v29, v60, v80
	v_mac_f32_e32 v28, v60, v66
	v_mac_f32_e32 v27, v60, v67
	v_mac_f32_e32 v26, v60, v68
	v_add_i32_e32 v60, vcc, 0x2090, v59
	v_mac_f32_e32 v32, v61, v79
	v_mac_f32_e32 v30, v61, v80
	v_mac_f32_e32 v29, v61, v81
	v_mac_f32_e32 v28, v61, v67
	v_mac_f32_e32 v27, v61, v68
	v_mac_f32_e32 v26, v61, v69
	ds_read2_b32 v[60:61], v60 offset1:1
	v_mac_f32_e32 v2, v63, v66
	ds_read2_b32 v[62:63], v62 offset1:1
	v_add_i32_e32 v74, vcc, 0x1fd0, v59
	v_add_i32_e32 v72, vcc, 0x1f08, v59
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v25, v60, v78
	v_mac_f32_e32 v24, v60, v79
	v_mac_f32_e32 v23, v60, v80
	v_mac_f32_e32 v22, v60, v66
	v_mac_f32_e32 v21, v60, v67
	v_mac_f32_e32 v20, v60, v68
	v_add_i32_e32 v60, vcc, 0x2158, v59
	v_mac_f32_e32 v25, v61, v79
	v_mac_f32_e32 v24, v61, v80
	v_mac_f32_e32 v23, v61, v81
	v_mac_f32_e32 v22, v61, v67
	v_mac_f32_e32 v21, v61, v68
	v_mac_f32_e32 v20, v61, v69
	ds_read2_b32 v[60:61], v60 offset1:1
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v7, v62, v77
	v_mac_f32_e32 v6, v62, v78
	v_mac_f32_e32 v5, v62, v79
	v_mac_f32_e32 v4, v62, v65
	v_mac_f32_e32 v3, v62, v66
	v_mac_f32_e32 v2, v62, v67
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v19, v60, v78
	v_mac_f32_e32 v18, v60, v79
	v_mac_f32_e32 v17, v60, v80
	v_mac_f32_e32 v16, v60, v66
	v_mac_f32_e32 v15, v60, v67
	v_mac_f32_e32 v14, v60, v68
	v_add_i32_e32 v60, vcc, 0x2224, v59
	v_add_i32_e32 v62, vcc, 0x22ec, v59
	v_mac_f32_e32 v7, v63, v78
	v_mac_f32_e32 v6, v63, v79
	v_mac_f32_e32 v5, v63, v80
	v_mac_f32_e32 v4, v63, v66
	v_mac_f32_e32 v3, v63, v67
	v_mac_f32_e32 v2, v63, v68
	ds_read2_b32 v[62:63], v62 offset1:1
	v_mac_f32_e32 v19, v61, v79
	v_mac_f32_e32 v18, v61, v80
	v_mac_f32_e32 v17, v61, v81
	v_mac_f32_e32 v16, v61, v67
	v_mac_f32_e32 v15, v61, v68
	v_mac_f32_e32 v14, v61, v69
	ds_read2_b32 v[60:61], v60 offset1:1
	ds_read2_b32 v[76:77], v74 offset1:1
	v_add_i32_e32 v74, vcc, 0x2098, v59
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v7, v62, v79
	ds_read2_b32 v[70:71], v70 offset1:1
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v13, v60, v79
	ds_read2_b32 v[78:79], v74 offset1:1
	v_add_i32_e32 v74, vcc, 0x2160, v59
	ds_read2_b32 v[72:73], v72 offset1:1
	ds_read2_b32 v[82:83], v74 offset1:1
	v_mac_f32_e32 v11, v60, v81
	v_mac_f32_e32 v5, v62, v81
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v51, v70, v75
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v39, v72, v75
	v_mac_f32_e32 v29, v76, v75
	v_mac_f32_e32 v23, v78, v75
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v17, v82, v75
	v_mac_f32_e32 v11, v61, v75
	v_mac_f32_e32 v5, v63, v75
	ds_read2_b32 v[74:75], v101 offset0:68 offset1:99
	v_mac_f32_e32 v12, v60, v80
	v_mac_f32_e32 v6, v62, v80
	v_mac_f32_e32 v10, v60, v67
	v_mac_f32_e32 v9, v60, v68
	v_mac_f32_e32 v8, v60, v69
	v_mac_f32_e32 v4, v62, v67
	v_mac_f32_e32 v3, v62, v68
	v_mac_f32_e32 v2, v62, v69
	v_mac_f32_e32 v13, v61, v80
	v_mac_f32_e32 v12, v61, v81
	v_mac_f32_e32 v7, v63, v80
	v_mac_f32_e32 v6, v63, v81
	v_mac_f32_e32 v10, v61, v68
	v_mac_f32_e32 v9, v61, v69
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v8, v61, v74
	v_mac_f32_e32 v4, v63, v68
	v_mac_f32_e32 v3, v63, v69
	v_mac_f32_e32 v2, v63, v74
	ds_read2_b32 v[60:61], v101 offset0:93 offset1:94
	ds_read2_b32 v[62:63], v101 offset0:95 offset1:96
	v_mac_f32_e32 v55, v70, v80
	v_mac_f32_e32 v54, v70, v81
	v_mac_f32_e32 v42, v72, v80
	v_mac_f32_e32 v40, v72, v81
	v_mac_f32_e32 v46, v70, v68
	v_mac_f32_e32 v44, v70, v69
	v_mac_f32_e32 v43, v70, v74
	v_mac_f32_e32 v38, v72, v68
	v_mac_f32_e32 v36, v72, v69
	v_mac_f32_e32 v35, v72, v74
	v_add_i32_e32 v70, vcc, 0x1e48, v59
	v_add_i32_e32 v72, vcc, 0x1e50, v59
	v_mac_f32_e32 v32, v76, v80
	v_mac_f32_e32 v30, v76, v81
	v_mac_f32_e32 v25, v78, v80
	v_mac_f32_e32 v24, v78, v81
	v_mac_f32_e32 v19, v82, v80
	v_mac_f32_e32 v18, v82, v81
	v_mac_f32_e32 v28, v76, v68
	v_mac_f32_e32 v27, v76, v69
	v_mac_f32_e32 v26, v76, v74
	v_mac_f32_e32 v55, v71, v64
	v_mac_f32_e32 v54, v71, v65
	v_mac_f32_e32 v51, v71, v66
	ds_read2_b32 v[80:81], v101 offset0:97 offset1:98
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v46, v71, v60
	v_mac_f32_e32 v44, v71, v61
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v43, v71, v62
	ds_read2_b32 v[70:71], v70 offset1:1
	v_mac_f32_e32 v42, v73, v64
	v_mac_f32_e32 v40, v73, v65
	v_mac_f32_e32 v39, v73, v66
	v_mac_f32_e32 v38, v73, v60
	v_mac_f32_e32 v36, v73, v61
	v_mac_f32_e32 v35, v73, v62
	ds_read2_b32 v[72:73], v72 offset1:1
	s_mov_b32 m0, -1
	v_add_i32_e32 v76, vcc, 0x222c, v59
	v_mac_f32_e32 v32, v77, v64
	v_mac_f32_e32 v30, v77, v65
	v_mac_f32_e32 v29, v77, v66
	v_mac_f32_e32 v28, v77, v60
	v_mac_f32_e32 v27, v77, v61
	v_mac_f32_e32 v26, v77, v62
	ds_read2_b32 v[76:77], v76 offset1:1
	v_mac_f32_e32 v22, v78, v68
	v_mac_f32_e32 v21, v78, v69
	v_mac_f32_e32 v20, v78, v74
	v_mac_f32_e32 v25, v79, v64
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v13, v76, v64
	v_mac_f32_e32 v12, v76, v65
	v_mac_f32_e32 v11, v76, v66
	v_mac_f32_e32 v10, v76, v60
	v_mac_f32_e32 v9, v76, v61
	v_mac_f32_e32 v8, v76, v62
	v_add_i32_e32 v76, vcc, 0x22f4, v59
	v_mac_f32_e32 v13, v77, v65
	v_mac_f32_e32 v12, v77, v66
	v_mac_f32_e32 v11, v77, v67
	v_mac_f32_e32 v10, v77, v61
	v_mac_f32_e32 v9, v77, v62
	v_mac_f32_e32 v8, v77, v63
	ds_read2_b32 v[76:77], v76 offset1:1
	v_mac_f32_e32 v19, v83, v64
	v_mac_f32_e32 v16, v82, v68
	v_mac_f32_e32 v15, v82, v69
	v_mac_f32_e32 v14, v82, v74
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v7, v76, v64
	v_add_i32_e32 v64, vcc, 0x1f10, v59
	v_mac_f32_e32 v24, v79, v65
	v_mac_f32_e32 v23, v79, v66
	v_mac_f32_e32 v22, v79, v60
	v_mac_f32_e32 v21, v79, v61
	v_mac_f32_e32 v20, v79, v62
	ds_read2_b32 v[78:79], v64 offset1:1
	v_add_i32_e32 v64, vcc, 0x1fd8, v59
	v_mac_f32_e32 v18, v83, v65
	v_mac_f32_e32 v17, v83, v66
	v_mac_f32_e32 v16, v83, v60
	v_mac_f32_e32 v15, v83, v61
	v_mac_f32_e32 v14, v83, v62
	ds_read2_b32 v[82:83], v64 offset1:1
	v_add_i32_e32 v64, vcc, 0x20a0, v59
	ds_read2_b32 v[84:85], v64 offset1:1
	v_add_i32_e32 v64, vcc, 0x2168, v59
	ds_read2_b32 v[86:87], v64 offset1:1
	v_add_i32_e32 v64, vcc, 0x2234, v59
	v_mac_f32_e32 v6, v76, v65
	v_mac_f32_e32 v55, v70, v65
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v42, v78, v65
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v32, v82, v65
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v25, v84, v65
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v19, v86, v65
	v_mac_f32_e32 v7, v77, v65
	ds_read2_b32 v[64:65], v64 offset1:1
	v_mac_f32_e32 v5, v76, v66
	v_mac_f32_e32 v54, v70, v66
	v_mac_f32_e32 v51, v70, v67
	v_mac_f32_e32 v46, v70, v61
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v13, v64, v66
	v_mac_f32_e32 v12, v64, v67
	v_mac_f32_e32 v11, v64, v68
	v_mac_f32_e32 v10, v64, v62
	v_mac_f32_e32 v9, v64, v63
	v_mac_f32_e32 v8, v64, v80
	v_add_i32_e32 v64, vcc, 0x22fc, v59
	v_mac_f32_e32 v13, v65, v67
	v_mac_f32_e32 v12, v65, v68
	v_mac_f32_e32 v11, v65, v69
	v_mac_f32_e32 v10, v65, v63
	v_mac_f32_e32 v9, v65, v80
	v_mac_f32_e32 v8, v65, v81
	ds_read2_b32 v[64:65], v64 offset1:1
	v_mac_f32_e32 v6, v77, v66
	v_mac_f32_e32 v44, v70, v62
	v_mac_f32_e32 v43, v70, v63
	v_mac_f32_e32 v55, v71, v66
	v_mac_f32_e32 v40, v78, v66
	v_mac_f32_e32 v30, v82, v66
	v_mac_f32_e32 v24, v84, v66
	v_mac_f32_e32 v18, v86, v66
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v7, v64, v66
	v_mac_f32_e32 v42, v79, v66
	v_mac_f32_e32 v32, v83, v66
	v_mac_f32_e32 v25, v85, v66
	v_mac_f32_e32 v19, v87, v66
	v_add_i32_e32 v66, vcc, 0x1f18, v59
	v_mac_f32_e32 v4, v76, v60
	v_mac_f32_e32 v3, v76, v61
	v_mac_f32_e32 v2, v76, v62
	v_mac_f32_e32 v54, v71, v67
	v_mac_f32_e32 v51, v71, v68
	v_mac_f32_e32 v46, v71, v62
	v_mac_f32_e32 v44, v71, v63
	v_mac_f32_e32 v43, v71, v80
	ds_read2_b32 v[70:71], v66 offset1:1
	v_add_i32_e32 v66, vcc, 0x1fe0, v59
	v_mac_f32_e32 v5, v77, v67
	v_mac_f32_e32 v4, v77, v61
	v_mac_f32_e32 v3, v77, v62
	v_mac_f32_e32 v2, v77, v63
	v_mac_f32_e32 v39, v78, v67
	v_mac_f32_e32 v38, v78, v61
	v_mac_f32_e32 v36, v78, v62
	v_mac_f32_e32 v35, v78, v63
	ds_read2_b32 v[76:77], v66 offset1:1
	v_add_i32_e32 v66, vcc, 0x20a8, v59
	v_mac_f32_e32 v40, v79, v67
	v_mac_f32_e32 v39, v79, v68
	v_mac_f32_e32 v38, v79, v62
	v_mac_f32_e32 v36, v79, v63
	v_mac_f32_e32 v35, v79, v80
	v_mac_f32_e32 v29, v82, v67
	v_mac_f32_e32 v28, v82, v61
	v_mac_f32_e32 v27, v82, v62
	v_mac_f32_e32 v26, v82, v63
	ds_read2_b32 v[78:79], v66 offset1:1
	v_add_i32_e32 v66, vcc, 0x2170, v59
	v_mac_f32_e32 v30, v83, v67
	v_mac_f32_e32 v29, v83, v68
	v_mac_f32_e32 v28, v83, v62
	v_mac_f32_e32 v27, v83, v63
	v_mac_f32_e32 v26, v83, v80
	ds_read2_b32 v[82:83], v66 offset1:1
	v_mac_f32_e32 v6, v64, v67
	v_mac_f32_e32 v5, v64, v68
	v_mac_f32_e32 v4, v64, v62
	v_mac_f32_e32 v3, v64, v63
	v_mac_f32_e32 v2, v64, v80
	v_add_i32_e32 v64, vcc, 0x223c, v59
	v_add_i32_e32 v66, vcc, 0x2304, v59
	v_mac_f32_e32 v23, v84, v67
	v_mac_f32_e32 v24, v85, v67
	v_mac_f32_e32 v17, v86, v67
	v_mac_f32_e32 v18, v87, v67
	v_mac_f32_e32 v55, v72, v67
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v42, v70, v67
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v32, v76, v67
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v25, v78, v67
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v19, v82, v67
	v_mac_f32_e32 v7, v65, v67
	ds_read2_b32 v[66:67], v66 offset1:1
	v_mac_f32_e32 v6, v65, v68
	v_mac_f32_e32 v5, v65, v69
	v_mac_f32_e32 v4, v65, v63
	v_mac_f32_e32 v3, v65, v80
	v_mac_f32_e32 v2, v65, v81
	ds_read2_b32 v[64:65], v64 offset1:1
	v_mac_f32_e32 v23, v85, v68
	v_mac_f32_e32 v17, v87, v68
	v_mac_f32_e32 v54, v72, v68
	v_mac_f32_e32 v40, v70, v68
	v_mac_f32_e32 v39, v70, v69
	v_mac_f32_e32 v38, v70, v63
	v_mac_f32_e32 v36, v70, v80
	v_mac_f32_e32 v35, v70, v81
	v_mac_f32_e32 v30, v76, v68
	v_mac_f32_e32 v24, v78, v68
	v_mac_f32_e32 v18, v82, v68
	v_mac_f32_e32 v51, v72, v69
	v_mac_f32_e32 v29, v76, v69
	v_mac_f32_e32 v23, v78, v69
	v_mac_f32_e32 v17, v82, v69
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v13, v64, v68
	v_mac_f32_e32 v12, v64, v69
	v_mac_f32_e32 v7, v66, v68
	v_mac_f32_e32 v6, v66, v69
	v_mac_f32_e32 v55, v73, v68
	v_mac_f32_e32 v54, v73, v69
	v_mac_f32_e32 v42, v71, v68
	v_mac_f32_e32 v40, v71, v69
	v_mac_f32_e32 v39, v71, v74
	v_mac_f32_e32 v38, v71, v80
	v_mac_f32_e32 v36, v71, v81
	v_mac_f32_e32 v35, v71, v75
	v_mac_f32_e32 v32, v77, v68
	v_mac_f32_e32 v30, v77, v69
	v_mac_f32_e32 v25, v79, v68
	v_mac_f32_e32 v24, v79, v69
	v_mac_f32_e32 v19, v83, v68
	v_mac_f32_e32 v18, v83, v69
	ds_read2_b32 v[68:69], v101 offset0:124 offset1:125
	ds_read2_b32 v[70:71], v101 offset0:126 offset1:127
	v_mac_f32_e32 v11, v64, v74
	v_mac_f32_e32 v10, v64, v80
	v_mac_f32_e32 v9, v64, v81
	v_mac_f32_e32 v8, v64, v75
	v_add_i32_e32 v64, vcc, 0x1e58, v59
	v_mac_f32_e32 v13, v65, v60
	v_mac_f32_e32 v12, v65, v61
	v_mac_f32_e32 v11, v65, v62
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v10, v65, v68
	v_mac_f32_e32 v9, v65, v69
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v8, v65, v70
	ds_read2_b32 v[64:65], v64 offset1:1
	v_mac_f32_e32 v46, v72, v63
	v_mac_f32_e32 v44, v72, v80
	v_mac_f32_e32 v43, v72, v81
	v_mac_f32_e32 v51, v73, v74
	v_mac_f32_e32 v46, v73, v80
	v_mac_f32_e32 v44, v73, v81
	v_mac_f32_e32 v43, v73, v75
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v55, v64, v60
	v_mac_f32_e32 v54, v64, v61
	v_mac_f32_e32 v51, v64, v62
	v_mac_f32_e32 v46, v64, v68
	v_mac_f32_e32 v44, v64, v69
	v_mac_f32_e32 v43, v64, v70
	v_add_i32_e32 v64, vcc, 0x1f20, v59
	v_mac_f32_e32 v55, v65, v61
	v_mac_f32_e32 v54, v65, v62
	v_mac_f32_e32 v51, v65, v63
	v_mac_f32_e32 v46, v65, v69
	v_mac_f32_e32 v44, v65, v70
	v_mac_f32_e32 v43, v65, v71
	ds_read2_b32 v[64:65], v64 offset1:1
	v_mac_f32_e32 v28, v76, v63
	v_mac_f32_e32 v27, v76, v80
	v_mac_f32_e32 v26, v76, v81
	v_mac_f32_e32 v29, v77, v74
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v42, v64, v60
	v_mac_f32_e32 v40, v64, v61
	v_mac_f32_e32 v39, v64, v62
	v_mac_f32_e32 v38, v64, v68
	v_mac_f32_e32 v36, v64, v69
	v_mac_f32_e32 v35, v64, v70
	v_add_i32_e32 v64, vcc, 0x1fe8, v59
	v_mac_f32_e32 v42, v65, v61
	v_mac_f32_e32 v40, v65, v62
	v_mac_f32_e32 v39, v65, v63
	v_mac_f32_e32 v38, v65, v69
	v_mac_f32_e32 v36, v65, v70
	v_mac_f32_e32 v35, v65, v71
	ds_read2_b32 v[64:65], v64 offset1:1
	v_mac_f32_e32 v28, v77, v80
	v_mac_f32_e32 v27, v77, v81
	v_mac_f32_e32 v26, v77, v75
	v_mac_f32_e32 v22, v84, v61
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v32, v64, v60
	v_mac_f32_e32 v30, v64, v61
	v_mac_f32_e32 v29, v64, v62
	v_mac_f32_e32 v28, v64, v68
	v_mac_f32_e32 v27, v64, v69
	v_mac_f32_e32 v26, v64, v70
	v_add_i32_e32 v64, vcc, 0x20b0, v59
	v_mac_f32_e32 v32, v65, v61
	v_mac_f32_e32 v30, v65, v62
	v_mac_f32_e32 v29, v65, v63
	v_mac_f32_e32 v28, v65, v69
	v_mac_f32_e32 v27, v65, v70
	v_mac_f32_e32 v26, v65, v71
	ds_read2_b32 v[64:65], v64 offset1:1
	v_mac_f32_e32 v21, v84, v62
	v_mac_f32_e32 v20, v84, v63
	v_mac_f32_e32 v22, v85, v62
	v_mac_f32_e32 v21, v85, v63
	v_mac_f32_e32 v20, v85, v80
	v_mac_f32_e32 v22, v78, v63
	v_mac_f32_e32 v21, v78, v80
	v_mac_f32_e32 v20, v78, v81
	v_mac_f32_e32 v23, v79, v74
	v_mac_f32_e32 v22, v79, v80
	v_mac_f32_e32 v21, v79, v81
	v_mac_f32_e32 v20, v79, v75
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v25, v64, v60
	v_mac_f32_e32 v24, v64, v61
	v_mac_f32_e32 v23, v64, v62
	v_mac_f32_e32 v22, v64, v68
	v_mac_f32_e32 v21, v64, v69
	v_mac_f32_e32 v20, v64, v70
	v_add_i32_e32 v64, vcc, 0x2178, v59
	v_mac_f32_e32 v25, v65, v61
	v_mac_f32_e32 v24, v65, v62
	v_mac_f32_e32 v23, v65, v63
	v_mac_f32_e32 v22, v65, v69
	v_mac_f32_e32 v21, v65, v70
	v_mac_f32_e32 v20, v65, v71
	ds_read2_b32 v[64:65], v64 offset1:1
	v_mac_f32_e32 v16, v86, v61
	v_mac_f32_e32 v15, v86, v62
	v_mac_f32_e32 v14, v86, v63
	v_mac_f32_e32 v16, v87, v62
	v_mac_f32_e32 v15, v87, v63
	v_mac_f32_e32 v14, v87, v80
	v_mac_f32_e32 v16, v82, v63
	v_mac_f32_e32 v15, v82, v80
	v_mac_f32_e32 v14, v82, v81
	v_mac_f32_e32 v17, v83, v74
	v_mac_f32_e32 v16, v83, v80
	v_mac_f32_e32 v15, v83, v81
	v_mac_f32_e32 v14, v83, v75
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v19, v64, v60
	v_mac_f32_e32 v18, v64, v61
	v_mac_f32_e32 v17, v64, v62
	v_mac_f32_e32 v16, v64, v68
	v_mac_f32_e32 v15, v64, v69
	v_mac_f32_e32 v14, v64, v70
	v_mac_f32_e32 v7, v67, v60
	v_add_i32_e32 v60, vcc, 0x2244, v59
	v_mac_f32_e32 v5, v66, v74
	v_mac_f32_e32 v4, v66, v80
	v_mac_f32_e32 v3, v66, v81
	v_mac_f32_e32 v2, v66, v75
	v_mac_f32_e32 v19, v65, v61
	v_mac_f32_e32 v18, v65, v62
	v_mac_f32_e32 v17, v65, v63
	v_mac_f32_e32 v16, v65, v69
	v_mac_f32_e32 v15, v65, v70
	v_mac_f32_e32 v14, v65, v71
	ds_read2_b32 v[64:65], v60 offset1:1
	v_add_i32_e32 v60, vcc, 0x230c, v59
	v_mac_f32_e32 v6, v67, v61
	v_mac_f32_e32 v5, v67, v62
	v_mac_f32_e32 v4, v67, v68
	v_mac_f32_e32 v3, v67, v69
	v_mac_f32_e32 v2, v67, v70
	ds_read2_b32 v[66:67], v60 offset1:1
	v_add_i32_e32 v60, vcc, 0x1e60, v59
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v13, v64, v61
	ds_read2_b32 v[72:73], v101 offset0:128 offset1:129
	v_mac_f32_e32 v10, v64, v69
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v7, v66, v61
	ds_read2_b32 v[60:61], v60 offset1:1
	v_mac_f32_e32 v12, v64, v62
	v_mac_f32_e32 v11, v64, v63
	v_mac_f32_e32 v9, v64, v70
	v_mac_f32_e32 v8, v64, v71
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v55, v60, v62
	v_mac_f32_e32 v54, v60, v63
	v_mac_f32_e32 v51, v60, v80
	v_mac_f32_e32 v46, v60, v70
	v_mac_f32_e32 v44, v60, v71
	v_mac_f32_e32 v43, v60, v72
	v_add_i32_e32 v60, vcc, 0x1f28, v59
	v_mac_f32_e32 v55, v61, v63
	v_mac_f32_e32 v54, v61, v80
	v_mac_f32_e32 v51, v61, v81
	v_mac_f32_e32 v46, v61, v71
	v_mac_f32_e32 v44, v61, v72
	v_mac_f32_e32 v43, v61, v73
	ds_read2_b32 v[60:61], v60 offset1:1
	v_mac_f32_e32 v6, v66, v62
	v_mac_f32_e32 v13, v65, v62
	v_mac_f32_e32 v7, v67, v62
	v_mac_f32_e32 v12, v65, v63
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v42, v60, v62
	v_mac_f32_e32 v40, v60, v63
	v_mac_f32_e32 v39, v60, v80
	v_mac_f32_e32 v38, v60, v70
	v_mac_f32_e32 v36, v60, v71
	v_mac_f32_e32 v35, v60, v72
	v_add_i32_e32 v60, vcc, 0x1ff0, v59
	v_mac_f32_e32 v42, v61, v63
	v_mac_f32_e32 v40, v61, v80
	v_mac_f32_e32 v39, v61, v81
	v_mac_f32_e32 v38, v61, v71
	v_mac_f32_e32 v36, v61, v72
	v_mac_f32_e32 v35, v61, v73
	ds_read2_b32 v[60:61], v60 offset1:1
	v_mac_f32_e32 v11, v65, v80
	v_mac_f32_e32 v10, v65, v70
	v_mac_f32_e32 v9, v65, v71
	v_mac_f32_e32 v8, v65, v72
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v32, v60, v62
	v_mac_f32_e32 v30, v60, v63
	v_mac_f32_e32 v29, v60, v80
	v_mac_f32_e32 v28, v60, v70
	v_mac_f32_e32 v27, v60, v71
	v_mac_f32_e32 v26, v60, v72
	v_add_i32_e32 v60, vcc, 0x20b8, v59
	v_mac_f32_e32 v32, v61, v63
	v_mac_f32_e32 v30, v61, v80
	v_mac_f32_e32 v29, v61, v81
	v_mac_f32_e32 v28, v61, v71
	v_mac_f32_e32 v27, v61, v72
	v_mac_f32_e32 v26, v61, v73
	ds_read2_b32 v[60:61], v60 offset1:1
	v_mac_f32_e32 v4, v66, v69
	v_mac_f32_e32 v5, v66, v63
	v_mac_f32_e32 v3, v66, v70
	v_mac_f32_e32 v2, v66, v71
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v25, v60, v62
	v_mac_f32_e32 v24, v60, v63
	v_mac_f32_e32 v23, v60, v80
	v_mac_f32_e32 v22, v60, v70
	v_mac_f32_e32 v21, v60, v71
	v_mac_f32_e32 v20, v60, v72
	v_add_i32_e32 v60, vcc, 0x2180, v59
	v_mac_f32_e32 v25, v61, v63
	v_mac_f32_e32 v24, v61, v80
	v_mac_f32_e32 v23, v61, v81
	v_mac_f32_e32 v22, v61, v71
	v_mac_f32_e32 v21, v61, v72
	v_mac_f32_e32 v20, v61, v73
	ds_read2_b32 v[60:61], v60 offset1:1
	v_mac_f32_e32 v6, v67, v63
	ds_read_b32 v66, v101 offset:520
	v_add_i32_e32 v56, vcc, 1, v56
	v_add_i32_e32 v58, vcc, s20, v58
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v19, v60, v62
	v_mac_f32_e32 v18, v60, v63
	v_mac_f32_e32 v17, v60, v80
	v_mac_f32_e32 v16, v60, v70
	v_mac_f32_e32 v15, v60, v71
	v_mac_f32_e32 v14, v60, v72
	v_add_i32_e32 v62, vcc, 0x2314, v59
	v_add_i32_e32 v60, vcc, 0x224c, v59
	ds_read2_b32 v[64:65], v62 offset1:1
	v_mac_f32_e32 v19, v61, v63
	v_mac_f32_e32 v18, v61, v80
	v_mac_f32_e32 v17, v61, v81
	v_mac_f32_e32 v16, v61, v71
	v_mac_f32_e32 v15, v61, v72
	v_mac_f32_e32 v14, v61, v73
	ds_read2_b32 v[60:61], v60 offset1:1
	v_add_i32_e32 v62, vcc, 0x1e68, v59
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v7, v64, v63
	v_mac_f32_e32 v5, v67, v80
	v_mac_f32_e32 v4, v67, v70
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v13, v60, v63
	ds_read2_b32 v[62:63], v62 offset1:50
	v_mac_f32_e32 v3, v67, v71
	v_mac_f32_e32 v2, v67, v72
	v_add_i32_e32 v57, vcc, s20, v57
	v_mac_f32_e32 v10, v60, v71
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v55, v62, v80
	v_mac_f32_e32 v54, v62, v81
	v_mac_f32_e32 v51, v62, v75
	v_mac_f32_e32 v46, v62, v72
	v_mac_f32_e32 v44, v62, v73
	v_mac_f32_e32 v43, v62, v66
	v_add_i32_e32 v62, vcc, 0x1ff8, v59
	ds_read_b32 v59, v59 offset:8584
	v_mac_f32_e32 v42, v63, v80
	v_mac_f32_e32 v40, v63, v81
	v_mac_f32_e32 v39, v63, v75
	v_mac_f32_e32 v38, v63, v72
	v_mac_f32_e32 v36, v63, v73
	v_mac_f32_e32 v35, v63, v66
	ds_read2_b32 v[62:63], v62 offset1:50
	v_mac_f32_e32 v4, v64, v71
	v_mac_f32_e32 v12, v60, v80
	v_mac_f32_e32 v11, v60, v81
	v_mac_f32_e32 v9, v60, v72
	v_mac_f32_e32 v8, v60, v73
	v_mac_f32_e32 v6, v64, v80
	v_mac_f32_e32 v5, v64, v81
	v_mac_f32_e32 v3, v64, v72
	v_mac_f32_e32 v2, v64, v73
	v_cmp_eq_u32_e32 vcc, 2, v56
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v32, v62, v80
	v_mac_f32_e32 v30, v62, v81
	v_mac_f32_e32 v29, v62, v75
	v_mac_f32_e32 v28, v62, v72
	v_mac_f32_e32 v27, v62, v73
	v_mac_f32_e32 v26, v62, v66
	v_mac_f32_e32 v25, v63, v80
	v_mac_f32_e32 v24, v63, v81
	v_mac_f32_e32 v23, v63, v75
	v_mac_f32_e32 v22, v63, v72
	v_mac_f32_e32 v21, v63, v73
	v_mac_f32_e32 v20, v63, v66
	v_mac_f32_e32 v13, v61, v80
	v_mac_f32_e32 v12, v61, v81
	v_mac_f32_e32 v11, v61, v75
	v_mac_f32_e32 v10, v61, v72
	v_mac_f32_e32 v9, v61, v73
	v_mac_f32_e32 v8, v61, v66
	v_mac_f32_e32 v19, v59, v80
	v_mac_f32_e32 v18, v59, v81
	v_mac_f32_e32 v7, v65, v80
	v_mac_f32_e32 v6, v65, v81
	v_mac_f32_e32 v17, v59, v75
	v_mac_f32_e32 v5, v65, v75
	v_mac_f32_e32 v16, v59, v72
	v_mac_f32_e32 v15, v59, v73
	v_mac_f32_e32 v4, v65, v72
	v_mac_f32_e32 v3, v65, v73
	v_mac_f32_e32 v2, v65, v66
	v_mac_f32_e32 v14, v59, v66
	s_and_b64 vcc, exec, vcc
	s_cbranch_vccz BB0_15
BB0_16:                                 ; %Flow405
                                        ;   in Loop: Header=BB0_3 Depth=1
	s_or_b64 exec, exec, s[6:7]
	v_add_i32_e32 v53, vcc, 2, v53
	v_add_i32_e32 v37, vcc, 50, v37
	v_add_i32_e32 v52, vcc, 0x5b2, v52
	v_cmp_lt_u32_e32 vcc, 63, v53
	s_and_b64 vcc, exec, vcc
	s_cbranch_vccz BB0_3
; BB#17:
	v_mov_b32_e32 v37, 0xfc
	v_cmp_lt_u32_e32 vcc, v0, v37
	s_and_saveexec_b64 s[0:1], vcc
	s_xor_b64 s[12:13], exec, s[0:1]
	; mask branch BB0_103
	s_cbranch_execz BB0_103
BB0_18:                                 ; %.preheader
	v_add_i32_e32 v0, vcc, s8, v33
	s_mov_b32 s0, 0x222c0
	v_mul_lo_i32 v33, v0, s0
	s_movk_i32 s0, 0x2d9
	v_add_i32_e32 v0, vcc, s9, v31
	v_mul_lo_i32 v37, v1, s0
	v_mul_lo_i32 v41, v0, 27
	v_mov_b32_e32 v48, 0
	v_cmp_gt_u32_e64 s[2:3], 27, v0
	v_add_i32_e32 v33, vcc, v37, v33
	v_add_i32_e32 v33, vcc, v41, v33
	v_add_i32_e32 v47, vcc, v34, v33
	v_lshlrev_b64 v[48:49], 2, v[47:48]
	v_mov_b32_e32 v33, 0xc0
	v_cmp_lt_u32_e64 s[0:1], v1, v33
	v_add_i32_e32 v48, vcc, s10, v48
	v_mov_b32_e32 v33, s11
	v_addc_u32_e32 v49, vcc, v33, v49, vcc
	v_cmp_gt_u32_e32 vcc, 27, v31
	s_and_b64 s[8:9], vcc, s[2:3]
	v_cmp_gt_u32_e64 s[2:3], 27, v34
	s_and_b64 s[14:15], s[8:9], s[2:3]
	s_and_b64 s[4:5], s[0:1], s[14:15]
	s_and_saveexec_b64 s[6:7], s[4:5]
	s_xor_b64 s[4:5], exec, s[6:7]
	; mask branch BB0_20
BB0_19:
	v_add_f32_e32 v33, 0, v55
	flat_store_dword v[48:49], v33
BB0_20:
	s_or_b64 exec, exec, s[4:5]
	v_add_i32_e32 v33, vcc, 1, v34
	v_cmp_gt_u32_e64 s[4:5], 27, v33
	s_and_b64 s[6:7], exec, s[8:9]
	s_and_b64 s[16:17], s[6:7], s[4:5]
	s_and_b64 s[6:7], exec, s[0:1]
	s_and_b64 s[6:7], s[6:7], s[16:17]
	s_and_saveexec_b64 s[18:19], s[6:7]
	s_xor_b64 s[6:7], exec, s[18:19]
	; mask branch BB0_22
BB0_21:
	v_add_f32_e32 v33, 0, v54
	flat_store_dword v[48:49], v33 offset:4
BB0_22:
	s_or_b64 exec, exec, s[6:7]
	v_add_i32_e32 v33, vcc, 2, v34
	v_cmp_gt_u32_e64 s[6:7], 27, v33
	s_and_b64 s[8:9], exec, s[8:9]
	s_and_b64 s[18:19], s[8:9], s[6:7]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[8:9], s[8:9], s[18:19]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_24
BB0_23:
	v_add_f32_e32 v33, 0, v51
	flat_store_dword v[48:49], v33 offset:8
BB0_24:
	s_or_b64 exec, exec, s[8:9]
	v_add_i32_e32 v33, vcc, 27, v47
	v_mov_b32_e32 v34, 0
	v_lshlrev_b64 v[33:34], 2, v[33:34]
	v_add_i32_e32 v33, vcc, s10, v33
	v_mov_b32_e32 v37, s11
	v_addc_u32_e32 v34, vcc, v37, v34, vcc
	v_add_i32_e32 v0, vcc, 1, v0
	v_or_b32_e32 v31, 1, v31
	v_cmp_gt_u32_e64 s[8:9], 27, v31
	v_cmp_gt_u32_e32 vcc, 27, v0
	s_and_b64 s[8:9], s[8:9], vcc
	s_and_b64 s[2:3], exec, s[2:3]
	s_and_b64 s[2:3], s[8:9], s[2:3]
	s_and_b64 s[20:21], exec, s[0:1]
	s_and_b64 s[20:21], s[20:21], s[2:3]
	s_and_saveexec_b64 s[22:23], s[20:21]
	s_xor_b64 s[20:21], exec, s[22:23]
	; mask branch BB0_26
BB0_25:
	v_add_f32_e32 v0, 0, v46
	flat_store_dword v[33:34], v0
BB0_26:
	s_or_b64 exec, exec, s[20:21]
	s_and_b64 s[20:21], exec, s[8:9]
	s_and_b64 s[4:5], exec, s[4:5]
	s_and_b64 s[4:5], s[20:21], s[4:5]
	s_and_b64 s[20:21], exec, s[0:1]
	s_and_b64 s[20:21], s[20:21], s[4:5]
	s_and_saveexec_b64 s[22:23], s[20:21]
	s_xor_b64 s[20:21], exec, s[22:23]
	; mask branch BB0_28
BB0_27:
	v_add_f32_e32 v0, 0, v44
	flat_store_dword v[33:34], v0 offset:4
BB0_28:
	s_or_b64 exec, exec, s[20:21]
	s_and_b64 s[8:9], exec, s[8:9]
	s_and_b64 s[6:7], exec, s[6:7]
	s_and_b64 s[6:7], s[8:9], s[6:7]
	s_and_b64 s[0:1], exec, s[0:1]
	s_and_b64 s[0:1], s[0:1], s[6:7]
	s_and_saveexec_b64 s[8:9], s[0:1]
	s_xor_b64 s[0:1], exec, s[8:9]
	; mask branch BB0_30
BB0_29:
	v_add_f32_e32 v0, 0, v43
	flat_store_dword v[33:34], v0 offset:8
BB0_30:                                 ; %.preheader.1304
	s_or_b64 exec, exec, s[0:1]
	v_add_i32_e32 v33, vcc, 0x2d9, v47
	v_mov_b32_e32 v34, 0
	v_add_i32_e32 v0, vcc, 1, v1
	v_lshlrev_b64 v[33:34], 2, v[33:34]
	v_mov_b32_e32 v31, 0xc0
	v_cmp_lt_u32_e64 s[0:1], v0, v31
	s_and_b64 s[8:9], exec, s[14:15]
	v_add_i32_e32 v33, vcc, s10, v33
	v_mov_b32_e32 v0, s11
	v_addc_u32_e32 v34, vcc, v0, v34, vcc
	s_and_b64 s[8:9], s[0:1], s[8:9]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_32
BB0_31:
	v_add_f32_e32 v0, 0, v42
	flat_store_dword v[33:34], v0
BB0_32:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[16:17]
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_34
BB0_33:
	v_add_f32_e32 v0, 0, v40
	flat_store_dword v[33:34], v0 offset:4
BB0_34:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[18:19]
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_36
BB0_35:
	v_add_f32_e32 v0, 0, v39
	flat_store_dword v[33:34], v0 offset:8
BB0_36:
	s_or_b64 exec, exec, s[8:9]
	v_add_i32_e32 v33, vcc, 0x2f4, v47
	v_mov_b32_e32 v34, 0
	v_lshlrev_b64 v[33:34], 2, v[33:34]
	v_add_i32_e32 v33, vcc, s10, v33
	v_mov_b32_e32 v0, s11
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[2:3]
	v_addc_u32_e32 v34, vcc, v0, v34, vcc
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_38
BB0_37:
	v_add_f32_e32 v0, 0, v38
	flat_store_dword v[33:34], v0
BB0_38:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[4:5]
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_40
BB0_39:
	v_add_f32_e32 v0, 0, v36
	flat_store_dword v[33:34], v0 offset:4
BB0_40:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[0:1], exec, s[0:1]
	s_and_b64 s[8:9], exec, s[6:7]
	s_and_b64 s[0:1], s[0:1], s[8:9]
	s_and_saveexec_b64 s[8:9], s[0:1]
	s_xor_b64 s[0:1], exec, s[8:9]
	; mask branch BB0_42
BB0_41:
	v_add_f32_e32 v0, 0, v35
	flat_store_dword v[33:34], v0 offset:8
BB0_42:                                 ; %.preheader.2305
	s_or_b64 exec, exec, s[0:1]
	v_add_i32_e32 v33, vcc, 0x5b2, v47
	v_mov_b32_e32 v34, 0
	v_add_i32_e32 v0, vcc, 2, v1
	v_lshlrev_b64 v[33:34], 2, v[33:34]
	v_mov_b32_e32 v31, 0xc0
	v_cmp_lt_u32_e64 s[0:1], v0, v31
	s_and_b64 s[8:9], exec, s[14:15]
	v_add_i32_e32 v33, vcc, s10, v33
	v_mov_b32_e32 v0, s11
	v_addc_u32_e32 v34, vcc, v0, v34, vcc
	s_and_b64 s[8:9], s[0:1], s[8:9]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_44
BB0_43:
	v_add_f32_e32 v0, 0, v32
	flat_store_dword v[33:34], v0
BB0_44:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[16:17]
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_46
BB0_45:
	v_add_f32_e32 v0, 0, v30
	flat_store_dword v[33:34], v0 offset:4
BB0_46:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[18:19]
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_48
BB0_47:
	v_add_f32_e32 v0, 0, v29
	flat_store_dword v[33:34], v0 offset:8
BB0_48:
	s_or_b64 exec, exec, s[8:9]
	v_add_i32_e32 v29, vcc, 0x5cd, v47
	v_mov_b32_e32 v30, 0
	v_lshlrev_b64 v[29:30], 2, v[29:30]
	v_add_i32_e32 v29, vcc, s10, v29
	v_mov_b32_e32 v0, s11
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[2:3]
	v_addc_u32_e32 v30, vcc, v0, v30, vcc
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_50
BB0_49:
	v_add_f32_e32 v0, 0, v28
	flat_store_dword v[29:30], v0
BB0_50:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[4:5]
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_52
BB0_51:
	v_add_f32_e32 v0, 0, v27
	flat_store_dword v[29:30], v0 offset:4
BB0_52:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[0:1], exec, s[0:1]
	s_and_b64 s[8:9], exec, s[6:7]
	s_and_b64 s[0:1], s[0:1], s[8:9]
	s_and_saveexec_b64 s[8:9], s[0:1]
	s_xor_b64 s[0:1], exec, s[8:9]
	; mask branch BB0_54
BB0_53:
	v_add_f32_e32 v0, 0, v26
	flat_store_dword v[29:30], v0 offset:8
BB0_54:                                 ; %.preheader.3306
	s_or_b64 exec, exec, s[0:1]
	v_add_i32_e32 v0, vcc, 3, v1
	v_mov_b32_e32 v27, 0xc0
	v_cmp_lt_u32_e64 s[0:1], v0, v27
	v_add_i32_e32 v26, vcc, 0x88b, v47
	v_mov_b32_e32 v27, 0
	v_lshlrev_b64 v[26:27], 2, v[26:27]
	s_and_b64 s[8:9], exec, s[14:15]
	v_add_i32_e32 v26, vcc, s10, v26
	v_mov_b32_e32 v0, s11
	v_addc_u32_e32 v27, vcc, v0, v27, vcc
	s_and_b64 s[8:9], s[0:1], s[8:9]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_56
BB0_55:
	v_add_f32_e32 v0, 0, v25
	flat_store_dword v[26:27], v0
BB0_56:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[16:17]
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_58
BB0_57:
	v_add_f32_e32 v0, 0, v24
	flat_store_dword v[26:27], v0 offset:4
BB0_58:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[18:19]
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_60
BB0_59:
	v_add_f32_e32 v0, 0, v23
	flat_store_dword v[26:27], v0 offset:8
BB0_60:
	s_or_b64 exec, exec, s[8:9]
	v_add_i32_e32 v23, vcc, 0x8a6, v47
	v_mov_b32_e32 v24, 0
	v_lshlrev_b64 v[23:24], 2, v[23:24]
	v_add_i32_e32 v23, vcc, s10, v23
	v_mov_b32_e32 v0, s11
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[2:3]
	v_addc_u32_e32 v24, vcc, v0, v24, vcc
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_62
BB0_61:
	v_add_f32_e32 v0, 0, v22
	flat_store_dword v[23:24], v0
BB0_62:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[4:5]
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_64
BB0_63:
	v_add_f32_e32 v0, 0, v21
	flat_store_dword v[23:24], v0 offset:4
BB0_64:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[0:1], exec, s[0:1]
	s_and_b64 s[8:9], exec, s[6:7]
	s_and_b64 s[0:1], s[0:1], s[8:9]
	s_and_saveexec_b64 s[8:9], s[0:1]
	s_xor_b64 s[0:1], exec, s[8:9]
	; mask branch BB0_66
BB0_65:
	v_add_f32_e32 v0, 0, v20
	flat_store_dword v[23:24], v0 offset:8
BB0_66:                                 ; %.preheader.4307
	s_or_b64 exec, exec, s[0:1]
	v_add_i32_e32 v0, vcc, 4, v1
	v_mov_b32_e32 v21, 0xc0
	v_cmp_lt_u32_e64 s[0:1], v0, v21
	v_add_i32_e32 v20, vcc, 0xb64, v47
	v_mov_b32_e32 v21, 0
	v_lshlrev_b64 v[20:21], 2, v[20:21]
	s_and_b64 s[8:9], exec, s[14:15]
	v_add_i32_e32 v20, vcc, s10, v20
	v_mov_b32_e32 v0, s11
	v_addc_u32_e32 v21, vcc, v0, v21, vcc
	s_and_b64 s[8:9], s[0:1], s[8:9]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_68
BB0_67:
	v_add_f32_e32 v0, 0, v19
	flat_store_dword v[20:21], v0
BB0_68:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[16:17]
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_70
BB0_69:
	v_add_f32_e32 v0, 0, v18
	flat_store_dword v[20:21], v0 offset:4
BB0_70:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[18:19]
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_72
BB0_71:
	v_add_f32_e32 v0, 0, v17
	flat_store_dword v[20:21], v0 offset:8
BB0_72:
	s_or_b64 exec, exec, s[8:9]
	v_add_i32_e32 v17, vcc, 0xb7f, v47
	v_mov_b32_e32 v18, 0
	v_lshlrev_b64 v[17:18], 2, v[17:18]
	v_add_i32_e32 v17, vcc, s10, v17
	v_mov_b32_e32 v0, s11
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[2:3]
	v_addc_u32_e32 v18, vcc, v0, v18, vcc
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_74
BB0_73:
	v_add_f32_e32 v0, 0, v16
	flat_store_dword v[17:18], v0
BB0_74:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[4:5]
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_76
BB0_75:
	v_add_f32_e32 v0, 0, v15
	flat_store_dword v[17:18], v0 offset:4
BB0_76:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[0:1], exec, s[0:1]
	s_and_b64 s[8:9], exec, s[6:7]
	s_and_b64 s[0:1], s[0:1], s[8:9]
	s_and_saveexec_b64 s[8:9], s[0:1]
	s_xor_b64 s[0:1], exec, s[8:9]
	; mask branch BB0_78
BB0_77:
	v_add_f32_e32 v0, 0, v14
	flat_store_dword v[17:18], v0 offset:8
BB0_78:                                 ; %.preheader.5308
	s_or_b64 exec, exec, s[0:1]
	v_add_i32_e32 v0, vcc, 5, v1
	v_mov_b32_e32 v15, 0xc0
	v_cmp_lt_u32_e64 s[0:1], v0, v15
	v_add_i32_e32 v14, vcc, 0xe3d, v47
	v_mov_b32_e32 v15, 0
	v_lshlrev_b64 v[14:15], 2, v[14:15]
	s_and_b64 s[8:9], exec, s[14:15]
	v_add_i32_e32 v14, vcc, s10, v14
	v_mov_b32_e32 v0, s11
	v_addc_u32_e32 v15, vcc, v0, v15, vcc
	s_and_b64 s[8:9], s[0:1], s[8:9]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_80
BB0_79:
	v_add_f32_e32 v0, 0, v13
	flat_store_dword v[14:15], v0
BB0_80:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[16:17]
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_82
BB0_81:
	v_add_f32_e32 v0, 0, v12
	flat_store_dword v[14:15], v0 offset:4
BB0_82:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[18:19]
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_84
BB0_83:
	v_add_f32_e32 v0, 0, v11
	flat_store_dword v[14:15], v0 offset:8
BB0_84:
	s_or_b64 exec, exec, s[8:9]
	v_add_i32_e32 v11, vcc, 0xe58, v47
	v_mov_b32_e32 v12, 0
	v_lshlrev_b64 v[11:12], 2, v[11:12]
	v_add_i32_e32 v11, vcc, s10, v11
	v_mov_b32_e32 v0, s11
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[2:3]
	v_addc_u32_e32 v12, vcc, v0, v12, vcc
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_86
BB0_85:
	v_add_f32_e32 v0, 0, v10
	flat_store_dword v[11:12], v0
BB0_86:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[20:21], exec, s[4:5]
	s_and_b64 s[8:9], s[8:9], s[20:21]
	s_and_saveexec_b64 s[20:21], s[8:9]
	s_xor_b64 s[8:9], exec, s[20:21]
	; mask branch BB0_88
BB0_87:
	v_add_f32_e32 v0, 0, v9
	flat_store_dword v[11:12], v0 offset:4
BB0_88:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[0:1], exec, s[0:1]
	s_and_b64 s[8:9], exec, s[6:7]
	s_and_b64 s[0:1], s[0:1], s[8:9]
	s_and_saveexec_b64 s[8:9], s[0:1]
	s_xor_b64 s[0:1], exec, s[8:9]
	; mask branch BB0_90
BB0_89:
	v_add_f32_e32 v0, 0, v8
	flat_store_dword v[11:12], v0 offset:8
BB0_90:                                 ; %.preheader.6309
	s_or_b64 exec, exec, s[0:1]
	v_add_i32_e32 v1, vcc, 6, v1
	v_mov_b32_e32 v8, 0xc0
	v_cmp_lt_u32_e64 s[0:1], v1, v8
	v_add_i32_e32 v0, vcc, 0x1116, v47
	v_mov_b32_e32 v1, 0
	v_lshlrev_b64 v[0:1], 2, v[0:1]
	s_and_b64 s[8:9], exec, s[14:15]
	v_add_i32_e32 v0, vcc, s10, v0
	v_mov_b32_e32 v8, s11
	v_addc_u32_e32 v1, vcc, v8, v1, vcc
	s_and_b64 s[8:9], s[0:1], s[8:9]
	s_and_saveexec_b64 s[14:15], s[8:9]
	s_xor_b64 s[8:9], exec, s[14:15]
	; mask branch BB0_92
BB0_91:
	v_add_f32_e32 v7, 0, v7
	flat_store_dword v[0:1], v7
BB0_92:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[14:15], exec, s[16:17]
	s_and_b64 s[8:9], s[8:9], s[14:15]
	s_and_saveexec_b64 s[14:15], s[8:9]
	s_xor_b64 s[8:9], exec, s[14:15]
	; mask branch BB0_94
BB0_93:
	v_add_f32_e32 v6, 0, v6
	flat_store_dword v[0:1], v6 offset:4
BB0_94:
	s_or_b64 exec, exec, s[8:9]
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[14:15], exec, s[18:19]
	s_and_b64 s[8:9], s[8:9], s[14:15]
	s_and_saveexec_b64 s[14:15], s[8:9]
	s_xor_b64 s[8:9], exec, s[14:15]
	; mask branch BB0_96
BB0_95:
	v_add_f32_e32 v5, 0, v5
	flat_store_dword v[0:1], v5 offset:8
BB0_96:
	s_or_b64 exec, exec, s[8:9]
	v_add_i32_e32 v0, vcc, 0x1131, v47
	v_mov_b32_e32 v1, 0
	v_lshlrev_b64 v[0:1], 2, v[0:1]
	v_add_i32_e32 v0, vcc, s10, v0
	v_mov_b32_e32 v5, s11
	s_and_b64 s[8:9], exec, s[0:1]
	s_and_b64 s[2:3], exec, s[2:3]
	v_addc_u32_e32 v1, vcc, v5, v1, vcc
	s_and_b64 s[2:3], s[8:9], s[2:3]
	s_and_saveexec_b64 s[8:9], s[2:3]
	s_xor_b64 s[2:3], exec, s[8:9]
	; mask branch BB0_98
BB0_97:
	v_add_f32_e32 v4, 0, v4
	flat_store_dword v[0:1], v4
BB0_98:
	s_or_b64 exec, exec, s[2:3]
	s_and_b64 s[2:3], exec, s[0:1]
	s_and_b64 s[4:5], exec, s[4:5]
	s_and_b64 s[2:3], s[2:3], s[4:5]
	s_and_saveexec_b64 s[4:5], s[2:3]
	s_xor_b64 s[2:3], exec, s[4:5]
	; mask branch BB0_100
BB0_99:
	v_add_f32_e32 v3, 0, v3
	flat_store_dword v[0:1], v3 offset:4
BB0_100:
	s_or_b64 exec, exec, s[2:3]
	s_and_b64 s[0:1], exec, s[0:1]
	s_and_b64 s[2:3], exec, s[6:7]
	s_and_b64 s[0:1], s[0:1], s[2:3]
	s_and_saveexec_b64 s[2:3], s[0:1]
	s_xor_b64 s[0:1], exec, s[2:3]
	; mask branch BB0_102
BB0_101:
	v_add_f32_e32 v2, 0, v2
	flat_store_dword v[0:1], v2 offset:8
BB0_102:                                ; %Flow
	s_or_b64 exec, exec, s[0:1]
BB0_103:                                ; %Flow404
	s_or_b64 exec, exec, s[12:13]
	s_endpgm
.Lfunc_end0:
	.size	MIOpenConvUniC, .Lfunc_end0-MIOpenConvUniC
                                        ; -- End function
	.section	.AMDGPU.csdata
; Kernel info:
; codeLenInByte = 9888
; NumSgprs: 28
; NumVgprs: 102
; ScratchSize: 0
; codeLenInByte = 9888
; NumSgprs: 28
; NumVgprs: 102
; FloatMode: 192
; IeeeMode: 1
; ScratchSize: 0
; LDSByteSize: 10488 bytes/workgroup (compile time only)
; SGPRBlocks: 3
; VGPRBlocks: 25
; NumSGPRsForWavesPerEU: 28
; NumVGPRsForWavesPerEU: 102
; ReservedVGPRFirst: 0
; ReservedVGPRCount: 0
; COMPUTE_PGM_RSRC2:USER_SGPR: 6
; COMPUTE_PGM_RSRC2:TRAP_HANDLER: 1
; COMPUTE_PGM_RSRC2:TGID_X_EN: 1
; COMPUTE_PGM_RSRC2:TGID_Y_EN: 1
; COMPUTE_PGM_RSRC2:TGID_Z_EN: 1
; COMPUTE_PGM_RSRC2:TIDIG_COMP_CNT: 0

	.ident	"clang version 4.0 "
	.section	".note.GNU-stack"
	.amdgpu_code_object_metadata
---
Version:         [ 1, 0 ]
Kernels:         
  - Name:            MIOpenConvUniC
    Language:        OpenCL C
    LanguageVersion: [ 1, 2 ]
    Attrs:           
      ReqdWorkGroupSize: [ 256, 1, 1 ]
    Args:            
      - Size:            8
        Align:           8
        ValueKind:       GlobalBuffer
        ValueType:       F32
        AccQual:         ReadOnly
        AddrSpaceQual:   Global
        IsConst:         true
        IsRestrict:      true
        Name:            in
        TypeName:        'float*'
      - Size:            8
        Align:           8
        ValueKind:       GlobalBuffer
        ValueType:       F32
        AccQual:         ReadOnly
        AddrSpaceQual:   Global
        IsConst:         true
        IsRestrict:      true
        Name:            weights
        TypeName:        'float*'
      - Size:            8
        Align:           8
        ValueKind:       GlobalBuffer
        ValueType:       F32
        AccQual:         Default
        AddrSpaceQual:   Global
        IsRestrict:      true
        Name:            out
        TypeName:        'float*'
      - Size:            4
        Align:           4
        ValueKind:       ByValue
        ValueType:       F32
        AccQual:         Default
        Name:            padding_val
        TypeName:        float
      - Size:            8
        Align:           8
        ValueKind:       HiddenGlobalOffsetX
        ValueType:       I64
      - Size:            8
        Align:           8
        ValueKind:       HiddenGlobalOffsetY
        ValueType:       I64
      - Size:            8
        Align:           8
        ValueKind:       HiddenGlobalOffsetZ
        ValueType:       I64
    CodeProps:       
      KernargSegmentSize: 64
      WorkgroupGroupSegmentSize: 10488
      WavefrontNumSGPRs: 28
      WorkitemNumVGPRs: 102
      KernargSegmentAlign: 4
      GroupSegmentAlign: 4
      PrivateSegmentAlign: 4
      WavefrontSize:   6
...
	.end_amdgpu_code_object_metadata
