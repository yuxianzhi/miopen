#ifndef __HIP_FUNDAMENTAL_H__
#define __HIP_FUNDAMENTAL_H__
#include <hip/hip_runtime.h>
#include "sys/time.h"

// ========= status check =========
//void GPU_CHECK(hipError_t error)
//{
//    if (error != hipSuccess) { 
//        fprintf(stderr, "error: '%s'(%d) at %s:%d\n", hipGetErrorString(error), error,__FILE__, __LINE__); 
//        exit(EXIT_FAILURE);
//    }
//}

#if 0
#define GPU_CHECK(cmd) \
{\
    hipError_t error  = cmd;\
    if (error != hipSuccess) { \
      fprintf(stderr, "error: '%s'(%d) at %s:%d\n", hipGetErrorString(error), error,__FILE__, __LINE__); \
    exit(EXIT_FAILURE);\
        }\
}

#else
#define GPU_CHECK(cmd) cmd
#endif


// ========== Data =========
// gpu data
inline void* gpuData(int size){
    void *data;
    GPU_CHECK(hipMalloc(&data, size));
    return data;
}
inline void gpuDataFree(void *data){
    if(data != NULL)
        GPU_CHECK(hipFree(data));
}
inline void gpuDataSet(void *data, int size, int val){
    GPU_CHECK(hipMemset(data, size, val));
}
#ifdef __cplusplus
template <typename T>
__global__ void gpuRandom(hipLaunchParm lp, T *data, size_t N, bool do_random)
{
    size_t threadId = (hipBlockIdx_x * hipBlockDim_x + hipThreadIdx_x);
    size_t stride = hipBlockDim_x * hipGridDim_x ;
    if (do_random)
        for (size_t i=threadId; i<N; i+=stride) {
            data[i] = T(threadId) / hipThreadIdx_x;
        }
    else
        for (size_t i=threadId; i<N; i+=stride) {
            data[i] = 1;
        }
}
template <typename T>
inline void gpuDataRandom(T *data, int N, bool do_random)
{
    const unsigned blocks = 512;
    const unsigned threadsPerBlock = 256;
    //GPU_CHECK(
    hipLaunchKernel(gpuRandom<T>,   /* compute kernel*/
                dim3(blocks), dim3(threadsPerBlock), 0/*dynamic shared*/, 0/*stream*/,     /* launch config*/
                data, N, do_random);
    //);  /* arguments to the compute kernel */
}
#endif

// cpu data
inline void* cpuData(int size){
    void *data = malloc(size);
    return data;  
}
inline void cpuDataFree(void *data){
    if(data != NULL)
        free(data);
}
inline void cpuDataSet(void *data, int size, int val){
    memset(data, size, val);
}
#ifdef __cplusplus
template <typename T>
inline void cpuDataRandom(T *data, size_t N, bool do_random)
{
    if (do_random)
        for (int i=0; i<N; i++)
            data[i] = T(rand()%100) + 1;
    else
        for (int i=0; i<N; i++)
            data[i] = 1;
}
#endif

// cpu <-> gpu
inline void cpu2gpu(void *data_h, void *data_d, int size){
    GPU_CHECK(hipMemcpy(data_d, data_h, size, hipMemcpyHostToDevice));
}
inline void gpu2cpu(void *data_d, void *data_h, int size){
    GPU_CHECK(hipMemcpy(data_h, data_d, size, hipMemcpyDeviceToHost));
}
inline void cpu2cpu(void *src, void *dst, int size){
    memcpy(dst, src, size);
}
inline void gpu2gpu(void *src, void *dst, int size){
    GPU_CHECK(hipMemcpy(dst, src, size, hipMemcpyDeviceToDevice));
}
inline void cpu2gpu_const(void *data_h, void *data_d, int size){
    GPU_CHECK(hipMemcpyToSymbol(data_d, data_h, size, 0, hipMemcpyHostToDevice));
}
inline void gpu2cpu_const(void *data_d, void *data_h, int size){
    GPU_CHECK(hipMemcpyFromSymbol(data_h, data_d, size, 0, hipMemcpyDeviceToHost));
}



// ========= time =========
struct timeval cpu_time_start, cpu_time_end;
hipEvent_t gpu_time_start, gpu_time_end;
double cpu_time_interval = 0;
float gpu_time_interval = 0;
inline double cpu_time_tic(){
    gettimeofday(&cpu_time_start, NULL);
    return 0;
}
inline double cpu_time_toc(){
    gettimeofday(&cpu_time_end, NULL);
    cpu_time_interval = (cpu_time_end.tv_sec-cpu_time_start.tv_sec)+ (cpu_time_end.tv_usec-cpu_time_start.tv_usec)/1000000.0;
    return cpu_time_interval;
}
inline double gpu_time_tic(){
    hipEventCreate(&gpu_time_start);
    hipEventCreate(&gpu_time_end);
    hipEventRecord(gpu_time_start, 0);
    return 0;
}
inline double gpu_time_toc(){
    hipEventRecord(gpu_time_end, 0);
    hipEventElapsedTime(&gpu_time_interval, gpu_time_start, gpu_time_end);
    gpu_time_interval /= 1000;
    hipEventDestroy(gpu_time_start);
    hipEventDestroy(gpu_time_end);
    return gpu_time_interval;
}
inline double TFLOPS(double OPS, double sec){
    return OPS/1000000000000.0/sec;
}
inline void gpuBarrier(){
    GPU_CHECK(hipDeviceSynchronize());
}


// ========= kernel =========
inline hipFunction_t loadKernelFromHsaco(const char *fileName, const char *kernelName)
{
    hipModule_t Module;
    hipFunction_t Function;
    GPU_CHECK(hipModuleLoad(&Module, fileName));
    GPU_CHECK(hipModuleGetFunction(&Function, Module, kernelName));
    return Function;
}
inline hipFunction_t loadKernelFromBytesArray(const unsigned char *bytesArray, const char *kernelName)
{
    hipModule_t Module;
    hipFunction_t Function;
    GPU_CHECK(hipModuleLoadData(&Module, bytesArray));
    GPU_CHECK(hipModuleGetFunction(&Function, Module, kernelName));
    return Function;
}
inline void** makeKernelParameter(void *hipFunctionArgs_ptr, void *hipFunctionArgsSize_ptr)
{
    void **hipLaunchParams = (void **)cpuData(sizeof(void *)*5);
    hipLaunchParams[0] = HIP_LAUNCH_PARAM_BUFFER_POINTER;
    hipLaunchParams[1] = hipFunctionArgs_ptr;
    hipLaunchParams[2] = HIP_LAUNCH_PARAM_BUFFER_SIZE;
    hipLaunchParams[3] = hipFunctionArgsSize_ptr;
    hipLaunchParams[4] = HIP_LAUNCH_PARAM_END;
    return hipLaunchParams;
}
#define MAKE_KERNEL_PARAMETER(hipFunctionArgs) \
    size_t hipFunctionArgsSize = sizeof(hipFunctionArgs);\
    void *KERNEL_PARAMETER[] ={HIP_LAUNCH_PARAM_BUFFER_POINTER, &hipFunctionArgs, HIP_LAUNCH_PARAM_BUFFER_SIZE, &hipFunctionArgsSize, HIP_LAUNCH_PARAM_END};

// ========= verify =========
#ifdef __cplusplus
template<typename T>
inline bool cpu_cpu_verify(T *data_a, T *data_b, int N){
    if(data_a == NULL && data_b == NULL)
        return true;
    if(data_a == NULL)
        return false;
    if(data_b == NULL)
        return false;

    //std::cout<<std::endl;
    for (int i=0; i<N; i++){
        //std::cout << i << " @ " << data_a[i] << " " << data_b[i] << std::endl;
        if (data_a[i] != data_b[i])
        {
            std::cout << i << "@ " << data_a[i] << "    " << data_b[i] <<std::endl;
            return false;
        }
    }
    return true;
}
template<typename T>
inline bool cpu_gpu_verify(T *data_h, T *data_d, int N){
    if(data_h == NULL && data_d == NULL)
        return true;
    if(data_h == NULL)
        return false;
    if(data_d == NULL)
        return false;
    T *data_d_h = (T *)cpuData(sizeof(T)*N);
    gpu2cpu(data_d, data_d_h, sizeof(T)*N) ;
    bool result = cpu_cpu_verify<T>(data_h, data_d_h, N);
    cpuDataFree(data_d_h);
    return result;
}
#else
typedef int bool;
inline bool cpu_cpu_verify(void *data_a, void *data_b, int length){
    if(data_a == NULL && data_b == NULL)
        return 1;
    if(data_a == NULL)
        return 0;
    if(data_b == NULL)
        return 0;
    for (int i=0; i<length; i++){
        if (data_a[i] != data_b[i])
            return 0;
    }
    return 1;
}
inline bool cpu_gpu_verify(void *data_h, void *data_d, int length){
    if(data_h == NULL && data_d == NULL)
        return 1; 
    if(data_h == NULL)
        return 0;
    if(data_d == NULL)
        return 0;
    void *data_d_h = cpuData(sizeof(void)*length);
    gpu2cpu(data_d, data_d_h, sizeof(void)*length) ;
    bool result = cpu_cpu_verify(data_h, data_d_h, length);
    cpuDataFree(data_d_h);
    return result;
}
#endif

#endif

