	.text
	.hsa_code_object_version 2,1
	.hsa_code_object_isa 9,0,0,"AMD","AMDGPU"
	.globl	MIOpenCvFwd11x11        ; -- Begin function MIOpenCvFwd11x11
	.p2align	8
	.type	MIOpenCvFwd11x11,@function
	.amdgpu_hsa_kernel MIOpenCvFwd11x11
MIOpenCvFwd11x11:                       ; @MIOpenCvFwd11x11
	.amd_kernel_code_t
		amd_code_version_major = 1
		amd_code_version_minor = 1
		amd_machine_kind = 1
		amd_machine_version_major = 9
		amd_machine_version_minor = 0
		amd_machine_version_stepping = 0
		kernel_code_entry_byte_offset = 256
		kernel_code_prefetch_byte_size = 0
		max_scratch_backing_memory_byte_size = 0
		granulated_workitem_vgpr_count = 15
		granulated_wavefront_sgpr_count = 4
		priority = 0
		float_mode = 192
		priv = 0
		enable_dx10_clamp = 1
		debug_mode = 0
		enable_ieee_mode = 1
		enable_sgpr_private_segment_wave_byte_offset = 0
		user_sgpr_count = 6
		enable_trap_handler = 1
		enable_sgpr_workgroup_id_x = 1
		enable_sgpr_workgroup_id_y = 1
		enable_sgpr_workgroup_id_z = 1
		enable_sgpr_workgroup_info = 0
		enable_vgpr_workitem_id = 0
		enable_exception_msb = 0
		granulated_lds_size = 0
		enable_exception = 0
		enable_sgpr_private_segment_buffer = 1
		enable_sgpr_dispatch_ptr = 0
		enable_sgpr_queue_ptr = 0
		enable_sgpr_kernarg_segment_ptr = 1
		enable_sgpr_dispatch_id = 0
		enable_sgpr_flat_scratch_init = 0
		enable_sgpr_private_segment_size = 0
		enable_sgpr_grid_workgroup_count_x = 0
		enable_sgpr_grid_workgroup_count_y = 0
		enable_sgpr_grid_workgroup_count_z = 0
		enable_ordered_append_gds = 0
		private_element_size = 1
		is_ptr64 = 1
		is_dynamic_callstack = 0
		is_debug_enabled = 0
		is_xnack_enabled = 0
		workitem_private_segment_byte_size = 0
		workgroup_group_segment_byte_size = 14832
		gds_segment_byte_size = 0
		kernarg_segment_byte_size = 64
		workgroup_fbarrier_count = 0
		wavefront_sgpr_count = 36
		workitem_vgpr_count = 63
		reserved_vgpr_first = 0
		reserved_vgpr_count = 0
		reserved_sgpr_first = 0
		reserved_sgpr_count = 0
		debug_wavefront_private_segment_offset_sgpr = 0
		debug_private_segment_buffer_sgpr = 0
		kernarg_segment_alignment = 4
		group_segment_alignment = 4
		private_segment_alignment = 4
		wavefront_size = 6
		call_convention = -1
		runtime_loader_kernel_symbol = 0
	.end_amd_kernel_code_t
; BB#0:                                 ; %.lr.ph.preheader
	s_load_dwordx4 s[20:23], s[4:5], 0x0
	s_load_dwordx2 s[18:19], s[4:5], 0x10
	s_mul_i32 s10, s6, 13
	s_mul_i32 s6, s6, 52
	s_add_i32 s6, s6, -2
	s_mul_i32 s0, s8, 0x24c00
	s_mul_i32 s25, s6, 0xe0
	s_mul_i32 s9, s7, 6
	s_mul_i32 s16, s7, 0x882
	s_add_i32 s25, s25, s0
	v_lshlrev_b32_e32 v1, 2, v0
	s_mov_b64 s[2:3], 0
	v_mov_b32_e32 v2, v0
	s_mov_b32 m0, -1
BB0_1:                                  ; %.lr.ph
                                        ; =>This Inner Loop Header: Depth=1
	v_mov_b32_e32 v40, 0xe7b
	v_mov_b32_e32 v41, 0
BB0_1_1:
	v_add_i32_e32 v2, vcc, 0x100, v2
	v_cmp_gt_u32_e64 s[0:1], v2, v40
	ds_write_b32 v1, v41
	v_add_i32_e32 v1, vcc, 0x400, v1
	s_or_b64 s[2:3], s[0:1], s[2:3]
	s_andn2_b64 exec, exec, s[2:3]
	s_cbranch_execnz BB0_1_1
; BB#2:                                 ; %.preheader481
	s_or_b64 exec, exec, s[2:3]
	v_cvt_f32_ubyte0_e32 v1, v0
	v_mov_b32_e32 v3, 0x3727c5ac
	s_mov_b32 s12, 0xffffff
	s_movk_i32 s0, 0xffed
	v_mac_f32_e32 v3, 0x3d579436, v1
	v_cvt_u32_f32_e32 v14, v3
	v_mov_b32_e32 v3, 0x3727c5ac
	v_mac_f32_e32 v3, 0x3eaaaaab, v1
	v_cvt_u32_f32_e32 v3, v3
	v_and_b32_e32 v12, s12, v14
	v_mul_lo_i32 v4, v12, s0
	v_mov_b32_e32 v2, 0x3727c5ac
	v_mac_f32_e32 v2, 0.5, v1
	s_movk_i32 s0, 0x16b
	v_add_i32_e32 v8, vcc, v4, v0
	v_add_i32_e32 v4, vcc, s9, v3
	v_cvt_u32_f32_e32 v5, v2
	v_cmp_gt_u32_e64 s[4:5], 64, v4
	v_mul_lo_i32 v4, v3, s0
	v_mul_lo_i32 v2, v3, 33
	v_and_b32_e32 v3, s12, v3
	v_mul_lo_i32 v3, v3, -3
	v_add_i32_e32 v10, vcc, s10, v14
	s_movk_i32 s13, 0xea
	s_movk_i32 s10, 0x3a8
	v_add_i32_e32 v3, vcc, v3, v0
	v_mul_lo_i32 v7, v3, 11
	v_mul_lo_i32 v13, v14, s13
	v_mul_lo_i32 v1, v8, 12
	v_mul_lo_i32 v16, v14, s10
	v_add_i32_e32 v2, vcc, v2, v7
	v_add_i32_e32 v15, vcc, s16, v4
	v_mul_lo_i32 v11, v5, s0
	v_mul_lo_i32 v4, v5, 33
	v_lshlrev_b32_e32 v2, 2, v2
	v_add_i32_e32 v1, vcc, v13, v1
	v_cmp_gt_u32_e64 s[0:1], 13, v14
	v_add_i32_e32 v7, vcc, 0x36d8, v2
	v_mad_u32_u24 v2, v0, 48, v16
	v_mul_u32_u24_e32 v14, 0x390, v14
	v_lshlrev_b32_e32 v6, 2, v3
	v_subrev_i32_e32 v2, vcc, v14, v2
	v_cmp_gt_u32_e64 s[2:3], 18, v0
	v_add_i32_e32 v9, vcc, s9, v5
	v_lshlrev_b32_e32 v1, 2, v1
	v_mul_u32_u24_e32 v3, 10, v0
	v_mov_b32_e32 v42, 0
	v_mov_b32_e32 v16, s25
	v_mov_b32_e32 v14, 0
	s_movk_i32 s14, 0xff1a
	s_movk_i32 s15, 0xffe9
	s_movk_i32 s17, 0x380
	v_mov_b32_e32 v35, 0
	v_mov_b32_e32 v36, 0
	v_mov_b32_e32 v37, 0
	v_mov_b32_e32 v38, 0
	v_mov_b32_e32 v39, 0
	v_mov_b32_e32 v40, 0
	v_mov_b32_e32 v41, 0
	v_mov_b32_e32 v43, 0
	v_mov_b32_e32 v44, 0
	v_mov_b32_e32 v45, 0
	v_mov_b32_e32 v46, 0
	v_mov_b32_e32 v47, 0
	v_mov_b32_e32 v48, 0
	v_mov_b32_e32 v49, 0
	v_mov_b32_e32 v50, 0
	v_mov_b32_e32 v51, 0
	v_mov_b32_e32 v52, 0

BB0_3:                                  ; =>This Loop Header: Depth=1
                                        ;     Child Loop BB0_7 Depth 2
                                        ;     Child Loop BB0_15 Depth 2
                                        ;     Child Loop BB0_17 Depth 2
                                        ;     Child Loop BB0_19 Depth 2
	v_cmp_eq_u32_e32 vcc, 3, v14
	s_and_b64 vcc, exec, vcc
	v_mov_b32_e32 v53, -1
	s_waitcnt lgkmcnt(0)
	s_barrier
	s_cbranch_vccnz BB0_21
; BB#4:                                 ;   in Loop: Header=BB0_3 Depth=1
	s_and_saveexec_b64 s[10:11], s[2:3]
	s_xor_b64 s[26:27], exec, s[10:11]
	; mask branch BB0_6
	s_cbranch_execz BB0_6
BB0_5:                                  ; %.lr.ph.i271
                                        ;   in Loop: Header=BB0_3 Depth=1
	;v_reg-18
	v_add_i32_e32 v17, vcc, v14, v6
	v_mul_lo_i32 v18, v17, 11
	v_cmp_gt_u32_e32 vcc, 11, v17
	s_and_b64 s[10:11], exec, s[4:5]
	s_and_b64 s[10:11], s[10:11], vcc
	v_add_i32_e32 v17, vcc, v15, v18
	v_ashrrev_i32_e32 v18, 31, v17
	v_cndmask_b32_e64 v18, 0, v18, s[10:11]
	v_cndmask_b32_e64 v17, 0, v17, s[10:11]
	v_lshlrev_b64 v[17:18], 2, v[17:18]
	v_add_i32_e32 v25, vcc, s22, v17
	v_mov_b32_e32 v17, s23
	v_addc_u32_e32 v26, vcc, v17, v18, vcc
	flat_load_dwordx4 v[17:20], v[25:26]
	flat_load_dwordx4 v[21:24], v[25:26] offset:16
	flat_load_dword v27, v[25:26] offset:40
	flat_load_dwordx2 v[25:26], v[25:26] offset:32
	s_mov_b32 m0, -1
	s_waitcnt vmcnt(3) lgkmcnt(3)
	ds_write2_b32 v7, v17, v18 offset1:1
	ds_write2_b32 v7, v19, v20 offset0:2 offset1:3
	s_waitcnt vmcnt(2) lgkmcnt(4)
	ds_write2_b32 v7, v21, v22 offset0:4 offset1:5
	ds_write2_b32 v7, v23, v24 offset0:6 offset1:7
	s_waitcnt vmcnt(0) lgkmcnt(4)
	ds_write2_b32 v7, v25, v26 offset0:8 offset1:9
	ds_write_b32 v7, v27 offset:40
BB0_6:                                  ; %.lr.ph.i273.preheader
                                        ;   in Loop: Header=BB0_3 Depth=1
	s_or_b64 exec, exec, s[26:27]
	v_add_i32_e32 v17, vcc, v16, v3
	s_mov_b64 s[10:11], 0
	v_mov_b32_e32 v18, 0
	v_mov_b32_e32 v19, v0
BB0_7:                                  ; %.lr.ph.i273
                                        ;   Parent Loop BB0_3 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_cvt_f32_u32_e32 v20, v19
	v_mov_b32_e32 v21, 0x3d321643
	v_mov_b32_e32 v22, 0xdf
                                        ; implicit-def: %VGPR41
	v_madak_f32 v20, v20, v21, 0x3727c5ac
	v_cvt_u32_f32_e32 v20, v20
	v_lshlrev_b32_e32 v21, 2, v20
	v_add_i32_e32 v21, vcc, v21, v14
	v_add_i32_e32 v21, vcc, s6, v21
	v_cmp_gt_u32_e32 vcc, v21, v22
	v_and_b32_e32 v32, s12, v20
                                        ; implicit-def: %VGPR39
	s_and_saveexec_b64 s[26:27], vcc
	s_xor_b64 s[26:27], exec, s[26:27]
	; mask branch BB0_9
BB0_8:                                  ; %.lr.ph..loopexit_crit_edge.i275
                                        ;   in Loop: Header=BB0_7 Depth=2
	v_mul_lo_i32 v21, v32, s14
	v_add_i32_e32 v22, vcc, v18, v3
	v_mov_b32_e32 v23, 0
	v_add_i32_e32 v21, vcc, v21, v22
BB0_9:                                  ; %Flow4616
                                        ;   in Loop: Header=BB0_7 Depth=2
	s_or_saveexec_b64 s[26:27], s[26:27]
	v_mov_b32_e32 v22, v23
	v_mov_b32_e32 v31, v23
	v_mov_b32_e32 v30, v23
	v_mov_b32_e32 v29, v23
	v_mov_b32_e32 v28, v23
	v_mov_b32_e32 v27, v23
	v_mov_b32_e32 v26, v23
	v_mov_b32_e32 v25, v23
	v_mov_b32_e32 v24, v23
	s_xor_b64 exec, exec, s[26:27]
	; mask branch BB0_13
	s_cbranch_execz BB0_13
BB0_10:                                 ;   in Loop: Header=BB0_7 Depth=2
	v_mul_lo_i32 v22, v32, 23
	v_mul_lo_i32 v23, v20, s17
	v_mul_lo_i32 v21, v32, s15
	v_add_i32_e32 v25, vcc, v18, v17
	v_add_i32_e32 v24, vcc, 22, v22
	v_mul_lo_i32 v22, v32, s14
	v_add_i32_e32 v23, vcc, v23, v25
	v_add_i32_e32 v21, vcc, v21, v19
	v_mul_lo_i32 v21, v21, 10
	v_add_i32_e32 v22, vcc, v22, v23
	v_ashrrev_i32_e32 v23, 31, v22
	v_lshlrev_b64 v[22:23], 2, v[22:23]
	v_add_i32_e32 v32, vcc, s20, v22
	v_mov_b32_e32 v22, s21
	v_addc_u32_e32 v33, vcc, v22, v23, vcc
	v_cmp_ne_u32_e32 vcc, v24, v19
	flat_load_dwordx4 v[24:27], v[32:33]
	v_mov_b32_e32 v28, 0
	v_mov_b32_e32 v29, 0
	v_mov_b32_e32 v30, 0
	v_mov_b32_e32 v31, 0
	v_mov_b32_e32 v22, 0
	v_mov_b32_e32 v23, 0
	s_and_saveexec_b64 s[28:29], vcc
	s_xor_b64 s[28:29], exec, s[28:29]
	; mask branch BB0_12
BB0_11:                                 ; %.preheader1.preheader.i276
                                        ;   in Loop: Header=BB0_7 Depth=2
	flat_load_dwordx4 v[28:31], v[32:33] offset:16
	flat_load_dwordx2 v[22:23], v[32:33] offset:32
BB0_12:                                 ; %Flow4615
                                        ;   in Loop: Header=BB0_7 Depth=2
	s_or_b64 exec, exec, s[28:29]
BB0_13:                                 ; %.loopexit.i278
                                        ;   in Loop: Header=BB0_7 Depth=2
	s_or_b64 exec, exec, s[26:27]
	v_mul_lo_i32 v20, v20, s13
	s_mov_b32 m0, -1
	v_add_i32_e32 v19, vcc, 0x100, v19
	v_add_i32_e32 v20, vcc, v20, v21
	v_lshlrev_b32_e32 v20, 2, v20
	s_waitcnt vmcnt(2) lgkmcnt(0)
	ds_write2_b32 v20, v24, v25 offset0:2 offset1:3
	ds_write2_b32 v20, v26, v27 offset0:4 offset1:5
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write2_b32 v20, v28, v29 offset0:6 offset1:7
	ds_write2_b32 v20, v30, v31 offset0:8 offset1:9
	ds_write2_b32 v20, v22, v23 offset0:10 offset1:11
	v_mov_b32_e32 v20, 0x158
	v_cmp_gt_u32_e32 vcc, v19, v20
	s_or_b64 s[10:11], vcc, s[10:11]
	v_add_i32_e32 v18, vcc, 0xa00, v18
	s_andn2_b64 exec, exec, s[10:11]
	s_cbranch_execnz BB0_7
; BB#14:                                ; %fetchData.4.exit279
                                        ;   in Loop: Header=BB0_3 Depth=1
	s_or_b64 exec, exec, s[10:11]
	v_mov_b32_e32 v53, -12
	s_waitcnt lgkmcnt(0)
	s_barrier
BB0_15:                                 ; %.lr.ph13.i283.preheader
                                        ;   Parent Loop BB0_3 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_add_i32_e32 v23, vcc, v53, v1
	v_add_i32_e32 v54, vcc, 0x36e4, v53
	v_add_i32_e32 v56, vcc, 0x3704, v53
	v_add_i32_e32 v58, vcc, 0x37ec, v53
	v_add_i32_e32 v24, vcc, 0x380c, v53
	v_add_i32_e32 v26, vcc, 0x38f4, v53
	v_add_i32_e32 v28, vcc, 0x3778, v53
	v_add_i32_e32 v30, vcc, 0x3880, v53
	ds_read2_b32 v[54:55], v54 offset1:4
	ds_read2_b32 v[19:20], v23 offset0:3 offset1:7
	ds_read2_b32 v[56:57], v56 offset1:25
	ds_read2_b32 v[21:22], v23 offset0:11 offset1:15
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read2_b32 v[24:25], v24 offset1:25
	ds_read2_b32 v[26:27], v26 offset1:4
	ds_read2_b32 v[28:29], v28 offset1:4
	ds_read2_b32 v[30:31], v30 offset1:4
	ds_read_b32 v23, v23 offset:76
	v_add_i32_e32 v32, vcc, 0x3914, v53
	v_add_i32_e32 v33, vcc, 0x3988, v53

	v_add_i32_e32 v53, vcc, 4, v53
	v_cmp_eq_u32_e32 vcc, 0, v53
	s_and_b64 vcc, exec, vcc
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v42, v19, v54
	v_mac_f32_e32 v52, v20, v54
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v51, v21, v54
	v_mac_f32_e32 v50, v19, v57
	v_mac_f32_e32 v49, v20, v57
	v_mac_f32_e32 v48, v21, v57
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v47, v19, v58
	v_mac_f32_e32 v46, v20, v58
	v_mac_f32_e32 v45, v21, v58
	ds_read2_b32 v[57:58], v32 offset1:25
	v_mac_f32_e32 v42, v21, v56
	v_mac_f32_e32 v52, v22, v56
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v47, v21, v24
	v_mac_f32_e32 v46, v22, v24
	v_mac_f32_e32 v44, v19, v25
	v_mac_f32_e32 v43, v20, v25
	v_mac_f32_e32 v41, v21, v25
	v_mac_f32_e32 v42, v20, v55
	v_mac_f32_e32 v52, v21, v55
	v_mac_f32_e32 v51, v22, v55
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v40, v19, v26
	v_mac_f32_e32 v39, v20, v26
	v_mac_f32_e32 v38, v21, v26
	ds_read2_b32 v[25:26], v33 offset1:4
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v37, v19, v58
	v_mac_f32_e32 v35, v21, v58
	v_mac_f32_e32 v36, v20, v58
	;s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v50, v20, v28
	v_mac_f32_e32 v49, v21, v28
	v_mac_f32_e32 v48, v22, v28
	v_mac_f32_e32 v47, v20, v59
	v_mac_f32_e32 v46, v21, v59
	v_mac_f32_e32 v45, v22, v59
	;s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v20, v30
	v_mac_f32_e32 v43, v21, v30
	v_mac_f32_e32 v41, v22, v30
	v_mac_f32_e32 v40, v20, v27
	v_mac_f32_e32 v39, v21, v27
	v_mac_f32_e32 v38, v22, v27
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v20, v25
	v_mac_f32_e32 v35, v22, v25
	v_mac_f32_e32 v36, v21, v25
	;s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v51, v23, v56
	v_mac_f32_e32 v50, v21, v29
	v_mac_f32_e32 v49, v22, v29
	v_mac_f32_e32 v48, v23, v29
	v_mac_f32_e32 v45, v23, v24
	v_mac_f32_e32 v44, v21, v31
	v_mac_f32_e32 v43, v22, v31
	v_mac_f32_e32 v41, v23, v31
	v_mac_f32_e32 v40, v21, v57
	v_mac_f32_e32 v39, v22, v57
	v_mac_f32_e32 v38, v23, v57
	v_mac_f32_e32 v37, v21, v26
	v_mac_f32_e32 v35, v23, v26
	v_mac_f32_e32 v36, v22, v26
	s_cbranch_vccz BB0_15
; BB#16:                                ; %.lr.ph13.i383.preheader
                                        ;   in Loop: Header=BB0_3 Depth=1
	v_mov_b32_e32 v23, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v53, vcc, 0x36e4, v23
	v_add_i32_e32 v55, vcc, 0x3768, v23
	v_add_i32_e32 v57, vcc, 0x37ec, v23
	v_add_i32_e32 v19, vcc, 0x3870, v23
	v_add_i32_e32 v21, vcc, 0x38f4, v23
	v_add_i32_e32 v23, vcc, 0x3978, v23
	ds_read2_b32 v[53:54], v53 offset1:4
	ds_read2_b32 v[25:26], v1 offset0:3 offset1:7
	ds_read2_b32 v[55:56], v55 offset1:4
	ds_read2_b32 v[27:28], v1 offset0:11 offset1:15
	ds_read2_b32 v[57:58], v57 offset1:4
	ds_read2_b32 v[19:20], v19 offset1:4
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[23:24], v23 offset1:4
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v42, v25, v53
	v_mac_f32_e32 v52, v26, v53
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v51, v27, v53
	v_mac_f32_e32 v50, v25, v55
	v_mac_f32_e32 v49, v26, v55
	v_mac_f32_e32 v48, v27, v55
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v47, v25, v57
	v_mac_f32_e32 v46, v26, v57
	v_mac_f32_e32 v45, v27, v57
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v25, v19
	v_mac_f32_e32 v43, v26, v19
	v_mac_f32_e32 v41, v27, v19
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v25, v21
	v_mac_f32_e32 v39, v26, v21
	v_mac_f32_e32 v38, v27, v21
	;s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v25, v23
	v_mac_f32_e32 v35, v27, v23
	v_mac_f32_e32 v36, v26, v23
	v_mac_f32_e32 v42, v26, v54
	v_mac_f32_e32 v51, v28, v54
	v_mac_f32_e32 v50, v26, v56
	v_mac_f32_e32 v49, v27, v56
	v_mac_f32_e32 v48, v28, v56
	v_mac_f32_e32 v47, v26, v58
	v_mac_f32_e32 v46, v27, v58
	v_mac_f32_e32 v45, v28, v58
	v_mac_f32_e32 v44, v26, v20
	v_mac_f32_e32 v43, v27, v20
	v_mac_f32_e32 v41, v28, v20
	v_mac_f32_e32 v40, v26, v22
	v_mac_f32_e32 v39, v27, v22
	v_mac_f32_e32 v38, v28, v22
	v_mac_f32_e32 v37, v26, v24
	v_mac_f32_e32 v35, v28, v24
	v_mac_f32_e32 v36, v27, v24
	v_mac_f32_e32 v52, v27, v54
	v_mov_b32_e32 v53, -12
BB0_17:                                 ; %.lr.ph13.i283.preheader.1
                                        ;   Parent Loop BB0_3 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	s_mov_b32 m0, -1
	v_add_i32_e32 v23, vcc, v53, v2
	v_add_i32_e32 v54, vcc, 0x3710, v53
	v_add_i32_e32 v56, vcc, 0x3730, v53
	v_add_i32_e32 v58, vcc, 0x3818, v53
	v_add_i32_e32 v24, vcc, 0x3838, v53
	v_add_i32_e32 v26, vcc, 0x3920, v53
	v_add_i32_e32 v28, vcc, 0x37a4, v53
	v_add_i32_e32 v30, vcc, 0x38ac, v53
	ds_read2_b32 v[54:55], v54 offset1:4
	ds_read2_b32 v[19:20], v23 offset0:237 offset1:241
	ds_read2_b32 v[56:57], v56 offset1:25
	ds_read2_b32 v[21:22], v23 offset0:245 offset1:249
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read2_b32 v[24:25], v24 offset1:25
	ds_read2_b32 v[26:27], v26 offset1:4
	ds_read2_b32 v[28:29], v28 offset1:4
	ds_read2_b32 v[30:31], v30 offset1:4
	ds_read_b32 v23, v23 offset:1012
	v_add_i32_e32 v32, vcc, 0x3940, v53
	v_add_i32_e32 v33, vcc, 0x39b4, v53

	v_add_i32_e32 v53, vcc, 4, v53
	v_cmp_ne_u32_e32 vcc, 0, v53
	s_and_b64 vcc, exec, vcc
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v42, v19, v54
	v_mac_f32_e32 v52, v20, v54
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v51, v21, v54
	v_mac_f32_e32 v50, v19, v57
	v_mac_f32_e32 v49, v20, v57
	v_mac_f32_e32 v48, v21, v57
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v47, v19, v58
	v_mac_f32_e32 v46, v20, v58
	v_mac_f32_e32 v45, v21, v58
	ds_read2_b32 v[57:58], v32 offset1:25
	v_mac_f32_e32 v52, v22, v56
	v_mac_f32_e32 v42, v21, v56
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v47, v21, v24
	v_mac_f32_e32 v46, v22, v24
	v_mac_f32_e32 v44, v19, v25
	v_mac_f32_e32 v43, v20, v25
	v_mac_f32_e32 v41, v21, v25
	v_mac_f32_e32 v42, v20, v55
	v_mac_f32_e32 v52, v21, v55
	v_mac_f32_e32 v51, v22, v55
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v40, v19, v26
	v_mac_f32_e32 v39, v20, v26
	v_mac_f32_e32 v38, v21, v26
	ds_read2_b32 v[25:26], v33 offset1:4
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v37, v19, v58
	v_mac_f32_e32 v35, v21, v58
	v_mac_f32_e32 v36, v20, v58
	;s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v50, v20, v28
	v_mac_f32_e32 v49, v21, v28
	v_mac_f32_e32 v48, v22, v28
	v_mac_f32_e32 v47, v20, v59
	v_mac_f32_e32 v46, v21, v59
	v_mac_f32_e32 v45, v22, v59
	;s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v20, v30
	v_mac_f32_e32 v43, v21, v30
	v_mac_f32_e32 v41, v22, v30
	v_mac_f32_e32 v40, v20, v27
	v_mac_f32_e32 v39, v21, v27
	v_mac_f32_e32 v38, v22, v27
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v20, v25
	v_mac_f32_e32 v35, v22, v25
	v_mac_f32_e32 v36, v21, v25
	;s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v51, v23, v56
	v_mac_f32_e32 v50, v21, v29
	v_mac_f32_e32 v49, v22, v29
	v_mac_f32_e32 v48, v23, v29
	v_mac_f32_e32 v45, v23, v24
	v_mac_f32_e32 v44, v21, v31
	v_mac_f32_e32 v43, v22, v31
	v_mac_f32_e32 v41, v23, v31
	v_mac_f32_e32 v40, v21, v57
	v_mac_f32_e32 v39, v22, v57
	v_mac_f32_e32 v38, v23, v57
	v_mac_f32_e32 v37, v21, v26
	v_mac_f32_e32 v35, v23, v26
	v_mac_f32_e32 v36, v22, v26
	s_cbranch_vccnz BB0_17
; BB#18:                                ; %.lr.ph13.i383.preheader.1
                                        ;   in Loop: Header=BB0_3 Depth=1
	v_mov_b32_e32 v23, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v53, vcc, 0x3710, v23
	v_add_i32_e32 v55, vcc, 0x3794, v23
	v_add_i32_e32 v57, vcc, 0x3818, v23
	v_add_i32_e32 v19, vcc, 0x389c, v23
	v_add_i32_e32 v21, vcc, 0x3920, v23
	v_add_i32_e32 v23, vcc, 0x39a4, v23
	ds_read2_b32 v[53:54], v53 offset1:4
	ds_read2_b32 v[25:26], v1 offset0:237 offset1:241
	ds_read2_b32 v[55:56], v55 offset1:4
	ds_read2_b32 v[57:58], v57 offset1:4
	ds_read2_b32 v[27:28], v1 offset0:245 offset1:249
	ds_read2_b32 v[19:20], v19 offset1:4
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[23:24], v23 offset1:4
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v42, v25, v53
	v_mac_f32_e32 v52, v26, v53
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v51, v27, v53
	v_mac_f32_e32 v50, v25, v55
	v_mac_f32_e32 v49, v26, v55
	v_mac_f32_e32 v48, v27, v55
	v_mac_f32_e32 v47, v25, v57
	v_mac_f32_e32 v46, v26, v57
	v_mac_f32_e32 v45, v27, v57
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v25, v19
	v_mac_f32_e32 v43, v26, v19
	v_mac_f32_e32 v41, v27, v19
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v25, v21
	v_mac_f32_e32 v39, v26, v21
	v_mac_f32_e32 v38, v27, v21
	;s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v25, v23
	v_mac_f32_e32 v35, v27, v23
	v_mac_f32_e32 v36, v26, v23
	v_mac_f32_e32 v42, v26, v54
	v_mac_f32_e32 v51, v28, v54
	v_mac_f32_e32 v50, v26, v56
	v_mac_f32_e32 v49, v27, v56
	v_mac_f32_e32 v48, v28, v56
	v_mac_f32_e32 v47, v26, v58
	v_mac_f32_e32 v46, v27, v58
	v_mac_f32_e32 v45, v28, v58
	v_mac_f32_e32 v44, v26, v20
	v_mac_f32_e32 v43, v27, v20
	v_mac_f32_e32 v41, v28, v20
	v_mac_f32_e32 v40, v26, v22
	v_mac_f32_e32 v39, v27, v22
	v_mac_f32_e32 v38, v28, v22
	v_mac_f32_e32 v37, v26, v24
	v_mac_f32_e32 v35, v28, v24
	v_mac_f32_e32 v36, v27, v24
	v_mac_f32_e32 v52, v27, v54
	v_mov_b32_e32 v53, -12
BB0_19:                                 ; %.lr.ph13.i283.preheader.2
                                        ;   Parent Loop BB0_3 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_add_i32_e32 v23, vcc, v53, v2
	s_mov_b32 m0, -1
	v_add_i32_e32 v54, vcc, 0x373c, v53
	v_add_i32_e32 v56, vcc, 0x375c, v53
	v_add_i32_e32 v58, vcc, 0x37d0, v53
	v_add_i32_e32 v19, vcc, 0x75c, v23
	v_add_i32_e32 v21, vcc, 0x77c, v23
	v_add_i32_e32 v24, vcc, 0x3844, v53
	v_add_i32_e32 v26, vcc, 0x3864, v53
	v_add_i32_e32 v28, vcc, 0x38d8, v53
	v_add_i32_e32 v30, vcc, 0x394c, v53
	ds_read2_b32 v[54:55], v54 offset1:4
	ds_read2_b32 v[19:20], v19 offset1:4
	ds_read2_b32 v[56:57], v56 offset1:25
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[24:25], v24 offset1:4
	ds_read2_b32 v[26:27], v26 offset1:25
	ds_read2_b32 v[30:31], v30 offset1:4
	ds_read2_b32 v[28:29], v28 offset1:4
	ds_read_b32 v23, v23 offset:1948
	v_add_i32_e32 v32, vcc, 0x396c, v53
	v_add_i32_e32 v33, vcc, 0x39e0, v53

	v_add_i32_e32 v53, vcc, 4, v53
	v_cmp_ne_u32_e32 vcc, 0, v53
	s_and_b64 vcc, exec, vcc
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v42, v19, v54
	v_mac_f32_e32 v52, v20, v54
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v51, v21, v54
	v_mac_f32_e32 v50, v19, v57
	v_mac_f32_e32 v49, v20, v57
	v_mac_f32_e32 v48, v21, v57
	v_mac_f32_e32 v42, v20, v55
	v_mac_f32_e32 v52, v21, v55
	v_mac_f32_e32 v51, v22, v55
	ds_read2_b32 v[54:55], v32 offset1:25
	v_mac_f32_e32 v50, v20, v58
	v_mac_f32_e32 v49, v21, v58
	v_mac_f32_e32 v48, v22, v58
	ds_read2_b32 v[57:58], v33 offset1:4
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v47, v19, v24
	v_mac_f32_e32 v46, v20, v24
	v_mac_f32_e32 v45, v21, v24
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v44, v19, v27
	v_mac_f32_e32 v43, v20, v27
	v_mac_f32_e32 v41, v21, v27
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v40, v19, v30
	v_mac_f32_e32 v39, v20, v30
	v_mac_f32_e32 v38, v21, v30
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v37, v19, v55
	v_mac_f32_e32 v35, v21, v55
	v_mac_f32_e32 v36, v20, v55
	;s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v47, v20, v25
	v_mac_f32_e32 v46, v21, v25
	v_mac_f32_e32 v45, v22, v25
	;s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v44, v20, v28
	v_mac_f32_e32 v43, v21, v28
	v_mac_f32_e32 v41, v22, v28
	v_mac_f32_e32 v40, v20, v31
	v_mac_f32_e32 v39, v21, v31
	v_mac_f32_e32 v38, v22, v31
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v20, v57
	v_mac_f32_e32 v35, v22, v57
	v_mac_f32_e32 v36, v21, v57
	v_mac_f32_e32 v52, v22, v56
	v_mac_f32_e32 v51, v23, v56
	v_mac_f32_e32 v50, v21, v59
	v_mac_f32_e32 v49, v22, v59
	v_mac_f32_e32 v48, v23, v59
	v_mac_f32_e32 v47, v21, v26
	v_mac_f32_e32 v46, v22, v26
	v_mac_f32_e32 v45, v23, v26
	v_mac_f32_e32 v44, v21, v29
	v_mac_f32_e32 v43, v22, v29
	v_mac_f32_e32 v41, v23, v29
	v_mac_f32_e32 v40, v21, v54
	v_mac_f32_e32 v39, v22, v54
	v_mac_f32_e32 v38, v23, v54
	v_mac_f32_e32 v37, v21, v58
	v_mac_f32_e32 v35, v23, v58
	v_mac_f32_e32 v36, v22, v58
	v_mac_f32_e32 v42, v21, v56
	s_cbranch_vccnz BB0_19
; BB#20:                                ; %.lr.ph13.i383.preheader.2
                                        ;   in Loop: Header=BB0_3 Depth=1
	v_mov_b32_e32 v53, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v54, vcc, 0x373c, v53
	v_add_i32_e32 v56, vcc, 0x37c0, v53
	v_add_i32_e32 v58, vcc, 0x3844, v53
	v_add_i32_e32 v19, vcc, 0x38c8, v53
	v_add_i32_e32 v21, vcc, 0x394c, v53
	v_add_i32_e32 v23, vcc, 0x39d0, v53
	v_add_i32_e32 v25, vcc, 0x75c, v1
	v_add_i32_e32 v27, vcc, 0x77c, v1
	ds_read2_b32 v[54:55], v54 offset1:4
	ds_read2_b32 v[25:26], v25 offset1:4
	ds_read2_b32 v[56:57], v56 offset1:4
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read2_b32 v[27:28], v27 offset1:4
	ds_read2_b32 v[19:20], v19 offset1:4
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[23:24], v23 offset1:4
	v_add_i32_e32 v14, vcc, 1, v14
	v_add_i32_e32 v16, vcc, 0xe0, v16
	v_cmp_ne_u32_e32 vcc, 0, v53
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v42, v25, v54
	v_mac_f32_e32 v52, v26, v54
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v51, v27, v54
	v_mac_f32_e32 v50, v25, v56
	v_mac_f32_e32 v49, v26, v56
	v_mac_f32_e32 v48, v27, v56
	v_mac_f32_e32 v47, v25, v58
	v_mac_f32_e32 v46, v26, v58
	v_mac_f32_e32 v45, v27, v58
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v25, v19
	v_mac_f32_e32 v43, v26, v19
	v_mac_f32_e32 v41, v27, v19
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v25, v21
	v_mac_f32_e32 v39, v26, v21
	v_mac_f32_e32 v38, v27, v21
	;s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v25, v23
	v_mac_f32_e32 v35, v27, v23
	v_mac_f32_e32 v36, v26, v23
	v_mac_f32_e32 v42, v26, v55
	v_mac_f32_e32 v51, v28, v55
	v_mac_f32_e32 v50, v26, v57
	v_mac_f32_e32 v49, v27, v57
	v_mac_f32_e32 v48, v28, v57
	v_mac_f32_e32 v47, v26, v59
	v_mac_f32_e32 v46, v27, v59
	v_mac_f32_e32 v45, v28, v59
	v_mac_f32_e32 v44, v26, v20
	v_mac_f32_e32 v43, v27, v20
	v_mac_f32_e32 v41, v28, v20
	v_mac_f32_e32 v40, v26, v22
	v_mac_f32_e32 v39, v27, v22
	v_mac_f32_e32 v38, v28, v22
	v_mac_f32_e32 v37, v26, v24
	v_mac_f32_e32 v35, v28, v24
	v_mac_f32_e32 v36, v27, v24
	v_mac_f32_e32 v52, v27, v55
	s_cbranch_vccz BB0_3
	s_branch BB0_22
BB0_21:                                 ;   in Loop: Header=BB0_3 Depth=1
                                        ; implicit-def: %VGPR14
                                        ; implicit-def: %VGPR52
                                        ; implicit-def: %VGPR51
                                        ; implicit-def: %VGPR50
                                        ; implicit-def: %VGPR49
                                        ; implicit-def: %VGPR48
                                        ; implicit-def: %VGPR47
                                        ; implicit-def: %VGPR46
                                        ; implicit-def: %VGPR45
                                        ; implicit-def: %VGPR44
                                        ; implicit-def: %VGPR43
                                        ; implicit-def: %VGPR41
                                        ; implicit-def: %VGPR40
                                        ; implicit-def: %VGPR39
                                        ; implicit-def: %VGPR38
                                        ; implicit-def: %VGPR37
                                        ; implicit-def: %VGPR36
                                        ; implicit-def: %VGPR35
                                        ; implicit-def: %VGPR42
                                        ; implicit-def: %VGPR16
	v_cmp_ne_u32_e32 vcc, 0, v53
	s_cbranch_vccz BB0_3
BB0_22:
	v_cmp_gt_u32_e64 s[14:15], 12, v0
	v_cmp_gt_u32_e64 s[12:13], 64, v9
	v_add_i32_e32 v16, vcc, s16, v11
	s_and_saveexec_b64 s[10:11], s[14:15]
	s_xor_b64 s[16:17], exec, s[10:11]
	; mask branch BB0_24
	s_cbranch_execz BB0_24
BB0_23:                                 ; %.lr.ph.i377
	v_lshlrev_b32_e32 v9, 1, v5
	v_and_b32_e32 v9, 0x1fffffe, v9
	v_subrev_i32_e32 v9, vcc, v9, v0
	v_lshlrev_b32_e32 v11, 2, v9
	v_or_b32_e32 v11, 3, v11
	v_mul_lo_i32 v14, v11, 11
	v_cmp_gt_u32_e32 vcc, 11, v11
	s_and_b64 s[10:11], exec, s[12:13]
	s_and_b64 s[10:11], s[10:11], vcc
	v_add_i32_e32 v11, vcc, v16, v14
	v_ashrrev_i32_e32 v14, 31, v11
	v_cndmask_b32_e64 v17, 0, v11, s[10:11]
	v_cndmask_b32_e64 v18, 0, v14, s[10:11]
	v_lshlrev_b64 v[17:18], 2, v[17:18]
	v_add_i32_e32 v21, vcc, s22, v17
	v_mov_b32_e32 v11, s23
	v_addc_u32_e32 v22, vcc, v11, v18, vcc
	flat_load_dwordx4 v[17:20], v[21:22]
	flat_load_dword v11, v[21:22] offset:40
	flat_load_dwordx2 v[25:26], v[21:22] offset:32
	flat_load_dwordx4 v[21:24], v[21:22] offset:16
	v_mul_lo_i32 v9, v9, 11
	s_mov_b32 m0, -1
	v_add_i32_e32 v9, vcc, v4, v9
	v_lshlrev_b32_e32 v9, 2, v9
	v_add_i32_e32 v14, vcc, 0x36d8, v9
	v_add_i32_e32 v27, vcc, 0x36e0, v9
	v_add_i32_e32 v28, vcc, 0x36e8, v9
	v_add_i32_e32 v29, vcc, 0x36f0, v9
	v_add_i32_e32 v30, vcc, 0x36f8, v9
	s_waitcnt vmcnt(3) lgkmcnt(3)
	ds_write2_b32 v14, v17, v18 offset1:1
	ds_write2_b32 v27, v19, v20 offset1:1
	s_waitcnt vmcnt(0) lgkmcnt(2)
	ds_write2_b32 v28, v21, v22 offset1:1
	ds_write2_b32 v29, v23, v24 offset1:1
	ds_write2_b32 v30, v25, v26 offset1:1
	ds_write_b32 v9, v11 offset:14080
BB0_24:                                 ; %.lr.ph.i.preheader
	s_or_b64 exec, exec, s[16:17]
	v_mul_lo_i32 v8, v8, 3
	v_mul_lo_i32 v9, v10, 55
	v_cmp_gt_u32_e64 s[10:11], 55, v10
	s_add_i32 s24, s25, 0x1c0
	s_mov_b64 s[16:17], 0
	s_movk_i32 s26, 0xff1a
	s_movk_i32 s27, 0xffe9
	s_movk_i32 s28, 0xe0
	s_movk_i32 s29, 0xea
	v_mov_b32_e32 v10, v3
	v_mov_b32_e32 v11, v0
	s_mov_b32 m0, -1
BB0_25:                                 ; %.lr.ph.i
                                        ; =>This Inner Loop Header: Depth=1
	v_cvt_f32_u32_e32 v14, v11
	v_mov_b32_e32 v17, 0x3d321643
	v_mov_b32_e32 v18, 0xdf
                                        ; implicit-def: %VGPR37
	v_madak_f32 v14, v14, v17, 0x3727c5ac
	v_cvt_u32_f32_e32 v14, v14
	v_lshlrev_b32_e32 v17, 2, v14
	v_or_b32_e32 v29, 3, v17
	v_add_i32_e32 v17, vcc, s6, v29
	v_cmp_gt_u32_e32 vcc, v17, v18
	v_and_b32_e32 v28, 0xffffff, v14
                                        ; implicit-def: %VGPR35
	s_and_saveexec_b64 s[30:31], vcc
	s_xor_b64 s[30:31], exec, s[30:31]
	; mask branch BB0_27
BB0_26:                                 ; %.lr.ph..loopexit_crit_edge.i
                                        ;   in Loop: Header=BB0_25 Depth=1
	v_mul_lo_i32 v17, v28, s26
	v_mov_b32_e32 v19, 0
	v_add_i32_e32 v17, vcc, v17, v10
BB0_27:                                 ; %Flow4614
                                        ;   in Loop: Header=BB0_25 Depth=1
	s_or_saveexec_b64 s[30:31], s[30:31]
	v_mov_b32_e32 v18, v19
	v_mov_b32_e32 v27, v19
	v_mov_b32_e32 v26, v19
	v_mov_b32_e32 v25, v19
	v_mov_b32_e32 v24, v19
	v_mov_b32_e32 v23, v19
	v_mov_b32_e32 v22, v19
	v_mov_b32_e32 v21, v19
	v_mov_b32_e32 v20, v19
	s_xor_b64 exec, exec, s[30:31]
	; mask branch BB0_31
	s_cbranch_execz BB0_31
BB0_28:                                 ;   in Loop: Header=BB0_25 Depth=1
	v_mul_lo_i32 v18, v28, 23
	v_mul_lo_i32 v19, v28, s26
	v_add_i32_e32 v21, vcc, s24, v10
	v_mul_lo_i32 v17, v28, s27
	v_add_i32_e32 v20, vcc, 22, v18
	v_mul_lo_i32 v18, v29, s28
	v_add_i32_e32 v19, vcc, v19, v21
	v_add_i32_e32 v17, vcc, v17, v11
	v_mul_lo_i32 v17, v17, 10
	v_add_i32_e32 v18, vcc, v18, v19
	v_add_i32_e32 v18, vcc, 0xfffffe40, v18
	v_ashrrev_i32_e32 v19, 31, v18
	v_lshlrev_b64 v[18:19], 2, v[18:19]
	v_add_i32_e32 v28, vcc, s20, v18
	v_mov_b32_e32 v18, s21
	v_addc_u32_e32 v29, vcc, v18, v19, vcc
	v_cmp_ne_u32_e32 vcc, v20, v11
	flat_load_dwordx4 v[20:23], v[28:29]
	v_mov_b32_e32 v24, 0
	v_mov_b32_e32 v25, 0
	v_mov_b32_e32 v26, 0
	v_mov_b32_e32 v27, 0
	v_mov_b32_e32 v18, 0
	v_mov_b32_e32 v19, 0
	s_and_saveexec_b64 s[32:33], vcc
	s_xor_b64 s[32:33], exec, s[32:33]
	; mask branch BB0_30
BB0_29:                                 ; %.preheader1.preheader.i
                                        ;   in Loop: Header=BB0_25 Depth=1
	flat_load_dwordx4 v[24:27], v[28:29] offset:16
	flat_load_dwordx2 v[18:19], v[28:29] offset:32
BB0_30:                                 ; %Flow4613
                                        ;   in Loop: Header=BB0_25 Depth=1
	s_or_b64 exec, exec, s[32:33]
BB0_31:                                 ; %.loopexit.i
                                        ;   in Loop: Header=BB0_25 Depth=1
	s_or_b64 exec, exec, s[30:31]
	v_mul_lo_i32 v14, v14, s29
	v_add_i32_e32 v11, vcc, 0x100, v11
	v_add_i32_e32 v14, vcc, v14, v17
	v_lshlrev_b32_e32 v14, 2, v14
	s_waitcnt vmcnt(2) lgkmcnt(0)
	ds_write2_b32 v14, v20, v21 offset0:2 offset1:3
	ds_write2_b32 v14, v22, v23 offset0:4 offset1:5
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write2_b32 v14, v24, v25 offset0:6 offset1:7
	ds_write2_b32 v14, v26, v27 offset0:8 offset1:9
	ds_write2_b32 v14, v18, v19 offset0:10 offset1:11
	v_mov_b32_e32 v14, 0x141
	v_cmp_gt_u32_e32 vcc, v11, v14
	s_or_b64 s[16:17], vcc, s[16:17]
	v_add_i32_e32 v10, vcc, 0xa00, v10
	s_andn2_b64 exec, exec, s[16:17]
	s_cbranch_execnz BB0_25
; BB#32:                                ; %fetchData.4.exit
	s_or_b64 exec, exec, s[16:17]
	s_movk_i32 s16, 0xe4
	v_mul_lo_i32 v12, v12, s16
	v_lshlrev_b32_e32 v14, 2, v8
	v_add_i32_e32 v17, vcc, 4, v14
	v_add_i32_e32 v18, vcc, 8, v14
	v_mad_u32_u24 v14, v0, 12, v13
	v_sub_i32_e32 v12, vcc, v14, v12
	v_lshlrev_b32_e32 v14, 2, v12
	v_add_i32_e32 v11, vcc, 1, v8
	v_add_i32_e32 v10, vcc, 2, v8
	v_mov_b32_e32 v12, 0
	s_mov_b32 m0, -1
	s_waitcnt lgkmcnt(0)
	s_barrier
BB0_33:                                 ; %.lr.ph13.i178.preheader
                                        ; =>This Inner Loop Header: Depth=1
	v_add_i32_e32 v24, vcc, 0x36d8, v12
	v_add_i32_e32 v26, vcc, 0x36f8, v12
	v_add_i32_e32 v58, vcc, 0x37e0, v12
	v_add_i32_e32 v23, vcc, v12, v14
	ds_read2_b32 v[24:25], v24 offset1:4
	ds_read2_b32 v[19:20], v23 offset1:4
	ds_read2_b32 v[26:27], v26 offset1:25
	ds_read2_b32 v[21:22], v23 offset0:8 offset1:12
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read_b32 v23, v23 offset:64
	v_add_i32_e32 v28, vcc, 0x3800, v12
	v_add_i32_e32 v29, vcc, 0x38e8, v12
	v_add_i32_e32 v30, vcc, 0x376c, v12
	v_add_i32_e32 v31, vcc, 0x3874, v12
	v_add_i32_e32 v32, vcc, 0x3908, v12
	v_add_i32_e32 v33, vcc, 0x397c, v12
	v_add_i32_e32 v12, vcc, 4, v12
	v_cmp_eq_u32_e32 vcc, 12, v12
	s_and_b64 vcc, exec, vcc
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v42, v19, v24
	v_mac_f32_e32 v52, v20, v24
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v51, v21, v24
	v_mac_f32_e32 v42, v20, v25
	v_mac_f32_e32 v52, v21, v25
	v_mac_f32_e32 v51, v22, v25
	ds_read2_b32 v[24:25], v28 offset1:25
	v_mac_f32_e32 v50, v19, v27
	v_mac_f32_e32 v49, v20, v27
	v_mac_f32_e32 v48, v21, v27
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v42, v21, v26
	v_mac_f32_e32 v52, v22, v26
	v_mac_f32_e32 v51, v23, v26
	ds_read2_b32 v[26:27], v29 offset1:4
	v_mac_f32_e32 v47, v19, v58
	v_mac_f32_e32 v46, v20, v58
	v_mac_f32_e32 v45, v21, v58
	v_mac_f32_e32 v47, v20, v59
	v_mac_f32_e32 v46, v21, v59
	v_mac_f32_e32 v45, v22, v59
	ds_read2_b32 v[58:59], v30 offset1:4
	ds_read2_b32 v[28:29], v31 offset1:4
	ds_read2_b32 v[30:31], v32 offset1:25
	ds_read2_b32 v[32:33], v33 offset1:4
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v47, v21, v24
	v_mac_f32_e32 v46, v22, v24
	v_mac_f32_e32 v45, v23, v24
	v_mac_f32_e32 v44, v19, v25
	v_mac_f32_e32 v43, v20, v25
	v_mac_f32_e32 v41, v21, v25
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v40, v19, v26
	v_mac_f32_e32 v39, v20, v26
	v_mac_f32_e32 v38, v21, v26
	v_mac_f32_e32 v40, v20, v27
	v_mac_f32_e32 v39, v21, v27
	v_mac_f32_e32 v38, v22, v27
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v50, v20, v58
	v_mac_f32_e32 v49, v21, v58
	v_mac_f32_e32 v48, v22, v58
	v_mac_f32_e32 v50, v21, v59
	v_mac_f32_e32 v49, v22, v59
	v_mac_f32_e32 v48, v23, v59
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v20, v28
	v_mac_f32_e32 v43, v21, v28
	v_mac_f32_e32 v41, v22, v28
	v_mac_f32_e32 v44, v21, v29
	v_mac_f32_e32 v43, v22, v29
	v_mac_f32_e32 v41, v23, v29
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v21, v30
	v_mac_f32_e32 v39, v22, v30
	v_mac_f32_e32 v38, v23, v30
	v_mac_f32_e32 v37, v19, v31
	v_mac_f32_e32 v35, v21, v31
	v_mac_f32_e32 v36, v20, v31
	v_mac_f32_e32 v37, v20, v32
	v_mac_f32_e32 v35, v22, v32
	v_mac_f32_e32 v36, v21, v32
	v_mac_f32_e32 v37, v21, v33
	v_mac_f32_e32 v35, v23, v33
	v_mac_f32_e32 v36, v22, v33
	s_cbranch_vccz BB0_33
; BB#34:                                ; %.lr.ph13.i.preheader
	v_mov_b32_e32 v12, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v24, vcc, 0x36e4, v12
	v_add_i32_e32 v26, vcc, 0x3768, v12
	v_add_i32_e32 v58, vcc, 0x37ec, v12
	v_add_i32_e32 v19, vcc, 0x3870, v12
	v_add_i32_e32 v21, vcc, 0x38f4, v12
	v_add_i32_e32 v28, vcc, 0x3978, v12
	v_add_i32_e32 v12, vcc, v13, v17
	v_add_i32_e32 v13, vcc, v13, v18
	v_lshlrev_b32_e32 v12, 2, v12
	v_lshlrev_b32_e32 v13, 2, v13
	ds_read2_b32 v[28:29], v28 offset1:4
	ds_read_b32 v30, v12 offset:12
	ds_read2_b32 v[24:25], v24 offset1:4
	ds_read_b32 v31, v13 offset:12
	ds_read2_b32 v[26:27], v26 offset1:4
	ds_read2_b32 v[32:33], v1 offset0:3 offset1:15
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read2_b32 v[19:20], v19 offset1:4
	ds_read2_b32 v[21:22], v21 offset1:4
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v52, v30, v24
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v42, v32, v24
	v_mac_f32_e32 v51, v31, v24
	v_mac_f32_e32 v50, v32, v26
	v_mac_f32_e32 v49, v30, v26
	v_mac_f32_e32 v48, v31, v26
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v47, v32, v58
	v_mac_f32_e32 v46, v30, v58
	v_mac_f32_e32 v45, v31, v58
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v44, v32, v19
	v_mac_f32_e32 v43, v30, v19
	v_mac_f32_e32 v41, v31, v19
	;s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v32, v21
	v_mac_f32_e32 v37, v32, v28
	v_mac_f32_e32 v39, v30, v21
	v_mac_f32_e32 v38, v31, v21
	v_mac_f32_e32 v35, v31, v28
	v_mac_f32_e32 v36, v30, v28
	v_mac_f32_e32 v42, v30, v25
	v_mac_f32_e32 v51, v33, v25
	v_mac_f32_e32 v50, v30, v27
	v_mac_f32_e32 v49, v31, v27
	v_mac_f32_e32 v48, v33, v27
	v_mac_f32_e32 v47, v30, v59
	v_mac_f32_e32 v46, v31, v59
	v_mac_f32_e32 v45, v33, v59
	v_mac_f32_e32 v44, v30, v20
	v_mac_f32_e32 v43, v31, v20
	v_mac_f32_e32 v41, v33, v20
	v_mac_f32_e32 v40, v30, v22
	v_mac_f32_e32 v39, v31, v22
	v_mac_f32_e32 v38, v33, v22
	v_mac_f32_e32 v37, v30, v29
	v_mac_f32_e32 v35, v33, v29
	v_mac_f32_e32 v36, v31, v29
	v_mac_f32_e32 v52, v31, v25
	v_mov_b32_e32 v17, -12
	s_mov_b32 m0, -1
BB0_35:                                 ; %.lr.ph13.i178.preheader.1
                                        ; =>This Inner Loop Header: Depth=1
	v_add_i32_e32 v23, vcc, 0x3710, v17
	v_add_i32_e32 v25, vcc, 0x3730, v17
	v_add_i32_e32 v57, vcc, 0x3818, v17
	v_add_i32_e32 v33, vcc, v17, v2
	ds_read2_b32 v[23:24], v23 offset1:4
	ds_read2_b32 v[19:20], v33 offset0:237 offset1:241
	ds_read2_b32 v[25:26], v25 offset1:25
	ds_read2_b32 v[21:22], v33 offset0:245 offset1:249
	ds_read2_b32 v[57:58], v57 offset1:4
	ds_read_b32 v33, v33 offset:1012
	v_add_i32_e32 v27, vcc, 0x3838, v17
	v_add_i32_e32 v28, vcc, 0x3920, v17
	v_add_i32_e32 v29, vcc, 0x37a4, v17
	v_add_i32_e32 v30, vcc, 0x38ac, v17
	v_add_i32_e32 v31, vcc, 0x3940, v17
	v_add_i32_e32 v32, vcc, 0x39b4, v17

	v_add_i32_e32 v17, vcc, 4, v17
	v_cmp_ne_u32_e32 vcc, 0, v17
	s_and_b64 vcc, exec, vcc
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v42, v19, v23
	v_mac_f32_e32 v52, v20, v23
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v51, v21, v23
	v_mac_f32_e32 v42, v20, v24
	v_mac_f32_e32 v52, v21, v24
	v_mac_f32_e32 v51, v22, v24
	ds_read2_b32 v[23:24], v27 offset1:25
	v_mac_f32_e32 v42, v21, v25
	v_mac_f32_e32 v52, v22, v25
	v_mac_f32_e32 v50, v19, v26
	v_mac_f32_e32 v49, v20, v26
	v_mac_f32_e32 v48, v21, v26
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v51, v33, v25
	ds_read2_b32 v[25:26], v28 offset1:4
	ds_read2_b32 v[27:28], v29 offset1:4
	v_mac_f32_e32 v47, v19, v57
	v_mac_f32_e32 v46, v20, v57
	v_mac_f32_e32 v45, v21, v57
	v_mac_f32_e32 v47, v20, v58
	v_mac_f32_e32 v46, v21, v58
	v_mac_f32_e32 v45, v22, v58
	ds_read2_b32 v[57:58], v30 offset1:4
	ds_read2_b32 v[29:30], v31 offset1:25
	ds_read2_b32 v[31:32], v32 offset1:4
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v47, v21, v23
	v_mac_f32_e32 v46, v22, v23
	v_mac_f32_e32 v45, v33, v23
	v_mac_f32_e32 v44, v19, v24
	v_mac_f32_e32 v43, v20, v24
	v_mac_f32_e32 v41, v21, v24
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v40, v19, v25
	v_mac_f32_e32 v39, v20, v25
	v_mac_f32_e32 v38, v21, v25
	v_mac_f32_e32 v40, v20, v26
	v_mac_f32_e32 v39, v21, v26
	v_mac_f32_e32 v38, v22, v26
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v50, v20, v27
	v_mac_f32_e32 v49, v21, v27
	v_mac_f32_e32 v48, v22, v27
	v_mac_f32_e32 v50, v21, v28
	v_mac_f32_e32 v49, v22, v28
	v_mac_f32_e32 v48, v33, v28
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v20, v57
	v_mac_f32_e32 v43, v21, v57
	v_mac_f32_e32 v41, v22, v57
	v_mac_f32_e32 v44, v21, v58
	v_mac_f32_e32 v43, v22, v58
	v_mac_f32_e32 v41, v33, v58
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v21, v29
	v_mac_f32_e32 v39, v22, v29
	v_mac_f32_e32 v38, v33, v29
	v_mac_f32_e32 v37, v19, v30
	v_mac_f32_e32 v35, v21, v30
	v_mac_f32_e32 v36, v20, v30
	v_mac_f32_e32 v37, v20, v31
	v_mac_f32_e32 v35, v22, v31
	v_mac_f32_e32 v36, v21, v31
	v_mac_f32_e32 v37, v21, v32
	v_mac_f32_e32 v35, v33, v32
	v_mac_f32_e32 v36, v22, v32
	s_cbranch_vccnz BB0_35
; BB#36:                                ; %.lr.ph13.i.preheader.1
	v_mov_b32_e32 v53, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v32, vcc, 0x3710, v53
	v_add_i32_e32 v24, vcc, 0x3794, v53
	v_add_i32_e32 v26, vcc, 0x3818, v53
	v_add_i32_e32 v58, vcc, 0x389c, v53
	v_add_i32_e32 v19, vcc, 0x3920, v53
	v_add_i32_e32 v21, vcc, 0x39a4, v53
	ds_read2_b32 v[32:33], v32 offset1:4
	ds_read_b32 v30, v12 offset:948
	ds_read2_b32 v[24:25], v24 offset1:4
	ds_read_b32 v31, v13 offset:948
	ds_read2_b32 v[26:27], v26 offset1:4
	ds_read2_b32 v[28:29], v1 offset0:237 offset1:249
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read2_b32 v[19:20], v19 offset1:4
	ds_read2_b32 v[21:22], v21 offset1:4
	s_add_i32 s16, s25, 0xc400
	v_add_i32_e32 v54, vcc, 0x79, v15
	v_mov_b32_e32 v55, s16
	s_movk_i32 s26, 0xff1a
	s_movk_i32 s27, 0xffe9
	s_movk_i32 s28, 0x380
	s_movk_i32 s29, 0xea
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v52, v30, v32
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v42, v28, v32
	v_mac_f32_e32 v51, v31, v32
	v_mac_f32_e32 v50, v28, v24
	v_mac_f32_e32 v49, v30, v24
	v_mac_f32_e32 v48, v31, v24
	v_mac_f32_e32 v47, v28, v26
	v_mac_f32_e32 v46, v30, v26
	v_mac_f32_e32 v45, v31, v26
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v28, v58
	v_mac_f32_e32 v43, v30, v58
	v_mac_f32_e32 v41, v31, v58
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v28, v19
	v_mac_f32_e32 v39, v30, v19
	v_mac_f32_e32 v38, v31, v19
	;s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v28, v21
	v_mac_f32_e32 v35, v31, v21
	v_mac_f32_e32 v36, v30, v21
	v_mac_f32_e32 v42, v30, v33
	v_mac_f32_e32 v51, v29, v33
	v_mac_f32_e32 v50, v30, v25
	v_mac_f32_e32 v49, v31, v25
	v_mac_f32_e32 v48, v29, v25
	v_mac_f32_e32 v47, v30, v27
	v_mac_f32_e32 v46, v31, v27
	v_mac_f32_e32 v45, v29, v27
	v_mac_f32_e32 v44, v30, v59
	v_mac_f32_e32 v43, v31, v59
	v_mac_f32_e32 v41, v29, v59
	v_mac_f32_e32 v40, v30, v20
	v_mac_f32_e32 v39, v31, v20
	v_mac_f32_e32 v38, v29, v20
	v_mac_f32_e32 v37, v30, v22
	v_mac_f32_e32 v35, v29, v22
	v_mac_f32_e32 v36, v31, v22
	v_mac_f32_e32 v52, v31, v33

BB0_37:                                 ; =>This Loop Header: Depth=1
                                        ;     Child Loop BB0_41 Depth 2
                                        ;     Child Loop BB0_49 Depth 2
                                        ;     Child Loop BB0_51 Depth 2
                                        ;     Child Loop BB0_53 Depth 2
	v_cmp_ne_u32_e32 vcc, 3, v53
	s_and_b64 vcc, exec, vcc
	v_mov_b32_e32 v56, -1
	s_barrier
	s_cbranch_vccz BB0_55
; BB#38:                                ;   in Loop: Header=BB0_37 Depth=1
	s_and_saveexec_b64 s[16:17], s[2:3]
	s_xor_b64 s[30:31], exec, s[16:17]
	; mask branch BB0_40
	s_cbranch_execz BB0_40
BB0_39:                                 ; %.lr.ph.i271.1
                                        ;   in Loop: Header=BB0_37 Depth=1
	v_add_i32_e32 v17, vcc, v53, v6
	v_mul_lo_i32 v18, v17, 11
	v_cmp_gt_u32_e32 vcc, 11, v17
	s_and_b64 s[16:17], exec, s[4:5]
	s_and_b64 s[16:17], s[16:17], vcc
	v_add_i32_e32 v17, vcc, v54, v18
	v_ashrrev_i32_e32 v18, 31, v17
	v_cndmask_b32_e64 v18, 0, v18, s[16:17]
	v_cndmask_b32_e64 v17, 0, v17, s[16:17]
	v_lshlrev_b64 v[17:18], 2, v[17:18]
	v_add_i32_e32 v25, vcc, s22, v17
	v_mov_b32_e32 v17, s23
	v_addc_u32_e32 v26, vcc, v17, v18, vcc
	flat_load_dwordx4 v[17:20], v[25:26]
	flat_load_dwordx4 v[21:24], v[25:26] offset:16
	flat_load_dword v27, v[25:26] offset:40
	flat_load_dwordx2 v[25:26], v[25:26] offset:32
	s_mov_b32 m0, -1
	s_waitcnt vmcnt(3) lgkmcnt(3)
	ds_write2_b32 v7, v17, v18 offset1:1
	ds_write2_b32 v7, v19, v20 offset0:2 offset1:3
	s_waitcnt vmcnt(2) lgkmcnt(4)
	ds_write2_b32 v7, v21, v22 offset0:4 offset1:5
	ds_write2_b32 v7, v23, v24 offset0:6 offset1:7
	s_waitcnt vmcnt(0) lgkmcnt(4)
	ds_write2_b32 v7, v25, v26 offset0:8 offset1:9
	ds_write_b32 v7, v27 offset:40
BB0_40:                                 ; %.lr.ph.i273.1.preheader
                                        ;   in Loop: Header=BB0_37 Depth=1
	s_or_b64 exec, exec, s[30:31]
	v_add_i32_e32 v17, vcc, v55, v3
	s_mov_b64 s[16:17], 0
	v_mov_b32_e32 v18, 0
	v_mov_b32_e32 v19, v0
BB0_41:                                 ; %.lr.ph.i273.1
                                        ;   Parent Loop BB0_37 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_cvt_f32_u32_e32 v20, v19
	v_mov_b32_e32 v21, 0x3d321643
	v_mov_b32_e32 v22, 0xdf
                                        ; implicit-def: %VGPR23
	v_madak_f32 v20, v20, v21, 0x3727c5ac
	v_cvt_u32_f32_e32 v20, v20
	v_lshlrev_b32_e32 v21, 2, v20
	v_add_i32_e32 v21, vcc, v21, v53
	v_add_i32_e32 v21, vcc, s6, v21
	v_cmp_gt_u32_e32 vcc, v21, v22
	v_and_b32_e32 v32, 0xffffff, v20
                                        ; implicit-def: %VGPR21
	s_and_saveexec_b64 s[30:31], vcc
	s_xor_b64 s[30:31], exec, s[30:31]
	; mask branch BB0_43
BB0_42:                                 ; %.lr.ph..loopexit_crit_edge.i275.1
                                        ;   in Loop: Header=BB0_41 Depth=2
	v_mul_lo_i32 v21, v32, s26
	v_add_i32_e32 v22, vcc, v18, v3
	v_mov_b32_e32 v23, 0
	v_add_i32_e32 v21, vcc, v21, v22
BB0_43:                                 ; %Flow4611
                                        ;   in Loop: Header=BB0_41 Depth=2
	s_or_saveexec_b64 s[30:31], s[30:31]
	v_mov_b32_e32 v22, v23
	v_mov_b32_e32 v31, v23
	v_mov_b32_e32 v30, v23
	v_mov_b32_e32 v29, v23
	v_mov_b32_e32 v28, v23
	v_mov_b32_e32 v27, v23
	v_mov_b32_e32 v26, v23
	v_mov_b32_e32 v25, v23
	v_mov_b32_e32 v24, v23
	s_xor_b64 exec, exec, s[30:31]
	; mask branch BB0_47
	s_cbranch_execz BB0_47
BB0_44:                                 ;   in Loop: Header=BB0_41 Depth=2
	v_mul_lo_i32 v22, v32, 23
	v_mul_lo_i32 v23, v20, s28
	v_mul_lo_i32 v21, v32, s27
	v_add_i32_e32 v25, vcc, v18, v17
	v_add_i32_e32 v24, vcc, 22, v22
	v_mul_lo_i32 v22, v32, s26
	v_add_i32_e32 v23, vcc, v23, v25
	v_add_i32_e32 v21, vcc, v21, v19
	v_mul_lo_i32 v21, v21, 10
	v_add_i32_e32 v22, vcc, v22, v23
	v_ashrrev_i32_e32 v23, 31, v22
	v_lshlrev_b64 v[22:23], 2, v[22:23]
	v_add_i32_e32 v32, vcc, s20, v22
	v_mov_b32_e32 v22, s21
	v_addc_u32_e32 v33, vcc, v22, v23, vcc
	v_cmp_ne_u32_e32 vcc, v24, v19
	flat_load_dwordx4 v[24:27], v[32:33]
	v_mov_b32_e32 v28, 0
	v_mov_b32_e32 v29, 0
	v_mov_b32_e32 v30, 0
	v_mov_b32_e32 v31, 0
	v_mov_b32_e32 v22, 0
	v_mov_b32_e32 v23, 0
	s_and_saveexec_b64 s[32:33], vcc
	s_xor_b64 s[32:33], exec, s[32:33]
	; mask branch BB0_46
BB0_45:                                 ; %.preheader1.preheader.i276.1
                                        ;   in Loop: Header=BB0_41 Depth=2
	flat_load_dwordx4 v[28:31], v[32:33] offset:16
	flat_load_dwordx2 v[22:23], v[32:33] offset:32
BB0_46:                                 ; %Flow4610
                                        ;   in Loop: Header=BB0_41 Depth=2
	s_or_b64 exec, exec, s[32:33]
BB0_47:                                 ; %.loopexit.i278.1
                                        ;   in Loop: Header=BB0_41 Depth=2
	s_or_b64 exec, exec, s[30:31]
	v_mul_lo_i32 v20, v20, s29
	s_mov_b32 m0, -1
	v_add_i32_e32 v19, vcc, 0x100, v19
	v_add_i32_e32 v20, vcc, v20, v21
	v_lshlrev_b32_e32 v20, 2, v20
	s_waitcnt vmcnt(2) lgkmcnt(0)
	ds_write2_b32 v20, v24, v25 offset0:2 offset1:3
	ds_write2_b32 v20, v26, v27 offset0:4 offset1:5
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write2_b32 v20, v28, v29 offset0:6 offset1:7
	ds_write2_b32 v20, v30, v31 offset0:8 offset1:9
	ds_write2_b32 v20, v22, v23 offset0:10 offset1:11
	v_mov_b32_e32 v20, 0x158
	v_cmp_gt_u32_e32 vcc, v19, v20
	s_or_b64 s[16:17], vcc, s[16:17]
	v_add_i32_e32 v18, vcc, 0xa00, v18
	s_andn2_b64 exec, exec, s[16:17]
	s_cbranch_execnz BB0_41
; BB#48:                                ; %fetchData.4.exit279.1
                                        ;   in Loop: Header=BB0_37 Depth=1
	s_or_b64 exec, exec, s[16:17]
	v_mov_b32_e32 v56, 0
	s_waitcnt lgkmcnt(0)
	s_barrier
BB0_49:                                 ; %.lr.ph13.i283.preheader.11022
                                        ;   Parent Loop BB0_37 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	s_mov_b32 m0, -1
	v_add_i32_e32 v57, vcc, 0x36d8, v56
	v_add_i32_e32 v23, vcc, v56, v14
	v_add_i32_e32 v26, vcc, 0x37e0, v56
	v_add_i32_e32 v24, vcc, 0x36f8, v56
	ds_read2_b32 v[57:58], v57 offset1:4
	ds_read2_b32 v[19:20], v23 offset1:4
	ds_read2_b32 v[26:27], v26 offset1:4
	ds_read2_b32 v[21:22], v23 offset0:8 offset1:12
	ds_read2_b32 v[24:25], v24 offset1:25
	ds_read_b32 v23, v23 offset:64

	v_add_i32_e32 v28, vcc, 0x3800, v56
	v_add_i32_e32 v29, vcc, 0x38e8, v56
	v_add_i32_e32 v30, vcc, 0x376c, v56
	v_add_i32_e32 v31, vcc, 0x3874, v56
	v_add_i32_e32 v32, vcc, 0x3908, v56
	v_add_i32_e32 v33, vcc, 0x397c, v56
	v_add_i32_e32 v56, vcc, 4, v56
	v_cmp_ne_u32_e32 vcc, 12, v56
	s_and_b64 vcc, exec, vcc
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v42, v19, v57
	v_mac_f32_e32 v52, v20, v57
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v51, v21, v57
	v_mac_f32_e32 v42, v20, v58
	v_mac_f32_e32 v52, v21, v58
	v_mac_f32_e32 v51, v22, v58
	v_mac_f32_e32 v47, v19, v26
	v_mac_f32_e32 v46, v20, v26
	v_mac_f32_e32 v45, v21, v26
	v_mac_f32_e32 v47, v20, v27
	v_mac_f32_e32 v46, v21, v27
	v_mac_f32_e32 v45, v22, v27
	ds_read2_b32 v[57:58], v28 offset1:25
	ds_read2_b32 v[26:27], v29 offset1:4
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v50, v19, v25
	v_mac_f32_e32 v49, v20, v25
	v_mac_f32_e32 v48, v21, v25
	v_mac_f32_e32 v42, v21, v24
	v_mac_f32_e32 v52, v22, v24
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v51, v23, v24
	ds_read2_b32 v[24:25], v30 offset1:4
	ds_read2_b32 v[28:29], v31 offset1:4
	ds_read2_b32 v[30:31], v32 offset1:25
	ds_read2_b32 v[32:33], v33 offset1:4
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v47, v21, v57
	v_mac_f32_e32 v46, v22, v57
	v_mac_f32_e32 v45, v23, v57
	v_mac_f32_e32 v44, v19, v58
	v_mac_f32_e32 v43, v20, v58
	v_mac_f32_e32 v41, v21, v58
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v40, v19, v26
	v_mac_f32_e32 v39, v20, v26
	v_mac_f32_e32 v38, v21, v26
	v_mac_f32_e32 v40, v20, v27
	v_mac_f32_e32 v39, v21, v27
	v_mac_f32_e32 v38, v22, v27
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v50, v20, v24
	v_mac_f32_e32 v49, v21, v24
	v_mac_f32_e32 v48, v22, v24
	v_mac_f32_e32 v50, v21, v25
	v_mac_f32_e32 v49, v22, v25
	v_mac_f32_e32 v48, v23, v25
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v21, v33
	v_mac_f32_e32 v35, v23, v33
	v_mac_f32_e32 v36, v22, v33
	v_mac_f32_e32 v44, v20, v28
	v_mac_f32_e32 v43, v21, v28
	v_mac_f32_e32 v41, v22, v28
	v_mac_f32_e32 v40, v21, v30
	v_mac_f32_e32 v39, v22, v30
	v_mac_f32_e32 v38, v23, v30
	v_mac_f32_e32 v37, v19, v31
	v_mac_f32_e32 v35, v21, v31
	v_mac_f32_e32 v36, v20, v31
	v_mac_f32_e32 v44, v21, v29
	v_mac_f32_e32 v43, v22, v29
	v_mac_f32_e32 v41, v23, v29
	v_mac_f32_e32 v37, v20, v32
	v_mac_f32_e32 v35, v22, v32
	v_mac_f32_e32 v36, v21, v32
	s_cbranch_vccnz BB0_49
; BB#50:                                ; %.lr.ph13.i383.preheader.11076
                                        ;   in Loop: Header=BB0_37 Depth=1
	v_mov_b32_e32 v25, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v56, vcc, 0x36e4, v25
	v_add_i32_e32 v58, vcc, 0x3768, v25
	v_add_i32_e32 v19, vcc, 0x37ec, v25
	v_add_i32_e32 v21, vcc, 0x3870, v25
	v_add_i32_e32 v23, vcc, 0x38f4, v25
	v_add_i32_e32 v25, vcc, 0x3978, v25
	ds_read2_b32 v[56:57], v56 offset1:4
	ds_read_b32 v29, v12 offset:12
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read_b32 v30, v13 offset:12
	ds_read2_b32 v[19:20], v19 offset1:4
	ds_read2_b32 v[27:28], v1 offset0:3 offset1:15
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[23:24], v23 offset1:4
	ds_read2_b32 v[25:26], v25 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v52, v29, v56
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v42, v27, v56
	v_mac_f32_e32 v51, v30, v56
	v_mac_f32_e32 v50, v27, v58
	v_mac_f32_e32 v49, v29, v58
	v_mac_f32_e32 v48, v30, v58
	v_mac_f32_e32 v47, v27, v19
	v_mac_f32_e32 v46, v29, v19
	v_mac_f32_e32 v45, v30, v19
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v27, v21
	v_mac_f32_e32 v43, v29, v21
	v_mac_f32_e32 v41, v30, v21
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v27, v23
	v_mac_f32_e32 v39, v29, v23
	v_mac_f32_e32 v38, v30, v23
	;s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v27, v25
	v_mac_f32_e32 v35, v30, v25
	v_mac_f32_e32 v36, v29, v25
	v_mac_f32_e32 v42, v29, v57
	v_mac_f32_e32 v51, v28, v57
	v_mac_f32_e32 v50, v29, v59
	v_mac_f32_e32 v49, v30, v59
	v_mac_f32_e32 v48, v28, v59
	v_mac_f32_e32 v47, v29, v20
	v_mac_f32_e32 v46, v30, v20
	v_mac_f32_e32 v45, v28, v20
	v_mac_f32_e32 v44, v29, v22
	v_mac_f32_e32 v43, v30, v22
	v_mac_f32_e32 v41, v28, v22
	v_mac_f32_e32 v40, v29, v24
	v_mac_f32_e32 v39, v30, v24
	v_mac_f32_e32 v38, v28, v24
	v_mac_f32_e32 v37, v29, v26
	v_mac_f32_e32 v35, v28, v26
	v_mac_f32_e32 v36, v30, v26
	v_mac_f32_e32 v52, v30, v57
	v_mov_b32_e32 v56, -12
BB0_51:                                 ; %.lr.ph13.i283.preheader.1.1
                                        ;   Parent Loop BB0_37 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	s_mov_b32 m0, -1
	v_add_i32_e32 v57, vcc, 0x3710, v56
	v_add_i32_e32 v23, vcc, v56, v2
	v_add_i32_e32 v26, vcc, 0x3818, v56
	v_add_i32_e32 v24, vcc, 0x3730, v56
	ds_read2_b32 v[57:58], v57 offset1:4
	ds_read2_b32 v[19:20], v23 offset0:237 offset1:241
	ds_read2_b32 v[21:22], v23 offset0:245 offset1:249
	ds_read2_b32 v[26:27], v26 offset1:4
	ds_read2_b32 v[24:25], v24 offset1:25
	ds_read_b32 v23, v23 offset:1012
	v_add_i32_e32 v28, vcc, 0x3838, v56
	v_add_i32_e32 v29, vcc, 0x3920, v56
	v_add_i32_e32 v30, vcc, 0x37a4, v56
	v_add_i32_e32 v31, vcc, 0x38ac, v56
	v_add_i32_e32 v32, vcc, 0x3940, v56
	v_add_i32_e32 v33, vcc, 0x39b4, v56

	v_add_i32_e32 v56, vcc, 4, v56
	v_cmp_ne_u32_e32 vcc, 0, v56
	s_and_b64 vcc, exec, vcc
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v42, v19, v57
	v_mac_f32_e32 v52, v20, v57
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v51, v21, v57
	v_mac_f32_e32 v42, v20, v58
	v_mac_f32_e32 v52, v21, v58
	v_mac_f32_e32 v51, v22, v58
	v_mac_f32_e32 v47, v19, v26
	v_mac_f32_e32 v46, v20, v26
	v_mac_f32_e32 v45, v21, v26
	v_mac_f32_e32 v47, v20, v27
	v_mac_f32_e32 v46, v21, v27
	v_mac_f32_e32 v45, v22, v27
	ds_read2_b32 v[57:58], v28 offset1:25
	ds_read2_b32 v[26:27], v29 offset1:4
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v50, v19, v25
	v_mac_f32_e32 v49, v20, v25
	v_mac_f32_e32 v48, v21, v25
	v_mac_f32_e32 v42, v21, v24
	v_mac_f32_e32 v52, v22, v24
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v51, v23, v24
	ds_read2_b32 v[24:25], v30 offset1:4
	ds_read2_b32 v[28:29], v31 offset1:4
	ds_read2_b32 v[30:31], v32 offset1:25
	ds_read2_b32 v[32:33], v33 offset1:4
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v47, v21, v57
	v_mac_f32_e32 v46, v22, v57
	v_mac_f32_e32 v45, v23, v57
	v_mac_f32_e32 v44, v19, v58
	v_mac_f32_e32 v43, v20, v58
	v_mac_f32_e32 v41, v21, v58
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v40, v19, v26
	v_mac_f32_e32 v39, v20, v26
	v_mac_f32_e32 v38, v21, v26
	v_mac_f32_e32 v40, v20, v27
	v_mac_f32_e32 v39, v21, v27
	v_mac_f32_e32 v38, v22, v27
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v50, v20, v24
	v_mac_f32_e32 v49, v21, v24
	v_mac_f32_e32 v48, v22, v24
	v_mac_f32_e32 v50, v21, v25
	v_mac_f32_e32 v49, v22, v25
	v_mac_f32_e32 v48, v23, v25
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v21, v33
	v_mac_f32_e32 v35, v23, v33
	v_mac_f32_e32 v36, v22, v33
	v_mac_f32_e32 v44, v20, v28
	v_mac_f32_e32 v43, v21, v28
	v_mac_f32_e32 v41, v22, v28
	v_mac_f32_e32 v40, v21, v30
	v_mac_f32_e32 v39, v22, v30
	v_mac_f32_e32 v38, v23, v30
	v_mac_f32_e32 v37, v19, v31
	v_mac_f32_e32 v35, v21, v31
	v_mac_f32_e32 v36, v20, v31
	v_mac_f32_e32 v44, v21, v29
	v_mac_f32_e32 v43, v22, v29
	v_mac_f32_e32 v41, v23, v29
	v_mac_f32_e32 v37, v20, v32
	v_mac_f32_e32 v35, v22, v32
	v_mac_f32_e32 v36, v21, v32
	s_cbranch_vccnz BB0_51
; BB#52:                                ; %.lr.ph13.i383.preheader.1.1
                                        ;   in Loop: Header=BB0_37 Depth=1
	v_mov_b32_e32 v25, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v56, vcc, 0x3710, v25
	v_add_i32_e32 v58, vcc, 0x3794, v25
	v_add_i32_e32 v19, vcc, 0x3818, v25
	v_add_i32_e32 v21, vcc, 0x389c, v25
	v_add_i32_e32 v23, vcc, 0x3920, v25
	v_add_i32_e32 v25, vcc, 0x39a4, v25
	ds_read2_b32 v[56:57], v56 offset1:4
	ds_read_b32 v29, v12 offset:948
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read_b32 v30, v13 offset:948
	ds_read2_b32 v[19:20], v19 offset1:4
	ds_read2_b32 v[27:28], v1 offset0:237 offset1:249
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[23:24], v23 offset1:4
	ds_read2_b32 v[25:26], v25 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v52, v29, v56
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v42, v27, v56
	v_mac_f32_e32 v51, v30, v56
	v_mac_f32_e32 v50, v27, v58
	v_mac_f32_e32 v49, v29, v58
	v_mac_f32_e32 v48, v30, v58
	v_mac_f32_e32 v47, v27, v19
	v_mac_f32_e32 v46, v29, v19
	v_mac_f32_e32 v45, v30, v19
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v27, v21
	v_mac_f32_e32 v43, v29, v21
	v_mac_f32_e32 v41, v30, v21
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v27, v23
	v_mac_f32_e32 v39, v29, v23
	v_mac_f32_e32 v38, v30, v23
	;s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v27, v25
	v_mac_f32_e32 v35, v30, v25
	v_mac_f32_e32 v36, v29, v25
	v_mac_f32_e32 v42, v29, v57
	v_mac_f32_e32 v51, v28, v57
	v_mac_f32_e32 v50, v29, v59
	v_mac_f32_e32 v49, v30, v59
	v_mac_f32_e32 v48, v28, v59
	v_mac_f32_e32 v47, v29, v20
	v_mac_f32_e32 v46, v30, v20
	v_mac_f32_e32 v45, v28, v20
	v_mac_f32_e32 v44, v29, v22
	v_mac_f32_e32 v43, v30, v22
	v_mac_f32_e32 v41, v28, v22
	v_mac_f32_e32 v40, v29, v24
	v_mac_f32_e32 v39, v30, v24
	v_mac_f32_e32 v38, v28, v24
	v_mac_f32_e32 v37, v29, v26
	v_mac_f32_e32 v35, v28, v26
	v_mac_f32_e32 v36, v30, v26
	v_mac_f32_e32 v52, v30, v57
	v_mov_b32_e32 v56, -12
BB0_53:                                 ; %.lr.ph13.i283.preheader.2.1
                                        ;   Parent Loop BB0_37 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_add_i32_e32 v23, vcc, v56, v2
	s_mov_b32 m0, -1
	v_add_i32_e32 v57, vcc, 0x373c, v56
	v_add_i32_e32 v20, vcc, 0x75c, v23
	v_add_i32_e32 v26, vcc, 0x3844, v56
	v_add_i32_e32 v21, vcc, 0x77c, v23
	v_add_i32_e32 v24, vcc, 0x375c, v56
	ds_read2_b32 v[57:58], v57 offset1:4
	ds_read2_b32 v[19:20], v20 offset1:4
	ds_read2_b32 v[26:27], v26 offset1:4
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[24:25], v24 offset1:25
	ds_read_b32 v23, v23 offset:1948
	v_add_i32_e32 v28, vcc, 0x37d0, v56
	v_add_i32_e32 v29, vcc, 0x3864, v56
	v_add_i32_e32 v30, vcc, 0x38d8, v56
	v_add_i32_e32 v31, vcc, 0x394c, v56
	v_add_i32_e32 v32, vcc, 0x396c, v56
	v_add_i32_e32 v33, vcc, 0x39e0, v56

	v_add_i32_e32 v56, vcc, 4, v56
	v_cmp_ne_u32_e32 vcc, 0, v56
	s_and_b64 vcc, exec, vcc
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v42, v19, v57
	v_mac_f32_e32 v52, v20, v57
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v51, v21, v57
	v_mac_f32_e32 v42, v20, v58
	v_mac_f32_e32 v52, v21, v58
	v_mac_f32_e32 v51, v22, v58
	v_mac_f32_e32 v47, v19, v26
	v_mac_f32_e32 v46, v20, v26
	v_mac_f32_e32 v45, v21, v26
	v_mac_f32_e32 v47, v20, v27
	v_mac_f32_e32 v46, v21, v27
	v_mac_f32_e32 v45, v22, v27
	ds_read2_b32 v[57:58], v28 offset1:4
	ds_read2_b32 v[26:27], v29 offset1:25
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v50, v19, v25
	v_mac_f32_e32 v49, v20, v25
	v_mac_f32_e32 v48, v21, v25
	v_mac_f32_e32 v42, v21, v24
	v_mac_f32_e32 v52, v22, v24
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v51, v23, v24
	ds_read2_b32 v[24:25], v30 offset1:4
	ds_read2_b32 v[28:29], v31 offset1:4
	ds_read2_b32 v[30:31], v32 offset1:25
	ds_read2_b32 v[32:33], v33 offset1:4
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v50, v20, v57
	v_mac_f32_e32 v49, v21, v57
	v_mac_f32_e32 v48, v22, v57
	v_mac_f32_e32 v50, v21, v58
	v_mac_f32_e32 v49, v22, v58
	v_mac_f32_e32 v48, v23, v58
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v47, v21, v26
	v_mac_f32_e32 v46, v22, v26
	v_mac_f32_e32 v45, v23, v26
	v_mac_f32_e32 v44, v19, v27
	v_mac_f32_e32 v43, v20, v27
	v_mac_f32_e32 v41, v21, v27
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v44, v20, v24
	v_mac_f32_e32 v43, v21, v24
	v_mac_f32_e32 v41, v22, v24
	v_mac_f32_e32 v44, v21, v25
	v_mac_f32_e32 v43, v22, v25
	v_mac_f32_e32 v41, v23, v25
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v19, v28
	v_mac_f32_e32 v39, v20, v28
	v_mac_f32_e32 v38, v21, v28
	v_mac_f32_e32 v37, v19, v31
	v_mac_f32_e32 v35, v21, v31
	v_mac_f32_e32 v36, v20, v31
	v_mac_f32_e32 v40, v20, v29
	v_mac_f32_e32 v39, v21, v29
	v_mac_f32_e32 v38, v22, v29
	v_mac_f32_e32 v37, v20, v32
	v_mac_f32_e32 v35, v22, v32
	v_mac_f32_e32 v36, v21, v32
	v_mac_f32_e32 v40, v21, v30
	v_mac_f32_e32 v39, v22, v30
	v_mac_f32_e32 v38, v23, v30
	v_mac_f32_e32 v37, v21, v33
	v_mac_f32_e32 v35, v23, v33
	v_mac_f32_e32 v36, v22, v33
	s_cbranch_vccnz BB0_53
; BB#54:                                ; %.lr.ph13.i383.preheader.2.1
                                        ;   in Loop: Header=BB0_37 Depth=1
	v_mov_b32_e32 v56, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v57, vcc, 0x373c, v56
	v_add_i32_e32 v19, vcc, 0x37c0, v56
	v_add_i32_e32 v21, vcc, 0x3844, v56
	v_add_i32_e32 v23, vcc, 0x38c8, v56
	v_add_i32_e32 v25, vcc, 0x394c, v56
	v_add_i32_e32 v27, vcc, 0x39d0, v56
	v_add_i32_e32 v29, vcc, 0x75c, v1
	ds_read2_b32 v[57:58], v57 offset1:4
	ds_read_b32 v31, v12 offset:1884
	ds_read2_b32 v[19:20], v19 offset1:4
	ds_read_b32 v32, v13 offset:1884
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[29:30], v29 offset1:12
	ds_read2_b32 v[23:24], v23 offset1:4
	ds_read2_b32 v[25:26], v25 offset1:4
	ds_read2_b32 v[27:28], v27 offset1:4
	v_add_i32_e32 v53, vcc, 1, v53
	v_add_i32_e32 v55, vcc, 0xe0, v55
	v_cmp_ne_u32_e32 vcc, 0, v56
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v52, v31, v57
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v42, v29, v57
	v_mac_f32_e32 v51, v32, v57
	v_mac_f32_e32 v50, v29, v19
	v_mac_f32_e32 v49, v31, v19
	v_mac_f32_e32 v48, v32, v19
	v_mac_f32_e32 v47, v29, v21
	v_mac_f32_e32 v46, v31, v21
	v_mac_f32_e32 v45, v32, v21
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v29, v23
	v_mac_f32_e32 v43, v31, v23
	v_mac_f32_e32 v41, v32, v23
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v29, v25
	v_mac_f32_e32 v39, v31, v25
	v_mac_f32_e32 v38, v32, v25
	;s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v29, v27
	v_mac_f32_e32 v35, v32, v27
	v_mac_f32_e32 v36, v31, v27
	v_mac_f32_e32 v42, v31, v58
	v_mac_f32_e32 v51, v30, v58
	v_mac_f32_e32 v50, v31, v20
	v_mac_f32_e32 v49, v32, v20
	v_mac_f32_e32 v48, v30, v20
	v_mac_f32_e32 v47, v31, v22
	v_mac_f32_e32 v46, v32, v22
	v_mac_f32_e32 v45, v30, v22
	v_mac_f32_e32 v44, v31, v24
	v_mac_f32_e32 v43, v32, v24
	v_mac_f32_e32 v41, v30, v24
	v_mac_f32_e32 v40, v31, v26
	v_mac_f32_e32 v39, v32, v26
	v_mac_f32_e32 v38, v30, v26
	v_mac_f32_e32 v37, v31, v28
	v_mac_f32_e32 v35, v30, v28
	v_mac_f32_e32 v36, v32, v28
	v_mac_f32_e32 v52, v32, v58
	s_cbranch_vccz BB0_37
	s_branch BB0_56
BB0_55:                                 ;   in Loop: Header=BB0_37 Depth=1
                                        ; implicit-def: %VGPR53
                                        ; implicit-def: %VGPR33
                                        ; implicit-def: %VGPR32
                                        ; implicit-def: %VGPR31
                                        ; implicit-def: %VGPR30
                                        ; implicit-def: %VGPR29
                                        ; implicit-def: %VGPR28
                                        ; implicit-def: %VGPR27
                                        ; implicit-def: %VGPR26
                                        ; implicit-def: %VGPR25
                                        ; implicit-def: %VGPR24
                                        ; implicit-def: %VGPR23
                                        ; implicit-def: %VGPR22
                                        ; implicit-def: %VGPR21
                                        ; implicit-def: %VGPR20
                                        ; implicit-def: %VGPR19
                                        ; implicit-def: %VGPR18
                                        ; implicit-def: %VGPR17
                                        ; implicit-def: %VGPR34
                                        ; implicit-def: %VGPR55
	v_cmp_ne_u32_e32 vcc, 0, v56
	s_cbranch_vccz BB0_37
BB0_56:
	s_and_saveexec_b64 s[16:17], s[14:15]
	s_xor_b64 s[26:27], exec, s[16:17]
	; mask branch BB0_58
	s_cbranch_execz BB0_58
BB0_57:                                 ; %.lr.ph.i377.1
	v_lshlrev_b32_e32 v17, 1, v5
	v_and_b32_e32 v17, 0x1fffffe, v17
	v_subrev_i32_e32 v19, vcc, v17, v0
	v_lshlrev_b32_e32 v17, 2, v19
	v_or_b32_e32 v17, 3, v17
	v_mul_lo_i32 v18, v17, 11
	v_cmp_gt_u32_e32 vcc, 11, v17
	s_and_b64 s[16:17], exec, s[12:13]
	s_and_b64 s[16:17], s[16:17], vcc
	v_add_i32_e32 v17, vcc, v16, v18
	v_add_i32_e32 v17, vcc, 0x79, v17
	v_ashrrev_i32_e32 v18, 31, v17
	v_cndmask_b32_e64 v18, 0, v18, s[16:17]
	v_cndmask_b32_e64 v17, 0, v17, s[16:17]
	v_lshlrev_b64 v[17:18], 2, v[17:18]
	v_add_i32_e32 v21, vcc, s22, v17
	v_mov_b32_e32 v17, s23
	v_addc_u32_e32 v22, vcc, v17, v18, vcc
	v_mul_lo_i32 v27, v19, 11
	flat_load_dwordx4 v[17:20], v[21:22]
	flat_load_dword v28, v[21:22] offset:40
	flat_load_dwordx2 v[25:26], v[21:22] offset:32
	flat_load_dwordx4 v[21:24], v[21:22] offset:16
	v_add_i32_e32 v27, vcc, v4, v27
	v_lshlrev_b32_e32 v27, 2, v27
	s_mov_b32 m0, -1
	v_add_i32_e32 v29, vcc, 0x36d8, v27
	v_add_i32_e32 v30, vcc, 0x36e0, v27
	v_add_i32_e32 v31, vcc, 0x36e8, v27
	v_add_i32_e32 v32, vcc, 0x36f0, v27
	v_add_i32_e32 v33, vcc, 0x36f8, v27
	s_waitcnt vmcnt(3) lgkmcnt(3)
	ds_write2_b32 v29, v17, v18 offset1:1
	ds_write2_b32 v30, v19, v20 offset1:1
	s_waitcnt vmcnt(0) lgkmcnt(2)
	ds_write2_b32 v31, v21, v22 offset1:1
	ds_write2_b32 v32, v23, v24 offset1:1
	ds_write2_b32 v33, v25, v26 offset1:1
	ds_write_b32 v27, v28 offset:14080
BB0_58:                                 ; %.lr.ph.i.1.preheader
	s_or_b64 exec, exec, s[26:27]
	s_mov_b64 s[16:17], 0
	s_movk_i32 s26, 0xff1a
	s_movk_i32 s27, 0xffe9
	s_movk_i32 s28, 0xe0
	s_movk_i32 s29, 0xea
	v_mov_b32_e32 v17, v3
	v_mov_b32_e32 v18, v0
	s_mov_b32 m0, -1
BB0_59:                                 ; %.lr.ph.i.1
                                        ; =>This Inner Loop Header: Depth=1
	v_cvt_f32_u32_e32 v19, v18
	v_mov_b32_e32 v20, 0x3d321643
	v_mov_b32_e32 v21, 0xdf
                                        ; implicit-def: %VGPR22
	v_madak_f32 v19, v19, v20, 0x3727c5ac
	v_cvt_u32_f32_e32 v19, v19
	v_lshlrev_b32_e32 v20, 2, v19
	v_or_b32_e32 v32, 3, v20
	v_add_i32_e32 v20, vcc, s6, v32
	v_cmp_gt_u32_e32 vcc, v20, v21
	v_and_b32_e32 v31, 0xffffff, v19
                                        ; implicit-def: %VGPR20
	s_and_saveexec_b64 s[30:31], vcc
	s_xor_b64 s[30:31], exec, s[30:31]
	; mask branch BB0_61
BB0_60:                                 ; %.lr.ph..loopexit_crit_edge.i.1
                                        ;   in Loop: Header=BB0_59 Depth=1
	v_mul_lo_i32 v20, v31, s26
	v_mov_b32_e32 v22, 0
	v_add_i32_e32 v20, vcc, v20, v17
BB0_61:                                 ; %Flow4609
                                        ;   in Loop: Header=BB0_59 Depth=1
	s_or_saveexec_b64 s[30:31], s[30:31]
	v_mov_b32_e32 v21, v22
	v_mov_b32_e32 v30, v22
	v_mov_b32_e32 v29, v22
	v_mov_b32_e32 v28, v22
	v_mov_b32_e32 v27, v22
	v_mov_b32_e32 v26, v22
	v_mov_b32_e32 v25, v22
	v_mov_b32_e32 v24, v22
	v_mov_b32_e32 v23, v22
	s_xor_b64 exec, exec, s[30:31]
	; mask branch BB0_65
	s_cbranch_execz BB0_65
BB0_62:                                 ;   in Loop: Header=BB0_59 Depth=1
	v_mul_lo_i32 v21, v31, 23
	v_mul_lo_i32 v22, v31, s26
	v_add_i32_e32 v24, vcc, s24, v17
	v_mul_lo_i32 v20, v31, s27
	v_add_i32_e32 v23, vcc, 22, v21
	v_mul_lo_i32 v21, v32, s28
	v_add_i32_e32 v22, vcc, v22, v24
	v_add_i32_e32 v20, vcc, v20, v18
	v_mul_lo_i32 v20, v20, 10
	v_add_i32_e32 v21, vcc, v21, v22
	v_add_i32_e32 v21, vcc, 0xc240, v21
	v_ashrrev_i32_e32 v22, 31, v21
	v_lshlrev_b64 v[21:22], 2, v[21:22]
	v_add_i32_e32 v31, vcc, s20, v21
	v_mov_b32_e32 v21, s21
	v_addc_u32_e32 v32, vcc, v21, v22, vcc
	v_cmp_ne_u32_e32 vcc, v23, v18
	flat_load_dwordx4 v[23:26], v[31:32]
	v_mov_b32_e32 v27, 0
	v_mov_b32_e32 v28, 0
	v_mov_b32_e32 v29, 0
	v_mov_b32_e32 v30, 0
	v_mov_b32_e32 v21, 0
	v_mov_b32_e32 v22, 0
	s_and_saveexec_b64 s[32:33], vcc
	s_xor_b64 s[32:33], exec, s[32:33]
	; mask branch BB0_64
BB0_63:                                 ; %.preheader1.preheader.i.1
                                        ;   in Loop: Header=BB0_59 Depth=1
	flat_load_dwordx4 v[27:30], v[31:32] offset:16
	flat_load_dwordx2 v[21:22], v[31:32] offset:32
BB0_64:                                 ; %Flow4608
                                        ;   in Loop: Header=BB0_59 Depth=1
	s_or_b64 exec, exec, s[32:33]
BB0_65:                                 ; %.loopexit.i.1
                                        ;   in Loop: Header=BB0_59 Depth=1
	s_or_b64 exec, exec, s[30:31]
	v_mul_lo_i32 v19, v19, s29
	v_add_i32_e32 v18, vcc, 0x100, v18
	v_add_i32_e32 v19, vcc, v19, v20
	v_lshlrev_b32_e32 v19, 2, v19
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write2_b32 v19, v23, v24 offset0:2 offset1:3
	ds_write2_b32 v19, v25, v26 offset0:4 offset1:5
	ds_write2_b32 v19, v27, v28 offset0:6 offset1:7
	ds_write2_b32 v19, v29, v30 offset0:8 offset1:9
	ds_write2_b32 v19, v21, v22 offset0:10 offset1:11
	v_mov_b32_e32 v19, 0x141
	v_cmp_gt_u32_e32 vcc, v18, v19
	s_or_b64 s[16:17], vcc, s[16:17]
	v_add_i32_e32 v17, vcc, 0xa00, v17
	s_andn2_b64 exec, exec, s[16:17]
	s_cbranch_execnz BB0_59
; BB#66:                                ; %fetchData.4.exit.1
	s_or_b64 exec, exec, s[16:17]
	v_mov_b32_e32 v17, 0
	s_mov_b32 m0, -1
	s_waitcnt lgkmcnt(0)
	s_barrier
BB0_67:                                 ; %.lr.ph13.i178.preheader.1914
                                        ; =>This Inner Loop Header: Depth=1
	v_add_i32_e32 v18, vcc, 0x36d8, v17
	v_add_i32_e32 v20, vcc, 0x36f8, v17
	v_add_i32_e32 v22, vcc, 0x37e0, v17
	v_add_i32_e32 v24, vcc, 0x3800, v17
	v_add_i32_e32 v26, vcc, 0x38e8, v17
	v_add_i32_e32 v32, vcc, 0x3908, v17
	v_add_i32_e32 v34, vcc, v17, v14
	v_add_i32_e32 v28, vcc, 0x376c, v17
	v_add_i32_e32 v30, vcc, 0x3874, v17
	v_add_i32_e32 v53, vcc, 0x397c, v17
	ds_read2_b32 v[18:19], v18 offset1:4
	ds_read2_b32 v[55:56], v34 offset1:4
	ds_read2_b32 v[20:21], v20 offset1:25
	ds_read2_b32 v[57:58], v34 offset0:8 offset1:12
	ds_read2_b32 v[22:23], v22 offset1:4
	ds_read2_b32 v[24:25], v24 offset1:25
	ds_read2_b32 v[26:27], v26 offset1:4
	ds_read2_b32 v[32:33], v32 offset1:25
	ds_read2_b32 v[28:29], v28 offset1:4
	ds_read2_b32 v[30:31], v30 offset1:4
	ds_read2_b32 v[53:54], v53 offset1:4
	ds_read_b32 v34, v34 offset:64
	v_add_i32_e32 v17, vcc, 4, v17
	v_cmp_ne_u32_e32 vcc, 12, v17
	s_and_b64 vcc, exec, vcc
	s_waitcnt lgkmcnt(10)
	v_mac_f32_e32 v42, v55, v18
	v_mac_f32_e32 v52, v56, v18
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v51, v57, v18
	v_mac_f32_e32 v50, v55, v21
	v_mac_f32_e32 v49, v56, v21
	v_mac_f32_e32 v48, v57, v21
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v47, v55, v22
	v_mac_f32_e32 v46, v56, v22
	v_mac_f32_e32 v45, v57, v22
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v44, v55, v25
	v_mac_f32_e32 v43, v56, v25
	v_mac_f32_e32 v41, v57, v25
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v40, v55, v26
	v_mac_f32_e32 v39, v56, v26
	v_mac_f32_e32 v38, v57, v26
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v55, v33
	v_mac_f32_e32 v35, v57, v33
	v_mac_f32_e32 v36, v56, v33
	v_mac_f32_e32 v42, v56, v19
	v_mac_f32_e32 v52, v57, v19
	v_mac_f32_e32 v51, v58, v19
	;s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v50, v56, v28
	v_mac_f32_e32 v49, v57, v28
	v_mac_f32_e32 v48, v58, v28
	v_mac_f32_e32 v47, v56, v23
	v_mac_f32_e32 v46, v57, v23
	v_mac_f32_e32 v45, v58, v23
	;s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v56, v30
	v_mac_f32_e32 v43, v57, v30
	v_mac_f32_e32 v41, v58, v30
	v_mac_f32_e32 v40, v56, v27
	v_mac_f32_e32 v39, v57, v27
	v_mac_f32_e32 v38, v58, v27
	;s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v37, v56, v53
	v_mac_f32_e32 v35, v58, v53
	v_mac_f32_e32 v36, v57, v53
	v_mac_f32_e32 v52, v58, v20
	;s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v51, v34, v20
	v_mac_f32_e32 v50, v57, v29
	v_mac_f32_e32 v49, v58, v29
	v_mac_f32_e32 v48, v34, v29
	v_mac_f32_e32 v47, v57, v24
	v_mac_f32_e32 v46, v58, v24
	v_mac_f32_e32 v45, v34, v24
	v_mac_f32_e32 v44, v57, v31
	v_mac_f32_e32 v43, v58, v31
	v_mac_f32_e32 v41, v34, v31
	v_mac_f32_e32 v40, v57, v32
	v_mac_f32_e32 v39, v58, v32
	v_mac_f32_e32 v38, v34, v32
	v_mac_f32_e32 v37, v57, v54
	v_mac_f32_e32 v35, v34, v54
	v_mac_f32_e32 v36, v58, v54
	v_mac_f32_e32 v42, v57, v20
	s_cbranch_vccnz BB0_67
; BB#68:                                ; %.lr.ph13.i.preheader.1968
	v_mov_b32_e32 v27, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v17, vcc, 0x36e4, v27
	v_add_i32_e32 v19, vcc, 0x3768, v27
	v_add_i32_e32 v21, vcc, 0x37ec, v27
	v_add_i32_e32 v23, vcc, 0x3870, v27
	v_add_i32_e32 v25, vcc, 0x38f4, v27
	v_add_i32_e32 v27, vcc, 0x3978, v27
	ds_read2_b32 v[17:18], v17 offset1:4
	ds_read_b32 v31, v12 offset:12
	ds_read2_b32 v[19:20], v19 offset1:4
	ds_read_b32 v32, v13 offset:12
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[29:30], v1 offset0:3 offset1:15
	ds_read2_b32 v[23:24], v23 offset1:4
	ds_read2_b32 v[25:26], v25 offset1:4
	ds_read2_b32 v[27:28], v27 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v52, v31, v17
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v42, v29, v17
	v_mac_f32_e32 v51, v32, v17
	v_mac_f32_e32 v50, v29, v19
	v_mac_f32_e32 v49, v31, v19
	v_mac_f32_e32 v48, v32, v19
	v_mac_f32_e32 v47, v29, v21
	v_mac_f32_e32 v46, v31, v21
	v_mac_f32_e32 v45, v32, v21
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v29, v23
	v_mac_f32_e32 v43, v31, v23
	v_mac_f32_e32 v41, v32, v23
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v40, v29, v25
	v_mac_f32_e32 v39, v31, v25
	v_mac_f32_e32 v38, v32, v25
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v29, v27
	v_mac_f32_e32 v35, v32, v27
	v_mac_f32_e32 v36, v31, v27
	v_mac_f32_e32 v42, v31, v18
	v_mac_f32_e32 v51, v30, v18
	v_mac_f32_e32 v50, v31, v20
	v_mac_f32_e32 v49, v32, v20
	v_mac_f32_e32 v48, v30, v20
	v_mac_f32_e32 v47, v31, v22
	v_mac_f32_e32 v46, v32, v22
	v_mac_f32_e32 v45, v30, v22
	v_mac_f32_e32 v44, v31, v24
	v_mac_f32_e32 v43, v32, v24
	v_mac_f32_e32 v41, v30, v24
	v_mac_f32_e32 v40, v31, v26
	v_mac_f32_e32 v39, v32, v26
	v_mac_f32_e32 v38, v30, v26
	v_mac_f32_e32 v37, v31, v28
	v_mac_f32_e32 v35, v30, v28
	v_mac_f32_e32 v36, v32, v28
	v_mac_f32_e32 v52, v32, v18
	v_mov_b32_e32 v17, -12
	s_mov_b32 m0, -1
BB0_69:                                 ; %.lr.ph13.i178.preheader.1.1
                                        ; =>This Inner Loop Header: Depth=1
	v_add_i32_e32 v18, vcc, 0x3710, v17
	v_add_i32_e32 v20, vcc, 0x3730, v17
	v_add_i32_e32 v22, vcc, 0x3818, v17
	v_add_i32_e32 v24, vcc, 0x3838, v17
	v_add_i32_e32 v26, vcc, 0x3920, v17
	v_add_i32_e32 v32, vcc, 0x3940, v17
	v_add_i32_e32 v34, vcc, v17, v2
	v_add_i32_e32 v28, vcc, 0x37a4, v17
	v_add_i32_e32 v30, vcc, 0x38ac, v17
	v_add_i32_e32 v53, vcc, 0x39b4, v17
	ds_read2_b32 v[18:19], v18 offset1:4
	ds_read2_b32 v[55:56], v34 offset0:237 offset1:241
	ds_read2_b32 v[20:21], v20 offset1:25
	ds_read2_b32 v[57:58], v34 offset0:245 offset1:249
	ds_read2_b32 v[22:23], v22 offset1:4
	ds_read2_b32 v[24:25], v24 offset1:25
	ds_read2_b32 v[26:27], v26 offset1:4
	ds_read2_b32 v[32:33], v32 offset1:25
	ds_read2_b32 v[28:29], v28 offset1:4
	ds_read2_b32 v[30:31], v30 offset1:4
	ds_read2_b32 v[53:54], v53 offset1:4
	ds_read_b32 v34, v34 offset:1012
	v_add_i32_e32 v17, vcc, 4, v17
	v_cmp_ne_u32_e32 vcc, 0, v17
	s_and_b64 vcc, exec, vcc
	s_waitcnt lgkmcnt(10)
	v_mac_f32_e32 v42, v55, v18
	v_mac_f32_e32 v52, v56, v18
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v51, v57, v18
	v_mac_f32_e32 v50, v55, v21
	v_mac_f32_e32 v49, v56, v21
	v_mac_f32_e32 v48, v57, v21
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v47, v55, v22
	v_mac_f32_e32 v46, v56, v22
	v_mac_f32_e32 v45, v57, v22
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v44, v55, v25
	v_mac_f32_e32 v43, v56, v25
	v_mac_f32_e32 v41, v57, v25
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v40, v55, v26
	v_mac_f32_e32 v39, v56, v26
	v_mac_f32_e32 v38, v57, v26
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v55, v33
	v_mac_f32_e32 v35, v57, v33
	v_mac_f32_e32 v36, v56, v33
	v_mac_f32_e32 v42, v56, v19
	v_mac_f32_e32 v52, v57, v19
	v_mac_f32_e32 v51, v58, v19
	;s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v50, v56, v28
	v_mac_f32_e32 v49, v57, v28
	v_mac_f32_e32 v48, v58, v28
	v_mac_f32_e32 v47, v56, v23
	v_mac_f32_e32 v46, v57, v23
	v_mac_f32_e32 v45, v58, v23
	;s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v56, v30
	v_mac_f32_e32 v43, v57, v30
	v_mac_f32_e32 v41, v58, v30
	v_mac_f32_e32 v40, v56, v27
	v_mac_f32_e32 v39, v57, v27
	v_mac_f32_e32 v38, v58, v27
	;s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v37, v56, v53
	v_mac_f32_e32 v35, v58, v53
	v_mac_f32_e32 v36, v57, v53
	v_mac_f32_e32 v52, v58, v20
	;s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v51, v34, v20
	v_mac_f32_e32 v50, v57, v29
	v_mac_f32_e32 v49, v58, v29
	v_mac_f32_e32 v48, v34, v29
	v_mac_f32_e32 v47, v57, v24
	v_mac_f32_e32 v46, v58, v24
	v_mac_f32_e32 v45, v34, v24
	v_mac_f32_e32 v44, v57, v31
	v_mac_f32_e32 v43, v58, v31
	v_mac_f32_e32 v41, v34, v31
	v_mac_f32_e32 v40, v57, v32
	v_mac_f32_e32 v39, v58, v32
	v_mac_f32_e32 v38, v34, v32
	v_mac_f32_e32 v37, v57, v54
	v_mac_f32_e32 v35, v34, v54
	v_mac_f32_e32 v36, v58, v54
	v_mac_f32_e32 v42, v57, v20
	s_cbranch_vccnz BB0_69
; BB#70:                                ; %.lr.ph13.i.preheader.1.1
	v_mov_b32_e32 v34, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v17, vcc, 0x3710, v34
	v_add_i32_e32 v19, vcc, 0x3794, v34
	v_add_i32_e32 v21, vcc, 0x3818, v34
	v_add_i32_e32 v23, vcc, 0x389c, v34
	v_add_i32_e32 v25, vcc, 0x3920, v34
	v_add_i32_e32 v27, vcc, 0x39a4, v34
	ds_read2_b32 v[17:18], v17 offset1:4
	ds_read_b32 v31, v12 offset:948
	ds_read2_b32 v[19:20], v19 offset1:4
	ds_read_b32 v32, v13 offset:948
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[29:30], v1 offset0:237 offset1:249
	ds_read2_b32 v[23:24], v23 offset1:4
	ds_read2_b32 v[25:26], v25 offset1:4
	ds_read2_b32 v[27:28], v27 offset1:4
	s_add_i32 s16, s25, 0x18800
	v_add_i32_e32 v53, vcc, 0xf2, v15
	v_mov_b32_e32 v54, s16
	s_movk_i32 s25, 0xff1a
	s_movk_i32 s26, 0xffe9
	s_movk_i32 s27, 0x380
	s_movk_i32 s28, 0xea
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v52, v31, v17
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v42, v29, v17
	v_mac_f32_e32 v51, v32, v17
	v_mac_f32_e32 v50, v29, v19
	v_mac_f32_e32 v49, v31, v19
	v_mac_f32_e32 v48, v32, v19
	v_mac_f32_e32 v47, v29, v21
	v_mac_f32_e32 v46, v31, v21
	v_mac_f32_e32 v45, v32, v21
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v29, v23
	v_mac_f32_e32 v43, v31, v23
	v_mac_f32_e32 v41, v32, v23
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v40, v29, v25
	v_mac_f32_e32 v39, v31, v25
	v_mac_f32_e32 v38, v32, v25
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v29, v27
	v_mac_f32_e32 v35, v32, v27
	v_mac_f32_e32 v36, v31, v27
	v_mac_f32_e32 v42, v31, v18
	v_mac_f32_e32 v51, v30, v18
	v_mac_f32_e32 v50, v31, v20
	v_mac_f32_e32 v49, v32, v20
	v_mac_f32_e32 v48, v30, v20
	v_mac_f32_e32 v47, v31, v22
	v_mac_f32_e32 v46, v32, v22
	v_mac_f32_e32 v45, v30, v22
	v_mac_f32_e32 v44, v31, v24
	v_mac_f32_e32 v43, v32, v24
	v_mac_f32_e32 v41, v30, v24
	v_mac_f32_e32 v40, v31, v26
	v_mac_f32_e32 v39, v32, v26
	v_mac_f32_e32 v38, v30, v26
	v_mac_f32_e32 v37, v31, v28
	v_mac_f32_e32 v35, v30, v28
	v_mac_f32_e32 v36, v32, v28
	v_mac_f32_e32 v52, v32, v18
BB0_71:                                 ; =>This Loop Header: Depth=1
                                        ;     Child Loop BB0_75 Depth 2
                                        ;     Child Loop BB0_83 Depth 2
                                        ;     Child Loop BB0_85 Depth 2
                                        ;     Child Loop BB0_87 Depth 2
	v_cmp_ne_u32_e32 vcc, 3, v34
	s_and_b64 vcc, exec, vcc
	v_mov_b32_e32 v55, -1
	s_barrier
	s_cbranch_vccz BB0_89
; BB#72:                                ;   in Loop: Header=BB0_71 Depth=1
	s_and_saveexec_b64 s[16:17], s[2:3]
	s_xor_b64 s[30:31], exec, s[16:17]
	; mask branch BB0_74
	s_cbranch_execz BB0_74
BB0_73:                                 ; %.lr.ph.i271.2
                                        ;   in Loop: Header=BB0_71 Depth=1
	v_add_i32_e32 v17, vcc, v34, v6
	v_mul_lo_i32 v18, v17, 11
	v_cmp_gt_u32_e32 vcc, 11, v17
	s_and_b64 s[16:17], exec, s[4:5]
	s_and_b64 s[16:17], s[16:17], vcc
	v_add_i32_e32 v17, vcc, v53, v18
	v_ashrrev_i32_e32 v18, 31, v17
	v_cndmask_b32_e64 v18, 0, v18, s[16:17]
	v_cndmask_b32_e64 v17, 0, v17, s[16:17]
	v_lshlrev_b64 v[17:18], 2, v[17:18]
	v_add_i32_e32 v25, vcc, s22, v17
	v_mov_b32_e32 v17, s23
	v_addc_u32_e32 v26, vcc, v17, v18, vcc
	flat_load_dwordx4 v[17:20], v[25:26]
	flat_load_dwordx4 v[21:24], v[25:26] offset:16
	flat_load_dword v27, v[25:26] offset:40
	flat_load_dwordx2 v[25:26], v[25:26] offset:32
	s_mov_b32 m0, -1
	s_waitcnt vmcnt(3) lgkmcnt(3)
	ds_write2_b32 v7, v17, v18 offset1:1
	ds_write2_b32 v7, v19, v20 offset0:2 offset1:3
	s_waitcnt vmcnt(2) lgkmcnt(4)
	ds_write2_b32 v7, v21, v22 offset0:4 offset1:5
	ds_write2_b32 v7, v23, v24 offset0:6 offset1:7
	s_waitcnt vmcnt(0) lgkmcnt(4)
	ds_write2_b32 v7, v25, v26 offset0:8 offset1:9
	ds_write_b32 v7, v27 offset:40
BB0_74:                                 ; %.lr.ph.i273.2.preheader
                                        ;   in Loop: Header=BB0_71 Depth=1
	s_or_b64 exec, exec, s[30:31]
	v_add_i32_e32 v17, vcc, v54, v3
	s_mov_b64 s[16:17], 0
	v_mov_b32_e32 v18, 0
	v_mov_b32_e32 v19, v0
BB0_75:                                 ; %.lr.ph.i273.2
                                        ;   Parent Loop BB0_71 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_cvt_f32_u32_e32 v20, v19
	v_mov_b32_e32 v21, 0x3d321643
	v_mov_b32_e32 v22, 0xdf
                                        ; implicit-def: %VGPR41
	v_madak_f32 v20, v20, v21, 0x3727c5ac
	v_cvt_u32_f32_e32 v20, v20
	v_lshlrev_b32_e32 v21, 2, v20
	v_add_i32_e32 v21, vcc, v21, v34
	v_add_i32_e32 v21, vcc, s6, v21
	v_cmp_gt_u32_e32 vcc, v21, v22
	v_and_b32_e32 v32, 0xffffff, v20
                                        ; implicit-def: %VGPR39
	s_and_saveexec_b64 s[30:31], vcc
	s_xor_b64 s[30:31], exec, s[30:31]
	; mask branch BB0_77
BB0_76:                                 ; %.lr.ph..loopexit_crit_edge.i275.2
                                        ;   in Loop: Header=BB0_75 Depth=2
	v_mul_lo_i32 v21, v32, s25
	v_add_i32_e32 v22, vcc, v18, v3
	v_mov_b32_e32 v23, 0
	v_add_i32_e32 v21, vcc, v21, v22
BB0_77:                                 ; %Flow4606
                                        ;   in Loop: Header=BB0_75 Depth=2
	s_or_saveexec_b64 s[30:31], s[30:31]
	v_mov_b32_e32 v22, v23
	v_mov_b32_e32 v31, v23
	v_mov_b32_e32 v30, v23
	v_mov_b32_e32 v29, v23
	v_mov_b32_e32 v28, v23
	v_mov_b32_e32 v27, v23
	v_mov_b32_e32 v26, v23
	v_mov_b32_e32 v25, v23
	v_mov_b32_e32 v24, v23
	s_xor_b64 exec, exec, s[30:31]
	; mask branch BB0_81
	s_cbranch_execz BB0_81
BB0_78:                                 ;   in Loop: Header=BB0_75 Depth=2
	v_mul_lo_i32 v22, v32, 23
	v_mul_lo_i32 v23, v20, s27
	v_mul_lo_i32 v21, v32, s26
	v_add_i32_e32 v25, vcc, v18, v17
	v_add_i32_e32 v24, vcc, 22, v22
	v_mul_lo_i32 v22, v32, s25
	v_add_i32_e32 v23, vcc, v23, v25
	v_add_i32_e32 v21, vcc, v21, v19
	v_mul_lo_i32 v21, v21, 10
	v_add_i32_e32 v22, vcc, v22, v23
	v_ashrrev_i32_e32 v23, 31, v22
	v_lshlrev_b64 v[22:23], 2, v[22:23]
	v_add_i32_e32 v32, vcc, s20, v22
	v_mov_b32_e32 v22, s21
	v_addc_u32_e32 v33, vcc, v22, v23, vcc
	v_cmp_ne_u32_e32 vcc, v24, v19
	flat_load_dwordx4 v[24:27], v[32:33]
	v_mov_b32_e32 v28, 0
	v_mov_b32_e32 v29, 0
	v_mov_b32_e32 v30, 0
	v_mov_b32_e32 v31, 0
	v_mov_b32_e32 v22, 0
	v_mov_b32_e32 v23, 0
	s_and_saveexec_b64 s[32:33], vcc
	s_xor_b64 s[32:33], exec, s[32:33]
	; mask branch BB0_80
BB0_79:                                 ; %.preheader1.preheader.i276.2
                                        ;   in Loop: Header=BB0_75 Depth=2
	flat_load_dwordx4 v[28:31], v[32:33] offset:16
	flat_load_dwordx2 v[22:23], v[32:33] offset:32
BB0_80:                                 ; %Flow4605
                                        ;   in Loop: Header=BB0_75 Depth=2
	s_or_b64 exec, exec, s[32:33]
BB0_81:                                 ; %.loopexit.i278.2
                                        ;   in Loop: Header=BB0_75 Depth=2
	s_or_b64 exec, exec, s[30:31]
	v_mul_lo_i32 v20, v20, s28
	s_mov_b32 m0, -1
	v_add_i32_e32 v19, vcc, 0x100, v19
	v_add_i32_e32 v20, vcc, v20, v21
	v_lshlrev_b32_e32 v20, 2, v20
	s_waitcnt vmcnt(2) lgkmcnt(0)
	ds_write2_b32 v20, v24, v25 offset0:2 offset1:3
	ds_write2_b32 v20, v26, v27 offset0:4 offset1:5
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write2_b32 v20, v28, v29 offset0:6 offset1:7
	ds_write2_b32 v20, v30, v31 offset0:8 offset1:9
	ds_write2_b32 v20, v22, v23 offset0:10 offset1:11
	v_mov_b32_e32 v20, 0x158
	v_cmp_gt_u32_e32 vcc, v19, v20
	s_or_b64 s[16:17], vcc, s[16:17]
	v_add_i32_e32 v18, vcc, 0xa00, v18
	s_andn2_b64 exec, exec, s[16:17]
	s_cbranch_execnz BB0_75
; BB#82:                                ; %fetchData.4.exit279.2
                                        ;   in Loop: Header=BB0_71 Depth=1
	s_or_b64 exec, exec, s[16:17]
	v_mov_b32_e32 v55, 0
	s_waitcnt lgkmcnt(0)
	s_barrier
BB0_83:                                 ; %.lr.ph13.i283.preheader.21132
                                        ;   Parent Loop BB0_71 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	s_mov_b32 m0, -1
	v_add_i32_e32 v56, vcc, 0x36d8, v55
	v_add_i32_e32 v23, vcc, v55, v14
	v_add_i32_e32 v24, vcc, 0x37e0, v55
	v_add_i32_e32 v58, vcc, 0x36f8, v55
	v_add_i32_e32 v26, vcc, 0x3800, v55
	v_add_i32_e32 v28, vcc, 0x38e8, v55
	ds_read2_b32 v[56:57], v56 offset1:4
	ds_read2_b32 v[19:20], v23 offset1:4
	ds_read2_b32 v[21:22], v23 offset0:8 offset1:12
	ds_read2_b32 v[24:25], v24 offset1:4
	ds_read2_b32 v[58:59], v58 offset1:25
	ds_read_b32 v23, v23 offset:64
	ds_read2_b32 v[26:27], v26 offset1:25
	ds_read2_b32 v[28:29], v28 offset1:4

	v_add_i32_e32 v30, vcc, 0x376c, v55
	v_add_i32_e32 v31, vcc, 0x3874, v55
	v_add_i32_e32 v32, vcc, 0x3908, v55
	v_add_i32_e32 v33, vcc, 0x397c, v55
	v_add_i32_e32 v55, vcc, 4, v55
	v_cmp_ne_u32_e32 vcc, 12, v55
	s_and_b64 vcc, exec, vcc
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v42, v19, v56
	v_mac_f32_e32 v52, v20, v56
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v51, v21, v56
	v_mac_f32_e32 v42, v20, v57
	v_mac_f32_e32 v52, v21, v57
	v_mac_f32_e32 v51, v22, v57
	v_mac_f32_e32 v47, v19, v24
	v_mac_f32_e32 v46, v20, v24
	v_mac_f32_e32 v45, v21, v24
	v_mac_f32_e32 v47, v20, v25
	v_mac_f32_e32 v46, v21, v25
	v_mac_f32_e32 v45, v22, v25
	ds_read2_b32 v[56:57], v30 offset1:4
	ds_read2_b32 v[24:25], v31 offset1:4
	ds_read2_b32 v[30:31], v32 offset1:25
	ds_read2_b32 v[32:33], v33 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v50, v19, v59
	v_mac_f32_e32 v49, v20, v59
	v_mac_f32_e32 v48, v21, v59
	v_mac_f32_e32 v42, v21, v58
	v_mac_f32_e32 v52, v22, v58
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v51, v23, v58
	v_mac_f32_e32 v47, v21, v26
	v_mac_f32_e32 v46, v22, v26
	v_mac_f32_e32 v45, v23, v26
	v_mac_f32_e32 v44, v19, v27
	v_mac_f32_e32 v43, v20, v27
	v_mac_f32_e32 v41, v21, v27
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v40, v19, v28
	v_mac_f32_e32 v39, v20, v28
	v_mac_f32_e32 v38, v21, v28
	v_mac_f32_e32 v40, v20, v29
	v_mac_f32_e32 v39, v21, v29
	v_mac_f32_e32 v38, v22, v29
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v50, v20, v56
	v_mac_f32_e32 v49, v21, v56
	v_mac_f32_e32 v48, v22, v56
	v_mac_f32_e32 v50, v21, v57
	v_mac_f32_e32 v49, v22, v57
	v_mac_f32_e32 v48, v23, v57
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v21, v33
	v_mac_f32_e32 v35, v23, v33
	v_mac_f32_e32 v36, v22, v33
	v_mac_f32_e32 v44, v20, v24
	v_mac_f32_e32 v43, v21, v24
	v_mac_f32_e32 v41, v22, v24
	v_mac_f32_e32 v40, v21, v30
	v_mac_f32_e32 v39, v22, v30
	v_mac_f32_e32 v38, v23, v30
	v_mac_f32_e32 v37, v19, v31
	v_mac_f32_e32 v35, v21, v31
	v_mac_f32_e32 v36, v20, v31
	v_mac_f32_e32 v44, v21, v25
	v_mac_f32_e32 v43, v22, v25
	v_mac_f32_e32 v41, v23, v25
	v_mac_f32_e32 v37, v20, v32
	v_mac_f32_e32 v35, v22, v32
	v_mac_f32_e32 v36, v21, v32
	s_cbranch_vccnz BB0_83
; BB#84:                                ; %.lr.ph13.i383.preheader.21186
                                        ;   in Loop: Header=BB0_71 Depth=1
	v_mov_b32_e32 v25, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v55, vcc, 0x36e4, v25
	v_add_i32_e32 v57, vcc, 0x3768, v25
	v_add_i32_e32 v19, vcc, 0x37ec, v25
	v_add_i32_e32 v21, vcc, 0x3870, v25
	v_add_i32_e32 v23, vcc, 0x38f4, v25
	v_add_i32_e32 v25, vcc, 0x3978, v25
	ds_read2_b32 v[55:56], v55 offset1:4
	ds_read_b32 v29, v12 offset:12
	ds_read2_b32 v[57:58], v57 offset1:4
	ds_read_b32 v30, v13 offset:12
	ds_read2_b32 v[19:20], v19 offset1:4
	ds_read2_b32 v[27:28], v1 offset0:3 offset1:15
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[23:24], v23 offset1:4
	ds_read2_b32 v[25:26], v25 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v52, v29, v55
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v42, v27, v55
	v_mac_f32_e32 v51, v30, v55
	v_mac_f32_e32 v50, v27, v57
	v_mac_f32_e32 v49, v29, v57
	v_mac_f32_e32 v48, v30, v57
	v_mac_f32_e32 v47, v27, v19
	v_mac_f32_e32 v46, v29, v19
	v_mac_f32_e32 v45, v30, v19
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v27, v21
	v_mac_f32_e32 v43, v29, v21
	v_mac_f32_e32 v41, v30, v21
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v40, v27, v23
	v_mac_f32_e32 v39, v29, v23
	v_mac_f32_e32 v38, v30, v23
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v27, v25
	v_mac_f32_e32 v35, v30, v25
	v_mac_f32_e32 v36, v29, v25
	v_mac_f32_e32 v42, v29, v56
	v_mac_f32_e32 v51, v28, v56
	v_mac_f32_e32 v50, v29, v58
	v_mac_f32_e32 v49, v30, v58
	v_mac_f32_e32 v48, v28, v58
	v_mac_f32_e32 v47, v29, v20
	v_mac_f32_e32 v46, v30, v20
	v_mac_f32_e32 v45, v28, v20
	v_mac_f32_e32 v44, v29, v22
	v_mac_f32_e32 v43, v30, v22
	v_mac_f32_e32 v41, v28, v22
	v_mac_f32_e32 v40, v29, v24
	v_mac_f32_e32 v39, v30, v24
	v_mac_f32_e32 v38, v28, v24
	v_mac_f32_e32 v37, v29, v26
	v_mac_f32_e32 v35, v28, v26
	v_mac_f32_e32 v36, v30, v26
	v_mac_f32_e32 v52, v30, v56
	v_mov_b32_e32 v55, -12
BB0_85:                                 ; %.lr.ph13.i283.preheader.1.2
                                        ;   Parent Loop BB0_71 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	s_mov_b32 m0, -1
	v_add_i32_e32 v56, vcc, 0x3710, v55
	v_add_i32_e32 v23, vcc, v55, v2
	v_add_i32_e32 v58, vcc, 0x3730, v55
	v_add_i32_e32 v24, vcc, 0x3818, v55
	v_add_i32_e32 v26, vcc, 0x3838, v55
	v_add_i32_e32 v28, vcc, 0x3920, v55
	ds_read2_b32 v[56:57], v56 offset1:4
	ds_read2_b32 v[19:20], v23 offset0:237 offset1:241
	ds_read2_b32 v[21:22], v23 offset0:245 offset1:249
	ds_read2_b32 v[24:25], v24 offset1:4
	ds_read2_b32 v[58:59], v58 offset1:25
	ds_read_b32 v23, v23 offset:1012
	ds_read2_b32 v[26:27], v26 offset1:25
	ds_read2_b32 v[28:29], v28 offset1:4
	v_add_i32_e32 v30, vcc, 0x37a4, v55
	v_add_i32_e32 v31, vcc, 0x38ac, v55
	v_add_i32_e32 v32, vcc, 0x3940, v55
	v_add_i32_e32 v33, vcc, 0x39b4, v55

	v_add_i32_e32 v55, vcc, 4, v55
	v_cmp_ne_u32_e32 vcc, 0, v55
	s_and_b64 vcc, exec, vcc
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v42, v19, v56
	v_mac_f32_e32 v52, v20, v56
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v51, v21, v56
	v_mac_f32_e32 v42, v20, v57
	v_mac_f32_e32 v52, v21, v57
	v_mac_f32_e32 v51, v22, v57
	v_mac_f32_e32 v47, v19, v24
	v_mac_f32_e32 v46, v20, v24
	v_mac_f32_e32 v45, v21, v24
	v_mac_f32_e32 v47, v20, v25
	v_mac_f32_e32 v46, v21, v25
	v_mac_f32_e32 v45, v22, v25
	ds_read2_b32 v[56:57], v30 offset1:4
	ds_read2_b32 v[24:25], v31 offset1:4
	ds_read2_b32 v[30:31], v32 offset1:25
	ds_read2_b32 v[32:33], v33 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v50, v19, v59
	v_mac_f32_e32 v49, v20, v59
	v_mac_f32_e32 v48, v21, v59
	v_mac_f32_e32 v42, v21, v58
	v_mac_f32_e32 v52, v22, v58
	v_mac_f32_e32 v51, v23, v58
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v47, v21, v26
	v_mac_f32_e32 v46, v22, v26
	v_mac_f32_e32 v45, v23, v26
	v_mac_f32_e32 v44, v19, v27
	v_mac_f32_e32 v43, v20, v27
	v_mac_f32_e32 v41, v21, v27
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v40, v19, v28
	v_mac_f32_e32 v39, v20, v28
	v_mac_f32_e32 v38, v21, v28
	v_mac_f32_e32 v40, v20, v29
	v_mac_f32_e32 v39, v21, v29
	v_mac_f32_e32 v38, v22, v29
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v50, v20, v56
	v_mac_f32_e32 v49, v21, v56
	v_mac_f32_e32 v48, v22, v56
	v_mac_f32_e32 v50, v21, v57
	v_mac_f32_e32 v49, v22, v57
	v_mac_f32_e32 v48, v23, v57
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v21, v33
	v_mac_f32_e32 v35, v23, v33
	v_mac_f32_e32 v36, v22, v33
	v_mac_f32_e32 v44, v20, v24
	v_mac_f32_e32 v43, v21, v24
	v_mac_f32_e32 v41, v22, v24
	v_mac_f32_e32 v40, v21, v30
	v_mac_f32_e32 v39, v22, v30
	v_mac_f32_e32 v38, v23, v30
	v_mac_f32_e32 v37, v19, v31
	v_mac_f32_e32 v35, v21, v31
	v_mac_f32_e32 v36, v20, v31
	v_mac_f32_e32 v44, v21, v25
	v_mac_f32_e32 v43, v22, v25
	v_mac_f32_e32 v41, v23, v25
	v_mac_f32_e32 v37, v20, v32
	v_mac_f32_e32 v35, v22, v32
	v_mac_f32_e32 v36, v21, v32
	s_cbranch_vccnz BB0_85
; BB#86:                                ; %.lr.ph13.i383.preheader.1.2
                                        ;   in Loop: Header=BB0_71 Depth=1
	v_mov_b32_e32 v25, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v55, vcc, 0x3710, v25
	v_add_i32_e32 v57, vcc, 0x3794, v25
	v_add_i32_e32 v19, vcc, 0x3818, v25
	v_add_i32_e32 v21, vcc, 0x389c, v25
	v_add_i32_e32 v23, vcc, 0x3920, v25
	v_add_i32_e32 v25, vcc, 0x39a4, v25
	ds_read2_b32 v[55:56], v55 offset1:4
	ds_read_b32 v29, v12 offset:948
	ds_read2_b32 v[57:58], v57 offset1:4
	ds_read_b32 v30, v13 offset:948
	ds_read2_b32 v[19:20], v19 offset1:4
	ds_read2_b32 v[27:28], v1 offset0:237 offset1:249
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[23:24], v23 offset1:4
	ds_read2_b32 v[25:26], v25 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v52, v29, v55
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v42, v27, v55
	v_mac_f32_e32 v51, v30, v55
	v_mac_f32_e32 v50, v27, v57
	v_mac_f32_e32 v49, v29, v57
	v_mac_f32_e32 v48, v30, v57
	v_mac_f32_e32 v47, v27, v19
	v_mac_f32_e32 v46, v29, v19
	v_mac_f32_e32 v45, v30, v19
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v27, v21
	v_mac_f32_e32 v43, v29, v21
	v_mac_f32_e32 v41, v30, v21
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v40, v27, v23
	v_mac_f32_e32 v39, v29, v23
	v_mac_f32_e32 v38, v30, v23
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v27, v25
	v_mac_f32_e32 v35, v30, v25
	v_mac_f32_e32 v36, v29, v25
	v_mac_f32_e32 v42, v29, v56
	v_mac_f32_e32 v51, v28, v56
	v_mac_f32_e32 v50, v29, v58
	v_mac_f32_e32 v49, v30, v58
	v_mac_f32_e32 v48, v28, v58
	v_mac_f32_e32 v47, v29, v20
	v_mac_f32_e32 v46, v30, v20
	v_mac_f32_e32 v45, v28, v20
	v_mac_f32_e32 v44, v29, v22
	v_mac_f32_e32 v43, v30, v22
	v_mac_f32_e32 v41, v28, v22
	v_mac_f32_e32 v40, v29, v24
	v_mac_f32_e32 v39, v30, v24
	v_mac_f32_e32 v38, v28, v24
	v_mac_f32_e32 v37, v29, v26
	v_mac_f32_e32 v35, v28, v26
	v_mac_f32_e32 v36, v30, v26
	v_mac_f32_e32 v52, v30, v56
	v_mov_b32_e32 v55, -12
BB0_87:                                 ; %.lr.ph13.i283.preheader.2.2
                                        ;   Parent Loop BB0_71 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_add_i32_e32 v23, vcc, v55, v2
	s_mov_b32 m0, -1
	v_add_i32_e32 v56, vcc, 0x373c, v55
	v_add_i32_e32 v20, vcc, 0x75c, v23
	v_add_i32_e32 v21, vcc, 0x77c, v23
	v_add_i32_e32 v26, vcc, 0x3844, v55
	v_add_i32_e32 v58, vcc, 0x375c, v55
	v_add_i32_e32 v24, vcc, 0x37d0, v55
	v_add_i32_e32 v28, vcc, 0x3864, v55
	ds_read2_b32 v[56:57], v56 offset1:4
	ds_read2_b32 v[19:20], v20 offset1:4
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[26:27], v26 offset1:4
	ds_read2_b32 v[58:59], v58 offset1:25
	ds_read_b32 v23, v23 offset:1948
	ds_read2_b32 v[24:25], v24 offset1:4
	ds_read2_b32 v[28:29], v28 offset1:25
	v_add_i32_e32 v30, vcc, 0x38d8, v55
	v_add_i32_e32 v31, vcc, 0x394c, v55
	v_add_i32_e32 v32, vcc, 0x396c, v55
	v_add_i32_e32 v33, vcc, 0x39e0, v55

	v_add_i32_e32 v55, vcc, 4, v55
	v_cmp_ne_u32_e32 vcc, 0, v55
	s_and_b64 vcc, exec, vcc
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v42, v19, v56
	v_mac_f32_e32 v52, v20, v56
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v51, v21, v56
	v_mac_f32_e32 v42, v20, v57
	v_mac_f32_e32 v52, v21, v57
	v_mac_f32_e32 v51, v22, v57
	v_mac_f32_e32 v47, v19, v26
	v_mac_f32_e32 v46, v20, v26
	v_mac_f32_e32 v45, v21, v26
	v_mac_f32_e32 v47, v20, v27
	v_mac_f32_e32 v46, v21, v27
	v_mac_f32_e32 v45, v22, v27
	ds_read2_b32 v[56:57], v30 offset1:4
	ds_read2_b32 v[26:27], v31 offset1:4
	ds_read2_b32 v[30:31], v32 offset1:25
	ds_read2_b32 v[32:33], v33 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v50, v19, v59
	v_mac_f32_e32 v49, v20, v59
	v_mac_f32_e32 v48, v21, v59
	v_mac_f32_e32 v42, v21, v58
	v_mac_f32_e32 v52, v22, v58
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v51, v23, v58
	v_mac_f32_e32 v50, v20, v24
	v_mac_f32_e32 v49, v21, v24
	v_mac_f32_e32 v48, v22, v24
	v_mac_f32_e32 v50, v21, v25
	v_mac_f32_e32 v49, v22, v25
	v_mac_f32_e32 v48, v23, v25
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v47, v21, v28
	v_mac_f32_e32 v46, v22, v28
	v_mac_f32_e32 v45, v23, v28
	v_mac_f32_e32 v44, v19, v29
	v_mac_f32_e32 v43, v20, v29
	v_mac_f32_e32 v41, v21, v29
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v44, v20, v56
	v_mac_f32_e32 v43, v21, v56
	v_mac_f32_e32 v41, v22, v56
	v_mac_f32_e32 v44, v21, v57
	v_mac_f32_e32 v43, v22, v57
	v_mac_f32_e32 v41, v23, v57
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v19, v26
	v_mac_f32_e32 v39, v20, v26
	v_mac_f32_e32 v38, v21, v26
	v_mac_f32_e32 v37, v20, v32
	v_mac_f32_e32 v35, v22, v32
	v_mac_f32_e32 v36, v21, v32
	v_mac_f32_e32 v40, v21, v30
	v_mac_f32_e32 v39, v22, v30
	v_mac_f32_e32 v38, v23, v30
	v_mac_f32_e32 v37, v19, v31
	v_mac_f32_e32 v35, v21, v31
	v_mac_f32_e32 v36, v20, v31
	v_mac_f32_e32 v40, v20, v27
	v_mac_f32_e32 v39, v21, v27
	v_mac_f32_e32 v38, v22, v27
	v_mac_f32_e32 v37, v21, v33
	v_mac_f32_e32 v35, v23, v33
	v_mac_f32_e32 v36, v22, v33
	s_cbranch_vccnz BB0_87
; BB#88:                                ; %.lr.ph13.i383.preheader.2.2
                                        ;   in Loop: Header=BB0_71 Depth=1
	v_mov_b32_e32 v55, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v56, vcc, 0x373c, v55
	v_add_i32_e32 v58, vcc, 0x37c0, v55
	v_add_i32_e32 v19, vcc, 0x3844, v55
	v_add_i32_e32 v21, vcc, 0x38c8, v55
	v_add_i32_e32 v23, vcc, 0x394c, v55
	v_add_i32_e32 v25, vcc, 0x39d0, v55
	v_add_i32_e32 v27, vcc, 0x75c, v1
	ds_read2_b32 v[56:57], v56 offset1:4
	ds_read_b32 v29, v12 offset:1884
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read_b32 v30, v13 offset:1884
	ds_read2_b32 v[19:20], v19 offset1:4
	ds_read2_b32 v[27:28], v27 offset1:12
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[23:24], v23 offset1:4
	ds_read2_b32 v[25:26], v25 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v52, v29, v56
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v42, v27, v56
	v_mac_f32_e32 v51, v30, v56
	v_mac_f32_e32 v50, v27, v58
	v_mac_f32_e32 v49, v29, v58
	v_mac_f32_e32 v48, v30, v58
	v_mac_f32_e32 v47, v27, v19
	v_mac_f32_e32 v46, v29, v19
	v_mac_f32_e32 v45, v30, v19
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v44, v27, v21
	v_mac_f32_e32 v43, v29, v21
	v_mac_f32_e32 v41, v30, v21
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v40, v27, v23
	v_mac_f32_e32 v39, v29, v23
	v_mac_f32_e32 v38, v30, v23
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v27, v25
	v_mac_f32_e32 v35, v30, v25
	v_mac_f32_e32 v36, v29, v25
	v_mac_f32_e32 v42, v29, v57
	v_mac_f32_e32 v51, v28, v57
	v_mac_f32_e32 v50, v29, v59
	v_mac_f32_e32 v49, v30, v59
	v_mac_f32_e32 v48, v28, v59
	v_mac_f32_e32 v47, v29, v20
	v_mac_f32_e32 v46, v30, v20
	v_mac_f32_e32 v45, v28, v20
	v_mac_f32_e32 v44, v29, v22
	v_mac_f32_e32 v43, v30, v22
	v_mac_f32_e32 v41, v28, v22
	v_mac_f32_e32 v40, v29, v24
	v_mac_f32_e32 v39, v30, v24
	v_mac_f32_e32 v38, v28, v24
	v_mac_f32_e32 v37, v29, v26
	v_mac_f32_e32 v35, v28, v26
	v_mac_f32_e32 v36, v30, v26
	v_mac_f32_e32 v52, v30, v57
	v_add_i32_e32 v34, vcc, 1, v34
	v_add_i32_e32 v54, vcc, 0xe0, v54
	v_cmp_ne_u32_e32 vcc, 0, v55
	s_cbranch_vccz BB0_71
	s_branch BB0_90
BB0_89:                                 ;   in Loop: Header=BB0_71 Depth=1
                                        ; implicit-def: %VGPR34
                                        ; implicit-def: %VGPR51
                                        ; implicit-def: %VGPR50
                                        ; implicit-def: %VGPR49
                                        ; implicit-def: %VGPR48
                                        ; implicit-def: %VGPR47
                                        ; implicit-def: %VGPR46
                                        ; implicit-def: %VGPR45
                                        ; implicit-def: %VGPR44
                                        ; implicit-def: %VGPR43
                                        ; implicit-def: %VGPR42
                                        ; implicit-def: %VGPR41
                                        ; implicit-def: %VGPR40
                                        ; implicit-def: %VGPR39
                                        ; implicit-def: %VGPR38
                                        ; implicit-def: %VGPR37
                                        ; implicit-def: %VGPR36
                                        ; implicit-def: %VGPR35
                                        ; implicit-def: %VGPR52
                                        ; implicit-def: %VGPR54
	v_cmp_ne_u32_e32 vcc, 0, v55
	s_cbranch_vccz BB0_71
BB0_90:
	s_and_saveexec_b64 s[2:3], s[14:15]
	s_xor_b64 s[4:5], exec, s[2:3]
	; mask branch BB0_92
	s_cbranch_execz BB0_92
BB0_91:                                 ; %.lr.ph.i377.2
	v_lshlrev_b32_e32 v5, 1, v5
	v_and_b32_e32 v5, 0x1fffffe, v5
	v_subrev_i32_e32 v7, vcc, v5, v0
	v_lshlrev_b32_e32 v5, 2, v7
	v_or_b32_e32 v5, 3, v5
	v_mul_lo_i32 v6, v5, 11
	v_cmp_gt_u32_e32 vcc, 11, v5
	s_and_b64 s[2:3], exec, s[12:13]
	s_and_b64 s[2:3], s[2:3], vcc
	v_add_i32_e32 v5, vcc, v16, v6
	v_add_i32_e32 v5, vcc, 0xf2, v5
	v_ashrrev_i32_e32 v6, 31, v5
	v_cndmask_b32_e64 v6, 0, v6, s[2:3]
	v_cndmask_b32_e64 v5, 0, v5, s[2:3]
	v_lshlrev_b64 v[5:6], 2, v[5:6]
	v_add_i32_e32 v5, vcc, s22, v5
	v_mov_b32_e32 v16, s23
	v_addc_u32_e32 v6, vcc, v16, v6, vcc
	flat_load_dwordx4 v[17:20], v[5:6]
	flat_load_dword v16, v[5:6] offset:40
	flat_load_dwordx2 v[25:26], v[5:6] offset:32
	flat_load_dwordx4 v[21:24], v[5:6] offset:16
	v_mul_lo_i32 v7, v7, 11
	s_mov_b32 m0, -1
	v_add_i32_e32 v4, vcc, v4, v7
	v_lshlrev_b32_e32 v4, 2, v4
	v_add_i32_e32 v5, vcc, 0x36d8, v4
	v_add_i32_e32 v6, vcc, 0x36e0, v4
	v_add_i32_e32 v7, vcc, 0x36e8, v4
	v_add_i32_e32 v27, vcc, 0x36f0, v4
	v_add_i32_e32 v28, vcc, 0x36f8, v4
	s_waitcnt vmcnt(3) lgkmcnt(3)
	ds_write2_b32 v5, v17, v18 offset1:1
	ds_write2_b32 v6, v19, v20 offset1:1
	s_waitcnt vmcnt(0) lgkmcnt(2)
	ds_write2_b32 v7, v21, v22 offset1:1
	ds_write2_b32 v27, v23, v24 offset1:1
	ds_write2_b32 v28, v25, v26 offset1:1
	ds_write_b32 v4, v16 offset:14080
BB0_92:                                 ; %.lr.ph.i.2.preheader
	s_or_b64 exec, exec, s[4:5]
	s_mov_b64 s[2:3], 0
	s_movk_i32 s4, 0xff1a
	s_movk_i32 s5, 0xffe9
	s_movk_i32 s12, 0xe0
	s_movk_i32 s13, 0xea
	s_mov_b32 m0, -1
BB0_93:                                 ; %.lr.ph.i.2
                                        ; =>This Inner Loop Header: Depth=1
	v_cvt_f32_u32_e32 v4, v0
	v_mov_b32_e32 v5, 0x3d321643
	v_mov_b32_e32 v6, 0xdf
                                        ; implicit-def: %VGPR7
	v_madak_f32 v4, v4, v5, 0x3727c5ac
	v_cvt_u32_f32_e32 v4, v4
	v_lshlrev_b32_e32 v5, 2, v4
	v_or_b32_e32 v25, 3, v5
	v_add_i32_e32 v5, vcc, s6, v25
	v_cmp_gt_u32_e32 vcc, v5, v6
	v_and_b32_e32 v16, 0xffffff, v4
                                        ; implicit-def: %VGPR5
	s_and_saveexec_b64 s[14:15], vcc
	s_xor_b64 s[14:15], exec, s[14:15]
	; mask branch BB0_95
BB0_94:                                 ; %.lr.ph..loopexit_crit_edge.i.2
                                        ;   in Loop: Header=BB0_93 Depth=1
	v_mul_lo_i32 v5, v16, s4
	v_mov_b32_e32 v7, 0
	v_add_i32_e32 v5, vcc, v5, v3
BB0_95:                                 ; %Flow4604
                                        ;   in Loop: Header=BB0_93 Depth=1
	s_or_saveexec_b64 s[14:15], s[14:15]
	v_mov_b32_e32 v6, v7
	v_mov_b32_e32 v24, v7
	v_mov_b32_e32 v23, v7
	v_mov_b32_e32 v22, v7
	v_mov_b32_e32 v21, v7
	v_mov_b32_e32 v20, v7
	v_mov_b32_e32 v19, v7
	v_mov_b32_e32 v18, v7
	v_mov_b32_e32 v17, v7
	s_xor_b64 exec, exec, s[14:15]
	; mask branch BB0_99
	s_cbranch_execz BB0_99
BB0_96:                                 ;   in Loop: Header=BB0_93 Depth=1
	v_mul_lo_i32 v6, v16, 23
	v_mul_lo_i32 v7, v16, s4
	v_mul_lo_i32 v5, v16, s5
	v_add_i32_e32 v16, vcc, s24, v3
	v_add_i32_e32 v17, vcc, 22, v6
	v_mul_lo_i32 v6, v25, s12
	v_add_i32_e32 v7, vcc, v7, v16
	v_add_i32_e32 v5, vcc, v5, v0
	v_mul_lo_i32 v5, v5, 10
	v_add_i32_e32 v6, vcc, v6, v7
	v_add_i32_e32 v6, vcc, 0x18640, v6
	v_ashrrev_i32_e32 v7, 31, v6
	v_lshlrev_b64 v[6:7], 2, v[6:7]
	v_add_i32_e32 v25, vcc, s20, v6
	v_mov_b32_e32 v6, s21
	v_addc_u32_e32 v26, vcc, v6, v7, vcc
	v_cmp_ne_u32_e32 vcc, v17, v0
	flat_load_dwordx4 v[17:20], v[25:26]
	v_mov_b32_e32 v21, 0
	v_mov_b32_e32 v22, 0
	v_mov_b32_e32 v23, 0
	v_mov_b32_e32 v24, 0
	v_mov_b32_e32 v6, 0
	v_mov_b32_e32 v7, 0
	s_and_saveexec_b64 s[16:17], vcc
	s_xor_b64 s[16:17], exec, s[16:17]
	; mask branch BB0_98
BB0_97:                                 ; %.preheader1.preheader.i.2
                                        ;   in Loop: Header=BB0_93 Depth=1
	flat_load_dwordx4 v[21:24], v[25:26] offset:16
	flat_load_dwordx2 v[6:7], v[25:26] offset:32
BB0_98:                                 ; %Flow
                                        ;   in Loop: Header=BB0_93 Depth=1
	s_or_b64 exec, exec, s[16:17]
BB0_99:                                 ; %.loopexit.i.2
                                        ;   in Loop: Header=BB0_93 Depth=1
	s_or_b64 exec, exec, s[14:15]
	v_mul_lo_i32 v4, v4, s13
	v_add_i32_e32 v0, vcc, 0x100, v0
	v_add_i32_e32 v4, vcc, v4, v5
	v_lshlrev_b32_e32 v4, 2, v4
	s_waitcnt vmcnt(2) lgkmcnt(0)
	ds_write2_b32 v4, v17, v18 offset0:2 offset1:3
	ds_write2_b32 v4, v19, v20 offset0:4 offset1:5
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write2_b32 v4, v21, v22 offset0:6 offset1:7
	ds_write2_b32 v4, v23, v24 offset0:8 offset1:9
	ds_write2_b32 v4, v6, v7 offset0:10 offset1:11
	v_mov_b32_e32 v4, 0x141
	v_cmp_gt_u32_e32 vcc, v0, v4
	s_or_b64 s[2:3], vcc, s[2:3]
	v_add_i32_e32 v3, vcc, 0xa00, v3
	s_andn2_b64 exec, exec, s[2:3]
	s_cbranch_execnz BB0_93
; BB#100:                               ; %fetchData.4.exit.2
	s_or_b64 exec, exec, s[2:3]
	v_mov_b32_e32 v0, 0
	s_mov_b32 m0, -1
	s_waitcnt lgkmcnt(0)
	s_barrier
BB0_101:                                ; %.lr.ph13.i178.preheader.2
                                        ; =>This Inner Loop Header: Depth=1
	v_add_i32_e32 v7, vcc, v0, v14
	v_add_i32_e32 v3, vcc, 0x36d8, v0
	v_add_i32_e32 v58, vcc, 0x37e0, v0
	v_add_i32_e32 v5, vcc, 0x36f8, v0
	v_add_i32_e32 v16, vcc, 0x3800, v0
	v_add_i32_e32 v25, vcc, 0x38e8, v0
	v_add_i32_e32 v27, vcc, 0x376c, v0
	v_add_i32_e32 v29, vcc, 0x3874, v0
	v_add_i32_e32 v31, vcc, 0x3908, v0
	ds_read2_b32 v[19:20], v7 offset1:4
	ds_read2_b32 v[3:4], v3 offset1:4
	ds_read2_b32 v[21:22], v7 offset0:8 offset1:12
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read2_b32 v[5:6], v5 offset1:25
	ds_read_b32 v7, v7 offset:64
	ds_read2_b32 v[23:24], v16 offset1:25
	ds_read2_b32 v[25:26], v25 offset1:4
	ds_read2_b32 v[27:28], v27 offset1:4
	ds_read2_b32 v[29:30], v29 offset1:4
	ds_read2_b32 v[31:32], v31 offset1:25
	v_add_i32_e32 v33, vcc, 0x397c, v0

	v_add_i32_e32 v0, vcc, 4, v0
	v_cmp_ne_u32_e32 vcc, 12, v0
	s_and_b64 vcc, exec, vcc
	s_waitcnt lgkmcnt(9)
	v_mac_f32_e32 v42, v19, v3
	v_mac_f32_e32 v52, v20, v3
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v51, v21, v3
	v_mac_f32_e32 v42, v20, v4
	v_mac_f32_e32 v52, v21, v4
	v_mac_f32_e32 v51, v22, v4
	ds_read2_b32 v[3:4], v33 offset1:4
	v_mac_f32_e32 v47, v19, v58
	v_mac_f32_e32 v46, v20, v58
	v_mac_f32_e32 v45, v21, v58
	v_mac_f32_e32 v47, v20, v59
	v_mac_f32_e32 v46, v21, v59
	v_mac_f32_e32 v45, v22, v59
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v50, v19, v6
	v_mac_f32_e32 v49, v20, v6
	v_mac_f32_e32 v48, v21, v6
	v_mac_f32_e32 v42, v21, v5
	v_mac_f32_e32 v52, v22, v5
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v51, v7, v5
	v_mac_f32_e32 v47, v21, v23
	v_mac_f32_e32 v46, v22, v23
	v_mac_f32_e32 v45, v7, v23
	v_mac_f32_e32 v44, v19, v24
	v_mac_f32_e32 v43, v20, v24
	v_mac_f32_e32 v41, v21, v24
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v40, v19, v25
	v_mac_f32_e32 v39, v20, v25
	v_mac_f32_e32 v38, v21, v25
	v_mac_f32_e32 v40, v20, v26
	v_mac_f32_e32 v39, v21, v26
	v_mac_f32_e32 v38, v22, v26
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v20, v3
	v_mac_f32_e32 v35, v22, v3
	v_mac_f32_e32 v36, v21, v3
	v_mac_f32_e32 v50, v20, v27
	v_mac_f32_e32 v49, v21, v27
	v_mac_f32_e32 v48, v22, v27
	v_mac_f32_e32 v44, v20, v29
	v_mac_f32_e32 v43, v21, v29
	v_mac_f32_e32 v41, v22, v29
	v_mac_f32_e32 v37, v21, v4
	v_mac_f32_e32 v35, v7, v4
	v_mac_f32_e32 v36, v22, v4
	v_mac_f32_e32 v50, v21, v28
	v_mac_f32_e32 v49, v22, v28
	v_mac_f32_e32 v48, v7, v28
	v_mac_f32_e32 v44, v21, v30
	v_mac_f32_e32 v43, v22, v30
	v_mac_f32_e32 v41, v7, v30
	v_mac_f32_e32 v40, v21, v31
	v_mac_f32_e32 v39, v22, v31
	v_mac_f32_e32 v38, v7, v31
	v_mac_f32_e32 v37, v19, v32
	v_mac_f32_e32 v35, v21, v32
	v_mac_f32_e32 v36, v20, v32
	s_cbranch_vccnz BB0_101
; BB#102:                               ; %.lr.ph13.i.preheader.2
	s_mov_b32 m0, -1
	ds_read2_b32 v[25:26], v1 offset0:3 offset1:15
	v_mov_b32_e32 v0, 0
	v_add_i32_e32 v58, vcc, 0x37ec, v0
	v_add_i32_e32 v19, vcc, 0x3870, v0
	v_add_i32_e32 v3, vcc, 0x36e4, v0
	v_add_i32_e32 v5, vcc, 0x3768, v0
	v_add_i32_e32 v21, vcc, 0x38f4, v0
	v_add_i32_e32 v23, vcc, 0x3978, v0
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read_b32 v7, v13 offset:12
	ds_read_b32 v0, v12 offset:12
	ds_read2_b32 v[19:20], v19 offset1:4
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[23:24], v23 offset1:4
	ds_read2_b32 v[3:4], v3 offset1:4
	ds_read2_b32 v[5:6], v5 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v47, v25, v58
	v_mac_f32_e32 v45, v26, v59
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v46, v7, v59
        v_mac_f32_e32 v44, v25, v19
        v_mac_f32_e32 v43, v0, v19
        v_mac_f32_e32 v41, v7, v19
	v_mac_f32_e32 v45, v7, v58
	v_mac_f32_e32 v46, v0, v58
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v35, v7, v23
	v_mac_f32_e32 v36, v0, v23
	v_mac_f32_e32 v37, v0, v24
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v42, v25, v3
	v_mac_f32_e32 v52, v0, v3
	v_mac_f32_e32 v51, v7, v3
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v50, v25, v5
	v_mac_f32_e32 v49, v0, v5
	v_mac_f32_e32 v48, v7, v5
	v_mac_f32_e32 v40, v25, v21
	v_mac_f32_e32 v39, v0, v21
	v_mac_f32_e32 v38, v7, v21
	v_mac_f32_e32 v37, v25, v23
	v_mac_f32_e32 v42, v0, v4
	v_mac_f32_e32 v51, v26, v4
	v_mac_f32_e32 v50, v0, v6
	v_mac_f32_e32 v49, v7, v6
	v_mac_f32_e32 v48, v26, v6
	v_mac_f32_e32 v47, v0, v59
	v_mac_f32_e32 v44, v0, v20
	v_mac_f32_e32 v43, v7, v20
	v_mac_f32_e32 v41, v26, v20
	v_mac_f32_e32 v40, v0, v22
	v_mac_f32_e32 v39, v7, v22
	v_mac_f32_e32 v38, v26, v22
	v_mac_f32_e32 v35, v26, v24
	v_mac_f32_e32 v36, v7, v24
	v_mac_f32_e32 v52, v7, v4
	v_mov_b32_e32 v0, -12
	s_mov_b32 m0, -1
BB0_103:                                ; %.lr.ph13.i178.preheader.1.2
                                        ; =>This Inner Loop Header: Depth=1
	v_add_i32_e32 v7, vcc, v0, v2
	v_add_i32_e32 v3, vcc, 0x3710, v0
	v_add_i32_e32 v58, vcc, 0x3818, v0
	v_add_i32_e32 v5, vcc, 0x3730, v0
	v_add_i32_e32 v14, vcc, 0x3838, v0
	v_add_i32_e32 v16, vcc, 0x3920, v0
	v_add_i32_e32 v27, vcc, 0x37a4, v0
	v_add_i32_e32 v29, vcc, 0x38ac, v0
	v_add_i32_e32 v31, vcc, 0x3940, v0
	ds_read2_b32 v[19:20], v7 offset0:237 offset1:241
	ds_read2_b32 v[3:4], v3 offset1:4
	ds_read2_b32 v[21:22], v7 offset0:245 offset1:249
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read2_b32 v[5:6], v5 offset1:25
	ds_read_b32 v7, v7 offset:1012
	ds_read2_b32 v[23:24], v14 offset1:25
	ds_read2_b32 v[25:26], v16 offset1:4
	ds_read2_b32 v[27:28], v27 offset1:4
	ds_read2_b32 v[29:30], v29 offset1:4
	ds_read2_b32 v[31:32], v31 offset1:25
	v_add_i32_e32 v33, vcc, 0x39b4, v0

	v_add_i32_e32 v0, vcc, 4, v0
	v_cmp_ne_u32_e32 vcc, 0, v0
	s_and_b64 vcc, exec, vcc
	s_waitcnt lgkmcnt(9)
	v_mac_f32_e32 v42, v19, v3
	v_mac_f32_e32 v52, v20, v3
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v51, v21, v3
	v_mac_f32_e32 v42, v20, v4
	v_mac_f32_e32 v52, v21, v4
	v_mac_f32_e32 v51, v22, v4
	ds_read2_b32 v[3:4], v33 offset1:4
	v_mac_f32_e32 v47, v19, v58
	v_mac_f32_e32 v46, v20, v58
	v_mac_f32_e32 v45, v21, v58
	v_mac_f32_e32 v47, v20, v59
	v_mac_f32_e32 v46, v21, v59
	v_mac_f32_e32 v45, v22, v59
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v50, v19, v6
	v_mac_f32_e32 v49, v20, v6
	v_mac_f32_e32 v48, v21, v6
	v_mac_f32_e32 v42, v21, v5
	v_mac_f32_e32 v52, v22, v5
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v51, v7, v5
	v_mac_f32_e32 v47, v21, v23
	v_mac_f32_e32 v46, v22, v23
	v_mac_f32_e32 v45, v7,  v23
	v_mac_f32_e32 v44, v19, v24
	v_mac_f32_e32 v43, v20, v24
	v_mac_f32_e32 v41, v21, v24
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v40, v19, v25
	v_mac_f32_e32 v39, v20, v25
	v_mac_f32_e32 v38, v21, v25
	v_mac_f32_e32 v40, v20, v26
	v_mac_f32_e32 v39, v21, v26
	v_mac_f32_e32 v38, v22, v26
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v20, v3
	v_mac_f32_e32 v35, v22, v3
	v_mac_f32_e32 v36, v21, v3
	v_mac_f32_e32 v50, v20, v27
	v_mac_f32_e32 v49, v21, v27
	v_mac_f32_e32 v48, v22, v27
	v_mac_f32_e32 v37, v21, v4
	v_mac_f32_e32 v35, v7,  v4
	v_mac_f32_e32 v36, v22, v4
	v_mac_f32_e32 v44, v20, v29
	v_mac_f32_e32 v43, v21, v29
	v_mac_f32_e32 v41, v22, v29
	v_mac_f32_e32 v44, v21, v30
	v_mac_f32_e32 v43, v22, v30
	v_mac_f32_e32 v41, v7,  v30
	v_mac_f32_e32 v40, v21, v31
	v_mac_f32_e32 v39, v22, v31
	v_mac_f32_e32 v38, v7,  v31
	v_mac_f32_e32 v50, v21, v28
	v_mac_f32_e32 v49, v22, v28
	v_mac_f32_e32 v48, v7,  v28
	v_mac_f32_e32 v37, v19, v32
	v_mac_f32_e32 v35, v21, v32
	v_mac_f32_e32 v36, v20, v32
	s_cbranch_vccnz BB0_103
; BB#104:                               ; %.lr.ph13.i.preheader.1.2
	v_mov_b32_e32 v14, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v0, vcc, 0x3710, v14
	ds_read2_b32 v[23:24], v0 offset1:4
	v_add_i32_e32 v0, vcc, 0x3794, v14
	ds_read2_b32 v[21:22], v0 offset1:4
	v_add_i32_e32 v0, vcc, 0x3818, v14
	ds_read2_b32 v[19:20], v0 offset1:4
	v_add_i32_e32 v0, vcc, 0x389c, v14
	ds_read2_b32 v[17:18], v0 offset1:4
	v_add_i32_e32 v0, vcc, 0x3920, v14
	s_mul_i32 s8, s8, 0x2f440
	v_add_i32_e32 v9, vcc, s8, v9
	ds_read2_b32 v[6:7], v0 offset1:4
	v_add_i32_e32 v0, vcc, 0x39a4, v14
	ds_read2_b32 v[2:3], v0 offset1:4
	ds_read_b32 v5, v12 offset:948
	ds_read_b32 v4, v13 offset:948
	ds_read2_b32 v[0:1], v1 offset0:237 offset1:249
	v_add_i32_e32 v9, vcc, v9, v8
	s_mulk_i32 s7, 0x46e6
	v_add_i32_e32 v13, vcc, s7, v9
	v_cmp_lt_u32_e64 s[2:3], s9, 64
	s_and_b64 s[4:5], exec, s[0:1]
	v_lshlrev_b64 v[12:13], 2, v[13:14]
	s_and_b64 s[2:3], s[2:3], s[4:5]
	s_and_b64 s[4:5], exec, s[10:11]
	v_add_i32_e32 v12, vcc, s18, v12
	v_mov_b32_e32 v14, s19
	s_and_b64 s[12:13], s[4:5], s[2:3]
	v_cmp_gt_u32_e64 s[6:7], 55, v8
	v_addc_u32_e32 v13, vcc, v14, v13, vcc
	v_cndmask_b32_e64 v14, 0, -1, s[12:13]
	s_and_b64 s[2:3], s[12:13], s[6:7]
	s_and_saveexec_b64 s[4:5], s[2:3]
	s_xor_b64 s[2:3], exec, s[4:5]
	; mask branch BB0_106
BB0_105:
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v42, v0, v23
	v_mac_f32_e32 v42, v5, v24
	flat_store_dword v[12:13], v42
BB0_106:
	s_or_b64 exec, exec, s[2:3]
	v_cmp_gt_u32_e64 s[4:5], 55, v11
	v_cmp_ne_u32_e32 vcc, 0, v14
	s_and_b64 s[2:3], vcc, s[4:5]
	s_and_saveexec_b64 s[14:15], s[2:3]
	s_xor_b64 s[2:3], exec, s[14:15]
	; mask branch BB0_108
BB0_107:
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v52, v5, v23
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v52, v4, v24
	flat_store_dword v[12:13], v52 offset:4
BB0_108:
	s_or_b64 exec, exec, s[2:3]
	v_cmp_gt_u32_e64 s[2:3], 55, v10
	s_and_b64 s[12:13], exec, s[12:13]
	s_and_b64 s[12:13], s[12:13], s[2:3]
	s_and_saveexec_b64 s[14:15], s[12:13]
	s_xor_b64 s[12:13], exec, s[14:15]
	; mask branch BB0_110
BB0_109:
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v51, v4, v23
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v51, v1, v24
	flat_store_dword v[12:13], v51 offset:8
BB0_110:
	s_or_b64 exec, exec, s[12:13]
	s_or_b32 s8, s9, 1
	s_mul_i32 s12, s8, 0xbd1
	v_add_i32_e32 v8, vcc, s12, v9
	v_mov_b32_e32 v9, 0
	v_cmp_lt_u32_e64 s[12:13], s8, 64
	s_and_b64 s[14:15], exec, s[0:1]
	v_lshlrev_b64 v[9:10], 2, v[8:9]
	s_and_b64 s[12:13], s[12:13], s[14:15]
	s_and_b64 s[14:15], exec, s[10:11]
	s_and_b64 s[12:13], s[14:15], s[12:13]
	s_and_b64 s[14:15], exec, s[6:7]
	v_add_i32_e32 v9, vcc, s18, v9
	v_mov_b32_e32 v11, s19
	v_addc_u32_e32 v10, vcc, v11, v10, vcc
	s_and_b64 s[14:15], s[12:13], s[14:15]
	s_and_saveexec_b64 s[16:17], s[14:15]
	s_xor_b64 s[14:15], exec, s[16:17]
	; mask branch BB0_112
BB0_111:
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v50, v0, v21
	v_mac_f32_e32 v50, v5, v22
	flat_store_dword v[9:10], v50
BB0_112:
	s_or_b64 exec, exec, s[14:15]
	s_and_b64 s[14:15], exec, s[12:13]
	s_and_b64 s[16:17], exec, s[4:5]
	s_and_b64 s[14:15], s[14:15], s[16:17]
	s_and_saveexec_b64 s[16:17], s[14:15]
	s_xor_b64 s[14:15], exec, s[16:17]
	; mask branch BB0_114
BB0_113:
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v49, v5, v21
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v49, v4, v22
	flat_store_dword v[9:10], v49 offset:4
BB0_114:
	s_or_b64 exec, exec, s[14:15]
	s_and_b64 s[12:13], exec, s[12:13]
	s_and_b64 s[14:15], exec, s[2:3]
	s_and_b64 s[12:13], s[12:13], s[14:15]
	s_and_saveexec_b64 s[14:15], s[12:13]
	s_xor_b64 s[12:13], exec, s[14:15]
	; mask branch BB0_116
BB0_115:
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v48, v4, v21
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v48, v1, v22
	flat_store_dword v[9:10], v48 offset:8
BB0_116:
	s_or_b64 exec, exec, s[12:13]
	s_add_i32 s8, s9, 2
	v_add_i32_e32 v8, vcc, 0xbd1, v8
	v_mov_b32_e32 v9, 0
	v_cmp_lt_u32_e64 s[12:13], s8, 64
	s_and_b64 s[14:15], exec, s[0:1]
	v_lshlrev_b64 v[9:10], 2, v[8:9]
	s_and_b64 s[12:13], s[12:13], s[14:15]
	s_and_b64 s[14:15], exec, s[10:11]
	s_and_b64 s[12:13], s[14:15], s[12:13]
	s_and_b64 s[14:15], exec, s[6:7]
	v_add_i32_e32 v9, vcc, s18, v9
	v_mov_b32_e32 v11, s19
	v_addc_u32_e32 v10, vcc, v11, v10, vcc
	s_and_b64 s[14:15], s[12:13], s[14:15]
	s_and_saveexec_b64 s[16:17], s[14:15]
	s_xor_b64 s[14:15], exec, s[16:17]
	; mask branch BB0_118
BB0_117:
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v47, v0, v19
	v_mac_f32_e32 v47, v5, v20
	flat_store_dword v[9:10], v47
BB0_118:
	s_or_b64 exec, exec, s[14:15]
	s_and_b64 s[14:15], exec, s[12:13]
	s_and_b64 s[16:17], exec, s[4:5]
	s_and_b64 s[14:15], s[14:15], s[16:17]
	s_and_saveexec_b64 s[16:17], s[14:15]
	s_xor_b64 s[14:15], exec, s[16:17]
	; mask branch BB0_120
BB0_119:
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v46, v5, v19
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v46, v4, v20
	flat_store_dword v[9:10], v46 offset:4
BB0_120:
	s_or_b64 exec, exec, s[14:15]
	s_and_b64 s[12:13], exec, s[12:13]
	s_and_b64 s[14:15], exec, s[2:3]
	s_and_b64 s[12:13], s[12:13], s[14:15]
	s_and_saveexec_b64 s[14:15], s[12:13]
	s_xor_b64 s[12:13], exec, s[14:15]
	; mask branch BB0_122
BB0_121:
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v45, v4, v19
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v45, v1, v20
	flat_store_dword v[9:10], v45 offset:8
BB0_122:
	s_or_b64 exec, exec, s[12:13]
	s_add_i32 s8, s9, 3
	v_add_i32_e32 v8, vcc, 0xbd1, v8
	v_mov_b32_e32 v9, 0
	v_cmp_lt_u32_e64 s[12:13], s8, 64
	s_and_b64 s[14:15], exec, s[0:1]
	v_lshlrev_b64 v[9:10], 2, v[8:9]
	s_and_b64 s[12:13], s[12:13], s[14:15]
	s_and_b64 s[14:15], exec, s[10:11]
	s_and_b64 s[12:13], s[14:15], s[12:13]
	s_and_b64 s[14:15], exec, s[6:7]
	v_add_i32_e32 v9, vcc, s18, v9
	v_mov_b32_e32 v11, s19
	v_addc_u32_e32 v10, vcc, v11, v10, vcc
	s_and_b64 s[14:15], s[12:13], s[14:15]
	s_and_saveexec_b64 s[16:17], s[14:15]
	s_xor_b64 s[14:15], exec, s[16:17]
	; mask branch BB0_124
BB0_123:
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v44, v0, v17
	v_mac_f32_e32 v44, v5, v18
	flat_store_dword v[9:10], v44
BB0_124:
	s_or_b64 exec, exec, s[14:15]
	s_and_b64 s[14:15], exec, s[12:13]
	s_and_b64 s[16:17], exec, s[4:5]
	s_and_b64 s[14:15], s[14:15], s[16:17]
	s_and_saveexec_b64 s[16:17], s[14:15]
	s_xor_b64 s[14:15], exec, s[16:17]
	; mask branch BB0_126
BB0_125:
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v43, v5, v17
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v43, v4, v18
	flat_store_dword v[9:10], v43 offset:4
BB0_126:
	s_or_b64 exec, exec, s[14:15]
	s_and_b64 s[12:13], exec, s[12:13]
	s_and_b64 s[14:15], exec, s[2:3]
	s_and_b64 s[12:13], s[12:13], s[14:15]
	s_and_saveexec_b64 s[14:15], s[12:13]
	s_xor_b64 s[12:13], exec, s[14:15]
	; mask branch BB0_128
BB0_127:
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v41, v4, v17
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v41, v1, v18
	flat_store_dword v[9:10], v41 offset:8
BB0_128:
	s_or_b64 exec, exec, s[12:13]
	s_add_i32 s8, s9, 4
	v_add_i32_e32 v8, vcc, 0xbd1, v8
	v_mov_b32_e32 v9, 0
	v_cmp_lt_u32_e64 s[12:13], s8, 64
	s_and_b64 s[14:15], exec, s[0:1]
	v_lshlrev_b64 v[9:10], 2, v[8:9]
	s_and_b64 s[12:13], s[12:13], s[14:15]
	s_and_b64 s[14:15], exec, s[10:11]
	s_and_b64 s[12:13], s[14:15], s[12:13]
	s_and_b64 s[14:15], exec, s[6:7]
	v_add_i32_e32 v9, vcc, s18, v9
	v_mov_b32_e32 v11, s19
	v_addc_u32_e32 v10, vcc, v11, v10, vcc
	s_and_b64 s[14:15], s[12:13], s[14:15]
	s_and_saveexec_b64 s[16:17], s[14:15]
	s_xor_b64 s[14:15], exec, s[16:17]
	; mask branch BB0_130
BB0_129:
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v0, v6
	v_mac_f32_e32 v40, v5, v7
	flat_store_dword v[9:10], v40
BB0_130:
	s_or_b64 exec, exec, s[14:15]
	s_and_b64 s[14:15], exec, s[12:13]
	s_and_b64 s[16:17], exec, s[4:5]
	s_and_b64 s[14:15], s[14:15], s[16:17]
	s_and_saveexec_b64 s[16:17], s[14:15]
	s_xor_b64 s[14:15], exec, s[16:17]
	; mask branch BB0_132
BB0_131:
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v39, v5, v6
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v39, v4, v7
	flat_store_dword v[9:10], v39 offset:4
BB0_132:
	s_or_b64 exec, exec, s[14:15]
	s_and_b64 s[12:13], exec, s[12:13]
	s_and_b64 s[14:15], exec, s[2:3]
	s_and_b64 s[12:13], s[12:13], s[14:15]
	s_and_saveexec_b64 s[14:15], s[12:13]
	s_xor_b64 s[12:13], exec, s[14:15]
	; mask branch BB0_134
BB0_133:
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v38, v4, v6
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v38, v1, v7
	flat_store_dword v[9:10], v38 offset:8
BB0_134:
	s_or_b64 exec, exec, s[12:13]
	s_add_i32 s9, s9, 5
	s_waitcnt lgkmcnt(4)
	v_add_i32_e32 v6, vcc, 0xbd1, v8
	v_mov_b32_e32 v7, 0
	v_cmp_lt_u32_e64 s[8:9], s9, 64
	s_and_b64 s[0:1], exec, s[0:1]
	v_lshlrev_b64 v[6:7], 2, v[6:7]
	s_and_b64 s[0:1], s[8:9], s[0:1]
	s_and_b64 s[8:9], exec, s[10:11]
	v_add_i32_e32 v6, vcc, s18, v6
	v_mov_b32_e32 v8, s19
	s_and_b64 s[0:1], s[8:9], s[0:1]
	s_and_b64 s[6:7], exec, s[6:7]
	v_addc_u32_e32 v7, vcc, v8, v7, vcc
	s_and_b64 s[6:7], s[0:1], s[6:7]
	s_and_saveexec_b64 s[8:9], s[6:7]
	s_xor_b64 s[6:7], exec, s[8:9]
	; mask branch BB0_136
BB0_135:
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v37, v0, v2
	v_mac_f32_e32 v37, v5, v3
	flat_store_dword v[6:7], v37
BB0_136:
	s_or_b64 exec, exec, s[6:7]
	s_and_b64 s[6:7], exec, s[0:1]
	s_and_b64 s[4:5], exec, s[4:5]
	s_and_b64 s[4:5], s[6:7], s[4:5]
	s_and_saveexec_b64 s[6:7], s[4:5]
	s_xor_b64 s[4:5], exec, s[6:7]
	; mask branch BB0_138
BB0_137:
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v36, v5, v2
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v36, v4, v3
	flat_store_dword v[6:7], v36 offset:4
BB0_138:
	s_or_b64 exec, exec, s[4:5]
	s_and_b64 s[0:1], exec, s[0:1]
	s_and_b64 s[2:3], exec, s[2:3]
	s_and_b64 s[0:1], s[0:1], s[2:3]
	s_and_saveexec_b64 s[2:3], s[0:1]
	s_xor_b64 s[0:1], exec, s[2:3]
	; mask branch BB0_140
BB0_139:
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v35, v4, v2
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v35, v1, v3
	flat_store_dword v[6:7], v35 offset:8
BB0_140:
	s_or_b64 exec, exec, s[0:1]
	s_endpgm
.Lfunc_end0:
	.size	MIOpenCvFwd11x11, .Lfunc_end0-MIOpenCvFwd11x11
                                        ; -- End function
	.section	.AMDGPU.csdata
; Kernel info:
; codeLenInByte = 17360
; NumSgprs: 36
; NumVgprs: 80
; ScratchSize: 0
; codeLenInByte = 17360
; NumSgprs: 36
; NumVgprs: 80
; FloatMode: 192
; IeeeMode: 1
; ScratchSize: 0
; LDSByteSize: 14832 bytes/workgroup (compile time only)
; SGPRBlocks: 4
; VGPRBlocks: 19
; NumSGPRsForWavesPerEU: 36
; NumVGPRsForWavesPerEU: 80
; ReservedVGPRFirst: 0
; ReservedVGPRCount: 0
; COMPUTE_PGM_RSRC2:USER_SGPR: 6
; COMPUTE_PGM_RSRC2:TRAP_HANDLER: 1
; COMPUTE_PGM_RSRC2:TGID_X_EN: 1
; COMPUTE_PGM_RSRC2:TGID_Y_EN: 1
; COMPUTE_PGM_RSRC2:TGID_Z_EN: 1
; COMPUTE_PGM_RSRC2:TIDIG_COMP_CNT: 0
	.text
	.globl	MIOpenCvFwd11x11_2      ; -- Begin function MIOpenCvFwd11x11_2
	.p2align	8
	.type	MIOpenCvFwd11x11_2,@function
	.amdgpu_hsa_kernel MIOpenCvFwd11x11_2
MIOpenCvFwd11x11_2:                     ; @MIOpenCvFwd11x11_2
	.amd_kernel_code_t
		amd_code_version_major = 1
		amd_code_version_minor = 1
		amd_machine_kind = 1
		amd_machine_version_major = 9
		amd_machine_version_minor = 0
		amd_machine_version_stepping = 0
		kernel_code_entry_byte_offset = 256
		kernel_code_prefetch_byte_size = 0
		max_scratch_backing_memory_byte_size = 0
		granulated_workitem_vgpr_count = 20
		granulated_wavefront_sgpr_count = 6
		priority = 0
		float_mode = 192
		priv = 0
		enable_dx10_clamp = 1
		debug_mode = 0
		enable_ieee_mode = 1
		enable_sgpr_private_segment_wave_byte_offset = 0
		user_sgpr_count = 6
		enable_trap_handler = 1
		enable_sgpr_workgroup_id_x = 1
		enable_sgpr_workgroup_id_y = 1
		enable_sgpr_workgroup_id_z = 1
		enable_sgpr_workgroup_info = 0
		enable_vgpr_workitem_id = 0
		enable_exception_msb = 0
		granulated_lds_size = 0
		enable_exception = 0
		enable_sgpr_private_segment_buffer = 1
		enable_sgpr_dispatch_ptr = 0
		enable_sgpr_queue_ptr = 0
		enable_sgpr_kernarg_segment_ptr = 1
		enable_sgpr_dispatch_id = 0
		enable_sgpr_flat_scratch_init = 0
		enable_sgpr_private_segment_size = 0
		enable_sgpr_grid_workgroup_count_x = 0
		enable_sgpr_grid_workgroup_count_y = 0
		enable_sgpr_grid_workgroup_count_z = 0
		enable_ordered_append_gds = 0
		private_element_size = 1
		is_ptr64 = 1
		is_dynamic_callstack = 0
		is_debug_enabled = 0
		is_xnack_enabled = 0
		workitem_private_segment_byte_size = 0
		workgroup_group_segment_byte_size = 19512
		gds_segment_byte_size = 0
		kernarg_segment_byte_size = 64
		workgroup_fbarrier_count = 0
		wavefront_sgpr_count = 50
		workitem_vgpr_count = 82
		reserved_vgpr_first = 0
		reserved_vgpr_count = 0
		reserved_sgpr_first = 0
		reserved_sgpr_count = 0
		debug_wavefront_private_segment_offset_sgpr = 0
		debug_private_segment_buffer_sgpr = 0
		kernarg_segment_alignment = 4
		group_segment_alignment = 4
		private_segment_alignment = 4
		wavefront_size = 6
		call_convention = -1
		runtime_loader_kernel_symbol = 0
	.end_amd_kernel_code_t
; BB#0:                                 ; %.lr.ph.preheader
	s_load_dwordx2 s[20:21], s[4:5], 0x0
	s_load_dwordx2 s[24:25], s[4:5], 0x8
	s_load_dwordx2 s[18:19], s[4:5], 0x10
	s_mul_i32 s23, s8, 0x93000
	s_lshl_b32 s22, s8, 2
	s_mul_i32 s6, s7, 6
	s_mul_i32 s8, s7, 0x882
	s_add_i32 s26, s23, 0xb440
	v_lshlrev_b32_e32 v1, 2, v0
	s_mov_b64 s[2:3], 0
	v_mov_b32_e32 v2, v0
	s_mov_b32 m0, -1
BB1_1:                                  ; %.lr.ph
                                        ; =>This Inner Loop Header: Depth=1
	v_add_i32_e32 v2, vcc, 0x100, v2
	v_mov_b32_e32 v3, 0x130d
	v_cmp_gt_u32_e64 s[0:1], v2, v3
	v_mov_b32_e32 v3, 0
	ds_write_b32 v1, v3
	v_add_i32_e32 v1, vcc, 0x400, v1
	s_or_b64 s[2:3], s[0:1], s[2:3]
	s_andn2_b64 exec, exec, s[2:3]
	s_cbranch_execnz BB1_1
; BB#2:                                 ; %.preheader500
	s_or_b64 exec, exec, s[2:3]
	v_cvt_f32_ubyte0_e32 v1, v0
	v_mov_b32_e32 v3, 0x3727c5ac
	s_mov_b32 s10, 0xffffff
	s_movk_i32 s0, 0xffc7
	v_mac_f32_e32 v3, 0x3c8fb824, v1
	v_cvt_u32_f32_e32 v11, v3
	v_mov_b32_e32 v3, 0x3727c5ac
	v_mov_b32_e32 v6, 0x3727c5ac
	v_mac_f32_e32 v6, 0x3eaaaaab, v1
	v_and_b32_e32 v4, s10, v11
	v_mul_lo_i32 v4, v4, s0
	s_movk_i32 s0, 0xffed
	v_cvt_u32_f32_e32 v8, v6
	v_mov_b32_e32 v2, 0x3727c5ac
	v_add_i32_e32 v4, vcc, v4, v0
	v_cvt_f32_u32_e32 v5, v4
	v_mac_f32_e32 v2, 0.5, v1
	v_cvt_u32_f32_e32 v7, v2
	s_movk_i32 s1, 0x16b
	v_mac_f32_e32 v3, 0x3d579436, v5
	v_cvt_u32_f32_e32 v15, v3
	v_mul_lo_i32 v6, v8, s1
	v_mul_lo_i32 v1, v7, s1
	v_add_i32_e32 v3, vcc, s6, v8
	v_and_b32_e32 v5, s10, v15
	v_mul_lo_i32 v5, v5, s0
	s_movk_i32 s11, 0x492
	s_movk_i32 s12, 0xea
	v_cmp_gt_u32_e64 s[4:5], 64, v3
	v_add_i32_e32 v10, vcc, v5, v4
	v_and_b32_e32 v5, s10, v8
	v_mul_lo_i32 v5, v5, -3
	v_mul_lo_i32 v4, v8, 33
	v_mul_lo_i32 v2, v11, s11
	v_mul_lo_i32 v3, v15, s12
	v_add_i32_e32 v5, vcc, v5, v0
	v_mul_lo_i32 v9, v5, 11
	v_add_i32_e32 v14, vcc, s8, v6
	v_add_i32_e32 v6, vcc, s8, v1
	s_movk_i32 s8, 0x1248
	v_add_i32_e32 v4, vcc, v4, v9
	v_mul_lo_i32 v16, v11, s8
	s_movk_i32 s8, 0x3a8
	v_add_i32_e32 v17, vcc, v3, v2
	v_lshlrev_b32_e32 v4, 2, v4
	v_mul_lo_i32 v19, v15, s8
	v_lshlrev_b32_e32 v8, 2, v5
	v_add_i32_e32 v9, vcc, 0x4920, v4
	v_mad_u32_u24 v4, v0, 12, v17
	v_mul_u32_u24_e32 v5, 0xe4, v15
	v_mul_lo_i32 v2, v10, 12
	v_subrev_i32_e32 v4, vcc, v5, v4
	v_mul_u32_u24_e32 v5, 0x2ac, v11
	v_subrev_i32_e32 v4, vcc, v5, v4
	v_add_i32_e32 v12, vcc, 52, v15
	v_lshlrev_b32_e32 v5, 2, v4
	v_add_i32_e32 v4, vcc, v19, v16
	v_mul_lo_i32 v1, v12, 55
	v_cmp_gt_u32_e64 s[0:1], 3, v15
	v_add_i32_e32 v18, vcc, 3, v17
	v_mad_u32_u24 v4, v0, 48, v4
	v_mul_u32_u24_e32 v15, 0x390, v15
	v_add_i32_e32 v3, vcc, v17, v2
	v_add_i32_e32 v2, vcc, v18, v2
	v_subrev_i32_e32 v4, vcc, v15, v4
	v_mul_u32_u24_e32 v15, 0xab0, v11
	v_subrev_i32_e32 v4, vcc, v15, v4
	v_cmp_gt_u32_e64 s[2:3], 18, v0
	v_add_i32_e32 v13, vcc, s6, v7
	v_lshlrev_b32_e32 v3, 2, v3
	v_lshlrev_b32_e32 v2, 2, v2
	v_mov_b32_e32 v42, 0
	v_mov_b32_e32 v15, 0
	s_movk_i32 s13, 0xff8d
	s_movk_i32 s14, 0xffe9
	s_movk_i32 s15, 0x73
	s_mov_b32 s28, 0
	s_mov_b32 s16, 0x24c00
	s_movk_i32 s17, 0xe0
	v_mov_b32_e32 v16, 0
	v_mov_b32_e32 v19, 0
	v_mov_b32_e32 v38, 0
	v_mov_b32_e32 v39, 0
	v_mov_b32_e32 v40, 0
	v_mov_b32_e32 v41, 0
	v_mov_b32_e32 v43, 0
	v_mov_b32_e32 v44, 0
	v_mov_b32_e32 v45, 0
	v_mov_b32_e32 v46, 0
	v_mov_b32_e32 v47, 0
	v_mov_b32_e32 v48, 0
	v_mov_b32_e32 v49, 0
	v_mov_b32_e32 v50, 0
	v_mov_b32_e32 v51, 0
	v_mov_b32_e32 v52, 0
	v_mov_b32_e32 v53, 0
BB1_3:                                  ; =>This Loop Header: Depth=1
                                        ;     Child Loop BB1_7 Depth 2
                                        ;     Child Loop BB1_21 Depth 2
                                        ;     Child Loop BB1_23 Depth 2
                                        ;     Child Loop BB1_25 Depth 2
	s_waitcnt lgkmcnt(0)
	s_barrier
	v_cmp_eq_u32_e32 vcc, 3, v15
	v_mov_b32_e32 v36, v53
	v_mov_b32_e32 v35, v52
	v_mov_b32_e32 v34, v51
	v_mov_b32_e32 v33, v50
	v_mov_b32_e32 v32, v49
	v_mov_b32_e32 v31, v48
	v_mov_b32_e32 v30, v47
	v_mov_b32_e32 v29, v46
	v_mov_b32_e32 v28, v45
	v_mov_b32_e32 v27, v44
	v_mov_b32_e32 v26, v43
	v_mov_b32_e32 v25, v41
	v_mov_b32_e32 v24, v40
	v_mov_b32_e32 v23, v39
	v_mov_b32_e32 v22, v38
	v_mov_b32_e32 v21, v19
	v_mov_b32_e32 v20, v16
	v_mov_b32_e32 v37, v42
	s_and_b64 vcc, exec, vcc
	v_mov_b32_e32 v54, -1
                                        ; implicit-def: %VGPR53
                                        ; implicit-def: %VGPR52
                                        ; implicit-def: %VGPR51
                                        ; implicit-def: %VGPR50
                                        ; implicit-def: %VGPR49
                                        ; implicit-def: %VGPR48
                                        ; implicit-def: %VGPR47
                                        ; implicit-def: %VGPR46
                                        ; implicit-def: %VGPR45
                                        ; implicit-def: %VGPR44
                                        ; implicit-def: %VGPR43
                                        ; implicit-def: %VGPR41
                                        ; implicit-def: %VGPR40
                                        ; implicit-def: %VGPR39
                                        ; implicit-def: %VGPR38
                                        ; implicit-def: %VGPR19
                                        ; implicit-def: %VGPR16
                                        ; implicit-def: %VGPR42
	s_cbranch_vccnz BB1_27
; BB#4:                                 ;   in Loop: Header=BB1_3 Depth=1
	s_and_saveexec_b64 s[8:9], s[2:3]
	s_xor_b64 s[30:31], exec, s[8:9]
	; mask branch BB1_6
	s_cbranch_execz BB1_6
BB1_5:                                  ; %.lr.ph.i288
                                        ;   in Loop: Header=BB1_3 Depth=1
	v_add_i32_e32 v16, vcc, v15, v8
	v_mul_lo_i32 v19, v16, 11
	v_cmp_gt_u32_e32 vcc, 11, v16
	s_and_b64 s[8:9], exec, s[4:5]
	s_and_b64 s[8:9], s[8:9], vcc
	v_add_i32_e32 v16, vcc, v14, v19
	v_ashrrev_i32_e32 v19, 31, v16
	v_cndmask_b32_e64 v38, 0, v16, s[8:9]
	v_cndmask_b32_e64 v39, 0, v19, s[8:9]
	v_lshlrev_b64 v[38:39], 2, v[38:39]
	v_add_i32_e32 v46, vcc, s24, v38
	v_mov_b32_e32 v16, s25
	v_addc_u32_e32 v47, vcc, v16, v39, vcc
	flat_load_dwordx4 v[38:41], v[46:47]
	flat_load_dwordx4 v[42:45], v[46:47] offset:16
	flat_load_dword v16, v[46:47] offset:40
	flat_load_dwordx2 v[46:47], v[46:47] offset:32
	s_mov_b32 m0, -1
	s_waitcnt vmcnt(3) lgkmcnt(3)
	ds_write2_b32 v9, v38, v39 offset1:1
	ds_write2_b32 v9, v40, v41 offset0:2 offset1:3
	s_waitcnt vmcnt(2) lgkmcnt(4)
	ds_write2_b32 v9, v42, v43 offset0:4 offset1:5
	ds_write2_b32 v9, v44, v45 offset0:6 offset1:7
	s_waitcnt vmcnt(0) lgkmcnt(4)
	ds_write2_b32 v9, v46, v47 offset0:8 offset1:9
	ds_write_b32 v9, v16 offset:40
BB1_6:                                  ; %.lr.ph.i290.preheader
                                        ;   in Loop: Header=BB1_3 Depth=1
	s_or_b64 exec, exec, s[30:31]
	s_mov_b64 s[32:33], 0
	v_mov_b32_e32 v16, v0
BB1_7:                                  ; %.lr.ph.i290
                                        ;   Parent Loop BB1_3 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_cvt_f32_u32_e32 v19, v16
	v_mov_b32_e32 v38, 0x3727c5ac
	s_waitcnt vmcnt(0) lgkmcnt(0)
	v_mov_b32_e32 v40, 0x3727c5ac
	v_mov_b32_e32 v41, 0xffffff31
	v_mac_f32_e32 v38, 0x3c0e7835, v19
	v_cvt_u32_f32_e32 v19, v38
	v_mov_b32_e32 v49, 0
	v_and_b32_e32 v39, s10, v19
	v_mul_lo_i32 v38, v39, s13
	v_add_i32_e32 v38, vcc, v38, v16
	v_cvt_f32_u32_e32 v38, v38
	v_mac_f32_e32 v40, 0x3d321643, v38
	v_cvt_u32_f32_e32 v38, v40
	v_mov_b32_e32 v40, 0xffffff32
	v_lshlrev_b32_e32 v42, 2, v38
	v_add_i32_e32 v46, vcc, v42, v15
	v_cmp_lt_i32_e32 vcc, v46, v40
	v_cmp_gt_i32_e64 s[8:9], v46, v41
	v_cndmask_b32_e64 v40, 0, -1, vcc
	s_and_saveexec_b64 s[30:31], s[8:9]
	s_xor_b64 s[30:31], exec, s[30:31]
	; mask branch BB1_9
BB1_8:                                  ;   in Loop: Header=BB1_7 Depth=2
	v_cmp_lt_i32_e32 vcc, 17, v46
	v_cmp_lt_u32_e64 s[8:9], 3, v19
	s_or_b64 s[8:9], s[8:9], vcc
	v_add_i32_e32 v40, vcc, s22, v19
	v_mov_b32_e32 v41, 0x7f
	v_cmp_gt_u32_e32 vcc, v40, v41
	s_or_b64 s[8:9], vcc, s[8:9]
	v_mov_b32_e32 v49, -1
	v_cndmask_b32_e64 v40, 0, -1, s[8:9]
BB1_9:                                  ; %Flow4643
                                        ;   in Loop: Header=BB1_7 Depth=2
	s_or_b64 exec, exec, s[30:31]
	v_and_b32_e32 v41, s10, v38
	v_mul_lo_i32 v39, v39, s15
	v_mul_lo_i32 v41, v41, s14
	v_mov_b32_e32 v48, 0
                                        ; implicit-def: %VGPR42_VGPR43_VGPR44_VGPR45
	v_sub_i32_e32 v39, vcc, v41, v39
	v_add_i32_e32 v47, vcc, v39, v16
	v_cmp_ne_u32_e32 vcc, 0, v40
                                        ; implicit-def: %VGPR40_VGPR41
                                        ; implicit-def: %VGPR39
	s_and_saveexec_b64 s[8:9], vcc
	s_xor_b64 s[8:9], exec, s[8:9]
	; mask branch BB1_13
	s_cbranch_execz BB1_13
BB1_10:                                 ; %.loopexit3.i295
                                        ;   in Loop: Header=BB1_7 Depth=2
	v_cmp_gt_u32_e32 vcc, 4, v19
	v_mov_b32_e32 v48, 0
                                        ; implicit-def: %VGPR40_VGPR41
                                        ; implicit-def: %VGPR42_VGPR43_VGPR44_VGPR45
                                        ; implicit-def: %VGPR39
	s_and_saveexec_b64 s[30:31], vcc
	s_xor_b64 s[30:31], exec, s[30:31]
	; mask branch BB1_12
	s_cbranch_execz BB1_12
BB1_11:                                 ; %.loopexit3.i295..loopexit.loopexit12.i296_crit_edge
                                        ;   in Loop: Header=BB1_7 Depth=2
	v_mul_lo_i32 v39, v47, 10
	s_mov_b32 s36, s28
	s_mov_b32 s37, s28
	s_mov_b32 s38, s28
	s_mov_b32 s39, s28
	s_mov_b32 s34, s28
	s_mov_b32 s35, s28
	v_mov_b32_e32 v45, s39
	v_mov_b32_e32 v41, s35
	v_mov_b32_e32 v48, -1
	v_mov_b32_e32 v40, s34
	v_mov_b32_e32 v44, s38
	v_mov_b32_e32 v43, s37
	v_mov_b32_e32 v42, s36
BB1_12:                                 ; %Flow4645
                                        ;   in Loop: Header=BB1_7 Depth=2
	s_or_b64 exec, exec, s[30:31]
	v_mov_b32_e32 v49, 0
BB1_13:                                 ; %Flow4644
                                        ;   in Loop: Header=BB1_7 Depth=2
	s_or_b64 exec, exec, s[8:9]
	v_cmp_ne_u32_e32 vcc, 0, v49
	v_mov_b32_e32 v52, v45
	v_mov_b32_e32 v51, v44
	v_mov_b32_e32 v50, v43
	v_mov_b32_e32 v49, v42
	s_and_saveexec_b64 s[8:9], vcc
	s_xor_b64 s[8:9], exec, s[8:9]
	; mask branch BB1_17
	s_cbranch_execz BB1_17
BB1_14:                                 ;   in Loop: Header=BB1_7 Depth=2
	v_mul_lo_i32 v40, v19, s16
	v_mul_lo_i32 v41, v46, s17
	v_mul_lo_i32 v39, v47, 10
	s_mov_b32 s29, s28
	v_add_i32_e32 v40, vcc, s26, v40
	v_add_i32_e32 v40, vcc, v40, v41
	v_add_i32_e32 v40, vcc, v40, v39
	v_ashrrev_i32_e32 v41, 31, v40
	v_lshlrev_b64 v[40:41], 2, v[40:41]
	v_add_i32_e32 v53, vcc, s20, v40
	v_mov_b32_e32 v40, s21
	v_addc_u32_e32 v54, vcc, v40, v41, vcc
	flat_load_dwordx4 v[49:52], v[53:54]
	s_mov_b32 s30, s28
	s_mov_b32 s31, s28
	s_mov_b32 s34, s28
	s_mov_b32 s35, s28
	v_mov_b32_e32 v45, s31
	v_mov_b32_e32 v41, s35
	v_cmp_ne_u32_e32 vcc, 22, v47
	v_mov_b32_e32 v44, s30
	v_mov_b32_e32 v43, s29
	v_mov_b32_e32 v42, s28
	v_mov_b32_e32 v40, s34
	s_and_saveexec_b64 s[30:31], vcc
	s_xor_b64 s[30:31], exec, s[30:31]
	; mask branch BB1_16
BB1_15:                                 ; %.preheader4.preheader.i294
                                        ;   in Loop: Header=BB1_7 Depth=2
	flat_load_dwordx4 v[42:45], v[53:54] offset:16
	flat_load_dwordx2 v[40:41], v[53:54] offset:32
BB1_16:                                 ; %Flow4642
                                        ;   in Loop: Header=BB1_7 Depth=2
	s_or_b64 exec, exec, s[30:31]
	v_mov_b32_e32 v48, -1
BB1_17:                                 ; %Flow4646
                                        ;   in Loop: Header=BB1_7 Depth=2
	s_or_b64 exec, exec, s[8:9]
	v_cmp_ne_u32_e32 vcc, 0, v48
	s_and_saveexec_b64 s[8:9], vcc
	s_xor_b64 s[8:9], exec, s[8:9]
	; mask branch BB1_19
	s_cbranch_execz BB1_19
BB1_18:                                 ; %.loopexit.loopexit12.i296
                                        ;   in Loop: Header=BB1_7 Depth=2
	v_mul_lo_i32 v19, v19, s11
	v_mul_lo_i32 v38, v38, s12
	s_mov_b32 m0, -1
	v_add_i32_e32 v19, vcc, v38, v19
	v_add_i32_e32 v19, vcc, v19, v39
	v_lshlrev_b32_e32 v19, 2, v19
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write2_b32 v19, v49, v50 offset0:2 offset1:3
	ds_write2_b32 v19, v51, v52 offset0:4 offset1:5
	ds_write2_b32 v19, v42, v43 offset0:6 offset1:7
	ds_write2_b32 v19, v44, v45 offset0:8 offset1:9
	ds_write2_b32 v19, v40, v41 offset0:10 offset1:11
BB1_19:                                 ; %.loopexit.i297
                                        ;   in Loop: Header=BB1_7 Depth=2
	s_or_b64 exec, exec, s[8:9]
	v_add_i32_e32 v16, vcc, 0x100, v16
	v_mov_b32_e32 v19, 0x1cb
	v_cmp_gt_u32_e32 vcc, v16, v19
	s_or_b64 s[32:33], vcc, s[32:33]
	s_andn2_b64 exec, exec, s[32:33]
	s_cbranch_execnz BB1_7
; BB#20:                                ; %fetchData2.6.exit298
                                        ;   in Loop: Header=BB1_3 Depth=1
	s_or_b64 exec, exec, s[32:33]
	s_waitcnt vmcnt(0) lgkmcnt(0)
	s_barrier
	v_mov_b32_e32 v54, -12
	v_mov_b32_e32 v16, v20
	v_mov_b32_e32 v19, v21
	v_mov_b32_e32 v38, v22
	v_mov_b32_e32 v39, v23
	v_mov_b32_e32 v40, v24
	v_mov_b32_e32 v41, v25
	v_mov_b32_e32 v43, v26
	v_mov_b32_e32 v44, v27
	v_mov_b32_e32 v45, v28
	v_mov_b32_e32 v46, v29
	v_mov_b32_e32 v47, v30
	v_mov_b32_e32 v48, v31
	v_mov_b32_e32 v49, v32
	v_mov_b32_e32 v50, v33
	v_mov_b32_e32 v51, v34
	v_mov_b32_e32 v52, v35
	v_mov_b32_e32 v53, v36
	v_mov_b32_e32 v42, v37
BB1_21:                                 ; %.lr.ph13.i302.preheader
                                        ;   Parent Loop BB1_3 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	s_mov_b32 m0, -1
	v_add_i32_e32 v55, vcc, 0x492c, v54
	v_add_i32_e32 v57, vcc, 0x494c, v54
	v_add_i32_e32 v59, vcc, 0x4a34, v54
	v_add_i32_e32 v63, vcc, 0x4a54, v54
	v_add_i32_e32 v67, vcc, 0x4b3c, v54
	v_add_i32_e32 v69, vcc, 0x4b5c, v54
	v_add_i32_e32 v77, vcc, v54, v5
	v_add_i32_e32 v61, vcc, 0x49c0, v54
	v_add_i32_e32 v65, vcc, 0x4ac8, v54
	v_add_i32_e32 v71, vcc, 0x4bd0, v54
	ds_read2_b32 v[55:56], v55 offset1:4
	ds_read2_b32 v[73:74], v77 offset0:3 offset1:7
	ds_read2_b32 v[57:58], v57 offset1:25
	ds_read2_b32 v[75:76], v77 offset0:11 offset1:15
	ds_read2_b32 v[59:60], v59 offset1:4
	ds_read2_b32 v[63:64], v63 offset1:25
	ds_read2_b32 v[67:68], v67 offset1:4
	ds_read2_b32 v[69:70], v69 offset1:25
	ds_read2_b32 v[61:62], v61 offset1:4
	ds_read2_b32 v[65:66], v65 offset1:4
	ds_read2_b32 v[71:72], v71 offset1:4
	ds_read_b32 v77, v77 offset:76
	v_add_i32_e32 v54, vcc, 4, v54
	s_waitcnt lgkmcnt(10)
	v_mac_f32_e32 v42, v73, v55
	v_mac_f32_e32 v53, v74, v55
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v52, v75, v55
	v_mac_f32_e32 v51, v73, v58
	v_mac_f32_e32 v50, v74, v58
	v_mac_f32_e32 v49, v75, v58
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v48, v73, v59
	v_mac_f32_e32 v47, v74, v59
	v_mac_f32_e32 v46, v75, v59
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v45, v73, v64
	v_mac_f32_e32 v44, v74, v64
	v_mac_f32_e32 v43, v75, v64
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v41, v73, v67
	v_mac_f32_e32 v40, v74, v67
	v_mac_f32_e32 v39, v75, v67
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v38, v73, v70
	v_mac_f32_e32 v16, v75, v70
	v_mac_f32_e32 v19, v74, v70
	v_cmp_eq_u32_e32 vcc, 0, v54
	v_mac_f32_e32 v42, v74, v56
	v_mac_f32_e32 v53, v75, v56
	v_mac_f32_e32 v52, v76, v56
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v51, v74, v61
	v_mac_f32_e32 v50, v75, v61
	v_mac_f32_e32 v49, v76, v61
	v_mac_f32_e32 v48, v74, v60
	v_mac_f32_e32 v47, v75, v60
	v_mac_f32_e32 v46, v76, v60
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v45, v74, v65
	v_mac_f32_e32 v44, v75, v65
	v_mac_f32_e32 v43, v76, v65
	v_mac_f32_e32 v41, v74, v68
	v_mac_f32_e32 v40, v75, v68
	v_mac_f32_e32 v39, v76, v68
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v38, v74, v71
	v_mac_f32_e32 v16, v76, v71
	v_mac_f32_e32 v19, v75, v71
	s_and_b64 vcc, exec, vcc
	v_mac_f32_e32 v53, v76, v57
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v52, v77, v57
	v_mac_f32_e32 v51, v75, v62
	v_mac_f32_e32 v50, v76, v62
	v_mac_f32_e32 v49, v77, v62
	v_mac_f32_e32 v48, v75, v63
	v_mac_f32_e32 v47, v76, v63
	v_mac_f32_e32 v46, v77, v63
	v_mac_f32_e32 v45, v75, v66
	v_mac_f32_e32 v44, v76, v66
	v_mac_f32_e32 v43, v77, v66
	v_mac_f32_e32 v41, v75, v69
	v_mac_f32_e32 v40, v76, v69
	v_mac_f32_e32 v39, v77, v69
	v_mac_f32_e32 v38, v75, v72
	v_mac_f32_e32 v16, v77, v72
	v_mac_f32_e32 v19, v76, v72
	v_mac_f32_e32 v42, v75, v57
	s_cbranch_vccz BB1_21
; BB#22:                                ; %.lr.ph13.i399.preheader
                                        ;   in Loop: Header=BB1_3 Depth=1
	v_mov_b32_e32 v54, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v55, vcc, 0x492c, v54
	v_add_i32_e32 v57, vcc, 0x49b0, v54
	v_add_i32_e32 v59, vcc, 0x4a34, v54
	v_add_i32_e32 v61, vcc, 0x4ab8, v54
	v_add_i32_e32 v63, vcc, 0x4b3c, v54
	v_add_i32_e32 v65, vcc, 0x4bc0, v54
	ds_read2_b32 v[55:56], v55 offset1:4
	ds_read2_b32 v[67:68], v2 offset0:4 offset1:8
	ds_read2_b32 v[57:58], v57 offset1:4
	ds_read2_b32 v[59:60], v59 offset1:4
	ds_read_b32 v69, v3 offset:12
	ds_read2_b32 v[61:62], v61 offset1:4
	ds_read2_b32 v[63:64], v63 offset1:4
	ds_read2_b32 v[65:66], v65 offset1:4
	ds_read_b32 v70, v2 offset:48
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v42, v69, v55
	v_mac_f32_e32 v53, v67, v55
	v_mac_f32_e32 v52, v68, v55
	v_mac_f32_e32 v51, v69, v57
	v_mac_f32_e32 v50, v67, v57
	v_mac_f32_e32 v49, v68, v57
	v_mac_f32_e32 v48, v69, v59
	v_mac_f32_e32 v47, v67, v59
	v_mac_f32_e32 v46, v68, v59
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v45, v69, v61
	v_mac_f32_e32 v44, v67, v61
	v_mac_f32_e32 v43, v68, v61
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v41, v69, v63
	v_mac_f32_e32 v40, v67, v63
	v_mac_f32_e32 v39, v68, v63
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v38, v69, v65
	v_mac_f32_e32 v16, v68, v65
	v_mac_f32_e32 v19, v67, v65
	v_mac_f32_e32 v42, v67, v56
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v52, v70, v56
	v_mac_f32_e32 v51, v67, v58
	v_mac_f32_e32 v50, v68, v58
	v_mac_f32_e32 v49, v70, v58
	v_mac_f32_e32 v48, v67, v60
	v_mac_f32_e32 v47, v68, v60
	v_mac_f32_e32 v46, v70, v60
	v_mac_f32_e32 v45, v67, v62
	v_mac_f32_e32 v44, v68, v62
	v_mac_f32_e32 v43, v70, v62
	v_mac_f32_e32 v41, v67, v64
	v_mac_f32_e32 v40, v68, v64
	v_mac_f32_e32 v39, v70, v64
	v_mac_f32_e32 v38, v67, v66
	v_mac_f32_e32 v16, v70, v66
	v_mac_f32_e32 v19, v68, v66
	v_mac_f32_e32 v53, v68, v56
BB1_23:                                 ; %.lr.ph13.i302.preheader.1
                                        ;   Parent Loop BB1_3 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	s_mov_b32 m0, -1
	v_add_i32_e32 v55, vcc, 0x494c, v54
	v_add_i32_e32 v57, vcc, 0x496c, v54
	v_add_i32_e32 v59, vcc, 0x4a54, v54
	v_add_i32_e32 v63, vcc, 0x4a74, v54
	v_add_i32_e32 v67, vcc, 0x4b5c, v54
	v_add_i32_e32 v69, vcc, 0x4b7c, v54
	v_add_i32_e32 v73, vcc, v54, v3
	v_add_i32_e32 v75, vcc, v54, v4
	v_add_i32_e32 v61, vcc, 0x49e0, v54
	v_add_i32_e32 v65, vcc, 0x4ae8, v54
	v_add_i32_e32 v71, vcc, 0x4bf0, v54
	ds_read_b32 v77, v73 offset:936
	ds_read2_b32 v[55:56], v55 offset1:4
	ds_read2_b32 v[73:74], v75 offset0:238 offset1:242
	ds_read2_b32 v[57:58], v57 offset1:25
	ds_read2_b32 v[59:60], v59 offset1:4
	ds_read2_b32 v[63:64], v63 offset1:25
	ds_read2_b32 v[67:68], v67 offset1:4
	ds_read2_b32 v[69:70], v69 offset1:25
	ds_read2_b32 v[61:62], v61 offset1:4
	ds_read2_b32 v[65:66], v65 offset1:4
	ds_read2_b32 v[71:72], v71 offset1:4
	ds_read2_b32 v[75:76], v75 offset0:246 offset1:250
	v_add_i32_e32 v54, vcc, 4, v54
	s_waitcnt lgkmcnt(10)
	v_mac_f32_e32 v42, v77, v55
	s_waitcnt lgkmcnt(9)
	v_mac_f32_e32 v53, v73, v55
	v_mac_f32_e32 v52, v74, v55
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v51, v77, v58
	v_mac_f32_e32 v50, v73, v58
	v_mac_f32_e32 v49, v74, v58
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v48, v77, v59
	v_mac_f32_e32 v47, v73, v59
	v_mac_f32_e32 v46, v74, v59
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v45, v77, v64
	v_mac_f32_e32 v44, v73, v64
	v_mac_f32_e32 v43, v74, v64
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v41, v77, v67
	v_mac_f32_e32 v40, v73, v67
	v_mac_f32_e32 v39, v74, v67
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v38, v77, v70
	v_mac_f32_e32 v16, v74, v70
	v_mac_f32_e32 v19, v73, v70
	v_cmp_ne_u32_e32 vcc, 12, v54
	v_mac_f32_e32 v42, v73, v56
	v_mac_f32_e32 v53, v74, v56
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v52, v75, v56
	v_mac_f32_e32 v51, v73, v61
	v_mac_f32_e32 v50, v74, v61
	v_mac_f32_e32 v49, v75, v61
	v_mac_f32_e32 v48, v73, v60
	v_mac_f32_e32 v47, v74, v60
	v_mac_f32_e32 v46, v75, v60
	v_mac_f32_e32 v45, v73, v65
	v_mac_f32_e32 v44, v74, v65
	v_mac_f32_e32 v43, v75, v65
	v_mac_f32_e32 v41, v73, v68
	v_mac_f32_e32 v40, v74, v68
	v_mac_f32_e32 v39, v75, v68
	v_mac_f32_e32 v38, v73, v71
	v_mac_f32_e32 v16, v75, v71
	v_mac_f32_e32 v19, v74, v71
	s_and_b64 vcc, exec, vcc
	v_mac_f32_e32 v53, v75, v57
	v_mac_f32_e32 v52, v76, v57
	v_mac_f32_e32 v51, v74, v62
	v_mac_f32_e32 v50, v75, v62
	v_mac_f32_e32 v49, v76, v62
	v_mac_f32_e32 v48, v74, v63
	v_mac_f32_e32 v47, v75, v63
	v_mac_f32_e32 v46, v76, v63
	v_mac_f32_e32 v45, v74, v66
	v_mac_f32_e32 v44, v75, v66
	v_mac_f32_e32 v43, v76, v66
	v_mac_f32_e32 v41, v74, v69
	v_mac_f32_e32 v40, v75, v69
	v_mac_f32_e32 v39, v76, v69
	v_mac_f32_e32 v38, v74, v72
	v_mac_f32_e32 v16, v76, v72
	v_mac_f32_e32 v19, v75, v72
	v_mac_f32_e32 v42, v74, v57
	s_cbranch_vccnz BB1_23
; BB#24:                                ; %.lr.ph13.i399.preheader.1
                                        ;   in Loop: Header=BB1_3 Depth=1
	v_mov_b32_e32 v54, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v55, vcc, 0x4958, v54
	v_add_i32_e32 v57, vcc, 0x49dc, v54
	v_add_i32_e32 v59, vcc, 0x4a60, v54
	v_add_i32_e32 v61, vcc, 0x4ae4, v54
	v_add_i32_e32 v63, vcc, 0x4b68, v54
	v_add_i32_e32 v65, vcc, 0x4bec, v54
	ds_read2_b32 v[55:56], v55 offset1:4
	ds_read2_b32 v[67:68], v2 offset0:234 offset1:238
	ds_read2_b32 v[57:58], v57 offset1:4
	ds_read2_b32 v[59:60], v59 offset1:4
	ds_read2_b32 v[69:70], v2 offset0:242 offset1:246
	ds_read2_b32 v[61:62], v61 offset1:4
	ds_read2_b32 v[63:64], v63 offset1:4
	ds_read2_b32 v[65:66], v65 offset1:4
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v42, v67, v55
	v_mac_f32_e32 v53, v68, v55
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v52, v69, v55
	v_mac_f32_e32 v51, v67, v57
	v_mac_f32_e32 v50, v68, v57
	v_mac_f32_e32 v49, v69, v57
	v_mac_f32_e32 v48, v67, v59
	v_mac_f32_e32 v47, v68, v59
	v_mac_f32_e32 v46, v69, v59
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v45, v67, v61
	v_mac_f32_e32 v44, v68, v61
	v_mac_f32_e32 v43, v69, v61
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v41, v67, v63
	v_mac_f32_e32 v40, v68, v63
	v_mac_f32_e32 v39, v69, v63
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v38, v67, v65
	v_mac_f32_e32 v16, v69, v65
	v_mac_f32_e32 v19, v68, v65
	v_mac_f32_e32 v42, v68, v56
	v_mac_f32_e32 v52, v70, v56
	v_mac_f32_e32 v51, v68, v58
	v_mac_f32_e32 v50, v69, v58
	v_mac_f32_e32 v49, v70, v58
	v_mac_f32_e32 v48, v68, v60
	v_mac_f32_e32 v47, v69, v60
	v_mac_f32_e32 v46, v70, v60
	v_mac_f32_e32 v45, v68, v62
	v_mac_f32_e32 v44, v69, v62
	v_mac_f32_e32 v43, v70, v62
	v_mac_f32_e32 v41, v68, v64
	v_mac_f32_e32 v40, v69, v64
	v_mac_f32_e32 v39, v70, v64
	v_mac_f32_e32 v38, v68, v66
	v_mac_f32_e32 v16, v70, v66
	v_mac_f32_e32 v19, v69, v66
	v_mac_f32_e32 v53, v69, v56
BB1_25:                                 ; %.lr.ph13.i302.preheader.2
                                        ;   Parent Loop BB1_3 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_add_i32_e32 v73, vcc, v54, v4
	s_mov_b32 m0, -1
	v_add_i32_e32 v55, vcc, 0x4978, v54
	v_add_i32_e32 v57, vcc, 0x4998, v54
	v_add_i32_e32 v61, vcc, 0x4a80, v54
	v_add_i32_e32 v63, vcc, 0x4aa0, v54
	v_add_i32_e32 v67, vcc, 0x4b88, v54
	v_add_i32_e32 v69, vcc, 0x4ba8, v54
	v_add_i32_e32 v74, vcc, v54, v3
	v_add_i32_e32 v75, vcc, 0x760, v73
	v_add_i32_e32 v59, vcc, 0x4a0c, v54
	v_add_i32_e32 v65, vcc, 0x4b14, v54
	v_add_i32_e32 v71, vcc, 0x4c1c, v54
	ds_read2_b32 v[55:56], v55 offset1:4
	ds_read_b32 v77, v74 offset:1872
	ds_read2_b32 v[57:58], v57 offset1:25
	ds_read2_b32 v[61:62], v61 offset1:4
	ds_read2_b32 v[63:64], v63 offset1:25
	ds_read2_b32 v[67:68], v67 offset1:4
	ds_read2_b32 v[69:70], v69 offset1:25
	v_add_i32_e32 v76, vcc, 0x780, v73
	ds_read2_b32 v[73:74], v75 offset1:4
	ds_read2_b32 v[59:60], v59 offset1:4
	ds_read2_b32 v[65:66], v65 offset1:4
	ds_read2_b32 v[71:72], v71 offset1:4
	ds_read2_b32 v[75:76], v76 offset1:4
	v_add_i32_e32 v54, vcc, 4, v54
	s_waitcnt lgkmcnt(10)
	v_mac_f32_e32 v42, v77, v55
	s_waitcnt lgkmcnt(9)
	v_mac_f32_e32 v51, v77, v58
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v48, v77, v61
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v45, v77, v64
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v41, v77, v67
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v38, v77, v70
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v53, v73, v55
	v_mac_f32_e32 v52, v74, v55
	v_mac_f32_e32 v50, v73, v58
	v_mac_f32_e32 v49, v74, v58
	v_mac_f32_e32 v47, v73, v61
	v_mac_f32_e32 v46, v74, v61
	v_mac_f32_e32 v44, v73, v64
	v_mac_f32_e32 v43, v74, v64
	v_mac_f32_e32 v40, v73, v67
	v_mac_f32_e32 v39, v74, v67
	v_mac_f32_e32 v16, v74, v70
	v_mac_f32_e32 v19, v73, v70
	v_cmp_ne_u32_e32 vcc, 12, v54
	v_mac_f32_e32 v42, v73, v56
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v51, v73, v59
	v_mac_f32_e32 v48, v73, v62
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v45, v73, v65
	v_mac_f32_e32 v41, v73, v68
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v38, v73, v71
	v_mac_f32_e32 v53, v74, v56
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v52, v75, v56
	v_mac_f32_e32 v50, v74, v59
	v_mac_f32_e32 v49, v75, v59
	v_mac_f32_e32 v47, v74, v62
	v_mac_f32_e32 v46, v75, v62
	v_mac_f32_e32 v44, v74, v65
	v_mac_f32_e32 v43, v75, v65
	v_mac_f32_e32 v40, v74, v68
	v_mac_f32_e32 v39, v75, v68
	v_mac_f32_e32 v16, v75, v71
	v_mac_f32_e32 v19, v74, v71
	s_and_b64 vcc, exec, vcc
	v_mac_f32_e32 v51, v74, v60
	v_mac_f32_e32 v48, v74, v63
	v_mac_f32_e32 v45, v74, v66
	v_mac_f32_e32 v41, v74, v69
	v_mac_f32_e32 v38, v74, v72
	v_mac_f32_e32 v42, v74, v57
	v_mac_f32_e32 v53, v75, v57
	v_mac_f32_e32 v52, v76, v57
	v_mac_f32_e32 v50, v75, v60
	v_mac_f32_e32 v49, v76, v60
	v_mac_f32_e32 v47, v75, v63
	v_mac_f32_e32 v46, v76, v63
	v_mac_f32_e32 v44, v75, v66
	v_mac_f32_e32 v43, v76, v66
	v_mac_f32_e32 v40, v75, v69
	v_mac_f32_e32 v39, v76, v69
	v_mac_f32_e32 v16, v76, v72
	v_mac_f32_e32 v19, v75, v72
	s_cbranch_vccnz BB1_25
; BB#26:                                ; %.lr.ph13.i399.preheader.2
                                        ;   in Loop: Header=BB1_3 Depth=1
	v_mov_b32_e32 v54, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v55, vcc, 0x4984, v54
	v_add_i32_e32 v57, vcc, 0x4a08, v54
	v_add_i32_e32 v59, vcc, 0x4a8c, v54
	v_add_i32_e32 v61, vcc, 0x4b10, v54
	v_add_i32_e32 v63, vcc, 0x4b94, v54
	v_add_i32_e32 v65, vcc, 0x4c18, v54
	v_add_i32_e32 v67, vcc, 0x750, v2
	v_add_i32_e32 v69, vcc, 0x770, v2
	ds_read2_b32 v[55:56], v55 offset1:4
	ds_read2_b32 v[67:68], v67 offset1:4
	ds_read2_b32 v[57:58], v57 offset1:4
	ds_read2_b32 v[59:60], v59 offset1:4
	ds_read2_b32 v[69:70], v69 offset1:4
	ds_read2_b32 v[61:62], v61 offset1:4
	ds_read2_b32 v[63:64], v63 offset1:4
	ds_read2_b32 v[65:66], v65 offset1:4
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v42, v67, v55
	v_mac_f32_e32 v53, v68, v55
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v52, v69, v55
	v_mac_f32_e32 v51, v67, v57
	v_mac_f32_e32 v50, v68, v57
	v_mac_f32_e32 v49, v69, v57
	v_mac_f32_e32 v48, v67, v59
	v_mac_f32_e32 v47, v68, v59
	v_mac_f32_e32 v46, v69, v59
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v45, v67, v61
	v_mac_f32_e32 v44, v68, v61
	v_mac_f32_e32 v43, v69, v61
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v41, v67, v63
	v_mac_f32_e32 v40, v68, v63
	v_mac_f32_e32 v39, v69, v63
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v38, v67, v65
	v_mac_f32_e32 v16, v69, v65
	v_mac_f32_e32 v19, v68, v65
	v_mac_f32_e32 v42, v68, v56
	v_mac_f32_e32 v52, v70, v56
	v_mac_f32_e32 v51, v68, v58
	v_mac_f32_e32 v50, v69, v58
	v_mac_f32_e32 v49, v70, v58
	v_mac_f32_e32 v48, v68, v60
	v_mac_f32_e32 v47, v69, v60
	v_mac_f32_e32 v46, v70, v60
	v_mac_f32_e32 v45, v68, v62
	v_mac_f32_e32 v44, v69, v62
	v_mac_f32_e32 v43, v70, v62
	v_mac_f32_e32 v41, v68, v64
	v_mac_f32_e32 v40, v69, v64
	v_mac_f32_e32 v39, v70, v64
	v_mac_f32_e32 v38, v68, v66
	v_mac_f32_e32 v16, v70, v66
	v_mac_f32_e32 v19, v69, v66
	v_mac_f32_e32 v53, v69, v56
	v_add_i32_e32 v15, vcc, 1, v15
BB1_27:                                 ; %Flow4647
                                        ;   in Loop: Header=BB1_3 Depth=1
	v_cmp_ne_u32_e32 vcc, 0, v54
	s_cbranch_vccz BB1_3
; BB#28:
	v_mul_lo_i32 v15, v7, 33
	v_cmp_gt_u32_e64 s[14:15], 12, v0
	v_cmp_gt_u32_e64 s[12:13], 64, v13
	s_and_saveexec_b64 s[8:9], s[14:15]
	s_xor_b64 s[10:11], exec, s[8:9]
	; mask branch BB1_30
	s_cbranch_execz BB1_30
BB1_29:                                 ; %.lr.ph.i493
	v_lshlrev_b32_e32 v13, 1, v7
	v_and_b32_e32 v13, 0x1fffffe, v13
	v_subrev_i32_e32 v13, vcc, v13, v0
	v_lshlrev_b32_e32 v16, 2, v13
	v_or_b32_e32 v16, 3, v16
	v_mul_lo_i32 v19, v16, 11
	v_cmp_gt_u32_e32 vcc, 11, v16
	s_and_b64 s[8:9], exec, s[12:13]
	s_and_b64 s[8:9], s[8:9], vcc
	v_add_i32_e32 v16, vcc, v6, v19
	v_ashrrev_i32_e32 v19, 31, v16
	v_cndmask_b32_e64 v38, 0, v16, s[8:9]
	v_cndmask_b32_e64 v39, 0, v19, s[8:9]
	v_lshlrev_b64 v[38:39], 2, v[38:39]
	v_add_i32_e32 v46, vcc, s24, v38
	v_mov_b32_e32 v16, s25
	v_addc_u32_e32 v47, vcc, v16, v39, vcc
	flat_load_dwordx4 v[38:41], v[46:47]
	flat_load_dwordx4 v[42:45], v[46:47] offset:16
	flat_load_dword v16, v[46:47] offset:40
	flat_load_dwordx2 v[46:47], v[46:47] offset:32
	v_mul_lo_i32 v13, v13, 11
	s_mov_b32 m0, -1
	v_add_i32_e32 v13, vcc, v15, v13
	v_lshlrev_b32_e32 v13, 2, v13
	v_add_i32_e32 v19, vcc, 0x4920, v13
	v_add_i32_e32 v48, vcc, 0x4928, v13
	v_add_i32_e32 v49, vcc, 0x4930, v13
	v_add_i32_e32 v50, vcc, 0x4938, v13
	v_add_i32_e32 v51, vcc, 0x4940, v13
	s_waitcnt vmcnt(3) lgkmcnt(3)
	ds_write2_b32 v19, v38, v39 offset1:1
	ds_write2_b32 v48, v40, v41 offset1:1
	s_waitcnt vmcnt(2) lgkmcnt(4)
	ds_write2_b32 v49, v42, v43 offset1:1
	ds_write2_b32 v50, v44, v45 offset1:1
	s_waitcnt vmcnt(0) lgkmcnt(4)
	ds_write2_b32 v51, v46, v47 offset1:1
	ds_write_b32 v13, v16 offset:18760
BB1_30:                                 ; %.lr.ph.i.preheader
	s_or_b64 exec, exec, s[10:11]
	v_mul_lo_i32 v10, v10, 3
	v_add_i32_e32 v13, vcc, s22, v11
	v_cmp_gt_u32_e64 s[10:11], 4, v11
	v_cmp_gt_u32_e64 s[8:9], 55, v12
	s_mov_b64 s[28:29], 0
	s_mov_b32 s27, 0xffffff
	s_movk_i32 s30, 0xffa4
	s_movk_i32 s31, 0xffe9
	s_movk_i32 s32, 0x5c
	s_mov_b32 s36, 0
	s_mov_b32 s33, 0x24c00
	s_movk_i32 s34, 0xe0
	s_movk_i32 s35, 0x492
	s_movk_i32 s40, 0xea
	v_mov_b32_e32 v11, v0
	s_mov_b32 m0, -1
BB1_31:                                 ; %.lr.ph.i
                                        ; =>This Inner Loop Header: Depth=1
	v_cvt_f32_u32_e32 v12, v11
	v_mov_b32_e32 v16, 0x3727c5ac
	s_waitcnt vmcnt(0) lgkmcnt(0)
	v_mov_b32_e32 v38, 0x3727c5ac
	v_mov_b32_e32 v39, 0xffffff31
	v_mac_f32_e32 v16, 0x3c321643, v12
	v_cvt_u32_f32_e32 v12, v16
	v_mov_b32_e32 v47, 0
	v_and_b32_e32 v19, s27, v12
	v_mul_lo_i32 v16, v19, s30
	v_add_i32_e32 v16, vcc, v16, v11
	v_cvt_f32_u32_e32 v16, v16
	v_mac_f32_e32 v38, 0x3d321643, v16
	v_cvt_u32_f32_e32 v16, v38
	v_mov_b32_e32 v38, 0xffffff32
	v_lshlrev_b32_e32 v40, 2, v16
	v_or_b32_e32 v44, 3, v40
	v_cmp_lt_i32_e32 vcc, v44, v38
	v_cmp_gt_i32_e64 s[16:17], v44, v39
	v_cndmask_b32_e64 v38, 0, -1, vcc
	s_and_saveexec_b64 s[38:39], s[16:17]
	s_xor_b64 s[38:39], exec, s[38:39]
	; mask branch BB1_33
BB1_32:                                 ;   in Loop: Header=BB1_31 Depth=1
	v_cmp_lt_i32_e32 vcc, 17, v44
	v_cmp_lt_u32_e64 s[16:17], 3, v12
	s_or_b64 s[16:17], s[16:17], vcc
	v_add_i32_e32 v38, vcc, s22, v12
	v_mov_b32_e32 v39, 0x7f
	v_cmp_gt_u32_e32 vcc, v38, v39
	s_or_b64 s[16:17], vcc, s[16:17]
	v_mov_b32_e32 v47, -1
	v_cndmask_b32_e64 v38, 0, -1, s[16:17]
BB1_33:                                 ; %Flow4638
                                        ;   in Loop: Header=BB1_31 Depth=1
	s_or_b64 exec, exec, s[38:39]
	v_and_b32_e32 v39, s27, v16
	v_mul_lo_i32 v19, v19, s32
	v_mul_lo_i32 v39, v39, s31
	v_mov_b32_e32 v46, 0
                                        ; implicit-def: %VGPR40_VGPR41_VGPR42_VGPR43
	v_sub_i32_e32 v19, vcc, v39, v19
	v_add_i32_e32 v45, vcc, v19, v11
	v_cmp_ne_u32_e32 vcc, 0, v38
                                        ; implicit-def: %VGPR38_VGPR39
                                        ; implicit-def: %VGPR19
	s_and_saveexec_b64 s[16:17], vcc
	s_xor_b64 s[16:17], exec, s[16:17]
	; mask branch BB1_37
	s_cbranch_execz BB1_37
BB1_34:                                 ; %.loopexit3.i
                                        ;   in Loop: Header=BB1_31 Depth=1
	v_cmp_gt_u32_e32 vcc, 4, v12
	v_mov_b32_e32 v46, 0
                                        ; implicit-def: %VGPR38_VGPR39
                                        ; implicit-def: %VGPR40_VGPR41_VGPR42_VGPR43
                                        ; implicit-def: %VGPR19
	s_and_saveexec_b64 s[38:39], vcc
	s_xor_b64 s[38:39], exec, s[38:39]
	; mask branch BB1_36
	s_cbranch_execz BB1_36
BB1_35:                                 ; %.loopexit3.i..loopexit.loopexit12.i_crit_edge
                                        ;   in Loop: Header=BB1_31 Depth=1
	v_mul_lo_i32 v19, v45, 10
	s_mov_b32 s44, s36
	s_mov_b32 s45, s36
	s_mov_b32 s46, s36
	s_mov_b32 s47, s36
	s_mov_b32 s42, s36
	s_mov_b32 s43, s36
	v_mov_b32_e32 v40, s44
	v_mov_b32_e32 v38, s42
	v_mov_b32_e32 v46, -1
	v_mov_b32_e32 v39, s43
	v_mov_b32_e32 v41, s45
	v_mov_b32_e32 v42, s46
	v_mov_b32_e32 v43, s47
BB1_36:                                 ; %Flow4640
                                        ;   in Loop: Header=BB1_31 Depth=1
	s_or_b64 exec, exec, s[38:39]
	v_mov_b32_e32 v47, 0
BB1_37:                                 ; %Flow4639
                                        ;   in Loop: Header=BB1_31 Depth=1
	s_or_b64 exec, exec, s[16:17]
	v_cmp_ne_u32_e32 vcc, 0, v47
	v_mov_b32_e32 v50, v43
	v_mov_b32_e32 v49, v42
	v_mov_b32_e32 v48, v41
	v_mov_b32_e32 v47, v40
	s_and_saveexec_b64 s[16:17], vcc
	s_xor_b64 s[16:17], exec, s[16:17]
	; mask branch BB1_41
	s_cbranch_execz BB1_41
BB1_38:                                 ;   in Loop: Header=BB1_31 Depth=1
	v_mul_lo_i32 v38, v12, s33
	v_mul_lo_i32 v39, v44, s34
	v_mul_lo_i32 v19, v45, 10
	s_mov_b32 s37, s36
	v_add_i32_e32 v38, vcc, s26, v38
	v_add_i32_e32 v38, vcc, v39, v38
	v_add_i32_e32 v38, vcc, v38, v19
	v_ashrrev_i32_e32 v39, 31, v38
	v_lshlrev_b64 v[38:39], 2, v[38:39]
	v_add_i32_e32 v51, vcc, s20, v38
	v_mov_b32_e32 v38, s21
	v_addc_u32_e32 v52, vcc, v38, v39, vcc
	flat_load_dwordx4 v[47:50], v[51:52]
	s_mov_b32 s38, s36
	s_mov_b32 s39, s36
	s_mov_b32 s42, s36
	s_mov_b32 s43, s36
	v_mov_b32_e32 v43, s39
	v_mov_b32_e32 v38, s42
	v_cmp_ne_u32_e32 vcc, 22, v45
	v_mov_b32_e32 v42, s38
	v_mov_b32_e32 v41, s37
	v_mov_b32_e32 v40, s36
	v_mov_b32_e32 v39, s43
	s_and_saveexec_b64 s[38:39], vcc
	s_xor_b64 s[38:39], exec, s[38:39]
	; mask branch BB1_40
BB1_39:                                 ; %.preheader4.preheader.i
                                        ;   in Loop: Header=BB1_31 Depth=1
	flat_load_dwordx4 v[40:43], v[51:52] offset:16
	flat_load_dwordx2 v[38:39], v[51:52] offset:32
BB1_40:                                 ; %Flow4637
                                        ;   in Loop: Header=BB1_31 Depth=1
	s_or_b64 exec, exec, s[38:39]
	v_mov_b32_e32 v46, -1
BB1_41:                                 ; %Flow4641
                                        ;   in Loop: Header=BB1_31 Depth=1
	s_or_b64 exec, exec, s[16:17]
	v_cmp_ne_u32_e32 vcc, 0, v46
	s_and_saveexec_b64 s[16:17], vcc
	s_xor_b64 s[16:17], exec, s[16:17]
	; mask branch BB1_43
BB1_42:                                 ; %.loopexit.loopexit12.i
                                        ;   in Loop: Header=BB1_31 Depth=1
	v_mul_lo_i32 v12, v12, s35
	v_mul_lo_i32 v16, v16, s40
	v_add_i32_e32 v12, vcc, v16, v12
	v_add_i32_e32 v12, vcc, v12, v19
	v_lshlrev_b32_e32 v12, 2, v12
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write2_b32 v12, v47, v48 offset0:2 offset1:3
	ds_write2_b32 v12, v49, v50 offset0:4 offset1:5
	ds_write2_b32 v12, v40, v41 offset0:6 offset1:7
	ds_write2_b32 v12, v42, v43 offset0:8 offset1:9
	ds_write2_b32 v12, v38, v39 offset0:10 offset1:11
BB1_43:                                 ; %.loopexit.i
                                        ;   in Loop: Header=BB1_31 Depth=1
	s_or_b64 exec, exec, s[16:17]
	v_add_i32_e32 v11, vcc, 0x100, v11
	v_mov_b32_e32 v12, 0x16f
	v_cmp_gt_u32_e32 vcc, v11, v12
	s_or_b64 s[28:29], vcc, s[28:29]
	s_andn2_b64 exec, exec, s[28:29]
	s_cbranch_execnz BB1_31
; BB#44:                                ; %fetchData2.6.exit
	s_or_b64 exec, exec, s[28:29]
	s_waitcnt vmcnt(0) lgkmcnt(0)
	s_barrier
	v_lshlrev_b32_e32 v16, 2, v10
	v_add_i32_e32 v12, vcc, 1, v10
	v_add_i32_e32 v19, vcc, 4, v16
	v_add_i32_e32 v38, vcc, 8, v16
	v_add_i32_e32 v11, vcc, 2, v10
	v_mov_b32_e32 v16, 0
	s_mov_b32 m0, -1
BB1_45:                                 ; %.lr.ph13.i195.preheader
                                        ; =>This Inner Loop Header: Depth=1
	v_add_i32_e32 v39, vcc, 0x4920, v16
	v_add_i32_e32 v41, vcc, 0x4940, v16
	v_add_i32_e32 v43, vcc, 0x4a28, v16
	v_add_i32_e32 v47, vcc, 0x4a48, v16
	v_add_i32_e32 v51, vcc, 0x4b30, v16
	v_add_i32_e32 v53, vcc, 0x4b50, v16
	v_add_i32_e32 v61, vcc, v16, v5
	v_add_i32_e32 v45, vcc, 0x49b4, v16
	v_add_i32_e32 v49, vcc, 0x4abc, v16
	v_add_i32_e32 v55, vcc, 0x4bc4, v16
	ds_read2_b32 v[39:40], v39 offset1:4
	ds_read2_b32 v[57:58], v61 offset1:4
	ds_read2_b32 v[41:42], v41 offset1:25
	ds_read2_b32 v[59:60], v61 offset0:8 offset1:12
	ds_read2_b32 v[43:44], v43 offset1:4
	ds_read2_b32 v[47:48], v47 offset1:25
	ds_read2_b32 v[51:52], v51 offset1:4
	ds_read2_b32 v[53:54], v53 offset1:25
	ds_read2_b32 v[45:46], v45 offset1:4
	ds_read2_b32 v[49:50], v49 offset1:4
	ds_read2_b32 v[55:56], v55 offset1:4
	ds_read_b32 v61, v61 offset:64
	v_add_i32_e32 v16, vcc, 4, v16
	s_waitcnt lgkmcnt(10)
	v_mac_f32_e32 v37, v57, v39
	v_mac_f32_e32 v36, v58, v39
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v35, v59, v39
	v_mac_f32_e32 v34, v57, v42
	v_mac_f32_e32 v33, v58, v42
	v_mac_f32_e32 v32, v59, v42
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v31, v57, v43
	v_mac_f32_e32 v30, v58, v43
	v_mac_f32_e32 v29, v59, v43
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v28, v57, v48
	v_mac_f32_e32 v27, v58, v48
	v_mac_f32_e32 v26, v59, v48
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v25, v57, v51
	v_mac_f32_e32 v24, v58, v51
	v_mac_f32_e32 v23, v59, v51
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v22, v57, v54
	v_mac_f32_e32 v20, v59, v54
	v_mac_f32_e32 v21, v58, v54
	v_cmp_eq_u32_e32 vcc, 12, v16
	v_mac_f32_e32 v37, v58, v40
	v_mac_f32_e32 v36, v59, v40
	v_mac_f32_e32 v35, v60, v40
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v34, v58, v45
	v_mac_f32_e32 v33, v59, v45
	v_mac_f32_e32 v32, v60, v45
	v_mac_f32_e32 v31, v58, v44
	v_mac_f32_e32 v30, v59, v44
	v_mac_f32_e32 v29, v60, v44
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v28, v58, v49
	v_mac_f32_e32 v27, v59, v49
	v_mac_f32_e32 v26, v60, v49
	v_mac_f32_e32 v25, v58, v52
	v_mac_f32_e32 v24, v59, v52
	v_mac_f32_e32 v23, v60, v52
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v22, v58, v55
	v_mac_f32_e32 v20, v60, v55
	v_mac_f32_e32 v21, v59, v55
	s_and_b64 vcc, exec, vcc
	v_mac_f32_e32 v36, v60, v41
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v35, v61, v41
	v_mac_f32_e32 v34, v59, v46
	v_mac_f32_e32 v33, v60, v46
	v_mac_f32_e32 v32, v61, v46
	v_mac_f32_e32 v31, v59, v47
	v_mac_f32_e32 v30, v60, v47
	v_mac_f32_e32 v29, v61, v47
	v_mac_f32_e32 v28, v59, v50
	v_mac_f32_e32 v27, v60, v50
	v_mac_f32_e32 v26, v61, v50
	v_mac_f32_e32 v25, v59, v53
	v_mac_f32_e32 v24, v60, v53
	v_mac_f32_e32 v23, v61, v53
	v_mac_f32_e32 v22, v59, v56
	v_mac_f32_e32 v20, v61, v56
	v_mac_f32_e32 v21, v60, v56
	v_mac_f32_e32 v37, v59, v41
	s_cbranch_vccz BB1_45
; BB#46:                                ; %.lr.ph13.i.preheader
	v_mov_b32_e32 v39, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v16, vcc, 0x492c, v39
	ds_read2_b32 v[40:41], v16 offset1:4
	v_add_i32_e32 v16, vcc, 0x49b0, v39
	ds_read2_b32 v[42:43], v16 offset1:4
	v_add_i32_e32 v16, vcc, 0x4a34, v39
	ds_read2_b32 v[44:45], v16 offset1:4
	v_add_i32_e32 v16, vcc, 0x4ab8, v39
	ds_read2_b32 v[46:47], v16 offset1:4
	v_add_i32_e32 v16, vcc, 0x4b3c, v39
	ds_read2_b32 v[48:49], v16 offset1:4
	v_add_i32_e32 v16, vcc, 0x4bc0, v39
	ds_read2_b32 v[50:51], v16 offset1:4
	v_add_i32_e32 v16, vcc, v17, v19
	v_add_i32_e32 v17, vcc, v17, v38
	v_lshlrev_b32_e32 v16, 2, v16
	v_lshlrev_b32_e32 v17, 2, v17
	ds_read_b32 v52, v3 offset:12
	ds_read_b32 v53, v16 offset:12
	ds_read_b32 v54, v17 offset:12
	ds_read_b32 v55, v2 offset:48
	s_mov_b32 m0, -1
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v37, v52, v40
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v36, v53, v40
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v35, v54, v40
	v_mac_f32_e32 v34, v52, v42
	v_mac_f32_e32 v33, v53, v42
	v_mac_f32_e32 v32, v54, v42
	v_mac_f32_e32 v31, v52, v44
	v_mac_f32_e32 v30, v53, v44
	v_mac_f32_e32 v29, v54, v44
	v_mac_f32_e32 v28, v52, v46
	v_mac_f32_e32 v27, v53, v46
	v_mac_f32_e32 v26, v54, v46
	v_mac_f32_e32 v25, v52, v48
	v_mac_f32_e32 v24, v53, v48
	v_mac_f32_e32 v23, v54, v48
	v_mac_f32_e32 v22, v52, v50
	v_mac_f32_e32 v20, v54, v50
	v_mac_f32_e32 v21, v53, v50
	v_mac_f32_e32 v37, v53, v41
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v35, v55, v41
	v_mac_f32_e32 v34, v53, v43
	v_mac_f32_e32 v33, v54, v43
	v_mac_f32_e32 v32, v55, v43
	v_mac_f32_e32 v31, v53, v45
	v_mac_f32_e32 v30, v54, v45
	v_mac_f32_e32 v29, v55, v45
	v_mac_f32_e32 v28, v53, v47
	v_mac_f32_e32 v27, v54, v47
	v_mac_f32_e32 v26, v55, v47
	v_mac_f32_e32 v25, v53, v49
	v_mac_f32_e32 v24, v54, v49
	v_mac_f32_e32 v23, v55, v49
	v_mac_f32_e32 v22, v53, v51
	v_mac_f32_e32 v20, v55, v51
	v_mac_f32_e32 v21, v54, v51
	v_mac_f32_e32 v36, v54, v41
BB1_47:                                 ; %.lr.ph13.i195.preheader.1
                                        ; =>This Inner Loop Header: Depth=1
	v_add_i32_e32 v40, vcc, 0x494c, v39
	v_add_i32_e32 v42, vcc, 0x496c, v39
	v_add_i32_e32 v44, vcc, 0x4a54, v39
	v_add_i32_e32 v48, vcc, 0x4a74, v39
	v_add_i32_e32 v52, vcc, 0x4b5c, v39
	v_add_i32_e32 v54, vcc, 0x4b7c, v39
	v_add_i32_e32 v58, vcc, v39, v3
	v_add_i32_e32 v60, vcc, v39, v4
	v_add_i32_e32 v46, vcc, 0x49e0, v39
	v_add_i32_e32 v50, vcc, 0x4ae8, v39
	v_add_i32_e32 v56, vcc, 0x4bf0, v39
	ds_read_b32 v62, v58 offset:936
	ds_read2_b32 v[40:41], v40 offset1:4
	ds_read2_b32 v[58:59], v60 offset0:238 offset1:242
	ds_read2_b32 v[42:43], v42 offset1:25
	ds_read2_b32 v[44:45], v44 offset1:4
	ds_read2_b32 v[48:49], v48 offset1:25
	ds_read2_b32 v[52:53], v52 offset1:4
	ds_read2_b32 v[54:55], v54 offset1:25
	ds_read2_b32 v[46:47], v46 offset1:4
	ds_read2_b32 v[50:51], v50 offset1:4
	ds_read2_b32 v[56:57], v56 offset1:4
	ds_read2_b32 v[60:61], v60 offset0:246 offset1:250
	v_add_i32_e32 v39, vcc, 4, v39
	s_waitcnt lgkmcnt(10)
	v_mac_f32_e32 v37, v62, v40
	s_waitcnt lgkmcnt(9)
	v_mac_f32_e32 v36, v58, v40
	v_mac_f32_e32 v35, v59, v40
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v34, v62, v43
	v_mac_f32_e32 v33, v58, v43
	v_mac_f32_e32 v32, v59, v43
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v31, v62, v44
	v_mac_f32_e32 v30, v58, v44
	v_mac_f32_e32 v29, v59, v44
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v28, v62, v49
	v_mac_f32_e32 v27, v58, v49
	v_mac_f32_e32 v26, v59, v49
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v25, v62, v52
	v_mac_f32_e32 v24, v58, v52
	v_mac_f32_e32 v23, v59, v52
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v22, v62, v55
	v_mac_f32_e32 v20, v59, v55
	v_mac_f32_e32 v21, v58, v55
	v_cmp_ne_u32_e32 vcc, 12, v39
	v_mac_f32_e32 v37, v58, v41
	v_mac_f32_e32 v36, v59, v41
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v35, v60, v41
	v_mac_f32_e32 v34, v58, v46
	v_mac_f32_e32 v33, v59, v46
	v_mac_f32_e32 v32, v60, v46
	v_mac_f32_e32 v31, v58, v45
	v_mac_f32_e32 v30, v59, v45
	v_mac_f32_e32 v29, v60, v45
	v_mac_f32_e32 v28, v58, v50
	v_mac_f32_e32 v27, v59, v50
	v_mac_f32_e32 v26, v60, v50
	v_mac_f32_e32 v25, v58, v53
	v_mac_f32_e32 v24, v59, v53
	v_mac_f32_e32 v23, v60, v53
	v_mac_f32_e32 v22, v58, v56
	v_mac_f32_e32 v20, v60, v56
	v_mac_f32_e32 v21, v59, v56
	s_and_b64 vcc, exec, vcc
	v_mac_f32_e32 v36, v60, v42
	v_mac_f32_e32 v35, v61, v42
	v_mac_f32_e32 v34, v59, v47
	v_mac_f32_e32 v33, v60, v47
	v_mac_f32_e32 v32, v61, v47
	v_mac_f32_e32 v31, v59, v48
	v_mac_f32_e32 v30, v60, v48
	v_mac_f32_e32 v29, v61, v48
	v_mac_f32_e32 v28, v59, v51
	v_mac_f32_e32 v27, v60, v51
	v_mac_f32_e32 v26, v61, v51
	v_mac_f32_e32 v25, v59, v54
	v_mac_f32_e32 v24, v60, v54
	v_mac_f32_e32 v23, v61, v54
	v_mac_f32_e32 v22, v59, v57
	v_mac_f32_e32 v20, v61, v57
	v_mac_f32_e32 v21, v60, v57
	v_mac_f32_e32 v37, v59, v42
	s_cbranch_vccnz BB1_47
; BB#48:                                ; %.lr.ph13.i.preheader.1
	v_mov_b32_e32 v56, 0
	v_add_i32_e32 v19, vcc, v18, v19
	v_add_i32_e32 v18, vcc, v18, v38
	s_mov_b32 m0, -1
	v_add_i32_e32 v39, vcc, 0x4958, v56
	v_add_i32_e32 v41, vcc, 0x49dc, v56
	v_add_i32_e32 v43, vcc, 0x4a60, v56
	v_add_i32_e32 v45, vcc, 0x4ae4, v56
	v_add_i32_e32 v47, vcc, 0x4b68, v56
	v_add_i32_e32 v49, vcc, 0x4bec, v56
	v_lshlrev_b32_e32 v19, 2, v19
	v_lshlrev_b32_e32 v18, 2, v18
	ds_read2_b32 v[39:40], v39 offset1:4
	ds_read_b32 v53, v19 offset:936
	ds_read2_b32 v[41:42], v41 offset1:4
	ds_read_b32 v38, v18 offset:936
	ds_read2_b32 v[43:44], v43 offset1:4
	ds_read2_b32 v[51:52], v2 offset0:234 offset1:246
	ds_read2_b32 v[45:46], v45 offset1:4
	ds_read2_b32 v[47:48], v47 offset1:4
	ds_read2_b32 v[49:50], v49 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v36, v53, v39
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v37, v51, v39
	v_mac_f32_e32 v35, v38, v39
	v_mac_f32_e32 v34, v51, v41
	v_mac_f32_e32 v33, v53, v41
	v_mac_f32_e32 v32, v38, v41
	v_mac_f32_e32 v31, v51, v43
	v_mac_f32_e32 v30, v53, v43
	v_mac_f32_e32 v29, v38, v43
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v28, v51, v45
	v_mac_f32_e32 v27, v53, v45
	v_mac_f32_e32 v26, v38, v45
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v25, v51, v47
	v_mac_f32_e32 v24, v53, v47
	v_mac_f32_e32 v23, v38, v47
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v22, v51, v49
	v_mac_f32_e32 v20, v38, v49
	v_mac_f32_e32 v21, v53, v49
	v_mac_f32_e32 v37, v53, v40
	v_mac_f32_e32 v35, v52, v40
	v_mac_f32_e32 v34, v53, v42
	v_mac_f32_e32 v33, v38, v42
	v_mac_f32_e32 v32, v52, v42
	v_mac_f32_e32 v31, v53, v44
	v_mac_f32_e32 v30, v38, v44
	v_mac_f32_e32 v29, v52, v44
	v_mac_f32_e32 v28, v53, v46
	v_mac_f32_e32 v27, v38, v46
	v_mac_f32_e32 v26, v52, v46
	v_mac_f32_e32 v25, v53, v48
	v_mac_f32_e32 v24, v38, v48
	v_mac_f32_e32 v23, v52, v48
	v_mac_f32_e32 v22, v53, v50
	v_mac_f32_e32 v20, v52, v50
	v_mac_f32_e32 v21, v38, v50
	v_mac_f32_e32 v36, v38, v40
	s_add_i32 s26, s23, 0x17840
	v_add_i32_e32 v57, vcc, 0x79, v14
	s_mov_b32 s27, 0xffffff
	s_movk_i32 s28, 0xff8d
	s_movk_i32 s29, 0xffe9
	s_movk_i32 s30, 0x73
	s_mov_b32 s32, 0
	s_mov_b32 s31, 0x24c00
	s_movk_i32 s36, 0xe0
	s_movk_i32 s37, 0x492
	s_movk_i32 s38, 0xea
BB1_49:                                 ; =>This Loop Header: Depth=1
                                        ;     Child Loop BB1_53 Depth 2
                                        ;     Child Loop BB1_67 Depth 2
                                        ;     Child Loop BB1_69 Depth 2
                                        ;     Child Loop BB1_71 Depth 2
	s_barrier
	v_cmp_ne_u32_e32 vcc, 3, v56
	v_mov_b32_e32 v54, v36
	v_mov_b32_e32 v53, v35
	v_mov_b32_e32 v52, v34
	v_mov_b32_e32 v51, v33
	v_mov_b32_e32 v50, v32
	v_mov_b32_e32 v49, v31
	v_mov_b32_e32 v48, v30
	v_mov_b32_e32 v47, v29
	v_mov_b32_e32 v46, v28
	v_mov_b32_e32 v45, v27
	v_mov_b32_e32 v44, v26
	v_mov_b32_e32 v43, v25
	v_mov_b32_e32 v42, v24
	v_mov_b32_e32 v41, v23
	v_mov_b32_e32 v40, v22
	v_mov_b32_e32 v39, v21
	v_mov_b32_e32 v38, v20
	v_mov_b32_e32 v55, v37
	s_and_b64 vcc, exec, vcc
	v_mov_b32_e32 v58, -1
                                        ; implicit-def: %VGPR36
                                        ; implicit-def: %VGPR35
                                        ; implicit-def: %VGPR34
                                        ; implicit-def: %VGPR33
                                        ; implicit-def: %VGPR32
                                        ; implicit-def: %VGPR31
                                        ; implicit-def: %VGPR30
                                        ; implicit-def: %VGPR29
                                        ; implicit-def: %VGPR28
                                        ; implicit-def: %VGPR27
                                        ; implicit-def: %VGPR26
                                        ; implicit-def: %VGPR25
                                        ; implicit-def: %VGPR24
                                        ; implicit-def: %VGPR23
                                        ; implicit-def: %VGPR22
                                        ; implicit-def: %VGPR21
                                        ; implicit-def: %VGPR20
                                        ; implicit-def: %VGPR37
	s_cbranch_vccz BB1_73
; BB#50:                                ;   in Loop: Header=BB1_49 Depth=1
	s_and_saveexec_b64 s[16:17], s[2:3]
	s_xor_b64 s[34:35], exec, s[16:17]
	; mask branch BB1_52
	s_cbranch_execz BB1_52
BB1_51:                                 ; %.lr.ph.i288.1
                                        ;   in Loop: Header=BB1_49 Depth=1
	v_add_i32_e32 v20, vcc, v56, v8
	v_mul_lo_i32 v21, v20, 11
	v_cmp_gt_u32_e32 vcc, 11, v20
	s_and_b64 s[16:17], exec, s[4:5]
	s_and_b64 s[16:17], s[16:17], vcc
	v_add_i32_e32 v20, vcc, v57, v21
	v_ashrrev_i32_e32 v21, 31, v20
	v_cndmask_b32_e64 v21, 0, v21, s[16:17]
	v_cndmask_b32_e64 v20, 0, v20, s[16:17]
	v_lshlrev_b64 v[20:21], 2, v[20:21]
	v_add_i32_e32 v28, vcc, s24, v20
	v_mov_b32_e32 v20, s25
	v_addc_u32_e32 v29, vcc, v20, v21, vcc
	flat_load_dwordx4 v[20:23], v[28:29]
	flat_load_dwordx4 v[24:27], v[28:29] offset:16
	flat_load_dword v30, v[28:29] offset:40
	flat_load_dwordx2 v[28:29], v[28:29] offset:32
	s_mov_b32 m0, -1
	s_waitcnt vmcnt(3) lgkmcnt(3)
	ds_write2_b32 v9, v20, v21 offset1:1
	ds_write2_b32 v9, v22, v23 offset0:2 offset1:3
	s_waitcnt vmcnt(2) lgkmcnt(4)
	ds_write2_b32 v9, v24, v25 offset0:4 offset1:5
	ds_write2_b32 v9, v26, v27 offset0:6 offset1:7
	s_waitcnt vmcnt(0) lgkmcnt(4)
	ds_write2_b32 v9, v28, v29 offset0:8 offset1:9
	ds_write_b32 v9, v30 offset:40
BB1_52:                                 ; %.lr.ph.i290.1.preheader
                                        ;   in Loop: Header=BB1_49 Depth=1
	s_or_b64 exec, exec, s[34:35]
	s_mov_b64 s[40:41], 0
	v_mov_b32_e32 v20, v0
BB1_53:                                 ; %.lr.ph.i290.1
                                        ;   Parent Loop BB1_49 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_cvt_f32_u32_e32 v21, v20
	v_mov_b32_e32 v22, 0x3727c5ac
	s_waitcnt vmcnt(0) lgkmcnt(0)
	v_mov_b32_e32 v24, 0x3727c5ac
	v_mov_b32_e32 v25, 0xffffff31
	v_mac_f32_e32 v22, 0x3c0e7835, v21
	v_cvt_u32_f32_e32 v21, v22
	v_mov_b32_e32 v33, 0
	v_and_b32_e32 v23, s27, v21
	v_mul_lo_i32 v22, v23, s28
	v_add_i32_e32 v22, vcc, v22, v20
	v_cvt_f32_u32_e32 v22, v22
	v_mac_f32_e32 v24, 0x3d321643, v22
	v_cvt_u32_f32_e32 v22, v24
	v_mov_b32_e32 v24, 0xffffff32
	v_lshlrev_b32_e32 v26, 2, v22
	v_add_i32_e32 v30, vcc, v26, v56
	v_cmp_lt_i32_e32 vcc, v30, v24
	v_cmp_gt_i32_e64 s[16:17], v30, v25
	v_cndmask_b32_e64 v24, 0, -1, vcc
	s_and_saveexec_b64 s[34:35], s[16:17]
	s_xor_b64 s[34:35], exec, s[34:35]
	; mask branch BB1_55
BB1_54:                                 ;   in Loop: Header=BB1_53 Depth=2
	v_cmp_lt_i32_e32 vcc, 17, v30
	v_cmp_lt_u32_e64 s[16:17], 3, v21
	s_or_b64 s[16:17], s[16:17], vcc
	v_add_i32_e32 v24, vcc, s22, v21
	v_mov_b32_e32 v25, 0x7f
	v_cmp_gt_u32_e32 vcc, v24, v25
	s_or_b64 s[16:17], vcc, s[16:17]
	v_mov_b32_e32 v33, -1
	v_cndmask_b32_e64 v24, 0, -1, s[16:17]
BB1_55:                                 ; %Flow4632
                                        ;   in Loop: Header=BB1_53 Depth=2
	s_or_b64 exec, exec, s[34:35]
	v_and_b32_e32 v25, s27, v22
	v_mul_lo_i32 v23, v23, s30
	v_mul_lo_i32 v25, v25, s29
	v_mov_b32_e32 v32, 0
                                        ; implicit-def: %VGPR26_VGPR27_VGPR28_VGPR29
	v_sub_i32_e32 v23, vcc, v25, v23
	v_add_i32_e32 v31, vcc, v23, v20
	v_cmp_ne_u32_e32 vcc, 0, v24
                                        ; implicit-def: %VGPR24_VGPR25
                                        ; implicit-def: %VGPR23
	s_and_saveexec_b64 s[16:17], vcc
	s_xor_b64 s[16:17], exec, s[16:17]
	; mask branch BB1_59
	s_cbranch_execz BB1_59
BB1_56:                                 ; %.loopexit3.i295.1
                                        ;   in Loop: Header=BB1_53 Depth=2
	v_cmp_gt_u32_e32 vcc, 4, v21
	v_mov_b32_e32 v32, 0
                                        ; implicit-def: %VGPR24_VGPR25
                                        ; implicit-def: %VGPR26_VGPR27_VGPR28_VGPR29
                                        ; implicit-def: %VGPR23
	s_and_saveexec_b64 s[34:35], vcc
	s_xor_b64 s[34:35], exec, s[34:35]
	; mask branch BB1_58
	s_cbranch_execz BB1_58
BB1_57:                                 ; %.loopexit3.i295.1..loopexit.loopexit12.i296.1_crit_edge
                                        ;   in Loop: Header=BB1_53 Depth=2
	v_mul_lo_i32 v23, v31, 10
	s_mov_b32 s44, s32
	s_mov_b32 s45, s32
	s_mov_b32 s46, s32
	s_mov_b32 s47, s32
	s_mov_b32 s42, s32
	s_mov_b32 s43, s32
	v_mov_b32_e32 v26, s44
	v_mov_b32_e32 v24, s42
	v_mov_b32_e32 v32, -1
	v_mov_b32_e32 v25, s43
	v_mov_b32_e32 v27, s45
	v_mov_b32_e32 v28, s46
	v_mov_b32_e32 v29, s47
BB1_58:                                 ; %Flow4634
                                        ;   in Loop: Header=BB1_53 Depth=2
	s_or_b64 exec, exec, s[34:35]
	v_mov_b32_e32 v33, 0
BB1_59:                                 ; %Flow4633
                                        ;   in Loop: Header=BB1_53 Depth=2
	s_or_b64 exec, exec, s[16:17]
	v_cmp_ne_u32_e32 vcc, 0, v33
	v_mov_b32_e32 v36, v29
	v_mov_b32_e32 v35, v28
	v_mov_b32_e32 v34, v27
	v_mov_b32_e32 v33, v26
	s_and_saveexec_b64 s[16:17], vcc
	s_xor_b64 s[16:17], exec, s[16:17]
	; mask branch BB1_63
	s_cbranch_execz BB1_63
BB1_60:                                 ;   in Loop: Header=BB1_53 Depth=2
	v_mul_lo_i32 v24, v21, s31
	v_mul_lo_i32 v25, v30, s36
	v_mul_lo_i32 v23, v31, 10
	s_mov_b32 s33, s32
	v_add_i32_e32 v24, vcc, s26, v24
	v_add_i32_e32 v24, vcc, v24, v25
	v_add_i32_e32 v24, vcc, v24, v23
	v_ashrrev_i32_e32 v25, 31, v24
	v_lshlrev_b64 v[24:25], 2, v[24:25]
	v_add_i32_e32 v58, vcc, s20, v24
	v_mov_b32_e32 v24, s21
	v_addc_u32_e32 v59, vcc, v24, v25, vcc
	flat_load_dwordx4 v[33:36], v[58:59]
	s_mov_b32 s34, s32
	s_mov_b32 s35, s32
	s_mov_b32 s42, s32
	s_mov_b32 s43, s32
	v_mov_b32_e32 v26, s32
	v_mov_b32_e32 v24, s42
	v_cmp_ne_u32_e32 vcc, 22, v31
	v_mov_b32_e32 v27, s33
	v_mov_b32_e32 v28, s34
	v_mov_b32_e32 v29, s35
	v_mov_b32_e32 v25, s43
	s_and_saveexec_b64 s[34:35], vcc
	s_xor_b64 s[34:35], exec, s[34:35]
	; mask branch BB1_62
BB1_61:                                 ; %.preheader4.preheader.i294.1
                                        ;   in Loop: Header=BB1_53 Depth=2
	flat_load_dwordx4 v[26:29], v[58:59] offset:16
	flat_load_dwordx2 v[24:25], v[58:59] offset:32
BB1_62:                                 ; %Flow4631
                                        ;   in Loop: Header=BB1_53 Depth=2
	s_or_b64 exec, exec, s[34:35]
	v_mov_b32_e32 v32, -1
BB1_63:                                 ; %Flow4635
                                        ;   in Loop: Header=BB1_53 Depth=2
	s_or_b64 exec, exec, s[16:17]
	v_cmp_ne_u32_e32 vcc, 0, v32
	s_and_saveexec_b64 s[16:17], vcc
	s_xor_b64 s[16:17], exec, s[16:17]
	; mask branch BB1_65
	s_cbranch_execz BB1_65
BB1_64:                                 ; %.loopexit.loopexit12.i296.1
                                        ;   in Loop: Header=BB1_53 Depth=2
	v_mul_lo_i32 v21, v21, s37
	v_mul_lo_i32 v22, v22, s38
	s_mov_b32 m0, -1
	v_add_i32_e32 v21, vcc, v22, v21
	v_add_i32_e32 v21, vcc, v21, v23
	v_lshlrev_b32_e32 v21, 2, v21
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write2_b32 v21, v33, v34 offset0:2 offset1:3
	ds_write2_b32 v21, v35, v36 offset0:4 offset1:5
	ds_write2_b32 v21, v26, v27 offset0:6 offset1:7
	ds_write2_b32 v21, v28, v29 offset0:8 offset1:9
	ds_write2_b32 v21, v24, v25 offset0:10 offset1:11
BB1_65:                                 ; %.loopexit.i297.1
                                        ;   in Loop: Header=BB1_53 Depth=2
	s_or_b64 exec, exec, s[16:17]
	v_add_i32_e32 v20, vcc, 0x100, v20
	v_mov_b32_e32 v21, 0x1cb
	v_cmp_gt_u32_e32 vcc, v20, v21
	s_or_b64 s[40:41], vcc, s[40:41]
	s_andn2_b64 exec, exec, s[40:41]
	s_cbranch_execnz BB1_53
; BB#66:                                ; %fetchData2.6.exit298.1
                                        ;   in Loop: Header=BB1_49 Depth=1
	s_or_b64 exec, exec, s[40:41]
	s_waitcnt vmcnt(0) lgkmcnt(0)
	s_barrier
	v_mov_b32_e32 v58, 0
	v_mov_b32_e32 v20, v38
	v_mov_b32_e32 v21, v39
	v_mov_b32_e32 v22, v40
	v_mov_b32_e32 v23, v41
	v_mov_b32_e32 v24, v42
	v_mov_b32_e32 v25, v43
	v_mov_b32_e32 v26, v44
	v_mov_b32_e32 v27, v45
	v_mov_b32_e32 v28, v46
	v_mov_b32_e32 v29, v47
	v_mov_b32_e32 v30, v48
	v_mov_b32_e32 v31, v49
	v_mov_b32_e32 v32, v50
	v_mov_b32_e32 v33, v51
	v_mov_b32_e32 v34, v52
	v_mov_b32_e32 v35, v53
	v_mov_b32_e32 v36, v54
	v_mov_b32_e32 v37, v55
BB1_67:                                 ; %.lr.ph13.i302.preheader.11041
                                        ;   Parent Loop BB1_49 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	s_mov_b32 m0, -1
	v_add_i32_e32 v59, vcc, 0x4920, v58
	v_add_i32_e32 v61, vcc, 0x4940, v58
	v_add_i32_e32 v63, vcc, 0x4a28, v58
	v_add_i32_e32 v67, vcc, 0x4a48, v58
	v_add_i32_e32 v71, vcc, 0x4b30, v58
	v_add_i32_e32 v73, vcc, 0x4b50, v58
	v_add_i32_e32 v81, vcc, v58, v5
	v_add_i32_e32 v65, vcc, 0x49b4, v58
	v_add_i32_e32 v69, vcc, 0x4abc, v58
	v_add_i32_e32 v75, vcc, 0x4bc4, v58
	ds_read2_b32 v[59:60], v59 offset1:4
	ds_read2_b32 v[77:78], v81 offset1:4
	ds_read2_b32 v[61:62], v61 offset1:25
	ds_read2_b32 v[79:80], v81 offset0:8 offset1:12
	ds_read2_b32 v[63:64], v63 offset1:4
	ds_read2_b32 v[67:68], v67 offset1:25
	ds_read2_b32 v[71:72], v71 offset1:4
	ds_read2_b32 v[73:74], v73 offset1:25
	ds_read2_b32 v[65:66], v65 offset1:4
	ds_read2_b32 v[69:70], v69 offset1:4
	ds_read2_b32 v[75:76], v75 offset1:4
	ds_read_b32 v81, v81 offset:64
	v_add_i32_e32 v58, vcc, 4, v58
	s_waitcnt lgkmcnt(10)
	v_mac_f32_e32 v37, v77, v59
	v_mac_f32_e32 v36, v78, v59
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v35, v79, v59
	v_mac_f32_e32 v34, v77, v62
	v_mac_f32_e32 v33, v78, v62
	v_mac_f32_e32 v32, v79, v62
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v31, v77, v63
	v_mac_f32_e32 v30, v78, v63
	v_mac_f32_e32 v29, v79, v63
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v28, v77, v68
	v_mac_f32_e32 v27, v78, v68
	v_mac_f32_e32 v26, v79, v68
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v25, v77, v71
	v_mac_f32_e32 v24, v78, v71
	v_mac_f32_e32 v23, v79, v71
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v22, v77, v74
	v_mac_f32_e32 v20, v79, v74
	v_mac_f32_e32 v21, v78, v74
	v_cmp_ne_u32_e32 vcc, 12, v58
	v_mac_f32_e32 v37, v78, v60
	v_mac_f32_e32 v36, v79, v60
	v_mac_f32_e32 v35, v80, v60
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v34, v78, v65
	v_mac_f32_e32 v33, v79, v65
	v_mac_f32_e32 v32, v80, v65
	v_mac_f32_e32 v31, v78, v64
	v_mac_f32_e32 v30, v79, v64
	v_mac_f32_e32 v29, v80, v64
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v28, v78, v69
	v_mac_f32_e32 v27, v79, v69
	v_mac_f32_e32 v26, v80, v69
	v_mac_f32_e32 v25, v78, v72
	v_mac_f32_e32 v24, v79, v72
	v_mac_f32_e32 v23, v80, v72
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v22, v78, v75
	v_mac_f32_e32 v20, v80, v75
	v_mac_f32_e32 v21, v79, v75
	s_and_b64 vcc, exec, vcc
	v_mac_f32_e32 v36, v80, v61
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v35, v81, v61
	v_mac_f32_e32 v34, v79, v66
	v_mac_f32_e32 v33, v80, v66
	v_mac_f32_e32 v32, v81, v66
	v_mac_f32_e32 v31, v79, v67
	v_mac_f32_e32 v30, v80, v67
	v_mac_f32_e32 v29, v81, v67
	v_mac_f32_e32 v28, v79, v70
	v_mac_f32_e32 v27, v80, v70
	v_mac_f32_e32 v26, v81, v70
	v_mac_f32_e32 v25, v79, v73
	v_mac_f32_e32 v24, v80, v73
	v_mac_f32_e32 v23, v81, v73
	v_mac_f32_e32 v22, v79, v76
	v_mac_f32_e32 v20, v81, v76
	v_mac_f32_e32 v21, v80, v76
	v_mac_f32_e32 v37, v79, v61
	s_cbranch_vccnz BB1_67
; BB#68:                                ; %.lr.ph13.i399.preheader.11095
                                        ;   in Loop: Header=BB1_49 Depth=1
	v_mov_b32_e32 v58, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v59, vcc, 0x492c, v58
	v_add_i32_e32 v61, vcc, 0x49b0, v58
	v_add_i32_e32 v63, vcc, 0x4a34, v58
	v_add_i32_e32 v65, vcc, 0x4ab8, v58
	v_add_i32_e32 v67, vcc, 0x4b3c, v58
	v_add_i32_e32 v69, vcc, 0x4bc0, v58
	ds_read2_b32 v[59:60], v59 offset1:4
	ds_read_b32 v71, v3 offset:12
	ds_read2_b32 v[61:62], v61 offset1:4
	ds_read_b32 v72, v16 offset:12
	ds_read2_b32 v[63:64], v63 offset1:4
	ds_read_b32 v73, v17 offset:12
	ds_read2_b32 v[65:66], v65 offset1:4
	ds_read2_b32 v[67:68], v67 offset1:4
	ds_read2_b32 v[69:70], v69 offset1:4
	ds_read_b32 v74, v2 offset:48
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v37, v71, v59
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v36, v72, v59
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v35, v73, v59
	v_mac_f32_e32 v34, v71, v61
	v_mac_f32_e32 v33, v72, v61
	v_mac_f32_e32 v32, v73, v61
	v_mac_f32_e32 v31, v71, v63
	v_mac_f32_e32 v30, v72, v63
	v_mac_f32_e32 v29, v73, v63
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v28, v71, v65
	v_mac_f32_e32 v27, v72, v65
	v_mac_f32_e32 v26, v73, v65
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v25, v71, v67
	v_mac_f32_e32 v24, v72, v67
	v_mac_f32_e32 v23, v73, v67
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v22, v71, v69
	v_mac_f32_e32 v20, v73, v69
	v_mac_f32_e32 v21, v72, v69
	v_mac_f32_e32 v37, v72, v60
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v35, v74, v60
	v_mac_f32_e32 v34, v72, v62
	v_mac_f32_e32 v33, v73, v62
	v_mac_f32_e32 v32, v74, v62
	v_mac_f32_e32 v31, v72, v64
	v_mac_f32_e32 v30, v73, v64
	v_mac_f32_e32 v29, v74, v64
	v_mac_f32_e32 v28, v72, v66
	v_mac_f32_e32 v27, v73, v66
	v_mac_f32_e32 v26, v74, v66
	v_mac_f32_e32 v25, v72, v68
	v_mac_f32_e32 v24, v73, v68
	v_mac_f32_e32 v23, v74, v68
	v_mac_f32_e32 v22, v72, v70
	v_mac_f32_e32 v20, v74, v70
	v_mac_f32_e32 v21, v73, v70
	v_mac_f32_e32 v36, v73, v60
BB1_69:                                 ; %.lr.ph13.i302.preheader.1.1
                                        ;   Parent Loop BB1_49 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	s_mov_b32 m0, -1
	v_add_i32_e32 v59, vcc, 0x494c, v58
	v_add_i32_e32 v61, vcc, 0x496c, v58
	v_add_i32_e32 v63, vcc, 0x4a54, v58
	v_add_i32_e32 v67, vcc, 0x4a74, v58
	v_add_i32_e32 v71, vcc, 0x4b5c, v58
	v_add_i32_e32 v73, vcc, 0x4b7c, v58
	v_add_i32_e32 v77, vcc, v58, v3
	v_add_i32_e32 v79, vcc, v58, v4
	v_add_i32_e32 v65, vcc, 0x49e0, v58
	v_add_i32_e32 v69, vcc, 0x4ae8, v58
	v_add_i32_e32 v75, vcc, 0x4bf0, v58
	ds_read_b32 v81, v77 offset:936
	ds_read2_b32 v[59:60], v59 offset1:4
	ds_read2_b32 v[77:78], v79 offset0:238 offset1:242
	ds_read2_b32 v[61:62], v61 offset1:25
	ds_read2_b32 v[63:64], v63 offset1:4
	ds_read2_b32 v[67:68], v67 offset1:25
	ds_read2_b32 v[71:72], v71 offset1:4
	ds_read2_b32 v[73:74], v73 offset1:25
	ds_read2_b32 v[65:66], v65 offset1:4
	ds_read2_b32 v[69:70], v69 offset1:4
	ds_read2_b32 v[75:76], v75 offset1:4
	ds_read2_b32 v[79:80], v79 offset0:246 offset1:250
	v_add_i32_e32 v58, vcc, 4, v58
	s_waitcnt lgkmcnt(10)
	v_mac_f32_e32 v37, v81, v59
	s_waitcnt lgkmcnt(9)
	v_mac_f32_e32 v36, v77, v59
	v_mac_f32_e32 v35, v78, v59
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v34, v81, v62
	v_mac_f32_e32 v33, v77, v62
	v_mac_f32_e32 v32, v78, v62
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v31, v81, v63
	v_mac_f32_e32 v30, v77, v63
	v_mac_f32_e32 v29, v78, v63
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v28, v81, v68
	v_mac_f32_e32 v27, v77, v68
	v_mac_f32_e32 v26, v78, v68
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v25, v81, v71
	v_mac_f32_e32 v24, v77, v71
	v_mac_f32_e32 v23, v78, v71
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v22, v81, v74
	v_mac_f32_e32 v20, v78, v74
	v_mac_f32_e32 v21, v77, v74
	v_cmp_ne_u32_e32 vcc, 12, v58
	v_mac_f32_e32 v37, v77, v60
	v_mac_f32_e32 v36, v78, v60
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v35, v79, v60
	v_mac_f32_e32 v34, v77, v65
	v_mac_f32_e32 v33, v78, v65
	v_mac_f32_e32 v32, v79, v65
	v_mac_f32_e32 v31, v77, v64
	v_mac_f32_e32 v30, v78, v64
	v_mac_f32_e32 v29, v79, v64
	v_mac_f32_e32 v28, v77, v69
	v_mac_f32_e32 v27, v78, v69
	v_mac_f32_e32 v26, v79, v69
	v_mac_f32_e32 v25, v77, v72
	v_mac_f32_e32 v24, v78, v72
	v_mac_f32_e32 v23, v79, v72
	v_mac_f32_e32 v22, v77, v75
	v_mac_f32_e32 v20, v79, v75
	v_mac_f32_e32 v21, v78, v75
	s_and_b64 vcc, exec, vcc
	v_mac_f32_e32 v36, v79, v61
	v_mac_f32_e32 v35, v80, v61
	v_mac_f32_e32 v34, v78, v66
	v_mac_f32_e32 v33, v79, v66
	v_mac_f32_e32 v32, v80, v66
	v_mac_f32_e32 v31, v78, v67
	v_mac_f32_e32 v30, v79, v67
	v_mac_f32_e32 v29, v80, v67
	v_mac_f32_e32 v28, v78, v70
	v_mac_f32_e32 v27, v79, v70
	v_mac_f32_e32 v26, v80, v70
	v_mac_f32_e32 v25, v78, v73
	v_mac_f32_e32 v24, v79, v73
	v_mac_f32_e32 v23, v80, v73
	v_mac_f32_e32 v22, v78, v76
	v_mac_f32_e32 v20, v80, v76
	v_mac_f32_e32 v21, v79, v76
	v_mac_f32_e32 v37, v78, v61
	s_cbranch_vccnz BB1_69
; BB#70:                                ; %.lr.ph13.i399.preheader.1.1
                                        ;   in Loop: Header=BB1_49 Depth=1
	v_mov_b32_e32 v58, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v59, vcc, 0x4958, v58
	v_add_i32_e32 v61, vcc, 0x49dc, v58
	v_add_i32_e32 v63, vcc, 0x4a60, v58
	v_add_i32_e32 v65, vcc, 0x4ae4, v58
	v_add_i32_e32 v67, vcc, 0x4b68, v58
	v_add_i32_e32 v69, vcc, 0x4bec, v58
	ds_read2_b32 v[59:60], v59 offset1:4
	ds_read_b32 v73, v19 offset:936
	ds_read2_b32 v[61:62], v61 offset1:4
	ds_read_b32 v74, v18 offset:936
	ds_read2_b32 v[63:64], v63 offset1:4
	ds_read2_b32 v[71:72], v2 offset0:234 offset1:246
	ds_read2_b32 v[65:66], v65 offset1:4
	ds_read2_b32 v[67:68], v67 offset1:4
	ds_read2_b32 v[69:70], v69 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v36, v73, v59
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v37, v71, v59
	v_mac_f32_e32 v35, v74, v59
	v_mac_f32_e32 v34, v71, v61
	v_mac_f32_e32 v33, v73, v61
	v_mac_f32_e32 v32, v74, v61
	v_mac_f32_e32 v31, v71, v63
	v_mac_f32_e32 v30, v73, v63
	v_mac_f32_e32 v29, v74, v63
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v28, v71, v65
	v_mac_f32_e32 v27, v73, v65
	v_mac_f32_e32 v26, v74, v65
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v25, v71, v67
	v_mac_f32_e32 v24, v73, v67
	v_mac_f32_e32 v23, v74, v67
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v22, v71, v69
	v_mac_f32_e32 v20, v74, v69
	v_mac_f32_e32 v21, v73, v69
	v_mac_f32_e32 v37, v73, v60
	v_mac_f32_e32 v35, v72, v60
	v_mac_f32_e32 v34, v73, v62
	v_mac_f32_e32 v33, v74, v62
	v_mac_f32_e32 v32, v72, v62
	v_mac_f32_e32 v31, v73, v64
	v_mac_f32_e32 v30, v74, v64
	v_mac_f32_e32 v29, v72, v64
	v_mac_f32_e32 v28, v73, v66
	v_mac_f32_e32 v27, v74, v66
	v_mac_f32_e32 v26, v72, v66
	v_mac_f32_e32 v25, v73, v68
	v_mac_f32_e32 v24, v74, v68
	v_mac_f32_e32 v23, v72, v68
	v_mac_f32_e32 v22, v73, v70
	v_mac_f32_e32 v20, v72, v70
	v_mac_f32_e32 v21, v74, v70
	v_mac_f32_e32 v36, v74, v60
BB1_71:                                 ; %.lr.ph13.i302.preheader.2.1
                                        ;   Parent Loop BB1_49 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_add_i32_e32 v77, vcc, v58, v4
	s_mov_b32 m0, -1
	v_add_i32_e32 v59, vcc, 0x4978, v58
	v_add_i32_e32 v61, vcc, 0x4998, v58
	v_add_i32_e32 v65, vcc, 0x4a80, v58
	v_add_i32_e32 v67, vcc, 0x4aa0, v58
	v_add_i32_e32 v71, vcc, 0x4b88, v58
	v_add_i32_e32 v73, vcc, 0x4ba8, v58
	v_add_i32_e32 v78, vcc, v58, v3
	v_add_i32_e32 v79, vcc, 0x760, v77
	v_add_i32_e32 v63, vcc, 0x4a0c, v58
	v_add_i32_e32 v69, vcc, 0x4b14, v58
	v_add_i32_e32 v75, vcc, 0x4c1c, v58
	ds_read2_b32 v[59:60], v59 offset1:4
	ds_read_b32 v81, v78 offset:1872
	ds_read2_b32 v[61:62], v61 offset1:25
	ds_read2_b32 v[65:66], v65 offset1:4
	ds_read2_b32 v[67:68], v67 offset1:25
	ds_read2_b32 v[71:72], v71 offset1:4
	ds_read2_b32 v[73:74], v73 offset1:25
	v_add_i32_e32 v80, vcc, 0x780, v77
	ds_read2_b32 v[77:78], v79 offset1:4
	ds_read2_b32 v[63:64], v63 offset1:4
	ds_read2_b32 v[69:70], v69 offset1:4
	ds_read2_b32 v[75:76], v75 offset1:4
	ds_read2_b32 v[79:80], v80 offset1:4
	v_add_i32_e32 v58, vcc, 4, v58
	s_waitcnt lgkmcnt(10)
	v_mac_f32_e32 v37, v81, v59
	s_waitcnt lgkmcnt(9)
	v_mac_f32_e32 v34, v81, v62
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v31, v81, v65
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v28, v81, v68
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v25, v81, v71
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v22, v81, v74
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v36, v77, v59
	v_mac_f32_e32 v35, v78, v59
	v_mac_f32_e32 v33, v77, v62
	v_mac_f32_e32 v32, v78, v62
	v_mac_f32_e32 v30, v77, v65
	v_mac_f32_e32 v29, v78, v65
	v_mac_f32_e32 v27, v77, v68
	v_mac_f32_e32 v26, v78, v68
	v_mac_f32_e32 v24, v77, v71
	v_mac_f32_e32 v23, v78, v71
	v_mac_f32_e32 v20, v78, v74
	v_mac_f32_e32 v21, v77, v74
	v_cmp_ne_u32_e32 vcc, 12, v58
	v_mac_f32_e32 v37, v77, v60
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v34, v77, v63
	v_mac_f32_e32 v31, v77, v66
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v28, v77, v69
	v_mac_f32_e32 v25, v77, v72
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v22, v77, v75
	v_mac_f32_e32 v36, v78, v60
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v35, v79, v60
	v_mac_f32_e32 v33, v78, v63
	v_mac_f32_e32 v32, v79, v63
	v_mac_f32_e32 v30, v78, v66
	v_mac_f32_e32 v29, v79, v66
	v_mac_f32_e32 v27, v78, v69
	v_mac_f32_e32 v26, v79, v69
	v_mac_f32_e32 v24, v78, v72
	v_mac_f32_e32 v23, v79, v72
	v_mac_f32_e32 v20, v79, v75
	v_mac_f32_e32 v21, v78, v75
	s_and_b64 vcc, exec, vcc
	v_mac_f32_e32 v34, v78, v64
	v_mac_f32_e32 v31, v78, v67
	v_mac_f32_e32 v28, v78, v70
	v_mac_f32_e32 v25, v78, v73
	v_mac_f32_e32 v22, v78, v76
	v_mac_f32_e32 v37, v78, v61
	v_mac_f32_e32 v36, v79, v61
	v_mac_f32_e32 v35, v80, v61
	v_mac_f32_e32 v33, v79, v64
	v_mac_f32_e32 v32, v80, v64
	v_mac_f32_e32 v30, v79, v67
	v_mac_f32_e32 v29, v80, v67
	v_mac_f32_e32 v27, v79, v70
	v_mac_f32_e32 v26, v80, v70
	v_mac_f32_e32 v24, v79, v73
	v_mac_f32_e32 v23, v80, v73
	v_mac_f32_e32 v20, v80, v76
	v_mac_f32_e32 v21, v79, v76
	s_cbranch_vccnz BB1_71
; BB#72:                                ; %.lr.ph13.i399.preheader.2.1
                                        ;   in Loop: Header=BB1_49 Depth=1
	v_mov_b32_e32 v58, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v59, vcc, 0x4984, v58
	v_add_i32_e32 v61, vcc, 0x4a08, v58
	v_add_i32_e32 v63, vcc, 0x4a8c, v58
	v_add_i32_e32 v65, vcc, 0x4b10, v58
	v_add_i32_e32 v67, vcc, 0x4b94, v58
	v_add_i32_e32 v69, vcc, 0x4c18, v58
	v_add_i32_e32 v71, vcc, 0x750, v2
	ds_read2_b32 v[59:60], v59 offset1:4
	ds_read_b32 v73, v19 offset:1872
	ds_read2_b32 v[61:62], v61 offset1:4
	ds_read_b32 v74, v18 offset:1872
	ds_read2_b32 v[63:64], v63 offset1:4
	ds_read2_b32 v[71:72], v71 offset1:12
	ds_read2_b32 v[65:66], v65 offset1:4
	ds_read2_b32 v[67:68], v67 offset1:4
	ds_read2_b32 v[69:70], v69 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v36, v73, v59
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v37, v71, v59
	v_mac_f32_e32 v35, v74, v59
	v_mac_f32_e32 v34, v71, v61
	v_mac_f32_e32 v33, v73, v61
	v_mac_f32_e32 v32, v74, v61
	v_mac_f32_e32 v31, v71, v63
	v_mac_f32_e32 v30, v73, v63
	v_mac_f32_e32 v29, v74, v63
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v28, v71, v65
	v_mac_f32_e32 v27, v73, v65
	v_mac_f32_e32 v26, v74, v65
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v25, v71, v67
	v_mac_f32_e32 v24, v73, v67
	v_mac_f32_e32 v23, v74, v67
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v22, v71, v69
	v_mac_f32_e32 v20, v74, v69
	v_mac_f32_e32 v21, v73, v69
	v_mac_f32_e32 v37, v73, v60
	v_mac_f32_e32 v35, v72, v60
	v_mac_f32_e32 v34, v73, v62
	v_mac_f32_e32 v33, v74, v62
	v_mac_f32_e32 v32, v72, v62
	v_mac_f32_e32 v31, v73, v64
	v_mac_f32_e32 v30, v74, v64
	v_mac_f32_e32 v29, v72, v64
	v_mac_f32_e32 v28, v73, v66
	v_mac_f32_e32 v27, v74, v66
	v_mac_f32_e32 v26, v72, v66
	v_mac_f32_e32 v25, v73, v68
	v_mac_f32_e32 v24, v74, v68
	v_mac_f32_e32 v23, v72, v68
	v_mac_f32_e32 v22, v73, v70
	v_mac_f32_e32 v20, v72, v70
	v_mac_f32_e32 v21, v74, v70
	v_mac_f32_e32 v36, v74, v60
	v_add_i32_e32 v56, vcc, 1, v56
BB1_73:                                 ; %Flow4636
                                        ;   in Loop: Header=BB1_49 Depth=1
	v_cmp_ne_u32_e32 vcc, 0, v58
	s_cbranch_vccz BB1_49
; BB#74:
	s_and_saveexec_b64 s[16:17], s[14:15]
	s_xor_b64 s[28:29], exec, s[16:17]
	; mask branch BB1_76
	s_cbranch_execz BB1_76
BB1_75:                                 ; %.lr.ph.i493.1
	v_lshlrev_b32_e32 v20, 1, v7
	v_and_b32_e32 v20, 0x1fffffe, v20
	v_subrev_i32_e32 v22, vcc, v20, v0
	v_lshlrev_b32_e32 v20, 2, v22
	v_or_b32_e32 v20, 3, v20
	v_mul_lo_i32 v21, v20, 11
	v_cmp_gt_u32_e32 vcc, 11, v20
	s_and_b64 s[16:17], exec, s[12:13]
	s_and_b64 s[16:17], s[16:17], vcc
	v_add_i32_e32 v20, vcc, v6, v21
	v_add_i32_e32 v20, vcc, 0x79, v20
	v_ashrrev_i32_e32 v21, 31, v20
	v_cndmask_b32_e64 v21, 0, v21, s[16:17]
	v_cndmask_b32_e64 v20, 0, v20, s[16:17]
	v_lshlrev_b64 v[20:21], 2, v[20:21]
	v_add_i32_e32 v28, vcc, s24, v20
	v_mov_b32_e32 v20, s25
	v_addc_u32_e32 v29, vcc, v20, v21, vcc
	v_mul_lo_i32 v30, v22, 11
	flat_load_dwordx4 v[20:23], v[28:29]
	flat_load_dwordx4 v[24:27], v[28:29] offset:16
	flat_load_dword v31, v[28:29] offset:40
	flat_load_dwordx2 v[28:29], v[28:29] offset:32
	v_add_i32_e32 v30, vcc, v15, v30
	v_lshlrev_b32_e32 v30, 2, v30
	s_mov_b32 m0, -1
	v_add_i32_e32 v32, vcc, 0x4920, v30
	v_add_i32_e32 v33, vcc, 0x4928, v30
	v_add_i32_e32 v34, vcc, 0x4930, v30
	v_add_i32_e32 v35, vcc, 0x4938, v30
	v_add_i32_e32 v36, vcc, 0x4940, v30
	s_waitcnt vmcnt(3) lgkmcnt(3)
	ds_write2_b32 v32, v20, v21 offset1:1
	ds_write2_b32 v33, v22, v23 offset1:1
	s_waitcnt vmcnt(2) lgkmcnt(4)
	ds_write2_b32 v34, v24, v25 offset1:1
	ds_write2_b32 v35, v26, v27 offset1:1
	s_waitcnt vmcnt(0) lgkmcnt(4)
	ds_write2_b32 v36, v28, v29 offset1:1
	ds_write_b32 v30, v31 offset:18760
BB1_76:                                 ; %.lr.ph.i.1.preheader
	s_or_b64 exec, exec, s[28:29]
	s_mov_b64 s[28:29], 0
	s_mov_b32 s27, 0xffffff
	s_movk_i32 s30, 0xffa4
	s_movk_i32 s31, 0xffe9
	s_movk_i32 s32, 0x5c
	s_mov_b32 s36, 0
	s_mov_b32 s33, 0x24c00
	s_movk_i32 s34, 0xe0
	s_movk_i32 s35, 0x492
	s_movk_i32 s40, 0xea
	v_mov_b32_e32 v20, v0
	s_mov_b32 m0, -1
BB1_77:                                 ; %.lr.ph.i.1
                                        ; =>This Inner Loop Header: Depth=1
	v_cvt_f32_u32_e32 v21, v20
	v_mov_b32_e32 v22, 0x3727c5ac
	s_waitcnt vmcnt(0) lgkmcnt(0)
	v_mov_b32_e32 v24, 0x3727c5ac
	v_mov_b32_e32 v25, 0xffffff31
	v_mac_f32_e32 v22, 0x3c321643, v21
	v_cvt_u32_f32_e32 v21, v22
	v_mov_b32_e32 v33, 0
	v_and_b32_e32 v23, s27, v21
	v_mul_lo_i32 v22, v23, s30
	v_add_i32_e32 v22, vcc, v22, v20
	v_cvt_f32_u32_e32 v22, v22
	v_mac_f32_e32 v24, 0x3d321643, v22
	v_cvt_u32_f32_e32 v22, v24
	v_mov_b32_e32 v24, 0xffffff32
	v_lshlrev_b32_e32 v26, 2, v22
	v_or_b32_e32 v30, 3, v26
	v_cmp_lt_i32_e32 vcc, v30, v24
	v_cmp_gt_i32_e64 s[16:17], v30, v25
	v_cndmask_b32_e64 v24, 0, -1, vcc
	s_and_saveexec_b64 s[38:39], s[16:17]
	s_xor_b64 s[38:39], exec, s[38:39]
	; mask branch BB1_79
BB1_78:                                 ;   in Loop: Header=BB1_77 Depth=1
	v_cmp_lt_i32_e32 vcc, 17, v30
	v_cmp_lt_u32_e64 s[16:17], 3, v21
	s_or_b64 s[16:17], s[16:17], vcc
	v_add_i32_e32 v24, vcc, s22, v21
	v_mov_b32_e32 v25, 0x7f
	v_cmp_gt_u32_e32 vcc, v24, v25
	s_or_b64 s[16:17], vcc, s[16:17]
	v_mov_b32_e32 v33, -1
	v_cndmask_b32_e64 v24, 0, -1, s[16:17]
BB1_79:                                 ; %Flow4627
                                        ;   in Loop: Header=BB1_77 Depth=1
	s_or_b64 exec, exec, s[38:39]
	v_and_b32_e32 v25, s27, v22
	v_mul_lo_i32 v23, v23, s32
	v_mul_lo_i32 v25, v25, s31
	v_mov_b32_e32 v32, 0
                                        ; implicit-def: %VGPR26_VGPR27_VGPR28_VGPR29
	v_sub_i32_e32 v23, vcc, v25, v23
	v_add_i32_e32 v31, vcc, v23, v20
	v_cmp_ne_u32_e32 vcc, 0, v24
                                        ; implicit-def: %VGPR24_VGPR25
                                        ; implicit-def: %VGPR23
	s_and_saveexec_b64 s[16:17], vcc
	s_xor_b64 s[16:17], exec, s[16:17]
	; mask branch BB1_83
	s_cbranch_execz BB1_83
BB1_80:                                 ; %.loopexit3.i.1
                                        ;   in Loop: Header=BB1_77 Depth=1
	v_cmp_gt_u32_e32 vcc, 4, v21
	v_mov_b32_e32 v32, 0
                                        ; implicit-def: %VGPR24_VGPR25
                                        ; implicit-def: %VGPR26_VGPR27_VGPR28_VGPR29
                                        ; implicit-def: %VGPR23
	s_and_saveexec_b64 s[38:39], vcc
	s_xor_b64 s[38:39], exec, s[38:39]
	; mask branch BB1_82
	s_cbranch_execz BB1_82
BB1_81:                                 ; %.loopexit3.i.1..loopexit.loopexit12.i.1_crit_edge
                                        ;   in Loop: Header=BB1_77 Depth=1
	v_mul_lo_i32 v23, v31, 10
	s_mov_b32 s44, s36
	s_mov_b32 s45, s36
	s_mov_b32 s46, s36
	s_mov_b32 s47, s36
	s_mov_b32 s42, s36
	s_mov_b32 s43, s36
	v_mov_b32_e32 v26, s44
	v_mov_b32_e32 v24, s42
	v_mov_b32_e32 v32, -1
	v_mov_b32_e32 v25, s43
	v_mov_b32_e32 v27, s45
	v_mov_b32_e32 v28, s46
	v_mov_b32_e32 v29, s47
BB1_82:                                 ; %Flow4629
                                        ;   in Loop: Header=BB1_77 Depth=1
	s_or_b64 exec, exec, s[38:39]
	v_mov_b32_e32 v33, 0
BB1_83:                                 ; %Flow4628
                                        ;   in Loop: Header=BB1_77 Depth=1
	s_or_b64 exec, exec, s[16:17]
	v_cmp_ne_u32_e32 vcc, 0, v33
	v_mov_b32_e32 v36, v29
	v_mov_b32_e32 v35, v28
	v_mov_b32_e32 v34, v27
	v_mov_b32_e32 v33, v26
	s_and_saveexec_b64 s[16:17], vcc
	s_xor_b64 s[16:17], exec, s[16:17]
	; mask branch BB1_87
	s_cbranch_execz BB1_87
BB1_84:                                 ;   in Loop: Header=BB1_77 Depth=1
	v_mul_lo_i32 v24, v21, s33
	v_mul_lo_i32 v25, v30, s34
	v_mul_lo_i32 v23, v31, 10
	s_mov_b32 s37, s36
	v_add_i32_e32 v24, vcc, s26, v24
	v_add_i32_e32 v24, vcc, v25, v24
	v_add_i32_e32 v24, vcc, v24, v23
	v_ashrrev_i32_e32 v25, 31, v24
	v_lshlrev_b64 v[24:25], 2, v[24:25]
	v_add_i32_e32 v56, vcc, s20, v24
	v_mov_b32_e32 v24, s21
	v_addc_u32_e32 v57, vcc, v24, v25, vcc
	flat_load_dwordx4 v[33:36], v[56:57]
	s_mov_b32 s38, s36
	s_mov_b32 s39, s36
	s_mov_b32 s42, s36
	s_mov_b32 s43, s36
	v_mov_b32_e32 v26, s36
	v_mov_b32_e32 v24, s42
	v_cmp_ne_u32_e32 vcc, 22, v31
	v_mov_b32_e32 v27, s37
	v_mov_b32_e32 v28, s38
	v_mov_b32_e32 v29, s39
	v_mov_b32_e32 v25, s43
	s_and_saveexec_b64 s[38:39], vcc
	s_xor_b64 s[38:39], exec, s[38:39]
	; mask branch BB1_86
BB1_85:                                 ; %.preheader4.preheader.i.1
                                        ;   in Loop: Header=BB1_77 Depth=1
	flat_load_dwordx4 v[26:29], v[56:57] offset:16
	flat_load_dwordx2 v[24:25], v[56:57] offset:32
BB1_86:                                 ; %Flow4626
                                        ;   in Loop: Header=BB1_77 Depth=1
	s_or_b64 exec, exec, s[38:39]
	v_mov_b32_e32 v32, -1
BB1_87:                                 ; %Flow4630
                                        ;   in Loop: Header=BB1_77 Depth=1
	s_or_b64 exec, exec, s[16:17]
	v_cmp_ne_u32_e32 vcc, 0, v32
	s_and_saveexec_b64 s[16:17], vcc
	s_xor_b64 s[16:17], exec, s[16:17]
	; mask branch BB1_89
BB1_88:                                 ; %.loopexit.loopexit12.i.1
                                        ;   in Loop: Header=BB1_77 Depth=1
	v_mul_lo_i32 v21, v21, s35
	v_mul_lo_i32 v22, v22, s40
	v_add_i32_e32 v21, vcc, v22, v21
	v_add_i32_e32 v21, vcc, v21, v23
	v_lshlrev_b32_e32 v21, 2, v21
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write2_b32 v21, v33, v34 offset0:2 offset1:3
	ds_write2_b32 v21, v35, v36 offset0:4 offset1:5
	ds_write2_b32 v21, v26, v27 offset0:6 offset1:7
	ds_write2_b32 v21, v28, v29 offset0:8 offset1:9
	ds_write2_b32 v21, v24, v25 offset0:10 offset1:11
BB1_89:                                 ; %.loopexit.i.1
                                        ;   in Loop: Header=BB1_77 Depth=1
	s_or_b64 exec, exec, s[16:17]
	v_add_i32_e32 v20, vcc, 0x100, v20
	v_mov_b32_e32 v21, 0x16f
	v_cmp_gt_u32_e32 vcc, v20, v21
	s_or_b64 s[28:29], vcc, s[28:29]
	s_andn2_b64 exec, exec, s[28:29]
	s_cbranch_execnz BB1_77
; BB#90:                                ; %fetchData2.6.exit.1
	s_or_b64 exec, exec, s[28:29]
	s_waitcnt vmcnt(0) lgkmcnt(0)
	s_barrier
	v_mov_b32_e32 v20, 0
	s_mov_b32 m0, -1
BB1_91:                                 ; %.lr.ph13.i195.preheader.1933
                                        ; =>This Inner Loop Header: Depth=1
	v_add_i32_e32 v21, vcc, 0x4920, v20
	v_add_i32_e32 v23, vcc, 0x4940, v20
	v_add_i32_e32 v25, vcc, 0x4a28, v20
	v_add_i32_e32 v29, vcc, 0x4a48, v20
	v_add_i32_e32 v33, vcc, 0x4b30, v20
	v_add_i32_e32 v35, vcc, 0x4b50, v20
	v_add_i32_e32 v37, vcc, v20, v5
	v_add_i32_e32 v27, vcc, 0x49b4, v20
	v_add_i32_e32 v31, vcc, 0x4abc, v20
	v_add_i32_e32 v56, vcc, 0x4bc4, v20
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[58:59], v37 offset1:4
	ds_read2_b32 v[23:24], v23 offset1:25
	ds_read2_b32 v[60:61], v37 offset0:8 offset1:12
	ds_read2_b32 v[25:26], v25 offset1:4
	ds_read2_b32 v[29:30], v29 offset1:25
	ds_read2_b32 v[33:34], v33 offset1:4
	ds_read2_b32 v[35:36], v35 offset1:25
	ds_read2_b32 v[27:28], v27 offset1:4
	ds_read2_b32 v[31:32], v31 offset1:4
	ds_read2_b32 v[56:57], v56 offset1:4
	ds_read_b32 v37, v37 offset:64
	v_add_i32_e32 v20, vcc, 4, v20
	s_waitcnt lgkmcnt(10)
	v_mac_f32_e32 v55, v58, v21
	v_mac_f32_e32 v54, v59, v21
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v53, v60, v21
	v_mac_f32_e32 v52, v58, v24
	v_mac_f32_e32 v51, v59, v24
	v_mac_f32_e32 v50, v60, v24
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v49, v58, v25
	v_mac_f32_e32 v48, v59, v25
	v_mac_f32_e32 v47, v60, v25
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v46, v58, v30
	v_mac_f32_e32 v45, v59, v30
	v_mac_f32_e32 v44, v60, v30
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v43, v58, v33
	v_mac_f32_e32 v42, v59, v33
	v_mac_f32_e32 v41, v60, v33
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v40, v58, v36
	v_mac_f32_e32 v38, v60, v36
	v_mac_f32_e32 v39, v59, v36
	v_cmp_ne_u32_e32 vcc, 12, v20
	v_mac_f32_e32 v55, v59, v22
	v_mac_f32_e32 v54, v60, v22
	v_mac_f32_e32 v53, v61, v22
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v52, v59, v27
	v_mac_f32_e32 v51, v60, v27
	v_mac_f32_e32 v50, v61, v27
	v_mac_f32_e32 v49, v59, v26
	v_mac_f32_e32 v48, v60, v26
	v_mac_f32_e32 v47, v61, v26
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v46, v59, v31
	v_mac_f32_e32 v45, v60, v31
	v_mac_f32_e32 v44, v61, v31
	v_mac_f32_e32 v43, v59, v34
	v_mac_f32_e32 v42, v60, v34
	v_mac_f32_e32 v41, v61, v34
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v40, v59, v56
	v_mac_f32_e32 v38, v61, v56
	v_mac_f32_e32 v39, v60, v56
	s_and_b64 vcc, exec, vcc
	v_mac_f32_e32 v54, v61, v23
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v53, v37, v23
	v_mac_f32_e32 v52, v60, v28
	v_mac_f32_e32 v51, v61, v28
	v_mac_f32_e32 v50, v37, v28
	v_mac_f32_e32 v49, v60, v29
	v_mac_f32_e32 v48, v61, v29
	v_mac_f32_e32 v47, v37, v29
	v_mac_f32_e32 v46, v60, v32
	v_mac_f32_e32 v45, v61, v32
	v_mac_f32_e32 v44, v37, v32
	v_mac_f32_e32 v43, v60, v35
	v_mac_f32_e32 v42, v61, v35
	v_mac_f32_e32 v41, v37, v35
	v_mac_f32_e32 v40, v60, v57
	v_mac_f32_e32 v38, v37, v57
	v_mac_f32_e32 v39, v61, v57
	v_mac_f32_e32 v55, v60, v23
	s_cbranch_vccnz BB1_91
; BB#92:                                ; %.lr.ph13.i.preheader.1987
	v_mov_b32_e32 v20, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v21, vcc, 0x492c, v20
	v_add_i32_e32 v23, vcc, 0x49b0, v20
	v_add_i32_e32 v25, vcc, 0x4a34, v20
	v_add_i32_e32 v27, vcc, 0x4ab8, v20
	v_add_i32_e32 v29, vcc, 0x4b3c, v20
	v_add_i32_e32 v31, vcc, 0x4bc0, v20
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read_b32 v33, v3 offset:12
	ds_read2_b32 v[23:24], v23 offset1:4
	ds_read_b32 v34, v16 offset:12
	ds_read2_b32 v[25:26], v25 offset1:4
	ds_read_b32 v35, v17 offset:12
	ds_read2_b32 v[27:28], v27 offset1:4
	ds_read2_b32 v[29:30], v29 offset1:4
	ds_read2_b32 v[31:32], v31 offset1:4
	ds_read_b32 v36, v2 offset:48
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v55, v33, v21
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v54, v34, v21
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v53, v35, v21
	v_mac_f32_e32 v52, v33, v23
	v_mac_f32_e32 v51, v34, v23
	v_mac_f32_e32 v50, v35, v23
	v_mac_f32_e32 v49, v33, v25
	v_mac_f32_e32 v48, v34, v25
	v_mac_f32_e32 v47, v35, v25
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v46, v33, v27
	v_mac_f32_e32 v45, v34, v27
	v_mac_f32_e32 v44, v35, v27
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v43, v33, v29
	v_mac_f32_e32 v42, v34, v29
	v_mac_f32_e32 v41, v35, v29
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v40, v33, v31
	v_mac_f32_e32 v38, v35, v31
	v_mac_f32_e32 v39, v34, v31
	v_mac_f32_e32 v55, v34, v22
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v53, v36, v22
	v_mac_f32_e32 v52, v34, v24
	v_mac_f32_e32 v51, v35, v24
	v_mac_f32_e32 v50, v36, v24
	v_mac_f32_e32 v49, v34, v26
	v_mac_f32_e32 v48, v35, v26
	v_mac_f32_e32 v47, v36, v26
	v_mac_f32_e32 v46, v34, v28
	v_mac_f32_e32 v45, v35, v28
	v_mac_f32_e32 v44, v36, v28
	v_mac_f32_e32 v43, v34, v30
	v_mac_f32_e32 v42, v35, v30
	v_mac_f32_e32 v41, v36, v30
	v_mac_f32_e32 v40, v34, v32
	v_mac_f32_e32 v38, v36, v32
	v_mac_f32_e32 v39, v35, v32
	v_mac_f32_e32 v54, v35, v22
	s_mov_b32 m0, -1
BB1_93:                                 ; %.lr.ph13.i195.preheader.1.1
                                        ; =>This Inner Loop Header: Depth=1
	v_add_i32_e32 v21, vcc, 0x494c, v20
	v_add_i32_e32 v23, vcc, 0x496c, v20
	v_add_i32_e32 v25, vcc, 0x4a54, v20
	v_add_i32_e32 v29, vcc, 0x4a74, v20
	v_add_i32_e32 v33, vcc, 0x4b5c, v20
	v_add_i32_e32 v35, vcc, 0x4b7c, v20
	v_add_i32_e32 v37, vcc, v20, v3
	v_add_i32_e32 v60, vcc, v20, v4
	v_add_i32_e32 v27, vcc, 0x49e0, v20
	v_add_i32_e32 v31, vcc, 0x4ae8, v20
	v_add_i32_e32 v56, vcc, 0x4bf0, v20
	ds_read2_b32 v[58:59], v60 offset0:238 offset1:242
	ds_read2_b32 v[21:22], v21 offset1:4
	ds_read2_b32 v[23:24], v23 offset1:25
	ds_read2_b32 v[25:26], v25 offset1:4
	ds_read_b32 v37, v37 offset:936
	ds_read2_b32 v[29:30], v29 offset1:25
	ds_read2_b32 v[33:34], v33 offset1:4
	ds_read2_b32 v[35:36], v35 offset1:25
	ds_read2_b32 v[27:28], v27 offset1:4
	ds_read2_b32 v[31:32], v31 offset1:4
	ds_read2_b32 v[56:57], v56 offset1:4
	ds_read2_b32 v[60:61], v60 offset0:246 offset1:250
	v_add_i32_e32 v20, vcc, 4, v20
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v55, v37, v21
	v_mac_f32_e32 v54, v58, v21
	v_mac_f32_e32 v53, v59, v21
	v_mac_f32_e32 v52, v37, v24
	v_mac_f32_e32 v51, v58, v24
	v_mac_f32_e32 v50, v59, v24
	v_mac_f32_e32 v49, v37, v25
	v_mac_f32_e32 v48, v58, v25
	v_mac_f32_e32 v47, v59, v25
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v46, v37, v30
	v_mac_f32_e32 v45, v58, v30
	v_mac_f32_e32 v44, v59, v30
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v43, v37, v33
	v_mac_f32_e32 v42, v58, v33
	v_mac_f32_e32 v41, v59, v33
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v40, v37, v36
	v_mac_f32_e32 v38, v59, v36
	v_mac_f32_e32 v39, v58, v36
	v_cmp_ne_u32_e32 vcc, 12, v20
	v_mac_f32_e32 v55, v58, v22
	v_mac_f32_e32 v54, v59, v22
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v53, v60, v22
	v_mac_f32_e32 v52, v58, v27
	v_mac_f32_e32 v51, v59, v27
	v_mac_f32_e32 v50, v60, v27
	v_mac_f32_e32 v49, v58, v26
	v_mac_f32_e32 v48, v59, v26
	v_mac_f32_e32 v47, v60, v26
	v_mac_f32_e32 v46, v58, v31
	v_mac_f32_e32 v45, v59, v31
	v_mac_f32_e32 v44, v60, v31
	v_mac_f32_e32 v43, v58, v34
	v_mac_f32_e32 v42, v59, v34
	v_mac_f32_e32 v41, v60, v34
	v_mac_f32_e32 v40, v58, v56
	v_mac_f32_e32 v38, v60, v56
	v_mac_f32_e32 v39, v59, v56
	s_and_b64 vcc, exec, vcc
	v_mac_f32_e32 v54, v60, v23
	v_mac_f32_e32 v53, v61, v23
	v_mac_f32_e32 v52, v59, v28
	v_mac_f32_e32 v51, v60, v28
	v_mac_f32_e32 v50, v61, v28
	v_mac_f32_e32 v49, v59, v29
	v_mac_f32_e32 v48, v60, v29
	v_mac_f32_e32 v47, v61, v29
	v_mac_f32_e32 v46, v59, v32
	v_mac_f32_e32 v45, v60, v32
	v_mac_f32_e32 v44, v61, v32
	v_mac_f32_e32 v43, v59, v35
	v_mac_f32_e32 v42, v60, v35
	v_mac_f32_e32 v41, v61, v35
	v_mac_f32_e32 v40, v59, v57
	v_mac_f32_e32 v38, v61, v57
	v_mac_f32_e32 v39, v60, v57
	v_mac_f32_e32 v55, v59, v23
	s_cbranch_vccnz BB1_93
; BB#94:                                ; %.lr.ph13.i.preheader.1.1
	v_mov_b32_e32 v37, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v20, vcc, 0x4958, v37
	v_add_i32_e32 v22, vcc, 0x49dc, v37
	v_add_i32_e32 v24, vcc, 0x4a60, v37
	v_add_i32_e32 v26, vcc, 0x4ae4, v37
	v_add_i32_e32 v28, vcc, 0x4b68, v37
	v_add_i32_e32 v30, vcc, 0x4bec, v37
	ds_read2_b32 v[20:21], v20 offset1:4
	ds_read_b32 v34, v19 offset:936
	ds_read2_b32 v[22:23], v22 offset1:4
	ds_read_b32 v35, v18 offset:936
	ds_read2_b32 v[24:25], v24 offset1:4
	ds_read2_b32 v[32:33], v2 offset0:234 offset1:246
	ds_read2_b32 v[26:27], v26 offset1:4
	ds_read2_b32 v[28:29], v28 offset1:4
	ds_read2_b32 v[30:31], v30 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v54, v34, v20
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v55, v32, v20
	v_mac_f32_e32 v53, v35, v20
	v_mac_f32_e32 v52, v32, v22
	v_mac_f32_e32 v51, v34, v22
	v_mac_f32_e32 v50, v35, v22
	v_mac_f32_e32 v49, v32, v24
	v_mac_f32_e32 v48, v34, v24
	v_mac_f32_e32 v47, v35, v24
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v46, v32, v26
	v_mac_f32_e32 v45, v34, v26
	v_mac_f32_e32 v44, v35, v26
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v43, v32, v28
	v_mac_f32_e32 v42, v34, v28
	v_mac_f32_e32 v41, v35, v28
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v32, v30
	v_mac_f32_e32 v38, v35, v30
	v_mac_f32_e32 v39, v34, v30
	v_mac_f32_e32 v55, v34, v21
	v_mac_f32_e32 v53, v33, v21
	v_mac_f32_e32 v52, v34, v23
	v_mac_f32_e32 v51, v35, v23
	v_mac_f32_e32 v50, v33, v23
	v_mac_f32_e32 v49, v34, v25
	v_mac_f32_e32 v48, v35, v25
	v_mac_f32_e32 v47, v33, v25
	v_mac_f32_e32 v46, v34, v27
	v_mac_f32_e32 v45, v35, v27
	v_mac_f32_e32 v44, v33, v27
	v_mac_f32_e32 v43, v34, v29
	v_mac_f32_e32 v42, v35, v29
	v_mac_f32_e32 v41, v33, v29
	v_mac_f32_e32 v40, v34, v31
	v_mac_f32_e32 v38, v33, v31
	v_mac_f32_e32 v39, v35, v31
	v_mac_f32_e32 v54, v35, v21
	s_add_i32 s23, s23, 0x23c40
	v_add_i32_e32 v56, vcc, 0xf2, v14
	s_mov_b32 s26, 0xffffff
	s_movk_i32 s27, 0xff8d
	s_movk_i32 s28, 0xffe9
	s_movk_i32 s29, 0x73
	s_mov_b32 s32, 0
	s_mov_b32 s30, 0x24c00
	s_movk_i32 s31, 0xe0
	s_movk_i32 s36, 0x492
	s_movk_i32 s37, 0xea
BB1_95:                                 ; =>This Loop Header: Depth=1
                                        ;     Child Loop BB1_99 Depth 2
                                        ;     Child Loop BB1_113 Depth 2
                                        ;     Child Loop BB1_115 Depth 2
                                        ;     Child Loop BB1_117 Depth 2
	s_barrier
	v_cmp_ne_u32_e32 vcc, 3, v37
	v_mov_b32_e32 v35, v54
	v_mov_b32_e32 v34, v53
	v_mov_b32_e32 v33, v52
	v_mov_b32_e32 v32, v51
	v_mov_b32_e32 v31, v50
	v_mov_b32_e32 v30, v49
	v_mov_b32_e32 v29, v48
	v_mov_b32_e32 v28, v47
	v_mov_b32_e32 v27, v46
	v_mov_b32_e32 v26, v45
	v_mov_b32_e32 v25, v44
	v_mov_b32_e32 v24, v43
	v_mov_b32_e32 v23, v42
	v_mov_b32_e32 v22, v41
	v_mov_b32_e32 v21, v40
	v_mov_b32_e32 v20, v39
	v_mov_b32_e32 v14, v38
	v_mov_b32_e32 v36, v55
	s_and_b64 vcc, exec, vcc
	v_mov_b32_e32 v57, -1
                                        ; implicit-def: %VGPR54
                                        ; implicit-def: %VGPR53
                                        ; implicit-def: %VGPR52
                                        ; implicit-def: %VGPR51
                                        ; implicit-def: %VGPR50
                                        ; implicit-def: %VGPR49
                                        ; implicit-def: %VGPR48
                                        ; implicit-def: %VGPR47
                                        ; implicit-def: %VGPR46
                                        ; implicit-def: %VGPR45
                                        ; implicit-def: %VGPR44
                                        ; implicit-def: %VGPR43
                                        ; implicit-def: %VGPR42
                                        ; implicit-def: %VGPR41
                                        ; implicit-def: %VGPR40
                                        ; implicit-def: %VGPR39
                                        ; implicit-def: %VGPR38
                                        ; implicit-def: %VGPR55
	s_cbranch_vccz BB1_119
; BB#96:                                ;   in Loop: Header=BB1_95 Depth=1
	s_and_saveexec_b64 s[16:17], s[2:3]
	s_xor_b64 s[34:35], exec, s[16:17]
	; mask branch BB1_98
	s_cbranch_execz BB1_98
BB1_97:                                 ; %.lr.ph.i288.2
                                        ;   in Loop: Header=BB1_95 Depth=1
	v_add_i32_e32 v38, vcc, v37, v8
	v_mul_lo_i32 v39, v38, 11
	v_cmp_gt_u32_e32 vcc, 11, v38
	s_and_b64 s[16:17], exec, s[4:5]
	s_and_b64 s[16:17], s[16:17], vcc
	v_add_i32_e32 v38, vcc, v56, v39
	v_ashrrev_i32_e32 v39, 31, v38
	v_cndmask_b32_e64 v39, 0, v39, s[16:17]
	v_cndmask_b32_e64 v38, 0, v38, s[16:17]
	v_lshlrev_b64 v[38:39], 2, v[38:39]
	v_add_i32_e32 v46, vcc, s24, v38
	v_mov_b32_e32 v38, s25
	v_addc_u32_e32 v47, vcc, v38, v39, vcc
	flat_load_dwordx4 v[38:41], v[46:47]
	flat_load_dwordx4 v[42:45], v[46:47] offset:16
	flat_load_dword v48, v[46:47] offset:40
	flat_load_dwordx2 v[46:47], v[46:47] offset:32
	s_mov_b32 m0, -1
	s_waitcnt vmcnt(3) lgkmcnt(3)
	ds_write2_b32 v9, v38, v39 offset1:1
	ds_write2_b32 v9, v40, v41 offset0:2 offset1:3
	s_waitcnt vmcnt(2) lgkmcnt(4)
	ds_write2_b32 v9, v42, v43 offset0:4 offset1:5
	ds_write2_b32 v9, v44, v45 offset0:6 offset1:7
	s_waitcnt vmcnt(0) lgkmcnt(4)
	ds_write2_b32 v9, v46, v47 offset0:8 offset1:9
	ds_write_b32 v9, v48 offset:40
BB1_98:                                 ; %.lr.ph.i290.2.preheader
                                        ;   in Loop: Header=BB1_95 Depth=1
	s_or_b64 exec, exec, s[34:35]
	s_mov_b64 s[38:39], 0
	v_mov_b32_e32 v38, v0
BB1_99:                                 ; %.lr.ph.i290.2
                                        ;   Parent Loop BB1_95 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_cvt_f32_u32_e32 v39, v38
	v_mov_b32_e32 v40, 0x3727c5ac
	s_waitcnt vmcnt(0) lgkmcnt(0)
	v_mov_b32_e32 v42, 0x3727c5ac
	v_mov_b32_e32 v43, 0xffffff31
	v_mac_f32_e32 v40, 0x3c0e7835, v39
	v_cvt_u32_f32_e32 v39, v40
	v_mov_b32_e32 v51, 0
	v_and_b32_e32 v41, s26, v39
	v_mul_lo_i32 v40, v41, s27
	v_add_i32_e32 v40, vcc, v40, v38
	v_cvt_f32_u32_e32 v40, v40
	v_mac_f32_e32 v42, 0x3d321643, v40
	v_cvt_u32_f32_e32 v40, v42
	v_mov_b32_e32 v42, 0xffffff32
	v_lshlrev_b32_e32 v44, 2, v40
	v_add_i32_e32 v48, vcc, v44, v37
	v_cmp_lt_i32_e32 vcc, v48, v42
	v_cmp_gt_i32_e64 s[16:17], v48, v43
	v_cndmask_b32_e64 v42, 0, -1, vcc
	s_and_saveexec_b64 s[34:35], s[16:17]
	s_xor_b64 s[34:35], exec, s[34:35]
	; mask branch BB1_101
BB1_100:                                ;   in Loop: Header=BB1_99 Depth=2
	v_cmp_lt_i32_e32 vcc, 17, v48
	v_cmp_lt_u32_e64 s[16:17], 3, v39
	s_or_b64 s[16:17], s[16:17], vcc
	v_add_i32_e32 v42, vcc, s22, v39
	v_mov_b32_e32 v43, 0x7f
	v_cmp_gt_u32_e32 vcc, v42, v43
	s_or_b64 s[16:17], vcc, s[16:17]
	v_mov_b32_e32 v51, -1
	v_cndmask_b32_e64 v42, 0, -1, s[16:17]
BB1_101:                                ; %Flow4621
                                        ;   in Loop: Header=BB1_99 Depth=2
	s_or_b64 exec, exec, s[34:35]
	v_and_b32_e32 v43, s26, v40
	v_mul_lo_i32 v41, v41, s29
	v_mul_lo_i32 v43, v43, s28
	v_mov_b32_e32 v50, 0
                                        ; implicit-def: %VGPR44_VGPR45_VGPR46_VGPR47
	v_sub_i32_e32 v41, vcc, v43, v41
	v_add_i32_e32 v49, vcc, v41, v38
	v_cmp_ne_u32_e32 vcc, 0, v42
                                        ; implicit-def: %VGPR42_VGPR43
                                        ; implicit-def: %VGPR41
	s_and_saveexec_b64 s[16:17], vcc
	s_xor_b64 s[16:17], exec, s[16:17]
	; mask branch BB1_105
	s_cbranch_execz BB1_105
BB1_102:                                ; %.loopexit3.i295.2
                                        ;   in Loop: Header=BB1_99 Depth=2
	v_cmp_gt_u32_e32 vcc, 4, v39
	v_mov_b32_e32 v50, 0
                                        ; implicit-def: %VGPR42_VGPR43
                                        ; implicit-def: %VGPR44_VGPR45_VGPR46_VGPR47
                                        ; implicit-def: %VGPR41
	s_and_saveexec_b64 s[34:35], vcc
	s_xor_b64 s[34:35], exec, s[34:35]
	; mask branch BB1_104
	s_cbranch_execz BB1_104
BB1_103:                                ; %.loopexit3.i295.2..loopexit.loopexit12.i296.2_crit_edge
                                        ;   in Loop: Header=BB1_99 Depth=2
	v_mul_lo_i32 v41, v49, 10
	s_mov_b32 s40, s32
	s_mov_b32 s41, s32
	s_mov_b32 s42, s32
	s_mov_b32 s43, s32
	s_mov_b32 s44, s32
	s_mov_b32 s45, s32
	v_mov_b32_e32 v47, s43
	v_mov_b32_e32 v42, s44
	v_mov_b32_e32 v50, -1
	v_mov_b32_e32 v43, s45
	v_mov_b32_e32 v46, s42
	v_mov_b32_e32 v45, s41
	v_mov_b32_e32 v44, s40
BB1_104:                                ; %Flow4623
                                        ;   in Loop: Header=BB1_99 Depth=2
	s_or_b64 exec, exec, s[34:35]
	v_mov_b32_e32 v51, 0
BB1_105:                                ; %Flow4622
                                        ;   in Loop: Header=BB1_99 Depth=2
	s_or_b64 exec, exec, s[16:17]
	v_cmp_ne_u32_e32 vcc, 0, v51
	v_mov_b32_e32 v54, v47
	v_mov_b32_e32 v53, v46
	v_mov_b32_e32 v52, v45
	v_mov_b32_e32 v51, v44
	s_and_saveexec_b64 s[16:17], vcc
	s_xor_b64 s[16:17], exec, s[16:17]
	; mask branch BB1_109
	s_cbranch_execz BB1_109
BB1_106:                                ;   in Loop: Header=BB1_99 Depth=2
	v_mul_lo_i32 v42, v39, s30
	v_mul_lo_i32 v43, v48, s31
	v_mul_lo_i32 v41, v49, 10
	s_mov_b32 s33, s32
	v_add_i32_e32 v42, vcc, s23, v42
	v_add_i32_e32 v42, vcc, v42, v43
	v_add_i32_e32 v42, vcc, v42, v41
	v_ashrrev_i32_e32 v43, 31, v42
	v_lshlrev_b64 v[42:43], 2, v[42:43]
	v_add_i32_e32 v57, vcc, s20, v42
	v_mov_b32_e32 v42, s21
	v_addc_u32_e32 v58, vcc, v42, v43, vcc
	flat_load_dwordx4 v[51:54], v[57:58]
	s_mov_b32 s34, s32
	s_mov_b32 s35, s32
	s_mov_b32 s40, s32
	s_mov_b32 s41, s32
	v_mov_b32_e32 v47, s35
	v_mov_b32_e32 v43, s41
	v_cmp_ne_u32_e32 vcc, 22, v49
	v_mov_b32_e32 v46, s34
	v_mov_b32_e32 v45, s33
	v_mov_b32_e32 v44, s32
	v_mov_b32_e32 v42, s40
	s_and_saveexec_b64 s[34:35], vcc
	s_xor_b64 s[34:35], exec, s[34:35]
	; mask branch BB1_108
BB1_107:                                ; %.preheader4.preheader.i294.2
                                        ;   in Loop: Header=BB1_99 Depth=2
	flat_load_dwordx4 v[44:47], v[57:58] offset:16
	flat_load_dwordx2 v[42:43], v[57:58] offset:32
BB1_108:                                ; %Flow4620
                                        ;   in Loop: Header=BB1_99 Depth=2
	s_or_b64 exec, exec, s[34:35]
	v_mov_b32_e32 v50, -1
BB1_109:                                ; %Flow4624
                                        ;   in Loop: Header=BB1_99 Depth=2
	s_or_b64 exec, exec, s[16:17]
	v_cmp_ne_u32_e32 vcc, 0, v50
	s_and_saveexec_b64 s[16:17], vcc
	s_xor_b64 s[16:17], exec, s[16:17]
	; mask branch BB1_111
	s_cbranch_execz BB1_111
BB1_110:                                ; %.loopexit.loopexit12.i296.2
                                        ;   in Loop: Header=BB1_99 Depth=2
	v_mul_lo_i32 v39, v39, s36
	v_mul_lo_i32 v40, v40, s37
	s_mov_b32 m0, -1
	v_add_i32_e32 v39, vcc, v40, v39
	v_add_i32_e32 v39, vcc, v39, v41
	v_lshlrev_b32_e32 v39, 2, v39
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write2_b32 v39, v51, v52 offset0:2 offset1:3
	ds_write2_b32 v39, v53, v54 offset0:4 offset1:5
	ds_write2_b32 v39, v44, v45 offset0:6 offset1:7
	ds_write2_b32 v39, v46, v47 offset0:8 offset1:9
	ds_write2_b32 v39, v42, v43 offset0:10 offset1:11
BB1_111:                                ; %.loopexit.i297.2
                                        ;   in Loop: Header=BB1_99 Depth=2
	s_or_b64 exec, exec, s[16:17]
	v_add_i32_e32 v38, vcc, 0x100, v38
	v_mov_b32_e32 v39, 0x1cb
	v_cmp_gt_u32_e32 vcc, v38, v39
	s_or_b64 s[38:39], vcc, s[38:39]
	s_andn2_b64 exec, exec, s[38:39]
	s_cbranch_execnz BB1_99
; BB#112:                               ; %fetchData2.6.exit298.2
                                        ;   in Loop: Header=BB1_95 Depth=1
	s_or_b64 exec, exec, s[38:39]
	s_waitcnt vmcnt(0) lgkmcnt(0)
	s_barrier
	v_mov_b32_e32 v57, 0
	v_mov_b32_e32 v38, v14
	v_mov_b32_e32 v39, v20
	v_mov_b32_e32 v40, v21
	v_mov_b32_e32 v41, v22
	v_mov_b32_e32 v42, v23
	v_mov_b32_e32 v43, v24
	v_mov_b32_e32 v44, v25
	v_mov_b32_e32 v45, v26
	v_mov_b32_e32 v46, v27
	v_mov_b32_e32 v47, v28
	v_mov_b32_e32 v48, v29
	v_mov_b32_e32 v49, v30
	v_mov_b32_e32 v50, v31
	v_mov_b32_e32 v51, v32
	v_mov_b32_e32 v52, v33
	v_mov_b32_e32 v53, v34
	v_mov_b32_e32 v54, v35
	v_mov_b32_e32 v55, v36
BB1_113:                                ; %.lr.ph13.i302.preheader.21151
                                        ;   Parent Loop BB1_95 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	s_mov_b32 m0, -1
	v_add_i32_e32 v58, vcc, 0x4920, v57
	v_add_i32_e32 v60, vcc, 0x4940, v57
	v_add_i32_e32 v62, vcc, 0x4a28, v57
	v_add_i32_e32 v66, vcc, 0x4a48, v57
	v_add_i32_e32 v70, vcc, 0x4b30, v57
	v_add_i32_e32 v72, vcc, 0x4b50, v57
	v_add_i32_e32 v80, vcc, v57, v5
	v_add_i32_e32 v64, vcc, 0x49b4, v57
	v_add_i32_e32 v68, vcc, 0x4abc, v57
	v_add_i32_e32 v74, vcc, 0x4bc4, v57
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read2_b32 v[76:77], v80 offset1:4
	ds_read2_b32 v[60:61], v60 offset1:25
	ds_read2_b32 v[78:79], v80 offset0:8 offset1:12
	ds_read2_b32 v[62:63], v62 offset1:4
	ds_read2_b32 v[66:67], v66 offset1:25
	ds_read2_b32 v[70:71], v70 offset1:4
	ds_read2_b32 v[72:73], v72 offset1:25
	ds_read2_b32 v[64:65], v64 offset1:4
	ds_read2_b32 v[68:69], v68 offset1:4
	ds_read2_b32 v[74:75], v74 offset1:4
	ds_read_b32 v80, v80 offset:64
	v_add_i32_e32 v57, vcc, 4, v57
	s_waitcnt lgkmcnt(10)
	v_mac_f32_e32 v55, v76, v58
	v_mac_f32_e32 v54, v77, v58
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v53, v78, v58
	v_mac_f32_e32 v52, v76, v61
	v_mac_f32_e32 v51, v77, v61
	v_mac_f32_e32 v50, v78, v61
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v49, v76, v62
	v_mac_f32_e32 v48, v77, v62
	v_mac_f32_e32 v47, v78, v62
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v46, v76, v67
	v_mac_f32_e32 v45, v77, v67
	v_mac_f32_e32 v44, v78, v67
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v43, v76, v70
	v_mac_f32_e32 v42, v77, v70
	v_mac_f32_e32 v41, v78, v70
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v40, v76, v73
	v_mac_f32_e32 v38, v78, v73
	v_mac_f32_e32 v39, v77, v73
	v_cmp_ne_u32_e32 vcc, 12, v57
	v_mac_f32_e32 v55, v77, v59
	v_mac_f32_e32 v54, v78, v59
	v_mac_f32_e32 v53, v79, v59
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v52, v77, v64
	v_mac_f32_e32 v51, v78, v64
	v_mac_f32_e32 v50, v79, v64
	v_mac_f32_e32 v49, v77, v63
	v_mac_f32_e32 v48, v78, v63
	v_mac_f32_e32 v47, v79, v63
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v46, v77, v68
	v_mac_f32_e32 v45, v78, v68
	v_mac_f32_e32 v44, v79, v68
	v_mac_f32_e32 v43, v77, v71
	v_mac_f32_e32 v42, v78, v71
	v_mac_f32_e32 v41, v79, v71
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v40, v77, v74
	v_mac_f32_e32 v38, v79, v74
	v_mac_f32_e32 v39, v78, v74
	s_and_b64 vcc, exec, vcc
	v_mac_f32_e32 v54, v79, v60
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v53, v80, v60
	v_mac_f32_e32 v52, v78, v65
	v_mac_f32_e32 v51, v79, v65
	v_mac_f32_e32 v50, v80, v65
	v_mac_f32_e32 v49, v78, v66
	v_mac_f32_e32 v48, v79, v66
	v_mac_f32_e32 v47, v80, v66
	v_mac_f32_e32 v46, v78, v69
	v_mac_f32_e32 v45, v79, v69
	v_mac_f32_e32 v44, v80, v69
	v_mac_f32_e32 v43, v78, v72
	v_mac_f32_e32 v42, v79, v72
	v_mac_f32_e32 v41, v80, v72
	v_mac_f32_e32 v40, v78, v75
	v_mac_f32_e32 v38, v80, v75
	v_mac_f32_e32 v39, v79, v75
	v_mac_f32_e32 v55, v78, v60
	s_cbranch_vccnz BB1_113
; BB#114:                               ; %.lr.ph13.i399.preheader.21205
                                        ;   in Loop: Header=BB1_95 Depth=1
	v_mov_b32_e32 v57, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v58, vcc, 0x492c, v57
	v_add_i32_e32 v60, vcc, 0x49b0, v57
	v_add_i32_e32 v62, vcc, 0x4a34, v57
	v_add_i32_e32 v64, vcc, 0x4ab8, v57
	v_add_i32_e32 v66, vcc, 0x4b3c, v57
	v_add_i32_e32 v68, vcc, 0x4bc0, v57
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read_b32 v70, v3 offset:12
	ds_read2_b32 v[60:61], v60 offset1:4
	ds_read_b32 v71, v16 offset:12
	ds_read2_b32 v[62:63], v62 offset1:4
	ds_read_b32 v72, v17 offset:12
	ds_read2_b32 v[64:65], v64 offset1:4
	ds_read2_b32 v[66:67], v66 offset1:4
	ds_read2_b32 v[68:69], v68 offset1:4
	ds_read_b32 v73, v2 offset:48
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v55, v70, v58
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v54, v71, v58
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v53, v72, v58
	v_mac_f32_e32 v52, v70, v60
	v_mac_f32_e32 v51, v71, v60
	v_mac_f32_e32 v50, v72, v60
	v_mac_f32_e32 v49, v70, v62
	v_mac_f32_e32 v48, v71, v62
	v_mac_f32_e32 v47, v72, v62
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v46, v70, v64
	v_mac_f32_e32 v45, v71, v64
	v_mac_f32_e32 v44, v72, v64
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v43, v70, v66
	v_mac_f32_e32 v42, v71, v66
	v_mac_f32_e32 v41, v72, v66
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v40, v70, v68
	v_mac_f32_e32 v38, v72, v68
	v_mac_f32_e32 v39, v71, v68
	v_mac_f32_e32 v55, v71, v59
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v53, v73, v59
	v_mac_f32_e32 v52, v71, v61
	v_mac_f32_e32 v51, v72, v61
	v_mac_f32_e32 v50, v73, v61
	v_mac_f32_e32 v49, v71, v63
	v_mac_f32_e32 v48, v72, v63
	v_mac_f32_e32 v47, v73, v63
	v_mac_f32_e32 v46, v71, v65
	v_mac_f32_e32 v45, v72, v65
	v_mac_f32_e32 v44, v73, v65
	v_mac_f32_e32 v43, v71, v67
	v_mac_f32_e32 v42, v72, v67
	v_mac_f32_e32 v41, v73, v67
	v_mac_f32_e32 v40, v71, v69
	v_mac_f32_e32 v38, v73, v69
	v_mac_f32_e32 v39, v72, v69
	v_mac_f32_e32 v54, v72, v59
BB1_115:                                ; %.lr.ph13.i302.preheader.1.2
                                        ;   Parent Loop BB1_95 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	s_mov_b32 m0, -1
	v_add_i32_e32 v58, vcc, 0x494c, v57
	v_add_i32_e32 v60, vcc, 0x496c, v57
	v_add_i32_e32 v62, vcc, 0x4a54, v57
	v_add_i32_e32 v66, vcc, 0x4a74, v57
	v_add_i32_e32 v70, vcc, 0x4b5c, v57
	v_add_i32_e32 v72, vcc, 0x4b7c, v57
	v_add_i32_e32 v76, vcc, v57, v3
	v_add_i32_e32 v78, vcc, v57, v4
	v_add_i32_e32 v64, vcc, 0x49e0, v57
	v_add_i32_e32 v68, vcc, 0x4ae8, v57
	v_add_i32_e32 v74, vcc, 0x4bf0, v57
	ds_read_b32 v80, v76 offset:936
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read2_b32 v[76:77], v78 offset0:238 offset1:242
	ds_read2_b32 v[60:61], v60 offset1:25
	ds_read2_b32 v[62:63], v62 offset1:4
	ds_read2_b32 v[66:67], v66 offset1:25
	ds_read2_b32 v[70:71], v70 offset1:4
	ds_read2_b32 v[72:73], v72 offset1:25
	ds_read2_b32 v[64:65], v64 offset1:4
	ds_read2_b32 v[68:69], v68 offset1:4
	ds_read2_b32 v[74:75], v74 offset1:4
	ds_read2_b32 v[78:79], v78 offset0:246 offset1:250
	v_add_i32_e32 v57, vcc, 4, v57
	s_waitcnt lgkmcnt(10)
	v_mac_f32_e32 v55, v80, v58
	s_waitcnt lgkmcnt(9)
	v_mac_f32_e32 v54, v76, v58
	v_mac_f32_e32 v53, v77, v58
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v52, v80, v61
	v_mac_f32_e32 v51, v76, v61
	v_mac_f32_e32 v50, v77, v61
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v49, v80, v62
	v_mac_f32_e32 v48, v76, v62
	v_mac_f32_e32 v47, v77, v62
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v46, v80, v67
	v_mac_f32_e32 v45, v76, v67
	v_mac_f32_e32 v44, v77, v67
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v43, v80, v70
	v_mac_f32_e32 v42, v76, v70
	v_mac_f32_e32 v41, v77, v70
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v40, v80, v73
	v_mac_f32_e32 v38, v77, v73
	v_mac_f32_e32 v39, v76, v73
	v_cmp_ne_u32_e32 vcc, 12, v57
	v_mac_f32_e32 v55, v76, v59
	v_mac_f32_e32 v54, v77, v59
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v53, v78, v59
	v_mac_f32_e32 v52, v76, v64
	v_mac_f32_e32 v51, v77, v64
	v_mac_f32_e32 v50, v78, v64
	v_mac_f32_e32 v49, v76, v63
	v_mac_f32_e32 v48, v77, v63
	v_mac_f32_e32 v47, v78, v63
	v_mac_f32_e32 v46, v76, v68
	v_mac_f32_e32 v45, v77, v68
	v_mac_f32_e32 v44, v78, v68
	v_mac_f32_e32 v43, v76, v71
	v_mac_f32_e32 v42, v77, v71
	v_mac_f32_e32 v41, v78, v71
	v_mac_f32_e32 v40, v76, v74
	v_mac_f32_e32 v38, v78, v74
	v_mac_f32_e32 v39, v77, v74
	s_and_b64 vcc, exec, vcc
	v_mac_f32_e32 v54, v78, v60
	v_mac_f32_e32 v53, v79, v60
	v_mac_f32_e32 v52, v77, v65
	v_mac_f32_e32 v51, v78, v65
	v_mac_f32_e32 v50, v79, v65
	v_mac_f32_e32 v49, v77, v66
	v_mac_f32_e32 v48, v78, v66
	v_mac_f32_e32 v47, v79, v66
	v_mac_f32_e32 v46, v77, v69
	v_mac_f32_e32 v45, v78, v69
	v_mac_f32_e32 v44, v79, v69
	v_mac_f32_e32 v43, v77, v72
	v_mac_f32_e32 v42, v78, v72
	v_mac_f32_e32 v41, v79, v72
	v_mac_f32_e32 v40, v77, v75
	v_mac_f32_e32 v38, v79, v75
	v_mac_f32_e32 v39, v78, v75
	v_mac_f32_e32 v55, v77, v60
	s_cbranch_vccnz BB1_115
; BB#116:                               ; %.lr.ph13.i399.preheader.1.2
                                        ;   in Loop: Header=BB1_95 Depth=1
	v_mov_b32_e32 v57, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v58, vcc, 0x4958, v57
	v_add_i32_e32 v60, vcc, 0x49dc, v57
	v_add_i32_e32 v62, vcc, 0x4a60, v57
	v_add_i32_e32 v64, vcc, 0x4ae4, v57
	v_add_i32_e32 v66, vcc, 0x4b68, v57
	v_add_i32_e32 v68, vcc, 0x4bec, v57
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read_b32 v72, v19 offset:936
	ds_read2_b32 v[60:61], v60 offset1:4
	ds_read_b32 v73, v18 offset:936
	ds_read2_b32 v[62:63], v62 offset1:4
	ds_read2_b32 v[70:71], v2 offset0:234 offset1:246
	ds_read2_b32 v[64:65], v64 offset1:4
	ds_read2_b32 v[66:67], v66 offset1:4
	ds_read2_b32 v[68:69], v68 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v54, v72, v58
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v55, v70, v58
	v_mac_f32_e32 v53, v73, v58
	v_mac_f32_e32 v52, v70, v60
	v_mac_f32_e32 v51, v72, v60
	v_mac_f32_e32 v50, v73, v60
	v_mac_f32_e32 v49, v70, v62
	v_mac_f32_e32 v48, v72, v62
	v_mac_f32_e32 v47, v73, v62
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v46, v70, v64
	v_mac_f32_e32 v45, v72, v64
	v_mac_f32_e32 v44, v73, v64
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v43, v70, v66
	v_mac_f32_e32 v42, v72, v66
	v_mac_f32_e32 v41, v73, v66
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v70, v68
	v_mac_f32_e32 v38, v73, v68
	v_mac_f32_e32 v39, v72, v68
	v_mac_f32_e32 v55, v72, v59
	v_mac_f32_e32 v53, v71, v59
	v_mac_f32_e32 v52, v72, v61
	v_mac_f32_e32 v51, v73, v61
	v_mac_f32_e32 v50, v71, v61
	v_mac_f32_e32 v49, v72, v63
	v_mac_f32_e32 v48, v73, v63
	v_mac_f32_e32 v47, v71, v63
	v_mac_f32_e32 v46, v72, v65
	v_mac_f32_e32 v45, v73, v65
	v_mac_f32_e32 v44, v71, v65
	v_mac_f32_e32 v43, v72, v67
	v_mac_f32_e32 v42, v73, v67
	v_mac_f32_e32 v41, v71, v67
	v_mac_f32_e32 v40, v72, v69
	v_mac_f32_e32 v38, v71, v69
	v_mac_f32_e32 v39, v73, v69
	v_mac_f32_e32 v54, v73, v59
BB1_117:                                ; %.lr.ph13.i302.preheader.2.2
                                        ;   Parent Loop BB1_95 Depth=1
                                        ; =>  This Inner Loop Header: Depth=2
	v_add_i32_e32 v76, vcc, v57, v4
	s_mov_b32 m0, -1
	v_add_i32_e32 v58, vcc, 0x4978, v57
	v_add_i32_e32 v60, vcc, 0x4998, v57
	v_add_i32_e32 v64, vcc, 0x4a80, v57
	v_add_i32_e32 v66, vcc, 0x4aa0, v57
	v_add_i32_e32 v70, vcc, 0x4b88, v57
	v_add_i32_e32 v72, vcc, 0x4ba8, v57
	v_add_i32_e32 v77, vcc, v57, v3
	v_add_i32_e32 v78, vcc, 0x760, v76
	v_add_i32_e32 v62, vcc, 0x4a0c, v57
	v_add_i32_e32 v68, vcc, 0x4b14, v57
	v_add_i32_e32 v74, vcc, 0x4c1c, v57
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read_b32 v80, v77 offset:1872
	ds_read2_b32 v[60:61], v60 offset1:25
	ds_read2_b32 v[64:65], v64 offset1:4
	ds_read2_b32 v[66:67], v66 offset1:25
	ds_read2_b32 v[70:71], v70 offset1:4
	ds_read2_b32 v[72:73], v72 offset1:25
	v_add_i32_e32 v79, vcc, 0x780, v76
	ds_read2_b32 v[76:77], v78 offset1:4
	ds_read2_b32 v[62:63], v62 offset1:4
	ds_read2_b32 v[68:69], v68 offset1:4
	ds_read2_b32 v[74:75], v74 offset1:4
	ds_read2_b32 v[78:79], v79 offset1:4
	v_add_i32_e32 v57, vcc, 4, v57
	s_waitcnt lgkmcnt(10)
	v_mac_f32_e32 v55, v80, v58
	s_waitcnt lgkmcnt(9)
	v_mac_f32_e32 v52, v80, v61
	s_waitcnt lgkmcnt(8)
	v_mac_f32_e32 v49, v80, v64
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v46, v80, v67
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v43, v80, v70
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v40, v80, v73
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v54, v76, v58
	v_mac_f32_e32 v53, v77, v58
	v_mac_f32_e32 v51, v76, v61
	v_mac_f32_e32 v50, v77, v61
	v_mac_f32_e32 v48, v76, v64
	v_mac_f32_e32 v47, v77, v64
	v_mac_f32_e32 v45, v76, v67
	v_mac_f32_e32 v44, v77, v67
	v_mac_f32_e32 v42, v76, v70
	v_mac_f32_e32 v41, v77, v70
	v_mac_f32_e32 v38, v77, v73
	v_mac_f32_e32 v39, v76, v73
	v_cmp_ne_u32_e32 vcc, 12, v57
	v_mac_f32_e32 v55, v76, v59
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v52, v76, v62
	v_mac_f32_e32 v49, v76, v65
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v46, v76, v68
	v_mac_f32_e32 v43, v76, v71
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v40, v76, v74
	v_mac_f32_e32 v54, v77, v59
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v53, v78, v59
	v_mac_f32_e32 v51, v77, v62
	v_mac_f32_e32 v50, v78, v62
	v_mac_f32_e32 v48, v77, v65
	v_mac_f32_e32 v47, v78, v65
	v_mac_f32_e32 v45, v77, v68
	v_mac_f32_e32 v44, v78, v68
	v_mac_f32_e32 v42, v77, v71
	v_mac_f32_e32 v41, v78, v71
	v_mac_f32_e32 v38, v78, v74
	v_mac_f32_e32 v39, v77, v74
	s_and_b64 vcc, exec, vcc
	v_mac_f32_e32 v52, v77, v63
	v_mac_f32_e32 v49, v77, v66
	v_mac_f32_e32 v46, v77, v69
	v_mac_f32_e32 v43, v77, v72
	v_mac_f32_e32 v40, v77, v75
	v_mac_f32_e32 v55, v77, v60
	v_mac_f32_e32 v54, v78, v60
	v_mac_f32_e32 v53, v79, v60
	v_mac_f32_e32 v51, v78, v63
	v_mac_f32_e32 v50, v79, v63
	v_mac_f32_e32 v48, v78, v66
	v_mac_f32_e32 v47, v79, v66
	v_mac_f32_e32 v45, v78, v69
	v_mac_f32_e32 v44, v79, v69
	v_mac_f32_e32 v42, v78, v72
	v_mac_f32_e32 v41, v79, v72
	v_mac_f32_e32 v38, v79, v75
	v_mac_f32_e32 v39, v78, v75
	s_cbranch_vccnz BB1_117
; BB#118:                               ; %.lr.ph13.i399.preheader.2.2
                                        ;   in Loop: Header=BB1_95 Depth=1
	v_mov_b32_e32 v57, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v58, vcc, 0x4984, v57
	v_add_i32_e32 v60, vcc, 0x4a08, v57
	v_add_i32_e32 v62, vcc, 0x4a8c, v57
	v_add_i32_e32 v64, vcc, 0x4b10, v57
	v_add_i32_e32 v66, vcc, 0x4b94, v57
	v_add_i32_e32 v68, vcc, 0x4c18, v57
	v_add_i32_e32 v70, vcc, 0x750, v2
	ds_read2_b32 v[58:59], v58 offset1:4
	ds_read_b32 v72, v19 offset:1872
	ds_read2_b32 v[60:61], v60 offset1:4
	ds_read_b32 v73, v18 offset:1872
	ds_read2_b32 v[62:63], v62 offset1:4
	ds_read2_b32 v[70:71], v70 offset1:12
	ds_read2_b32 v[64:65], v64 offset1:4
	ds_read2_b32 v[66:67], v66 offset1:4
	ds_read2_b32 v[68:69], v68 offset1:4
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v54, v72, v58
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v55, v70, v58
	v_mac_f32_e32 v53, v73, v58
	v_mac_f32_e32 v52, v70, v60
	v_mac_f32_e32 v51, v72, v60
	v_mac_f32_e32 v50, v73, v60
	v_mac_f32_e32 v49, v70, v62
	v_mac_f32_e32 v48, v72, v62
	v_mac_f32_e32 v47, v73, v62
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v46, v70, v64
	v_mac_f32_e32 v45, v72, v64
	v_mac_f32_e32 v44, v73, v64
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v43, v70, v66
	v_mac_f32_e32 v42, v72, v66
	v_mac_f32_e32 v41, v73, v66
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v40, v70, v68
	v_mac_f32_e32 v38, v73, v68
	v_mac_f32_e32 v39, v72, v68
	v_mac_f32_e32 v55, v72, v59
	v_mac_f32_e32 v53, v71, v59
	v_mac_f32_e32 v52, v72, v61
	v_mac_f32_e32 v51, v73, v61
	v_mac_f32_e32 v50, v71, v61
	v_mac_f32_e32 v49, v72, v63
	v_mac_f32_e32 v48, v73, v63
	v_mac_f32_e32 v47, v71, v63
	v_mac_f32_e32 v46, v72, v65
	v_mac_f32_e32 v45, v73, v65
	v_mac_f32_e32 v44, v71, v65
	v_mac_f32_e32 v43, v72, v67
	v_mac_f32_e32 v42, v73, v67
	v_mac_f32_e32 v41, v71, v67
	v_mac_f32_e32 v40, v72, v69
	v_mac_f32_e32 v38, v71, v69
	v_mac_f32_e32 v39, v73, v69
	v_mac_f32_e32 v54, v73, v59
	v_add_i32_e32 v37, vcc, 1, v37
BB1_119:                                ; %Flow4625
                                        ;   in Loop: Header=BB1_95 Depth=1
	v_cmp_ne_u32_e32 vcc, 0, v57
	s_cbranch_vccz BB1_95
; BB#120:
	s_and_saveexec_b64 s[2:3], s[14:15]
	s_xor_b64 s[4:5], exec, s[2:3]
	; mask branch BB1_122
	s_cbranch_execz BB1_122
BB1_121:                                ; %.lr.ph.i493.2
	v_lshlrev_b32_e32 v7, 1, v7
	v_and_b32_e32 v7, 0x1fffffe, v7
	v_subrev_i32_e32 v8, vcc, v7, v0
	v_lshlrev_b32_e32 v7, 2, v8
	v_or_b32_e32 v7, 3, v7
	v_mul_lo_i32 v9, v7, 11
	v_cmp_gt_u32_e32 vcc, 11, v7
	s_and_b64 s[2:3], exec, s[12:13]
	s_and_b64 s[2:3], s[2:3], vcc
	v_add_i32_e32 v6, vcc, v6, v9
	v_add_i32_e32 v6, vcc, 0xf2, v6
	v_ashrrev_i32_e32 v7, 31, v6
	v_cndmask_b32_e64 v7, 0, v7, s[2:3]
	v_cndmask_b32_e64 v6, 0, v6, s[2:3]
	v_lshlrev_b64 v[6:7], 2, v[6:7]
	v_add_i32_e32 v41, vcc, s24, v6
	v_mov_b32_e32 v6, s25
	v_addc_u32_e32 v42, vcc, v6, v7, vcc
	v_mul_lo_i32 v43, v8, 11
	flat_load_dwordx4 v[6:9], v[41:42]
	flat_load_dwordx4 v[37:40], v[41:42] offset:16
	flat_load_dword v44, v[41:42] offset:40
	flat_load_dwordx2 v[41:42], v[41:42] offset:32
	v_add_i32_e32 v15, vcc, v15, v43
	v_lshlrev_b32_e32 v15, 2, v15
	s_mov_b32 m0, -1
	v_add_i32_e32 v43, vcc, 0x4920, v15
	v_add_i32_e32 v45, vcc, 0x4928, v15
	v_add_i32_e32 v46, vcc, 0x4930, v15
	v_add_i32_e32 v47, vcc, 0x4938, v15
	v_add_i32_e32 v48, vcc, 0x4940, v15
	s_waitcnt vmcnt(3) lgkmcnt(3)
	ds_write2_b32 v43, v6, v7 offset1:1
	ds_write2_b32 v45, v8, v9 offset1:1
	s_waitcnt vmcnt(2) lgkmcnt(4)
	ds_write2_b32 v46, v37, v38 offset1:1
	ds_write2_b32 v47, v39, v40 offset1:1
	s_waitcnt vmcnt(0) lgkmcnt(4)
	ds_write2_b32 v48, v41, v42 offset1:1
	ds_write_b32 v15, v44 offset:18760
BB1_122:                                ; %.lr.ph.i.2.preheader
	s_or_b64 exec, exec, s[4:5]
	s_mov_b64 s[4:5], 0
	s_mov_b32 s12, 0xffffff
	s_movk_i32 s13, 0xffa4
	s_movk_i32 s14, 0xffe9
	s_movk_i32 s15, 0x5c
	s_mov_b32 s24, 0
	s_mov_b32 s16, 0x24c00
	s_movk_i32 s17, 0xe0
	s_movk_i32 s28, 0x492
	s_movk_i32 s29, 0xea
	s_mov_b32 m0, -1
BB1_123:                                ; %.lr.ph.i.2
                                        ; =>This Inner Loop Header: Depth=1
	v_cvt_f32_u32_e32 v6, v0
	v_mov_b32_e32 v7, 0x3727c5ac
	v_mov_b32_e32 v9, 0x3727c5ac
	v_mov_b32_e32 v15, 0xffffff32
	v_mac_f32_e32 v7, 0x3c321643, v6
	v_cvt_u32_f32_e32 v6, v7
	s_waitcnt vmcnt(0) lgkmcnt(0)
	v_mov_b32_e32 v37, 0xffffff31
	v_mov_b32_e32 v44, 0
	v_and_b32_e32 v8, s12, v6
	v_mul_lo_i32 v7, v8, s13
	v_add_i32_e32 v7, vcc, v7, v0
	v_cvt_f32_u32_e32 v7, v7
	v_mac_f32_e32 v9, 0x3d321643, v7
	v_cvt_u32_f32_e32 v7, v9
	v_lshlrev_b32_e32 v9, 2, v7
	v_or_b32_e32 v9, 3, v9
	v_cmp_lt_i32_e32 vcc, v9, v15
	v_cmp_gt_i32_e64 s[2:3], v9, v37
	v_cndmask_b32_e64 v37, 0, -1, vcc
	s_and_saveexec_b64 s[26:27], s[2:3]
	s_xor_b64 s[26:27], exec, s[26:27]
	; mask branch BB1_125
BB1_124:                                ;   in Loop: Header=BB1_123 Depth=1
	v_cmp_lt_i32_e32 vcc, 17, v9
	v_cmp_lt_u32_e64 s[2:3], 3, v6
	s_or_b64 s[2:3], s[2:3], vcc
	v_add_i32_e32 v15, vcc, s22, v6
	v_mov_b32_e32 v37, 0x7f
	v_cmp_gt_u32_e32 vcc, v15, v37
	s_or_b64 s[2:3], vcc, s[2:3]
	v_mov_b32_e32 v44, -1
	v_cndmask_b32_e64 v37, 0, -1, s[2:3]
BB1_125:                                ; %Flow4616
                                        ;   in Loop: Header=BB1_123 Depth=1
	s_or_b64 exec, exec, s[26:27]
	v_and_b32_e32 v15, s12, v7
	v_mul_lo_i32 v8, v8, s15
	v_mul_lo_i32 v15, v15, s14
	v_mov_b32_e32 v43, 0
                                        ; implicit-def: %VGPR39_VGPR40_VGPR41_VGPR42
	v_sub_i32_e32 v8, vcc, v15, v8
	v_add_i32_e32 v15, vcc, v8, v0
	v_cmp_ne_u32_e32 vcc, 0, v37
                                        ; implicit-def: %VGPR37_VGPR38
                                        ; implicit-def: %VGPR8
	s_and_saveexec_b64 s[2:3], vcc
	s_xor_b64 s[2:3], exec, s[2:3]
	; mask branch BB1_129
	s_cbranch_execz BB1_129
BB1_126:                                ; %.loopexit3.i.2
                                        ;   in Loop: Header=BB1_123 Depth=1
	v_cmp_gt_u32_e32 vcc, 4, v6
	v_mov_b32_e32 v43, 0
                                        ; implicit-def: %VGPR37_VGPR38
                                        ; implicit-def: %VGPR39_VGPR40_VGPR41_VGPR42
                                        ; implicit-def: %VGPR8
	s_and_saveexec_b64 s[26:27], vcc
	s_xor_b64 s[26:27], exec, s[26:27]
	; mask branch BB1_128
	s_cbranch_execz BB1_128
BB1_127:                                ; %.loopexit3.i.2..loopexit.loopexit12.i.2_crit_edge
                                        ;   in Loop: Header=BB1_123 Depth=1
	v_mul_lo_i32 v8, v15, 10
	s_mov_b32 s32, s24
	s_mov_b32 s33, s24
	s_mov_b32 s34, s24
	s_mov_b32 s35, s24
	s_mov_b32 s30, s24
	s_mov_b32 s31, s24
	v_mov_b32_e32 v42, s35
	v_mov_b32_e32 v38, s31
	v_mov_b32_e32 v43, -1
	v_mov_b32_e32 v37, s30
	v_mov_b32_e32 v41, s34
	v_mov_b32_e32 v40, s33
	v_mov_b32_e32 v39, s32
BB1_128:                                ; %Flow4618
                                        ;   in Loop: Header=BB1_123 Depth=1
	s_or_b64 exec, exec, s[26:27]
	v_mov_b32_e32 v44, 0
BB1_129:                                ; %Flow4617
                                        ;   in Loop: Header=BB1_123 Depth=1
	s_or_b64 exec, exec, s[2:3]
	v_cmp_ne_u32_e32 vcc, 0, v44
	v_mov_b32_e32 v47, v42
	v_mov_b32_e32 v46, v41
	v_mov_b32_e32 v45, v40
	v_mov_b32_e32 v44, v39
	s_and_saveexec_b64 s[2:3], vcc
	s_xor_b64 s[2:3], exec, s[2:3]
	; mask branch BB1_133
	s_cbranch_execz BB1_133
BB1_130:                                ;   in Loop: Header=BB1_123 Depth=1
	v_mul_lo_i32 v37, v6, s16
	v_mul_lo_i32 v9, v9, s17
	v_mul_lo_i32 v8, v15, 10
	s_mov_b32 s25, s24
	v_add_i32_e32 v37, vcc, s23, v37
	v_add_i32_e32 v9, vcc, v9, v37
	v_add_i32_e32 v37, vcc, v9, v8
	v_ashrrev_i32_e32 v38, 31, v37
	v_lshlrev_b64 v[37:38], 2, v[37:38]
	v_add_i32_e32 v48, vcc, s20, v37
	v_mov_b32_e32 v9, s21
	v_addc_u32_e32 v49, vcc, v9, v38, vcc
	flat_load_dwordx4 v[44:47], v[48:49]
	s_mov_b32 s26, s24
	s_mov_b32 s27, s24
	s_mov_b32 s30, s24
	s_mov_b32 s31, s24
	v_mov_b32_e32 v42, s27
	v_mov_b32_e32 v38, s31
	v_cmp_ne_u32_e32 vcc, 22, v15
	v_mov_b32_e32 v41, s26
	v_mov_b32_e32 v40, s25
	v_mov_b32_e32 v39, s24
	v_mov_b32_e32 v37, s30
	s_and_saveexec_b64 s[26:27], vcc
	s_xor_b64 s[26:27], exec, s[26:27]
	; mask branch BB1_132
BB1_131:                                ; %.preheader4.preheader.i.2
                                        ;   in Loop: Header=BB1_123 Depth=1
	flat_load_dwordx4 v[39:42], v[48:49] offset:16
	flat_load_dwordx2 v[37:38], v[48:49] offset:32
BB1_132:                                ; %Flow4615
                                        ;   in Loop: Header=BB1_123 Depth=1
	s_or_b64 exec, exec, s[26:27]
	v_mov_b32_e32 v43, -1
BB1_133:                                ; %Flow4619
                                        ;   in Loop: Header=BB1_123 Depth=1
	s_or_b64 exec, exec, s[2:3]
	v_cmp_ne_u32_e32 vcc, 0, v43
	s_and_saveexec_b64 s[2:3], vcc
	s_xor_b64 s[2:3], exec, s[2:3]
	; mask branch BB1_135
BB1_134:                                ; %.loopexit.loopexit12.i.2
                                        ;   in Loop: Header=BB1_123 Depth=1
	v_mul_lo_i32 v6, v6, s28
	v_mul_lo_i32 v7, v7, s29
	v_add_i32_e32 v6, vcc, v7, v6
	v_add_i32_e32 v6, vcc, v6, v8
	v_lshlrev_b32_e32 v6, 2, v6
	s_waitcnt vmcnt(0) lgkmcnt(0)
	ds_write2_b32 v6, v44, v45 offset0:2 offset1:3
	ds_write2_b32 v6, v46, v47 offset0:4 offset1:5
	ds_write2_b32 v6, v39, v40 offset0:6 offset1:7
	ds_write2_b32 v6, v41, v42 offset0:8 offset1:9
	ds_write2_b32 v6, v37, v38 offset0:10 offset1:11
BB1_135:                                ; %.loopexit.i.2
                                        ;   in Loop: Header=BB1_123 Depth=1
	s_or_b64 exec, exec, s[2:3]
	v_add_i32_e32 v0, vcc, 0x100, v0
	v_mov_b32_e32 v6, 0x16f
	v_cmp_gt_u32_e32 vcc, v0, v6
	s_or_b64 s[4:5], vcc, s[4:5]
	s_andn2_b64 exec, exec, s[4:5]
	s_cbranch_execnz BB1_123
; BB#136:                               ; %fetchData2.6.exit.2
	s_or_b64 exec, exec, s[4:5]
	s_waitcnt vmcnt(0) lgkmcnt(0)
	s_barrier
	v_mov_b32_e32 v0, 0
	s_mov_b32 m0, -1
BB1_137:                                ; %.lr.ph13.i195.preheader.2
                                        ; =>This Inner Loop Header: Depth=1
	v_add_i32_e32 v6, vcc, 0x4920, v0
	v_add_i32_e32 v8, vcc, 0x4940, v0
	v_add_i32_e32 v41, vcc, 0x4a48, v0
	v_add_i32_e32 v45, vcc, 0x4b30, v0
	v_add_i32_e32 v47, vcc, 0x4b50, v0
	v_add_i32_e32 v55, vcc, v0, v5
	v_add_i32_e32 v15, vcc, 0x4a28, v0
	v_add_i32_e32 v39, vcc, 0x49b4, v0
	v_add_i32_e32 v49, vcc, 0x4bc4, v0
	ds_read2_b32 v[37:38], v15 offset1:4
	v_add_i32_e32 v15, vcc, 0x4abc, v0
	ds_read2_b32 v[51:52], v55 offset1:4
	ds_read2_b32 v[6:7], v6 offset1:4
	ds_read2_b32 v[8:9], v8 offset1:25
	ds_read2_b32 v[53:54], v55 offset0:8 offset1:12
	ds_read2_b32 v[41:42], v41 offset1:25
	ds_read2_b32 v[45:46], v45 offset1:4
	ds_read2_b32 v[47:48], v47 offset1:25
	ds_read2_b32 v[43:44], v15 offset1:4
	ds_read2_b32 v[39:40], v39 offset1:4
	ds_read2_b32 v[49:50], v49 offset1:4
	ds_read_b32 v15, v55 offset:64
	v_add_i32_e32 v0, vcc, 4, v0
	s_waitcnt lgkmcnt(9)
	v_mac_f32_e32 v36, v51, v6
	v_mac_f32_e32 v35, v52, v6
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v34, v53, v6
	v_mac_f32_e32 v33, v51, v9
	v_mac_f32_e32 v32, v52, v9
	v_mac_f32_e32 v31, v53, v9
	v_mac_f32_e32 v30, v51, v37
	v_mac_f32_e32 v29, v52, v37
	v_mac_f32_e32 v28, v53, v37
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v27, v51, v42
	v_mac_f32_e32 v26, v52, v42
	v_mac_f32_e32 v25, v53, v42
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v24, v51, v45
	v_mac_f32_e32 v23, v52, v45
	v_mac_f32_e32 v22, v53, v45
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v21, v51, v48
	v_mac_f32_e32 v14, v53, v48
	v_mac_f32_e32 v20, v52, v48
	v_cmp_ne_u32_e32 vcc, 12, v0
	v_mac_f32_e32 v36, v52, v7
	v_mac_f32_e32 v35, v53, v7
	v_mac_f32_e32 v34, v54, v7
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v33, v52, v39
	v_mac_f32_e32 v32, v53, v39
	v_mac_f32_e32 v31, v54, v39
	v_mac_f32_e32 v30, v52, v38
	v_mac_f32_e32 v29, v53, v38
	v_mac_f32_e32 v28, v54, v38
	v_mac_f32_e32 v27, v52, v43
	v_mac_f32_e32 v26, v53, v43
	v_mac_f32_e32 v25, v54, v43
	v_mac_f32_e32 v24, v52, v46
	v_mac_f32_e32 v23, v53, v46
	v_mac_f32_e32 v22, v54, v46
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v21, v52, v49
	v_mac_f32_e32 v14, v54, v49
	v_mac_f32_e32 v20, v53, v49
	s_and_b64 vcc, exec, vcc
	v_mac_f32_e32 v35, v54, v8
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v34, v15, v8
	v_mac_f32_e32 v33, v53, v40
	v_mac_f32_e32 v32, v54, v40
	v_mac_f32_e32 v31, v15, v40
	v_mac_f32_e32 v30, v53, v41
	v_mac_f32_e32 v29, v54, v41
	v_mac_f32_e32 v28, v15, v41
	v_mac_f32_e32 v27, v53, v44
	v_mac_f32_e32 v26, v54, v44
	v_mac_f32_e32 v25, v15, v44
	v_mac_f32_e32 v24, v53, v47
	v_mac_f32_e32 v23, v54, v47
	v_mac_f32_e32 v22, v15, v47
	v_mac_f32_e32 v21, v53, v50
	v_mac_f32_e32 v14, v15, v50
	v_mac_f32_e32 v20, v54, v50
	v_mac_f32_e32 v36, v53, v8
	s_cbranch_vccnz BB1_137
; BB#138:                               ; %.lr.ph13.i.preheader.2
	v_mov_b32_e32 v0, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v9, vcc, 0x4a34, v0
	ds_read2_b32 v[37:38], v9 offset1:4
	v_add_i32_e32 v9, vcc, 0x4ab8, v0
	ds_read2_b32 v[39:40], v9 offset1:4
	v_add_i32_e32 v9, vcc, 0x4b3c, v0
	v_add_i32_e32 v5, vcc, 0x492c, v0
	v_add_i32_e32 v7, vcc, 0x49b0, v0
	ds_read2_b32 v[41:42], v9 offset1:4
	v_add_i32_e32 v9, vcc, 0x4bc0, v0
	ds_read_b32 v15, v16 offset:12
	ds_read_b32 v16, v17 offset:12
	ds_read2_b32 v[43:44], v9 offset1:4
	ds_read_b32 v9, v3 offset:12
	ds_read2_b32 v[5:6], v5 offset1:4
	ds_read2_b32 v[7:8], v7 offset1:4
	ds_read_b32 v17, v2 offset:48
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v29, v15, v37
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v30, v9, v37
	s_waitcnt lgkmcnt(2)
	v_mac_f32_e32 v36, v9, v5
	v_mac_f32_e32 v35, v15, v5
	v_mac_f32_e32 v34, v16, v5
	s_waitcnt lgkmcnt(1)
	v_mac_f32_e32 v33, v9, v7
	v_mac_f32_e32 v32, v15, v7
	v_mac_f32_e32 v31, v16, v7
	v_mac_f32_e32 v28, v16, v37
	v_mac_f32_e32 v27, v9, v39
	v_mac_f32_e32 v26, v15, v39
	v_mac_f32_e32 v25, v16, v39
	v_mac_f32_e32 v24, v9, v41
	v_mac_f32_e32 v23, v15, v41
	v_mac_f32_e32 v22, v16, v41
	v_mac_f32_e32 v21, v9, v43
	v_mac_f32_e32 v14, v16, v43
	v_mac_f32_e32 v20, v15, v43
	v_mac_f32_e32 v36, v15, v6
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v34, v17, v6
	v_mac_f32_e32 v33, v15, v8
	v_mac_f32_e32 v32, v16, v8
	v_mac_f32_e32 v31, v17, v8
	v_mac_f32_e32 v30, v15, v38
	v_mac_f32_e32 v29, v16, v38
	v_mac_f32_e32 v28, v17, v38
	v_mac_f32_e32 v27, v15, v40
	v_mac_f32_e32 v26, v16, v40
	v_mac_f32_e32 v25, v17, v40
	v_mac_f32_e32 v24, v15, v42
	v_mac_f32_e32 v23, v16, v42
	v_mac_f32_e32 v22, v17, v42
	v_mac_f32_e32 v21, v15, v44
	v_mac_f32_e32 v14, v17, v44
	v_mac_f32_e32 v20, v16, v44
	v_mac_f32_e32 v35, v16, v6
	s_mov_b32 m0, -1
BB1_139:                                ; %.lr.ph13.i195.preheader.1.2
                                        ; =>This Inner Loop Header: Depth=1
	v_add_i32_e32 v9, vcc, 0x4a54, v0
	v_add_i32_e32 v5, vcc, 0x494c, v0
	v_add_i32_e32 v7, vcc, 0x496c, v0
	v_add_i32_e32 v43, vcc, 0x4b5c, v0
	v_add_i32_e32 v45, vcc, 0x4b7c, v0
	v_add_i32_e32 v17, vcc, 0x4a74, v0
	ds_read2_b32 v[15:16], v9 offset1:4
	v_add_i32_e32 v9, vcc, 0x4ae8, v0
	v_add_i32_e32 v49, vcc, v0, v3
	v_add_i32_e32 v51, vcc, v0, v4
	v_add_i32_e32 v37, vcc, 0x49e0, v0
	v_add_i32_e32 v47, vcc, 0x4bf0, v0
	ds_read2_b32 v[41:42], v9 offset1:4
	ds_read_b32 v9, v49 offset:936
	ds_read2_b32 v[49:50], v51 offset0:238 offset1:242
	ds_read2_b32 v[5:6], v5 offset1:4
	ds_read2_b32 v[7:8], v7 offset1:25
	ds_read2_b32 v[39:40], v17 offset1:25
	ds_read2_b32 v[43:44], v43 offset1:4
	ds_read2_b32 v[45:46], v45 offset1:25
	ds_read2_b32 v[37:38], v37 offset1:4
	ds_read2_b32 v[47:48], v47 offset1:4
	ds_read2_b32 v[51:52], v51 offset0:246 offset1:250
	v_add_i32_e32 v0, vcc, 4, v0
	s_waitcnt lgkmcnt(7)
	v_mac_f32_e32 v36, v9, v5
	v_mac_f32_e32 v35, v49, v5
	v_mac_f32_e32 v34, v50, v5
	s_waitcnt lgkmcnt(6)
	v_mac_f32_e32 v33, v9, v8
	v_mac_f32_e32 v32, v49, v8
	v_mac_f32_e32 v31, v50, v8
	v_mac_f32_e32 v30, v9, v15
	v_mac_f32_e32 v29, v49, v15
	v_mac_f32_e32 v28, v50, v15
	s_waitcnt lgkmcnt(5)
	v_mac_f32_e32 v27, v9, v40
	v_mac_f32_e32 v26, v49, v40
	v_mac_f32_e32 v25, v50, v40
	s_waitcnt lgkmcnt(4)
	v_mac_f32_e32 v24, v9, v43
	v_mac_f32_e32 v23, v49, v43
	v_mac_f32_e32 v22, v50, v43
	s_waitcnt lgkmcnt(3)
	v_mac_f32_e32 v21, v9, v46
	v_mac_f32_e32 v14, v50, v46
	v_mac_f32_e32 v20, v49, v46
	v_cmp_ne_u32_e32 vcc, 12, v0
	v_mac_f32_e32 v36, v49, v6
	v_mac_f32_e32 v35, v50, v6
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v34, v51, v6
	v_mac_f32_e32 v33, v49, v37
	v_mac_f32_e32 v32, v50, v37
	v_mac_f32_e32 v31, v51, v37
	v_mac_f32_e32 v30, v49, v16
	v_mac_f32_e32 v29, v50, v16
	v_mac_f32_e32 v28, v51, v16
	v_mac_f32_e32 v27, v49, v41
	v_mac_f32_e32 v26, v50, v41
	v_mac_f32_e32 v25, v51, v41
	v_mac_f32_e32 v24, v49, v44
	v_mac_f32_e32 v23, v50, v44
	v_mac_f32_e32 v22, v51, v44
	v_mac_f32_e32 v21, v49, v47
	v_mac_f32_e32 v14, v51, v47
	v_mac_f32_e32 v20, v50, v47
	s_and_b64 vcc, exec, vcc
	v_mac_f32_e32 v35, v51, v7
	v_mac_f32_e32 v34, v52, v7
	v_mac_f32_e32 v33, v50, v38
	v_mac_f32_e32 v32, v51, v38
	v_mac_f32_e32 v31, v52, v38
	v_mac_f32_e32 v30, v50, v39
	v_mac_f32_e32 v29, v51, v39
	v_mac_f32_e32 v28, v52, v39
	v_mac_f32_e32 v27, v50, v42
	v_mac_f32_e32 v26, v51, v42
	v_mac_f32_e32 v25, v52, v42
	v_mac_f32_e32 v24, v50, v45
	v_mac_f32_e32 v23, v51, v45
	v_mac_f32_e32 v22, v52, v45
	v_mac_f32_e32 v21, v50, v48
	v_mac_f32_e32 v14, v52, v48
	v_mac_f32_e32 v20, v51, v48
	v_mac_f32_e32 v36, v50, v7
	s_cbranch_vccnz BB1_139
; BB#140:                               ; %.lr.ph13.i.preheader.1.2
	v_mov_b32_e32 v0, 0
	s_mov_b32 m0, -1
	v_add_i32_e32 v3, vcc, 0x49dc, v0
	ds_read2_b32 v[41:42], v3 offset1:4
	v_add_i32_e32 v3, vcc, 0x4a60, v0
	ds_read2_b32 v[39:40], v3 offset1:4
	v_add_i32_e32 v3, vcc, 0x4ae4, v0
	ds_read2_b32 v[37:38], v3 offset1:4
	v_add_i32_e32 v3, vcc, 0x4b68, v0
	v_add_i32_e32 v0, vcc, 0x4bec, v0
	s_mov_b32 s2, 0x2f440
	ds_read2_b32 v[15:16], v3 offset1:4
	ds_read2_b32 v[3:4], v0 offset1:4
	v_mul_lo_i32 v0, v13, s2
	ds_read_b32 v5, v19 offset:936
	ds_read_b32 v6, v18 offset:936
	ds_read2_b32 v[7:8], v2 offset0:234 offset1:246
	v_add_i32_e32 v0, vcc, v1, v0
	v_add_i32_e32 v0, vcc, v0, v10
	v_mov_b32_e32 v1, 0x80
	v_cmp_lt_u32_e32 vcc, v13, v1
	s_and_b64 s[2:3], exec, s[10:11]
	s_and_b64 s[2:3], s[2:3], vcc
	s_and_saveexec_b64 s[4:5], s[2:3]
	s_xor_b64 s[4:5], exec, s[4:5]
	; mask branch BB1_148
	s_cbranch_execz BB1_148
BB1_141:
	v_mov_b32_e32 v18, 0
	s_mulk_i32 s7, 0x46e6
	s_mov_b32 m0, -1
	v_add_i32_e32 v1, vcc, 0x4958, v18
	v_add_i32_e32 v17, vcc, s7, v0
	v_lshlrev_b64 v[17:18], 2, v[17:18]
	ds_read2_b32 v[1:2], v1 offset1:4
	v_add_i32_e32 v17, vcc, s18, v17
	v_mov_b32_e32 v9, s19
	v_cmp_lt_u32_e64 s[10:11], s6, 64
	s_and_b64 s[12:13], exec, s[0:1]
	v_addc_u32_e32 v18, vcc, v9, v18, vcc
	s_and_b64 s[10:11], s[10:11], s[12:13]
	s_and_b64 s[12:13], exec, s[8:9]
	s_and_b64 s[10:11], s[12:13], s[10:11]
	v_cmp_gt_u32_e32 vcc, 55, v10
	s_and_b64 s[12:13], s[10:11], vcc
	s_and_saveexec_b64 s[14:15], s[12:13]
	s_xor_b64 s[12:13], exec, s[14:15]
	; mask branch BB1_143
BB1_142:
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v36, v7, v1
	v_mac_f32_e32 v36, v5, v2
	flat_store_dword v[17:18], v36
BB1_143:
	s_or_b64 exec, exec, s[12:13]
	v_cmp_gt_u32_e32 vcc, 55, v12
	s_and_b64 s[12:13], exec, s[10:11]
	s_and_b64 s[12:13], s[12:13], vcc
	s_and_saveexec_b64 s[14:15], s[12:13]
	s_xor_b64 s[12:13], exec, s[14:15]
	; mask branch BB1_145
BB1_144:
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v35, v5, v1
	v_mac_f32_e32 v35, v6, v2
	flat_store_dword v[17:18], v35 offset:4
BB1_145:
	s_or_b64 exec, exec, s[12:13]
	v_cmp_gt_u32_e32 vcc, 55, v11
	s_and_b64 s[10:11], exec, s[10:11]
	s_and_b64 s[10:11], s[10:11], vcc
	s_and_saveexec_b64 s[12:13], s[10:11]
	s_xor_b64 s[10:11], exec, s[12:13]
	; mask branch BB1_147
BB1_146:
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v34, v6, v1
	v_mac_f32_e32 v34, v8, v2
	flat_store_dword v[17:18], v34 offset:8
BB1_147:                                ; %Flow4613
	s_or_b64 exec, exec, s[10:11]
BB1_148:                                ; %Flow4614
	s_or_b64 exec, exec, s[4:5]
	s_or_b32 s7, s6, 1
	s_waitcnt lgkmcnt(0)
	v_mac_f32_e32 v33, v7, v41
	v_mac_f32_e32 v32, v5, v41
	v_mac_f32_e32 v31, v6, v41
	v_mac_f32_e32 v30, v7, v39
	v_mac_f32_e32 v29, v5, v39
	v_mac_f32_e32 v28, v6, v39
	v_mac_f32_e32 v27, v7, v37
	v_mac_f32_e32 v26, v5, v37
	v_mac_f32_e32 v25, v6, v37
	v_mac_f32_e32 v24, v7, v15
	v_mac_f32_e32 v23, v5, v15
	v_mac_f32_e32 v22, v6, v15
	v_mac_f32_e32 v21, v7, v3
	v_mac_f32_e32 v14, v6, v3
	v_mac_f32_e32 v20, v5, v3
	s_mul_i32 s4, s7, 0xbd1
	v_mac_f32_e32 v33, v5, v42
	v_mac_f32_e32 v32, v6, v42
	v_mac_f32_e32 v31, v8, v42
	v_mac_f32_e32 v30, v5, v40
	v_mac_f32_e32 v29, v6, v40
	v_mac_f32_e32 v28, v8, v40
	v_mac_f32_e32 v27, v5, v38
	v_mac_f32_e32 v26, v6, v38
	v_mac_f32_e32 v25, v8, v38
	v_mac_f32_e32 v24, v5, v16
	v_mac_f32_e32 v23, v6, v16
	v_mac_f32_e32 v22, v8, v16
	v_mac_f32_e32 v21, v5, v4
	v_mac_f32_e32 v14, v8, v4
	v_mac_f32_e32 v20, v6, v4
	v_add_i32_e32 v0, vcc, s4, v0
	s_and_saveexec_b64 s[4:5], s[2:3]
	s_xor_b64 s[4:5], exec, s[4:5]
	; mask branch BB1_156
	s_cbranch_execz BB1_156
BB1_149:
	v_mov_b32_e32 v1, 0
	v_lshlrev_b64 v[1:2], 2, v[0:1]
	v_add_i32_e32 v1, vcc, s18, v1
	v_mov_b32_e32 v3, s19
	v_cmp_lt_u32_e64 s[10:11], s7, 64
	s_and_b64 s[12:13], exec, s[0:1]
	v_addc_u32_e32 v2, vcc, v3, v2, vcc
	s_and_b64 s[10:11], s[10:11], s[12:13]
	s_and_b64 s[12:13], exec, s[8:9]
	s_and_b64 s[10:11], s[12:13], s[10:11]
	v_cmp_gt_u32_e32 vcc, 55, v10
	s_and_b64 s[12:13], s[10:11], vcc
	s_and_saveexec_b64 s[14:15], s[12:13]
	s_xor_b64 s[12:13], exec, s[14:15]
	; mask branch BB1_151
BB1_150:
	flat_store_dword v[1:2], v33
BB1_151:
	s_or_b64 exec, exec, s[12:13]
	v_cmp_gt_u32_e32 vcc, 55, v12
	s_and_b64 s[12:13], exec, s[10:11]
	s_and_b64 s[12:13], s[12:13], vcc
	s_and_saveexec_b64 s[14:15], s[12:13]
	s_xor_b64 s[12:13], exec, s[14:15]
	; mask branch BB1_153
BB1_152:
	flat_store_dword v[1:2], v32 offset:4
BB1_153:
	s_or_b64 exec, exec, s[12:13]
	v_cmp_gt_u32_e32 vcc, 55, v11
	s_and_b64 s[10:11], exec, s[10:11]
	s_and_b64 s[10:11], s[10:11], vcc
	s_and_saveexec_b64 s[12:13], s[10:11]
	s_xor_b64 s[10:11], exec, s[12:13]
	; mask branch BB1_155
BB1_154:
	flat_store_dword v[1:2], v31 offset:8
BB1_155:                                ; %Flow4611
	s_or_b64 exec, exec, s[10:11]
BB1_156:                                ; %Flow4612
	s_or_b64 exec, exec, s[4:5]
	v_add_i32_e32 v0, vcc, 0xbd1, v0
	s_and_saveexec_b64 s[4:5], s[2:3]
	s_xor_b64 s[4:5], exec, s[4:5]
	; mask branch BB1_164
	s_cbranch_execz BB1_164
BB1_157:
	v_mov_b32_e32 v1, 0
	v_lshlrev_b64 v[1:2], 2, v[0:1]
	s_add_i32 s7, s6, 2
	v_add_i32_e32 v1, vcc, s18, v1
	v_mov_b32_e32 v3, s19
	v_cmp_lt_u32_e64 s[10:11], s7, 64
	s_and_b64 s[12:13], exec, s[0:1]
	v_addc_u32_e32 v2, vcc, v3, v2, vcc
	s_and_b64 s[10:11], s[10:11], s[12:13]
	s_and_b64 s[12:13], exec, s[8:9]
	s_and_b64 s[10:11], s[12:13], s[10:11]
	v_cmp_gt_u32_e32 vcc, 55, v10
	s_and_b64 s[12:13], s[10:11], vcc
	s_and_saveexec_b64 s[14:15], s[12:13]
	s_xor_b64 s[12:13], exec, s[14:15]
	; mask branch BB1_159
BB1_158:
	flat_store_dword v[1:2], v30
BB1_159:
	s_or_b64 exec, exec, s[12:13]
	v_cmp_gt_u32_e32 vcc, 55, v12
	s_and_b64 s[12:13], exec, s[10:11]
	s_and_b64 s[12:13], s[12:13], vcc
	s_and_saveexec_b64 s[14:15], s[12:13]
	s_xor_b64 s[12:13], exec, s[14:15]
	; mask branch BB1_161
BB1_160:
	flat_store_dword v[1:2], v29 offset:4
BB1_161:
	s_or_b64 exec, exec, s[12:13]
	v_cmp_gt_u32_e32 vcc, 55, v11
	s_and_b64 s[10:11], exec, s[10:11]
	s_and_b64 s[10:11], s[10:11], vcc
	s_and_saveexec_b64 s[12:13], s[10:11]
	s_xor_b64 s[10:11], exec, s[12:13]
	; mask branch BB1_163
BB1_162:
	flat_store_dword v[1:2], v28 offset:8
BB1_163:                                ; %Flow4609
	s_or_b64 exec, exec, s[10:11]
BB1_164:                                ; %Flow4610
	s_or_b64 exec, exec, s[4:5]
	v_add_i32_e32 v0, vcc, 0xbd1, v0
	s_and_saveexec_b64 s[4:5], s[2:3]
	s_xor_b64 s[4:5], exec, s[4:5]
	; mask branch BB1_172
	s_cbranch_execz BB1_172
BB1_165:
	v_mov_b32_e32 v1, 0
	v_lshlrev_b64 v[1:2], 2, v[0:1]
	s_add_i32 s7, s6, 3
	v_add_i32_e32 v1, vcc, s18, v1
	v_mov_b32_e32 v3, s19
	v_cmp_lt_u32_e64 s[10:11], s7, 64
	s_and_b64 s[12:13], exec, s[0:1]
	v_addc_u32_e32 v2, vcc, v3, v2, vcc
	s_and_b64 s[10:11], s[10:11], s[12:13]
	s_and_b64 s[12:13], exec, s[8:9]
	s_and_b64 s[10:11], s[12:13], s[10:11]
	v_cmp_gt_u32_e32 vcc, 55, v10
	s_and_b64 s[12:13], s[10:11], vcc
	s_and_saveexec_b64 s[14:15], s[12:13]
	s_xor_b64 s[12:13], exec, s[14:15]
	; mask branch BB1_167
BB1_166:
	flat_store_dword v[1:2], v27
BB1_167:
	s_or_b64 exec, exec, s[12:13]
	v_cmp_gt_u32_e32 vcc, 55, v12
	s_and_b64 s[12:13], exec, s[10:11]
	s_and_b64 s[12:13], s[12:13], vcc
	s_and_saveexec_b64 s[14:15], s[12:13]
	s_xor_b64 s[12:13], exec, s[14:15]
	; mask branch BB1_169
BB1_168:
	flat_store_dword v[1:2], v26 offset:4
BB1_169:
	s_or_b64 exec, exec, s[12:13]
	v_cmp_gt_u32_e32 vcc, 55, v11
	s_and_b64 s[10:11], exec, s[10:11]
	s_and_b64 s[10:11], s[10:11], vcc
	s_and_saveexec_b64 s[12:13], s[10:11]
	s_xor_b64 s[10:11], exec, s[12:13]
	; mask branch BB1_171
BB1_170:
	flat_store_dword v[1:2], v25 offset:8
BB1_171:                                ; %Flow4607
	s_or_b64 exec, exec, s[10:11]
BB1_172:                                ; %Flow4608
	s_or_b64 exec, exec, s[4:5]
	v_add_i32_e32 v0, vcc, 0xbd1, v0
	s_and_saveexec_b64 s[4:5], s[2:3]
	s_xor_b64 s[4:5], exec, s[4:5]
	; mask branch BB1_180
	s_cbranch_execz BB1_180
BB1_173:
	v_mov_b32_e32 v1, 0
	v_lshlrev_b64 v[1:2], 2, v[0:1]
	s_add_i32 s7, s6, 4
	v_add_i32_e32 v1, vcc, s18, v1
	v_mov_b32_e32 v3, s19
	v_cmp_lt_u32_e64 s[10:11], s7, 64
	s_and_b64 s[12:13], exec, s[0:1]
	v_addc_u32_e32 v2, vcc, v3, v2, vcc
	s_and_b64 s[10:11], s[10:11], s[12:13]
	s_and_b64 s[12:13], exec, s[8:9]
	s_and_b64 s[10:11], s[12:13], s[10:11]
	v_cmp_gt_u32_e32 vcc, 55, v10
	s_and_b64 s[12:13], s[10:11], vcc
	s_and_saveexec_b64 s[14:15], s[12:13]
	s_xor_b64 s[12:13], exec, s[14:15]
	; mask branch BB1_175
BB1_174:
	flat_store_dword v[1:2], v24
BB1_175:
	s_or_b64 exec, exec, s[12:13]
	v_cmp_gt_u32_e32 vcc, 55, v12
	s_and_b64 s[12:13], exec, s[10:11]
	s_and_b64 s[12:13], s[12:13], vcc
	s_and_saveexec_b64 s[14:15], s[12:13]
	s_xor_b64 s[12:13], exec, s[14:15]
	; mask branch BB1_177
BB1_176:
	flat_store_dword v[1:2], v23 offset:4
BB1_177:
	s_or_b64 exec, exec, s[12:13]
	v_cmp_gt_u32_e32 vcc, 55, v11
	s_and_b64 s[10:11], exec, s[10:11]
	s_and_b64 s[10:11], s[10:11], vcc
	s_and_saveexec_b64 s[12:13], s[10:11]
	s_xor_b64 s[10:11], exec, s[12:13]
	; mask branch BB1_179
BB1_178:
	flat_store_dword v[1:2], v22 offset:8
BB1_179:                                ; %Flow4605
	s_or_b64 exec, exec, s[10:11]
BB1_180:                                ; %Flow4606
	s_or_b64 exec, exec, s[4:5]
	s_and_saveexec_b64 s[4:5], s[2:3]
	s_xor_b64 s[2:3], exec, s[4:5]
	; mask branch BB1_188
	s_cbranch_execz BB1_188
BB1_181:
	v_add_i32_e32 v0, vcc, 0xbd1, v0
	v_mov_b32_e32 v1, 0
	v_lshlrev_b64 v[0:1], 2, v[0:1]
	s_add_i32 s6, s6, 5
	v_add_i32_e32 v0, vcc, s18, v0
	v_mov_b32_e32 v2, s19
	v_cmp_lt_u32_e64 s[4:5], s6, 64
	s_and_b64 s[0:1], exec, s[0:1]
	v_addc_u32_e32 v1, vcc, v2, v1, vcc
	s_and_b64 s[0:1], s[4:5], s[0:1]
	s_and_b64 s[4:5], exec, s[8:9]
	s_and_b64 s[0:1], s[4:5], s[0:1]
	v_cmp_gt_u32_e32 vcc, 55, v10
	s_and_b64 s[4:5], s[0:1], vcc
	s_and_saveexec_b64 s[6:7], s[4:5]
	s_xor_b64 s[4:5], exec, s[6:7]
	; mask branch BB1_183
BB1_182:
	flat_store_dword v[0:1], v21
BB1_183:
	s_or_b64 exec, exec, s[4:5]
	v_cmp_gt_u32_e32 vcc, 55, v12
	s_and_b64 s[4:5], exec, s[0:1]
	s_and_b64 s[4:5], s[4:5], vcc
	s_and_saveexec_b64 s[6:7], s[4:5]
	s_xor_b64 s[4:5], exec, s[6:7]
	; mask branch BB1_185
BB1_184:
	flat_store_dword v[0:1], v20 offset:4
BB1_185:
	s_or_b64 exec, exec, s[4:5]
	v_cmp_gt_u32_e32 vcc, 55, v11
	s_and_b64 s[0:1], exec, s[0:1]
	s_and_b64 s[0:1], s[0:1], vcc
	s_and_saveexec_b64 s[4:5], s[0:1]
	s_xor_b64 s[0:1], exec, s[4:5]
	; mask branch BB1_187
BB1_186:
	flat_store_dword v[0:1], v14 offset:8
BB1_187:                                ; %Flow
	s_or_b64 exec, exec, s[0:1]
BB1_188:                                ; %Flow4604
	s_or_b64 exec, exec, s[2:3]
	s_endpgm
.Lfunc_end1:
	.size	MIOpenCvFwd11x11_2, .Lfunc_end1-MIOpenCvFwd11x11_2
                                        ; -- End function
	.section	.AMDGPU.csdata
; Kernel info:
; codeLenInByte = 19004
; NumSgprs: 50
; NumVgprs: 82
; ScratchSize: 0
; codeLenInByte = 19004
; NumSgprs: 50
; NumVgprs: 82
; FloatMode: 192
; IeeeMode: 1
; ScratchSize: 0
; LDSByteSize: 19512 bytes/workgroup (compile time only)
; SGPRBlocks: 6
; VGPRBlocks: 20
; NumSGPRsForWavesPerEU: 50
; NumVGPRsForWavesPerEU: 82
; ReservedVGPRFirst: 0
; ReservedVGPRCount: 0
; COMPUTE_PGM_RSRC2:USER_SGPR: 6
; COMPUTE_PGM_RSRC2:TRAP_HANDLER: 1
; COMPUTE_PGM_RSRC2:TGID_X_EN: 1
; COMPUTE_PGM_RSRC2:TGID_Y_EN: 1
; COMPUTE_PGM_RSRC2:TGID_Z_EN: 1
; COMPUTE_PGM_RSRC2:TIDIG_COMP_CNT: 0

	.ident	"clang version 4.0 "
	.section	".note.GNU-stack"
	.amdgpu_code_object_metadata
---
Version:         [ 1, 0 ]
Kernels:         
  - Name:            MIOpenCvFwd11x11
    Language:        OpenCL C
    LanguageVersion: [ 1, 2 ]
    Attrs:           
      ReqdWorkGroupSize: [ 256, 1, 1 ]
    Args:            
      - Size:            8
        Align:           8
        ValueKind:       GlobalBuffer
        ValueType:       F32
        AccQual:         ReadOnly
        AddrSpaceQual:   Global
        IsConst:         true
        IsRestrict:      true
        Name:            bot
        TypeName:        'float*'
      - Size:            8
        Align:           8
        ValueKind:       GlobalBuffer
        ValueType:       F32
        AccQual:         ReadOnly
        AddrSpaceQual:   Global
        IsConst:         true
        IsRestrict:      true
        Name:            weights
        TypeName:        'float*'
      - Size:            8
        Align:           8
        ValueKind:       GlobalBuffer
        ValueType:       F32
        AccQual:         Default
        AddrSpaceQual:   Global
        IsRestrict:      true
        Name:            top
        TypeName:        'float*'
      - Size:            4
        Align:           4
        ValueKind:       ByValue
        ValueType:       F32
        AccQual:         Default
        Name:            padding_val
        TypeName:        float
      - Size:            8
        Align:           8
        ValueKind:       HiddenGlobalOffsetX
        ValueType:       I64
      - Size:            8
        Align:           8
        ValueKind:       HiddenGlobalOffsetY
        ValueType:       I64
      - Size:            8
        Align:           8
        ValueKind:       HiddenGlobalOffsetZ
        ValueType:       I64
    CodeProps:       
      KernargSegmentSize: 64
      WorkgroupGroupSegmentSize: 14832
      WavefrontNumSGPRs: 36
      WorkitemNumVGPRs: 80
      KernargSegmentAlign: 4
      GroupSegmentAlign: 4
      PrivateSegmentAlign: 4
      WavefrontSize:   6
  - Name:            MIOpenCvFwd11x11_2
    Language:        OpenCL C
    LanguageVersion: [ 1, 2 ]
    Attrs:           
      ReqdWorkGroupSize: [ 256, 1, 1 ]
    Args:            
      - Size:            8
        Align:           8
        ValueKind:       GlobalBuffer
        ValueType:       F32
        AccQual:         ReadOnly
        AddrSpaceQual:   Global
        IsConst:         true
        IsRestrict:      true
        Name:            bot
        TypeName:        'float*'
      - Size:            8
        Align:           8
        ValueKind:       GlobalBuffer
        ValueType:       F32
        AccQual:         ReadOnly
        AddrSpaceQual:   Global
        IsConst:         true
        IsRestrict:      true
        Name:            weights
        TypeName:        'float*'
      - Size:            8
        Align:           8
        ValueKind:       GlobalBuffer
        ValueType:       F32
        AccQual:         Default
        AddrSpaceQual:   Global
        IsRestrict:      true
        Name:            top
        TypeName:        'float*'
      - Size:            4
        Align:           4
        ValueKind:       ByValue
        ValueType:       F32
        AccQual:         Default
        Name:            padding_val
        TypeName:        float
      - Size:            8
        Align:           8
        ValueKind:       HiddenGlobalOffsetX
        ValueType:       I64
      - Size:            8
        Align:           8
        ValueKind:       HiddenGlobalOffsetY
        ValueType:       I64
      - Size:            8
        Align:           8
        ValueKind:       HiddenGlobalOffsetZ
        ValueType:       I64
    CodeProps:       
      KernargSegmentSize: 64
      WorkgroupGroupSegmentSize: 19512
      WavefrontNumSGPRs: 50
      WorkitemNumVGPRs: 82
      KernargSegmentAlign: 4
      GroupSegmentAlign: 4
      PrivateSegmentAlign: 4
      WavefrontSize:   6
...
	.end_amdgpu_code_object_metadata
