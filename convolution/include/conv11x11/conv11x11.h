#ifndef __CONV11X11_H__
#define __CONV11X11_H__

#include "hip_fundamental.h"
#include "tensor.h"
#include "convolution_cpu.h"
#include "conv11x11/conv11x11_kernel_parameter.h"

#define LOAD_FROM_HSACO 1
#define LOAD_FROM_BYTESARRAY 1

#define ITER 20


// CPU conv11x11, no bias, stride, padding
void conv11x11_cpu(Tensor input, Tensor weight, Tensor stride, int *padding, Tensor output){
    double conv_cpu_time = 0;
    for (int iter=0; iter<1; iter++){
        conv_cpu_time += convolution_cpu(input,
                                           weight,
                                           output,
                                           stride,
                                           padding,
                                           0,
                                           NULL,
                                           NULL);
    }
    //conv_cpu_time /= ITER;
    double conv_cpu_tflops = conv_TFLOPS(input, weight, output, conv_cpu_time);
    std::cout << "[INFO] Convolution 11x11 CPU Time: " << conv_cpu_time << " sec " << std::endl;
    std::cout << "[INFO] Convolution 11x11 CPU Perf: " << conv_cpu_tflops << " TFLOPS " << std::endl;
}


#ifdef LOAD_FROM_HSACO
// GPU conv11x11, bias, no stride, padding
// hsaco kernel
void conv11x11_gpu_load_from_hsaco(Tensor input, Tensor weight, Tensor stride, int* padding, Tensor output){
    Tensor input_d = gpuTensor(input);
    Tensor weight_d = gpuTensor(weight);
    Tensor output_d = gpuTensor(output, false);

    // ------- grid & block --------
    int PROCESING_WIDTH = ((MLO_OUT_WIDTH + MLO_OUT_PIX_TILE0- 1) / MLO_OUT_PIX_TILE0);
    int OUT_EXTENT1 = std::min(MLO_OUT_HEIGHT, (MLO_GRP_SZ / PROCESING_WIDTH));
    int n_out_stacks = 1;
    // n_in_stacks input map will be written in the local memory.
    int n_in_stacks = 1;
    n_in_stacks  = std::min(MLO_N_INPUTS, n_in_stacks);
    n_out_stacks = std::min(MLO_N_OUTPUTS, n_out_stacks);
    // number of maps in a stack or number of input read blocks written into 1 wk-item (lane)
    // param
    int n_in_data_tiles = 1;

    int result_n_out_pix_tiles =
#if MLO_DIR_FORWARD
              std::min(6, (MLO_N_OUTPUTS + n_out_stacks - 1) / n_out_stacks);
#else
              std::min(MLO_N_OUTPUTS, ((data_multiplier1 > 1 || data_multiplier0 > 1) ? 1 : 4));
#endif
    // total maps per group
    int total_out_maps = result_n_out_pix_tiles
#if MLO_DIR_FORWARD
                         * n_out_stacks
#endif
        ;

    // second pass if needed
    int n_extents           = ((MLO_OUT_HEIGHT + OUT_EXTENT1 - 1) / OUT_EXTENT1);
    int n_output_map_blocks = ((MLO_N_OUTPUTS + total_out_maps - 1) / total_out_maps);
    int n_out_pix_tiles = MLO_OUT_PIX_TILE0 * MLO_OUT_PIX_TILE1;
    int last_out_extent1 = MLO_OUT_HEIGHT - (std::max(1, MLO_OUT_HEIGHT / OUT_EXTENT1) * OUT_EXTENT1);
    last_out_extent1    = (last_out_extent1 < 0) ? 0 : last_out_extent1;
#if MLO_DIR_FORWARD
    if(0 < last_out_extent1 && last_out_extent1 <= OUT_EXTENT1 / 2)
    {
        n_extents       = std::max(1, MLO_OUT_HEIGHT / OUT_EXTENT1);
    }
#endif
    int gbl_0 = 
#if MLO_DIR_FORWARD
                MLO_GRP_SZ * n_extents;
#else
                ((n_out_pix_tiles + MLO_GRP_SZ - 1) / MLO_GRP_SZ) * MLO_GRP_SZ;
#endif
    int gbl_1 = n_output_map_blocks;
    int gbl_2 = MLO_BATCH_SZ;

    //std::cout << "Grid: (" << gbl_0 << ", " << gbl_1 << ", " << gbl_2 << ")" << std::endl;
    const char *hsacoFileName = "conv11x11.hsaco";
    const char *kernelName1 = "MIOpenCvFwd11x11";
    hipFunction_t kernel_function1 = loadKernelFromHsaco(hsacoFileName, kernelName1);
    const char *kernelName2 = "MIOpenCvFwd11x11_2";
    hipFunction_t kernel_function2 = loadKernelFromHsaco(hsacoFileName, kernelName2);
    struct {
        const FLOAT* in_ptr;
        const FLOAT* wei_ptr;
#if MLO_CONV_BIAS
        const FLOAT* bias;
#endif
        FLOAT* out_ptr;
        FLOAT padding_val;
    } hipFunctionArgs;
    hipFunctionArgs.in_ptr = (const FLOAT *)input_d.data;
    hipFunctionArgs.wei_ptr = (const FLOAT *)weight_d.data;
#if MLO_CONV_BIAS
    hipFunctionArgs.bias = NULL;
#endif
    hipFunctionArgs.out_ptr = (FLOAT *)output_d.data;
    hipFunctionArgs.padding_val = 0;
    size_t hipFunctionArgsSize = sizeof(hipFunctionArgs);
    void *hipLaunchParams[] = {HIP_LAUNCH_PARAM_BUFFER_POINTER, &hipFunctionArgs, HIP_LAUNCH_PARAM_BUFFER_SIZE, &hipFunctionArgsSize, HIP_LAUNCH_PARAM_END};

    double conv_gpu_time = 0;
    for(int iter=0; iter<ITER; iter++){
        gpu_time_tic();
        GPU_CHECK(hipModuleLaunchKernel(kernel_function1,
                                        //gbl_0/MLO_GRP_SZ0, gbl_1/MLO_GRP_SZ1, gbl_2/MLO_GRP_SZ2,
                                        4, 11, 128,
                                        MLO_GRP_SZ0, MLO_GRP_SZ1, MLO_GRP_SZ2,
                                        0, 0, NULL,
                                        (void **)hipLaunchParams));
        GPU_CHECK(hipModuleLaunchKernel(kernel_function2,
                                        1, 11, 32,
                                        MLO_GRP_SZ0, MLO_GRP_SZ1, MLO_GRP_SZ2,
                                        0, 0, NULL,
                                        (void **)hipLaunchParams));
        conv_gpu_time += gpu_time_toc();
    }
    conv_gpu_time /= ITER;
    double conv_gpu_tflops = conv_TFLOPS(input_d, weight_d, output_d, conv_gpu_time);
    std::cout << "[INFO] Convolution 11x11 GPU Time: " << conv_gpu_time << " sec " << std::endl;
    std::cout << "[INFO] Convolution 11x11 GPU Perf: " << conv_gpu_tflops << " TFLOPS " << std::endl;
    std::cout << "[INFO] Convolution 11x11 CPU&GPU Verify: " << cpuTensor_gpuTensor_verify(output, output_d) << std::endl;
}
#endif

#ifdef LOAD_FROM_BYTESARRAY
#include "conv11x11/conv11x11_opencl_kernel_bytesArray.h"
// GPU conv11x11, no bias, stride, padding
// code bytes array kernel
void conv11x11_gpu_load_from_bytesArray(Tensor input, Tensor weight, Tensor stride, int* padding, Tensor output){
    Tensor input_d = gpuTensor(input);
    Tensor weight_d = gpuTensor(weight);
    Tensor output_d = gpuTensor(output, false);

    // ------- grid & block --------
    int PROCESING_WIDTH = ((MLO_OUT_WIDTH + MLO_OUT_PIX_TILE0- 1) / MLO_OUT_PIX_TILE0);
    int OUT_EXTENT1 = std::min(MLO_OUT_HEIGHT, (MLO_GRP_SZ / PROCESING_WIDTH));
    int n_out_stacks = 1;
    // n_in_stacks input map will be written in the local memory.
    int n_in_stacks = 1;
    n_in_stacks  = std::min(MLO_N_INPUTS, n_in_stacks);
    n_out_stacks = std::min(MLO_N_OUTPUTS, n_out_stacks);
    // number of maps in a stack or number of input read blocks written into 1 wk-item (lane)
    // param
    int n_in_data_tiles = 1;

    int result_n_out_pix_tiles =
#if MLO_DIR_FORWARD
              std::min(6, (MLO_N_OUTPUTS + n_out_stacks - 1) / n_out_stacks);
#else
              std::min(MLO_N_OUTPUTS, ((data_multiplier1 > 1 || data_multiplier0 > 1) ? 1 : 4));
#endif
    // total maps per group
    int total_out_maps = result_n_out_pix_tiles
#if MLO_DIR_FORWARD
                         * n_out_stacks
#endif
        ;

    // second pass if needed
    int n_extents           = ((MLO_OUT_HEIGHT + OUT_EXTENT1 - 1) / OUT_EXTENT1);
    int n_output_map_blocks = ((MLO_N_OUTPUTS + total_out_maps - 1) / total_out_maps);
    int n_out_pix_tiles = MLO_OUT_PIX_TILE0 * MLO_OUT_PIX_TILE1;
    int last_out_extent1 = MLO_OUT_HEIGHT - (std::max(1, MLO_OUT_HEIGHT / OUT_EXTENT1) * OUT_EXTENT1);
    last_out_extent1    = (last_out_extent1 < 0) ? 0 : last_out_extent1;
#if MLO_DIR_FORWARD
    if(0 < last_out_extent1 && last_out_extent1 <= OUT_EXTENT1 / 2)
    {
        n_extents       = std::max(1, MLO_OUT_HEIGHT / OUT_EXTENT1);
    }
#endif
    int gbl_0 = 
#if MLO_DIR_FORWARD
                MLO_GRP_SZ * n_extents;
#else
                ((n_out_pix_tiles + MLO_GRP_SZ - 1) / MLO_GRP_SZ) * MLO_GRP_SZ;
#endif
    int gbl_1 = n_output_map_blocks;
    int gbl_2 = MLO_BATCH_SZ;

    const char *kernelName1 = "MIOpenCvFwd11x11";
    hipFunction_t kernel_function1 = loadKernelFromBytesArray(_conv11x11_HSA_CodeObjMem, kernelName1);
    const char *kernelName2 = "MIOpenCvFwd11x11_2";
    hipFunction_t kernel_function2 = loadKernelFromBytesArray(_conv11x11_HSA_CodeObjMem, kernelName2);
    struct {
        const FLOAT* in_ptr;
        const FLOAT* wei_ptr;
#if MLO_CONV_BIAS
        const FLOAT* bias;
#endif
        FLOAT* out_ptr;
        FLOAT padding_val;
    } hipFunctionArgs;
    hipFunctionArgs.in_ptr = (const FLOAT *)input_d.data;
    hipFunctionArgs.wei_ptr = (const FLOAT *)weight_d.data;
#if MLO_CONV_BIAS
    hipFunctionArgs.bias = NULL;
#endif
    hipFunctionArgs.out_ptr = (FLOAT *)output_d.data;
    hipFunctionArgs.padding_val = 0;
    size_t hipFunctionArgsSize = sizeof(hipFunctionArgs);
    void *hipLaunchParams[] = {HIP_LAUNCH_PARAM_BUFFER_POINTER, &hipFunctionArgs, HIP_LAUNCH_PARAM_BUFFER_SIZE, &hipFunctionArgsSize, HIP_LAUNCH_PARAM_END};

    double conv_gpu_time = 0;
    for(int i=0; i<ITER; i++){
        gpu_time_tic();
        GPU_CHECK(hipModuleLaunchKernel(kernel_function1,
                                        //gbl_0/MLO_GRP_SZ0, gbl_1/MLO_GRP_SZ1, gbl_2/MLO_GRP_SZ2,
                                        4, 11, 128,
                                        MLO_GRP_SZ0, MLO_GRP_SZ1, MLO_GRP_SZ2,
                                        0, 0, NULL,
                                        (void **)hipLaunchParams));
        GPU_CHECK(hipModuleLaunchKernel(kernel_function2,
                                        1, 11, 32,
                                        MLO_GRP_SZ0, MLO_GRP_SZ1, MLO_GRP_SZ2,
                                        0, 0, NULL,
                                        (void **)hipLaunchParams));
        conv_gpu_time += gpu_time_toc();
    }
    conv_gpu_time /= ITER;
    double conv_gpu_tflops = conv_TFLOPS(input_d, weight_d, output_d, conv_gpu_time);
    std::cout << "[INFO] Convolution 11x11 GPU Time: " << conv_gpu_time << " sec " << std::endl;
    std::cout << "[INFO] Convolution 11x11 GPU Perf: " << conv_gpu_tflops << " TFLOPS " << std::endl;
    //std::cout << "[INFO] Convolution 11x11 CPU&GPU Verify: " << cpuTensor_gpuTensor_verify(output, output_d) << std::endl;
    gpu2cpu(output_d.data, output.data, sizeof(FLOAT)*output.n*output.c*output.h*output.w);
}
#endif


void conv11x11(){
    const int batch = MLO_BATCH_SZ;
    const int in_channel = MLO_N_INPUTS;
    const int out_channel = MLO_N_OUTPUTS;
    const int input_n = batch, output_n = batch;
    const int input_c = in_channel, weight_c = in_channel;
    const int input_h = MLO_IN_HEIGHT;
    const int input_w = MLO_IN_WIDTH;
    const int weight_h = 11;
    const int weight_w = 11;
    const int weight_n = out_channel, output_c = out_channel;
    const int padding_h = 2;
    const int padding_w = 2;
    const int stride_h = STRIDE_H;
    const int stride_w = STRIDE_W;
    const int output_h = MLO_OUT_HEIGHT;
    const int output_w = MLO_OUT_WIDTH;

    Tensor input = cpuTensor(input_n, input_c, input_h, input_w, true, true);
    Tensor weight = cpuTensor(weight_n, weight_c, weight_h, weight_w, true, true);
    Tensor output = cpuTensor(output_n, output_c, output_h, output_w, true, true);
    Tensor stride = cpuTensor(1, 1, stride_h, stride_w, false, false);
    int padding[2] = {padding_h, padding_w};
    
    // CPU
    //conv11x11_cpu(input, weight, stride, padding, output);
   
    // GPU
#if LOAD_FROM_BYTESARRAY
    conv11x11_gpu_load_from_bytesArray(input, weight, stride, padding, output);
#endif
#if LOAD_FROM_HSACO
    conv11x11_gpu_load_from_hsaco(input, weight, stride, padding, output);
#endif
}

#endif
