#ifndef __CONV1X1_H__
#define __CONV1X1_H__

#include "hip_fundamental.h"
#include "tensor.h"
#include "convolution_cpu.h"
#include "conv1x1/conv1x1_kernel_parameter.h"

#define LOAD_FROM_HSACO 1
#define LOAD_FROM_BYTESARRAY 1
#define LOAD_FROM_STRING 0

#define ITER 20


// CPU conv1x1, no bias, no stride, no padding
void conv1x1_cpu(Tensor input, Tensor weight, Tensor stride, int *padding, Tensor output){
    //std::cout << input.data << std::endl;
    double conv_cpu_time = 0;
    for (int iter=0; iter<1; iter++){
        conv_cpu_time += convolution_cpu(input,
                                           weight,
                                           output,
                                           stride,
                                           NULL,
                                           0,
                                           NULL,
                                           NULL);
    }
    //conv_cpu_time /= ITER;
    double conv_cpu_tflops = conv_TFLOPS(input, weight, output, conv_cpu_time);
    std::cout << "[INFO] Convolution 1x1 CPU Time: " << conv_cpu_time << " sec " << std::endl;
    std::cout << "[INFO] Convolution 1x1 CPU Perf: " << conv_cpu_tflops << " TFLOPS " << std::endl;
}


#ifdef LOAD_FROM_STRING
#include "conv1x1/conv1x1_hip_kernel_string.h"
// GPU conv1x1, no bias, no stride, no padding
// string kernel
void conv1x1_gpu_load_from_string(Tensor input, Tensor weight, Tensor stride, int *padding, Tensor output){
    Tensor input_d = gpuTensor(input);
    Tensor weight_d = gpuTensor(weight);
    //cpu2gpu_const(weight.data, wei_ptr, weight.n*weight.c*weight.h*weight.w*sizeof(FLOAT));
    Tensor output_d = gpuTensor(output, false);
    uint blocks = BATCHSIZE * (MLO_N_OUTPUTS/MLO_N_LCL_OUT_MAPS) * H_out * W_out * (MLO_N_INPUTS/MLO_N_LCL_IN_MAPS) / FIXED_WORKGROUP_SIZE;
    double conv_gpu_time = 0;
    for (int iter=0; iter<ITER; iter++){
        gpu_time_tic();
    
        hipLaunchKernel(conv1x1_pass2,   /* compute kernel*/
                    dim3(blocks), dim3(FIXED_WORKGROUP_SIZE), 0/*dynamic shared*/, 0/*stream*/,     /* launch config*/
                    input_d.data, weight_d.data, output_d.data);
        conv_gpu_time += gpu_time_toc();
    }
    conv_gpu_time /= ITER;
    double conv_gpu_tflops = conv_TFLOPS(input_d, weight, output_d, conv_gpu_time);
    std::cout << "[INFO] Convolution 1x1 GPU Time: " << conv_gpu_time << " sec " << std::endl;
    std::cout << "[INFO] Convolution 1x1 GPU Perf: " << conv_gpu_tflops << " TFLOPS " << std::endl;
    std::cout << "[INFO] Convolution 1x1 CPU&GPU Verify: " << cpuTensor_gpuTensor_verify(output, output_d) << std::endl;
}
#endif


#ifdef LOAD_FROM_HSACO
// GPU conv1x1, no bias, no stride, no padding
// hsaco kernel
void conv1x1_gpu_load_from_hsaco(Tensor input, Tensor weight, Tensor stride, int *padding, Tensor output){
    Tensor input_d = gpuTensor(input);
    Tensor weight_d = gpuTensor(weight);
    Tensor output_d = gpuTensor(output, false);
    uint blocks = BATCHSIZE * (MLO_N_OUTPUTS/MLO_N_LCL_OUT_MAPS) * H_out * W_out * (MLO_N_INPUTS/MLO_N_LCL_IN_MAPS) / FIXED_WORKGROUP_SIZE;

    //const char *hsacoFileName = "conv1x1_hip_kernel.hsaco";
    //const char *kernelName = "_ZZ7conv1x1vEN67HIP_kernel_functor_name_begin_unnamed_HIP_kernel_functor_name_end_419__cxxamp_trampolineEPfS0_S0_";
    //const char *hsacoFileName = "conv1x1_opencl_kernel.hsaco";
    //const char *kernelName = "MIOpenConv1x1";
    const char *hsacoFileName = "conv1x1.hsaco";
    const char *kernelName = "MIOpenConv1x1";
    hipFunction_t kernel_function = loadKernelFromHsaco(hsacoFileName, kernelName);
    struct {
        const FLOAT* in_ptr;
        const FLOAT* wei_ptr;
#if MLO_CONV_BIAS
        const FLOAT* bias;
#endif
        FLOAT* out_ptr;
    } hipFunctionArgs;
    hipFunctionArgs.in_ptr = (const FLOAT *)input_d.data;
    hipFunctionArgs.wei_ptr = (const FLOAT *)weight_d.data;
#if MLO_CONV_BIAS
    hipFunctionArgs.bias = NULL;
#endif
    hipFunctionArgs.out_ptr = (FLOAT *)output_d.data;
    size_t hipFunctionArgsSize = sizeof(hipFunctionArgs);
    void *hipLaunchParams[] = {HIP_LAUNCH_PARAM_BUFFER_POINTER, &hipFunctionArgs, HIP_LAUNCH_PARAM_BUFFER_SIZE, &hipFunctionArgsSize, HIP_LAUNCH_PARAM_END};

    double conv_gpu_time = 0;
    for(int iter=0; iter<ITER; iter++){
        gpu_time_tic();
        GPU_CHECK(hipModuleLaunchKernel(kernel_function,
                                        blocks, 1, 1,
                                        FIXED_WORKGROUP_SIZE, 1, 1,
                                        0, 0, NULL,
                                        (void **)hipLaunchParams));
        conv_gpu_time += gpu_time_toc();
    }
    conv_gpu_time /= ITER;
    double conv_gpu_tflops = conv_TFLOPS(input_d, weight_d, output_d, conv_gpu_time);
    std::cout << "[INFO] Convolution 1x1 GPU Time: " << conv_gpu_time << " sec " << std::endl;
    std::cout << "[INFO] Convolution 1x1 GPU Perf: " << conv_gpu_tflops << " TFLOPS " << std::endl;
    std::cout << "[INFO] Convolution 1x1 CPU&GPU Verify: " << cpuTensor_gpuTensor_verify(output, output_d) << std::endl;
}
#endif

#ifdef LOAD_FROM_BYTESARRAY
#include "conv1x1/conv1x1_opencl_kernel_bytesArray.h"
// GPU conv1x1, no bias, no stride, no padding
// code bytes array kernel
void conv1x1_gpu_load_from_bytesArray(Tensor input, Tensor weight, Tensor stride, int *padding, Tensor output){
    Tensor input_d = gpuTensor(input);
    Tensor weight_d = gpuTensor(weight);
    Tensor output_d = gpuTensor(output, false);
    uint blocks = BATCHSIZE * (MLO_N_OUTPUTS/MLO_N_LCL_OUT_MAPS) * H_out * W_out * (MLO_N_INPUTS/MLO_N_LCL_IN_MAPS) / FIXED_WORKGROUP_SIZE;
    //const char *kernelName = "_ZZ7conv1x1vEN67HIP_kernel_functor_name_begin_unnamed_HIP_kernel_functor_name_end_419__cxxamp_trampolineEPfS0_S0_";

    const char *kernelName = "MIOpenConv1x1";
    hipFunction_t kernel_function = loadKernelFromBytesArray(_conv1x1_opencl_kernel_HSA_CodeObjMem, kernelName);
    struct {
        const FLOAT* in_ptr;
        const FLOAT* wei_ptr;
#if MLO_CONV_BIAS
        const FLOAT* bias;
#endif
        FLOAT* out_ptr;
    } hipFunctionArgs;
    hipFunctionArgs.in_ptr = (const FLOAT *)input_d.data;
    hipFunctionArgs.wei_ptr = (const FLOAT *)weight_d.data;
#if MLO_CONV_BIAS
    hipFunctionArgs.bias = NULL;
#endif
    hipFunctionArgs.out_ptr = (FLOAT *)output_d.data;
    size_t hipFunctionArgsSize = sizeof(hipFunctionArgs);
    void *hipLaunchParams[] = {HIP_LAUNCH_PARAM_BUFFER_POINTER, &hipFunctionArgs, HIP_LAUNCH_PARAM_BUFFER_SIZE, &hipFunctionArgsSize, HIP_LAUNCH_PARAM_END};

    double conv_gpu_time = 0;
    for(int i=0; i<ITER; i++){
        gpu_time_tic();
        GPU_CHECK(hipModuleLaunchKernel(kernel_function,
                                        blocks, 1, 1,
                                        FIXED_WORKGROUP_SIZE, 1, 1,
                                        0, 0, NULL,
                                        (void **)hipLaunchParams));
        conv_gpu_time += gpu_time_toc();
    }
    conv_gpu_time /= ITER;
    double conv_gpu_tflops = conv_TFLOPS(input_d, weight_d, output_d, conv_gpu_time);
    std::cout << "[INFO] Convolution 1x1 GPU Time: " << conv_gpu_time << " sec " << std::endl;
    std::cout << "[INFO] Convolution 1x1 GPU Perf: " << conv_gpu_tflops << " TFLOPS " << std::endl;
    std::cout << "[INFO] Convolution 1x1 CPU&GPU Verify: " << cpuTensor_gpuTensor_verify(output, output_d) << std::endl;
}
#endif


void conv1x1(){
    const int batch = BATCHSIZE;
    const int in_channel = MLO_N_INPUTS;
    const int out_channel = MLO_N_OUTPUTS;
    const int input_n = batch, output_n = batch;
    const int input_c = in_channel, weight_c = in_channel;
    const int input_h = H;
    const int input_w = W;
    const int weight_h = 1;
    const int weight_w = 1;
    const int weight_n = out_channel, output_c = out_channel;
    const int padding_h = 0;
    const int padding_w = 0;
    const int stride_h = 1;
    const int stride_w = 1;
    const int output_h = H;
    const int output_w = W;

    Tensor input = cpuTensor(input_n, input_c, input_h, input_w, true, true);
    Tensor weight = cpuTensor(weight_n, weight_c, weight_h, weight_w, true, true);
    Tensor output = cpuTensor(output_n, output_c, output_h, output_w, true, true);
    Tensor stride = cpuTensor(1, 1, stride_h, stride_w, false, false);
    int padding[2] = {padding_h, padding_w};

    // CPU
    conv1x1_cpu(input, weight, stride, padding, output);
   
    // GPU
#if LOAD_FROM_HSACO
    conv1x1_gpu_load_from_hsaco(input, weight, stride, padding, output);
#endif
#if LOAD_FROM_BYTESARRAY
    conv1x1_gpu_load_from_bytesArray(input, weight, stride, padding, output);
#endif
#if LOAD_FROM_STRING
    conv1x1_gpu_load_from_string(input, weight, stride, padding, output);
#endif
}

#endif

