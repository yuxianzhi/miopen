	.text
	.hsa_code_object_version 2,1
	.hsa_code_object_isa 9,0,0,"AMD","AMDGPU"
	.globl	MIOpenConv1x1           ; -- Begin function MIOpenConv1x1
	.p2align	8
	.type	MIOpenConv1x1,@function
	.amdgpu_hsa_kernel MIOpenConv1x1
MIOpenConv1x1:                          ; @MIOpenConv1x1
	.amd_kernel_code_t
		amd_code_version_major = 1
		amd_code_version_minor = 1
		amd_machine_kind = 1
		amd_machine_version_major = 9
		amd_machine_version_minor = 0
		amd_machine_version_stepping = 0
		kernel_code_entry_byte_offset = 256
		kernel_code_prefetch_byte_size = 0
		max_scratch_backing_memory_byte_size = 0
		granulated_workitem_vgpr_count = 10
		granulated_wavefront_sgpr_count = 12
		priority = 0
		float_mode = 240
		priv = 0
		enable_dx10_clamp = 1
		debug_mode = 0
		enable_ieee_mode = 1
		enable_sgpr_private_segment_wave_byte_offset = 0
		user_sgpr_count = 6
		enable_trap_handler = 1
		enable_sgpr_workgroup_id_x = 1
		enable_sgpr_workgroup_id_y = 0
		enable_sgpr_workgroup_id_z = 0
		enable_sgpr_workgroup_info = 0
		enable_vgpr_workitem_id = 0
		enable_exception_msb = 0
		granulated_lds_size = 0
		enable_exception = 0
		enable_sgpr_private_segment_buffer = 1
		enable_sgpr_dispatch_ptr = 0
		enable_sgpr_queue_ptr = 0
		enable_sgpr_kernarg_segment_ptr = 1
		enable_sgpr_dispatch_id = 0
		enable_sgpr_flat_scratch_init = 0
		enable_sgpr_private_segment_size = 0
		enable_sgpr_grid_workgroup_count_x = 0
		enable_sgpr_grid_workgroup_count_y = 0
		enable_sgpr_grid_workgroup_count_z = 0
		enable_ordered_append_gds = 0
		private_element_size = 1
		is_ptr64 = 1
		is_dynamic_callstack = 0
		is_debug_enabled = 0
		is_xnack_enabled = 0
		workitem_private_segment_byte_size = 0
		workgroup_group_segment_byte_size = 0
		gds_segment_byte_size = 0
		kernarg_segment_byte_size = 56
		workgroup_fbarrier_count = 0
		wavefront_sgpr_count = 98
		workitem_vgpr_count = 43
		reserved_vgpr_first = 0
		reserved_vgpr_count = 0
		reserved_sgpr_first = 0
		reserved_sgpr_count = 0
		debug_wavefront_private_segment_offset_sgpr = 0
		debug_private_segment_buffer_sgpr = 0
		kernarg_segment_alignment = 4
		group_segment_alignment = 4
		private_segment_alignment = 4
		wavefront_size = 6
		call_convention = -1
		runtime_loader_kernel_symbol = 0
	.end_amd_kernel_code_t
; BB#0:
	s_lshl_b32 s6, s6, 4
	s_and_b32 s0, s6, 0xffffffc0
	v_or_b32_e32 v0, s0, v0
	v_mov_b32_e32 v1, 0xc400
	v_cmp_lt_u32_e32 vcc, v0, v1
	s_and_saveexec_b64 s[0:1], vcc
	s_xor_b64 s[0:1], exec, s[0:1]
	; mask branch BB0_5
	s_cbranch_execz BB0_5
BB0_1:                                  ; %.preheader232.preheader349
	v_mov_b32_e32 v1, 0x5397829d
	v_mul_hi_u32 v1, v0, v1
	v_writelane_b32 v42, s0, 0
	v_writelane_b32 v42, s1, 1
	s_load_dwordx4 s[0:3], s[4:5], 0x0
	v_lshrrev_b32_e32 v1, 10, v1
	s_load_dwordx2 s[4:5], s[4:5], 0x10
	v_mul_u32_u24_e32 v2, 0xc40, v1
	v_subrev_i32_e32 v0, vcc, v2, v0
	v_mul_u32_u24_e32 v1, 0x31000, v1
	v_mov_b32_e32 v25, 0
	v_or_b32_e32 v24, v0, v1
	v_lshlrev_b64 v[2:3], 2, v[24:25]
	s_waitcnt lgkmcnt(0)
	v_writelane_b32 v42, s4, 2
	v_mov_b32_e32 v4, s1
	v_add_i32_e32 v10, vcc, s0, v2
	v_writelane_b32 v42, s5, 3
	v_addc_u32_e32 v11, vcc, v4, v3, vcc
	s_movk_i32 s5, 0x3100
	flat_load_dword v8, v[10:11]
	v_add_i32_e32 v2, vcc, s5, v10
	v_addc_u32_e32 v3, vcc, 0, v11, vcc
	s_movk_i32 s8, 0x6200
        flat_load_dword v6, v[2:3]
	v_add_i32_e32 v4, vcc, s8, v10
	v_addc_u32_e32 v5, vcc, 0, v11, vcc
	s_mov_b32 s9, 0x9300
        flat_load_dword v7, v[4:5]
	v_add_i32_e32 v12, vcc, s9, v10
	v_addc_u32_e32 v13, vcc, 0, v11, vcc
	s_mov_b32 s10, 0xc400
        flat_load_dword v9, v[12:13]
	v_add_i32_e32 v14, vcc, s10, v10
	v_addc_u32_e32 v15, vcc, 0, v11, vcc
	s_mov_b32 s11, 0xf500
        flat_load_dword v5, v[14:15]
	v_add_i32_e32 v16, vcc, s11, v10
	v_addc_u32_e32 v17, vcc, 0, v11, vcc
	s_mov_b32 s12, 0x12600
        flat_load_dword v4, v[16:17]
	v_add_i32_e32 v18, vcc, s12, v10
	v_addc_u32_e32 v19, vcc, 0, v11, vcc
	s_mov_b32 s13, 0x15700
        flat_load_dword v3, v[18:19]
	v_add_i32_e32 v20, vcc, s13, v10
	v_addc_u32_e32 v21, vcc, 0, v11, vcc
	flat_load_dword v2, v[20:21]
	s_and_b32 s4, s6, 48
	s_lshl_b32 s0, s4, 8
	s_mov_b32 s14, 0x18800
	v_add_i32_e32 v15, vcc, s14, v10
	s_add_u32 s6, s2, s0
	v_addc_u32_e32 v16, vcc, 0, v11, vcc
	s_addc_u32 s7, s3, 0
	v_mov_b32_e32 v21, s7
	v_mov_b32_e32 v23, v16
	v_mov_b32_e32 v10, v25
	v_mov_b32_e32 v11, v25
	v_mov_b32_e32 v12, v25
	v_mov_b32_e32 v13, v25
	v_mov_b32_e32 v14, v25
	v_mov_b32_e32 v17, v25
	v_mov_b32_e32 v18, v25
	v_mov_b32_e32 v24, v25
	v_mov_b32_e32 v26, v25
	v_mov_b32_e32 v27, v25
	v_mov_b32_e32 v28, v25
	v_mov_b32_e32 v29, v25
	v_mov_b32_e32 v30, v25
	v_mov_b32_e32 v31, v25
	v_mov_b32_e32 v32, v25
	v_mov_b32_e32 v19, v25
	v_mov_b32_e32 v20, s6
	v_mov_b32_e32 v22, v15
	s_branch BB0_3
BB0_2:                                  ; %.preheader232..preheader232_crit_edge
                                        ;   in Loop: Header=BB0_3 Depth=1
	v_add_i32_e32 v20, vcc, 64, v20
	v_addc_u32_e32 v21, vcc, 0, v21, vcc
	v_add_i32_e32 v22, vcc, 0x31000, v22
	v_addc_u32_e32 v23, vcc, 0, v23, vcc
BB0_3:                                  ; %.preheader232
                                        ; =>This Inner Loop Header: Depth=1
	v_readfirstlane_b32 s16, v20
	v_readfirstlane_b32 s17, v21
	s_load_dwordx4 s[48:51], s[16:17], 0x400
	s_load_dwordx4 s[52:55], s[16:17], 0x500
	s_load_dwordx4 s[32:35], s[16:17], 0x0
	s_waitcnt vmcnt(4) lgkmcnt(0)
        s_load_dwordx4 s[56:59], s[16:17], 0x600
        s_load_dwordx4 s[60:63], s[16:17], 0x700
        s_load_dwordx4 s[36:39], s[16:17], 0x100
        s_load_dwordx4 s[40:43], s[16:17], 0x200
        s_load_dwordx4 s[44:47], s[16:17], 0x300
	s_load_dwordx4 s[24:27], s[16:17], 0xf00
        s_load_dwordx4 s[84:87], s[16:17], 0xd00
        s_load_dwordx4 s[92:95], s[16:17], 0xe00
	v_fma_f32 v28, v8, s48, v28
	v_fma_f32 v28, v6, s49, v28
	v_fma_f32 v28, v7, s50, v28
	v_fma_f32 v28, v9, s51, v28
	v_add_i32_e32 v33, vcc, s5, v22
	v_addc_u32_e32 v34, vcc, 0, v23, vcc
	flat_load_dword v35, v[33:34]
	v_fma_f32 v27, v8, s52, v27
	v_fma_f32 v27, v6, s53, v27
	v_fma_f32 v27, v7, s54, v27
	v_fma_f32 v27, v9, s55, v27
	v_add_i32_e32 v33, vcc, s8, v22
	v_fma_f32 v32, v8, s32, v32
	s_waitcnt lgkmcnt(0)
	s_load_dwordx4 s[48:51], s[16:17], 0x800
	s_load_dwordx4 s[52:55], s[16:17], 0x900
	v_fma_f32 v26, v8, s56, v26
	v_fma_f32 v26, v6, s57, v26
	v_fma_f32 v26, v7, s58, v26
	v_fma_f32 v26, v9, s59, v26
	v_fma_f32 v31, v8, s36, v31
	v_fma_f32 v30, v8, s40, v30
	v_fma_f32 v29, v8, s44, v29
	v_addc_u32_e32 v34, vcc, 0, v23, vcc
	v_fma_f32 v24, v8, s60, v24
	v_fma_f32 v24, v6, s61, v24
	v_fma_f32 v24, v7, s62, v24
	v_fma_f32 v24, v9, s63, v24
	v_fma_f32 v32, v6, s33, v32
	v_fma_f32 v31, v6, s37, v31
	v_fma_f32 v30, v6, s41, v30
	v_fma_f32 v29, v6, s45, v29
	s_waitcnt lgkmcnt(0)
	s_load_dwordx4 s[56:59], s[16:17], 0xa00
	s_load_dwordx4 s[60:63], s[16:17], 0xb00
	v_fma_f32 v18, v8, s48, v18
	v_fma_f32 v18, v6, s49, v18
	v_fma_f32 v18, v7, s50, v18
	v_fma_f32 v18, v9, s51, v18
	flat_load_dword v36, v[33:34]
	v_add_i32_e32 v33, vcc, s9, v22
	v_fma_f32 v32, v7, s34, v32
	v_fma_f32 v31, v7, s38, v31
	v_fma_f32 v17, v8, s52, v17
	v_fma_f32 v17, v6, s53, v17
	v_fma_f32 v17, v7, s54, v17
	v_fma_f32 v17, v9, s55, v17
	v_fma_f32 v30, v7, s42, v30
	v_fma_f32 v29, v7, s46, v29
	v_addc_u32_e32 v34, vcc, 0, v23, vcc
	flat_load_dword v37, v[33:34]
	s_waitcnt lgkmcnt(0)
	s_load_dwordx4 s[48:51], s[16:17], 0xc00
	s_load_dwordx4 s[52:55], s[16:17], 0x510
        s_load_dwordx4 s[64:67], s[16:17], 0x810
        s_load_dwordx4 s[68:71], s[16:17], 0x910
        s_load_dwordx4 s[72:75], s[16:17], 0xa10
        s_load_dwordx4 s[76:79], s[16:17], 0xb10
        s_load_dwordx4 s[20:23], s[16:17], 0xf10
        s_load_dwordx4 s[28:31], s[16:17], 0x10
        s_load_dwordx4 s[80:83], s[16:17], 0xc10
        s_load_dwordx4 s[88:91], s[16:17], 0xd10
        s_load_dwordx4 s[0:3], s[16:17], 0xe10
	v_fma_f32 v14, v8, s56, v14
	v_fma_f32 v14, v6, s57, v14
	v_fma_f32 v14, v7, s58, v14
	v_fma_f32 v14, v9, s59, v14
	v_add_i32_e32 v33, vcc, s10, v22
	v_fma_f32 v32, v9, s35, v32
	v_fma_f32 v31, v9, s39, v31
	v_fma_f32 v30, v9, s43, v30
	v_fma_f32 v13, v8, s60, v13
	v_fma_f32 v13, v6, s61, v13
	v_fma_f32 v13, v7, s62, v13
	v_fma_f32 v13, v9, s63, v13
	v_fma_f32 v29, v9, s47, v29
        s_load_dwordx4 s[40:43], s[16:17], 0x310
	s_load_dwordx4 s[44:47], s[16:17], 0x410
	s_load_dwordx4 s[56:59], s[16:17], 0x610
	s_load_dwordx4 s[60:63], s[16:17], 0x710
	s_load_dwordx4 s[32:35], s[16:17], 0x110
	s_load_dwordx4 s[36:39], s[16:17], 0x210
	v_fma_f32 v11, v8, s84, v11
	v_addc_u32_e32 v34, vcc, 0, v23, vcc
	v_fma_f32 v25, v8, s24, v25
	s_waitcnt lgkmcnt(0)
	v_fma_f32 v12, v8, s48, v12
	v_fma_f32 v8, v8, s92, v10
	v_fma_f32 v10, v6, s49, v12
	v_fma_f32 v11, v6, s85, v11
	v_fma_f32 v8, v6, s93, v8
	v_fma_f32 v6, v6, s25, v25
	flat_load_dword v38, v[33:34]
	v_add_i32_e32 v33, vcc, s11, v22
	v_fma_f32 v10, v7, s50, v10
	v_fma_f32 v8, v7, s94, v8
	v_fma_f32 v6, v7, s26, v6
	v_fma_f32 v11, v7, s86, v11
	v_addc_u32_e32 v34, vcc, 0, v23, vcc
	v_fma_f32 v7, v9, s87, v11
	v_fma_f32 v10, v9, s51, v10
	v_fma_f32 v8, v9, s95, v8
	v_fma_f32 v6, v9, s27, v6
	flat_load_dword v39, v[33:34]
	v_add_i32_e32 v33, vcc, s12, v22
 	s_waitcnt vmcnt(8)
	v_fma_f32 v27, v5, s52, v27
	v_fma_f32 v26, v5, s56, v26
	v_fma_f32 v24, v5, s60, v24
	v_fma_f32 v18, v5, s64, v18
	v_fma_f32 v17, v5, s68, v17
	v_fma_f32 v14, v5, s72, v14
	v_fma_f32 v13, v5, s76, v13
	v_fma_f32 v10, v5, s80, v10
	v_fma_f32 v7, v5, s88, v7
	v_fma_f32 v8, v5, s0, v8
	v_addc_u32_e32 v34, vcc, 0, v23, vcc
	v_fma_f32 v9, v5, s28, v32
	v_fma_f32 v11, v5, s32, v31
	v_fma_f32 v12, v5, s36, v30
	v_fma_f32 v25, v5, s40, v29
	v_fma_f32 v28, v5, s44, v28
	v_fma_f32 v5, v5, s20, v6
 	s_waitcnt vmcnt(7)
	v_fma_f32 v26, v4, s57, v26
	v_fma_f32 v24, v4, s61, v24
	v_fma_f32 v18, v4, s65, v18
	v_fma_f32 v17, v4, s69, v17
	v_fma_f32 v14, v4, s73, v14
	v_fma_f32 v13, v4, s77, v13
	v_fma_f32 v10, v4, s81, v10
	v_fma_f32 v7, v4, s89, v7
	v_fma_f32 v8, v4, s1, v8
	flat_load_dword v40, v[33:34]
	v_add_i32_e32 v33, vcc, s13, v22
	v_fma_f32 v6, v4, s29, v9
	v_fma_f32 v9, v4, s33, v11
	v_fma_f32 v11, v4, s37, v12
	v_fma_f32 v12, v4, s41, v25
	v_fma_f32 v25, v4, s45, v28
	v_fma_f32 v27, v4, s53, v27
	v_fma_f32 v4, v4, s21, v5
 	s_waitcnt vmcnt(7)
	v_fma_f32 v26, v3, s58, v26
	v_fma_f32 v24, v3, s62, v24
	v_fma_f32 v18, v3, s66, v18
	v_fma_f32 v17, v3, s70, v17
	v_fma_f32 v14, v3, s74, v14
	v_fma_f32 v13, v3, s78, v13
	v_addc_u32_e32 v34, vcc, 0, v23, vcc
	v_fma_f32 v5, v3, s30, v6
	v_fma_f32 v6, v3, s34, v9
	v_fma_f32 v9, v3, s38, v11
	v_fma_f32 v11, v3, s42, v12
	v_fma_f32 v12, v3, s46, v25
	v_fma_f32 v25, v3, s54, v27
	v_fma_f32 v10, v3, s82, v10
	v_fma_f32 v7, v3, s90, v7
	v_fma_f32 v8, v3, s2, v8
	v_fma_f32 v3, v3, s22, v4
	flat_load_dword v33, v[33:34]
	s_waitcnt vmcnt(7)
	v_fma_f32 v27, v2, s31, v5
	v_fma_f32 v28, v2, s35, v6
	v_fma_f32 v29, v2, s39, v9
	v_fma_f32 v30, v2, s43, v11
	v_fma_f32 v12, v2, s47, v12
	v_fma_f32 v25, v2, s55, v25
	v_fma_f32 v26, v2, s59, v26
	v_fma_f32 v24, v2, s63, v24
	v_fma_f32 v18, v2, s67, v18
	v_fma_f32 v17, v2, s71, v17
	v_fma_f32 v14, v2, s75, v14
	v_fma_f32 v13, v2, s79, v13
	v_fma_f32 v31, v2, s83, v10
	v_fma_f32 v32, v2, s91, v7
	v_fma_f32 v34, v2, s3, v8
	v_fma_f32 v41, v2, s23, v3
        s_load_dwordx4 s[0:3], s[16:17], 0x20
        s_load_dwordx4 s[60:63], s[16:17], 0x120
        s_load_dwordx4 s[76:79], s[16:17], 0xc20
        s_load_dwordx4 s[80:83], s[16:17], 0xd20
        s_load_dwordx4 s[84:87], s[16:17], 0xe20
        s_load_dwordx4 s[20:23], s[16:17], 0x30
        s_load_dwordx4 s[24:27], s[16:17], 0x130
        s_load_dwordx4 s[28:31], s[16:17], 0x230
        s_load_dwordx4 s[32:35], s[16:17], 0x330
        s_load_dwordx4 s[36:39], s[16:17], 0x430
        s_load_dwordx4 s[40:43], s[16:17], 0x530
        s_load_dwordx4 s[44:47], s[16:17], 0x630
        s_load_dwordx4 s[48:51], s[16:17], 0x730
        s_load_dwordx4 s[52:55], s[16:17], 0x830
        s_load_dwordx4 s[56:59], s[16:17], 0x930
        s_load_dwordx4 s[68:71], s[16:17], 0xc30
        s_load_dwordx4 s[72:75], s[16:17], 0xd30
        s_load_dwordx4 s[88:91], s[16:17], 0xe30
	v_add_i32_e32 v2, vcc, s14, v22
	v_addc_u32_e32 v3, vcc, 0, v23, vcc
	flat_load_dword v8, v[2:3]
	v_add_i32_e32 v2, vcc, 0x1b900, v22
	v_addc_u32_e32 v3, vcc, 0, v23, vcc
	flat_load_dword v6, v[2:3]
	v_add_i32_e32 v2, vcc, 0x1ea00, v22
	v_addc_u32_e32 v3, vcc, 0, v23, vcc
	flat_load_dword v7, v[2:3]
	v_add_i32_e32 v2, vcc, 0x21b00, v22
	v_addc_u32_e32 v3, vcc, 0, v23, vcc
	flat_load_dword v9, v[2:3]
	v_add_i32_e32 v2, vcc, 0x24c00, v22
	v_addc_u32_e32 v3, vcc, 0, v23, vcc
	flat_load_dword v5, v[2:3]
	v_add_i32_e32 v2, vcc, 0x27d00, v22
	v_addc_u32_e32 v3, vcc, 0, v23, vcc
	flat_load_dword v4, v[2:3]
	v_add_i32_e32 v2, vcc, 0x2ae00, v22
	v_addc_u32_e32 v3, vcc, 0, v23, vcc
	v_add_i32_e32 v10, vcc, 0x2df00, v22
	v_addc_u32_e32 v11, vcc, 0, v23, vcc
	flat_load_dword v3, v[2:3]
	flat_load_dword v2, v[10:11]
	flat_load_dword v10, v[22:23]
	v_add_i32_e32 v19, vcc, 2, v19
	v_cmp_lt_u32_e32 vcc, 5, v19
	s_and_b64 vcc, exec, vcc
	s_waitcnt vmcnt(0) lgkmcnt(0)
	s_load_dwordx4 s[64:67], s[16:17], 0x220
	s_load_dwordx4 s[92:95], s[16:17], 0x320
	v_fma_f32 v11, v10, s0, v27
	v_fma_f32 v11, v35, s1, v11
	v_fma_f32 v11, v36, s2, v11
	v_fma_f32 v11, v37, s3, v11
	v_fma_f32 v11, v38, s20, v11
	v_fma_f32 v11, v39, s21, v11
	v_fma_f32 v11, v40, s22, v11
	v_fma_f32 v27, v10, s60, v28
	v_fma_f32 v27, v35, s61, v27
	v_fma_f32 v27, v36, s62, v27
	v_fma_f32 v27, v37, s63, v27
	v_fma_f32 v27, v38, s24, v27
	v_fma_f32 v27, v39, s25, v27
	v_fma_f32 v27, v40, s26, v27
	s_waitcnt lgkmcnt(0)
	s_load_dwordx4 s[0:3], s[16:17], 0x420
	s_load_dwordx4 s[60:63], s[16:17], 0x520
	v_fma_f32 v28, v10, s64, v29
	v_fma_f32 v28, v35, s65, v28
	v_fma_f32 v28, v36, s66, v28
	v_fma_f32 v28, v37, s67, v28
	v_fma_f32 v28, v38, s28, v28
	v_fma_f32 v28, v39, s29, v28
	v_fma_f32 v28, v40, s30, v28
	v_fma_f32 v29, v10, s92, v30
	v_fma_f32 v29, v35, s93, v29
	v_fma_f32 v29, v36, s94, v29
	v_fma_f32 v29, v37, s95, v29
	v_fma_f32 v30, v10, s76, v31
	v_fma_f32 v31, v10, s80, v32
	v_fma_f32 v32, v10, s84, v34
	v_fma_f32 v30, v35, s77, v30
	s_waitcnt lgkmcnt(0)
	s_load_dwordx4 s[64:67], s[16:17], 0x620
	s_load_dwordx4 s[92:95], s[16:17], 0x720
	v_fma_f32 v12, v10, s0, v12
	v_fma_f32 v12, v35, s1, v12
	v_fma_f32 v12, v36, s2, v12
	v_fma_f32 v12, v37, s3, v12
	v_fma_f32 v31, v35, s81, v31
	v_fma_f32 v32, v35, s85, v32
	v_fma_f32 v30, v36, s78, v30
	v_fma_f32 v31, v36, s82, v31
	v_fma_f32 v25, v10, s60, v25
	v_fma_f32 v25, v35, s61, v25
	v_fma_f32 v25, v36, s62, v25
	v_fma_f32 v25, v37, s63, v25
	v_fma_f32 v32, v36, s86, v32
	v_fma_f32 v30, v37, s79, v30
	v_fma_f32 v31, v37, s83, v31
	v_fma_f32 v32, v37, s87, v32
	s_waitcnt lgkmcnt(0)
	s_load_dwordx4 s[0:3], s[16:17], 0x820
	s_load_dwordx4 s[60:63], s[16:17], 0x920
	v_fma_f32 v26, v10, s64, v26
	v_fma_f32 v26, v35, s65, v26
	v_fma_f32 v26, v36, s66, v26
	v_fma_f32 v26, v37, s67, v26
	v_fma_f32 v29, v38, s32, v29
	v_fma_f32 v12, v38, s36, v12
	v_fma_f32 v25, v38, s40, v25
	v_fma_f32 v26, v38, s44, v26
	v_fma_f32 v24, v10, s92, v24
	v_fma_f32 v24, v35, s93, v24
	v_fma_f32 v24, v36, s94, v24
	v_fma_f32 v24, v37, s95, v24
	v_fma_f32 v24, v38, s48, v24
	v_fma_f32 v30, v38, s68, v30
	v_fma_f32 v31, v38, s72, v31
	v_fma_f32 v32, v38, s88, v32
	s_waitcnt lgkmcnt(0)
        s_load_dwordx4 s[64:67], s[16:17], 0xb30
        s_load_dwordx4 s[92:95], s[16:17], 0xf20
	v_fma_f32 v18, v10, s0, v18
	v_fma_f32 v18, v35, s1, v18
	v_fma_f32 v18, v36, s2, v18
	v_fma_f32 v18, v37, s3, v18
	s_load_dwordx4 s[0:3], s[16:17], 0xa20
	v_fma_f32 v18, v38, s52, v18
	v_fma_f32 v29, v39, s33, v29
	v_fma_f32 v12, v39, s37, v12
	v_fma_f32 v25, v39, s41, v25
	v_fma_f32 v17, v10, s60, v17
	v_fma_f32 v17, v35, s61, v17
	v_fma_f32 v17, v36, s62, v17
	v_fma_f32 v17, v37, s63, v17
        s_load_dwordx4 s[60:63], s[16:17], 0xa30
	v_fma_f32 v17, v38, s56, v17
	v_fma_f32 v26, v39, s45, v26
	v_fma_f32 v24, v39, s49, v24
	v_fma_f32 v18, v39, s53, v18
	s_waitcnt lgkmcnt(0)
	v_fma_f32 v14, v10, s0, v14
	v_fma_f32 v14, v35, s1, v14
	v_fma_f32 v14, v36, s2, v14
	v_fma_f32 v14, v37, s3, v14
	s_load_dwordx4 s[0:3], s[16:17], 0xb20
	s_load_dwordx4 s[16:19], s[16:17], 0xf30
	v_fma_f32 v14, v38, s60, v14
	v_fma_f32 v17, v39, s57, v17
	v_fma_f32 v14, v39, s61, v14
	s_waitcnt lgkmcnt(0)
	v_fma_f32 v13, v10, s0, v13
	v_fma_f32 v10, v10, s92, v41
	v_fma_f32 v13, v35, s1, v13
	v_fma_f32 v10, v35, s93, v10
	v_fma_f32 v13, v36, s2, v13
	v_fma_f32 v10, v36, s94, v10
	v_fma_f32 v13, v37, s3, v13
	v_fma_f32 v10, v37, s95, v10
	v_fma_f32 v13, v38, s64, v13
	v_fma_f32 v10, v38, s16, v10
	v_fma_f32 v13, v39, s65, v13
	v_fma_f32 v30, v39, s69, v30
	v_fma_f32 v31, v39, s73, v31
	v_fma_f32 v32, v39, s89, v32
	v_fma_f32 v10, v39, s17, v10
	v_fma_f32 v29, v40, s34, v29
	v_fma_f32 v26, v40, s46, v26
	v_fma_f32 v24, v40, s50, v24
	v_fma_f32 v18, v40, s54, v18
	v_fma_f32 v17, v40, s58, v17
	v_fma_f32 v14, v40, s62, v14
	v_fma_f32 v13, v40, s66, v13
	v_fma_f32 v34, v40, s70, v30
	v_fma_f32 v35, v40, s74, v31
	v_fma_f32 v36, v40, s90, v32
	v_fma_f32 v37, v40, s18, v10
	v_fma_f32 v12, v40, s38, v12
	v_fma_f32 v25, v40, s42, v25
	v_fma_f32 v32, v33, s23, v11
	v_fma_f32 v31, v33, s27, v27
	v_fma_f32 v30, v33, s31, v28
	v_fma_f32 v28, v33, s39, v12
	v_fma_f32 v27, v33, s43, v25
	v_fma_f32 v29, v33, s35, v29
	v_fma_f32 v26, v33, s47, v26
	v_fma_f32 v24, v33, s51, v24
	v_fma_f32 v18, v33, s55, v18
	v_fma_f32 v17, v33, s59, v17
	v_fma_f32 v14, v33, s63, v14
	v_fma_f32 v13, v33, s67, v13
	v_fma_f32 v12, v33, s71, v34
	v_fma_f32 v11, v33, s75, v35
	v_fma_f32 v10, v33, s91, v36
	v_fma_f32 v25, v33, s19, v37
	s_cbranch_vccz BB0_2
; BB#4:                                 ; %.preheader227
	s_load_dwordx8 s[8:15], s[6:7], 0xc0
        s_load_dwordx8 s[16:23], s[6:7], 0x1c0
	v_add_i32_e32 v19, vcc, 0x93000, v15
	v_addc_u32_e32 v20, vcc, 0, v16, vcc
	flat_load_dword v21, v[19:20]
	v_add_i32_e32 v19, vcc, 0x96100, v15
	v_addc_u32_e32 v20, vcc, 0, v16, vcc
	flat_load_dword v22, v[19:20]
	v_add_i32_e32 v19, vcc, 0x99200, v15
	v_addc_u32_e32 v20, vcc, 0, v16, vcc
	flat_load_dword v23, v[19:20]
	v_add_i32_e32 v19, vcc, 0x9c300, v15
	v_addc_u32_e32 v20, vcc, 0, v16, vcc
	flat_load_dword v33, v[19:20]
	v_add_i32_e32 v19, vcc, 0x9f400, v15
	v_addc_u32_e32 v20, vcc, 0, v16, vcc
	flat_load_dword v34, v[19:20]
	v_add_i32_e32 v19, vcc, 0xa2500, v15
	v_addc_u32_e32 v20, vcc, 0, v16, vcc
	flat_load_dword v35, v[19:20]
	v_add_i32_e32 v19, vcc, 0xa5600, v15
	v_addc_u32_e32 v20, vcc, 0, v16, vcc
	flat_load_dword v19, v[19:20]
	v_add_i32_e32 v15, vcc, 0xa8700, v15
	v_addc_u32_e32 v16, vcc, 0, v16, vcc
	flat_load_dword v20, v[15:16]
	s_waitcnt lgkmcnt(0)
	s_load_dwordx8 s[24:31], s[6:7], 0x2c0
	s_load_dwordx8 s[32:39], s[6:7], 0x3c0
	v_fma_f32 v15, v8, s8, v32
	v_fma_f32 v15, v6, s9, v15
	v_fma_f32 v15, v7, s10, v15
	v_fma_f32 v15, v9, s11, v15
        v_fma_f32 v15, v5, s12, v15
        v_fma_f32 v15, v4, s13, v15
        v_fma_f32 v15, v3, s14, v15
        v_fma_f32 v15, v2, s15, v15
        v_fma_f32 v16, v8, s16, v31
        v_fma_f32 v16, v6, s17, v16
        v_fma_f32 v16, v7, s18, v16
	v_fma_f32 v16, v9, s19, v16
        v_fma_f32 v16, v5, s20, v16
        v_fma_f32 v16, v4, s21, v16
        v_fma_f32 v16, v3, s22, v16
        v_fma_f32 v16, v2, s23, v16
	s_waitcnt lgkmcnt(0)
	s_load_dwordx8 s[8:15], s[6:7], 0x4c0
        s_load_dwordx8 s[16:23], s[6:7], 0x5c0
        v_fma_f32 v30, v8, s24, v30
	v_fma_f32 v30, v6, s25, v30
	v_fma_f32 v30, v7, s26, v30
	v_fma_f32 v30, v9, s27, v30
	v_fma_f32 v30, v5, s28, v30
	v_fma_f32 v30, v4, s29, v30
	v_fma_f32 v30, v3, s30, v30
	v_fma_f32 v30, v2, s31, v30
	v_fma_f32 v29, v8, s32, v29
	v_fma_f32 v29, v6, s33, v29
	v_fma_f32 v29, v7, s34, v29
	v_fma_f32 v29, v9, s35, v29
	v_fma_f32 v29, v5, s36, v29
	v_fma_f32 v29, v4, s37, v29
	v_fma_f32 v29, v3, s38, v29
	v_fma_f32 v29, v2, s39, v29
	s_waitcnt lgkmcnt(0)
	s_load_dwordx8 s[24:31], s[6:7], 0x6c0
	s_load_dwordx8 s[32:39], s[6:7], 0x7c0
	v_fma_f32 v28, v8, s8, v28
	v_fma_f32 v28, v6, s9, v28
	v_fma_f32 v28, v7, s10, v28
	v_fma_f32 v28, v9, s11, v28
	v_fma_f32 v28, v5, s12, v28
	v_fma_f32 v28, v4, s13, v28
	v_fma_f32 v28, v3, s14, v28
	v_fma_f32 v28, v2, s15, v28
	v_fma_f32 v27, v8, s16, v27
	v_fma_f32 v27, v6, s17, v27
	v_fma_f32 v27, v7, s18, v27
	v_fma_f32 v27, v9, s19, v27
	v_fma_f32 v27, v5, s20, v27
	v_fma_f32 v27, v4, s21, v27
	v_fma_f32 v27, v3, s22, v27
	s_waitcnt lgkmcnt(0)
	s_load_dwordx8 s[8:15], s[6:7], 0x8c0
	s_load_dwordx8 s[16:23], s[6:7], 0x9c0
	v_fma_f32 v26, v8, s24, v26
	v_fma_f32 v26, v6, s25, v26
	v_fma_f32 v26, v7, s26, v26
	v_fma_f32 v26, v9, s27, v26
	v_fma_f32 v26, v5, s28, v26
	v_fma_f32 v26, v4, s29, v26
	v_fma_f32 v24, v8, s32, v24
	v_fma_f32 v24, v6, s33, v24
	v_fma_f32 v24, v7, s34, v24
	v_fma_f32 v24, v9, s35, v24
	v_fma_f32 v24, v5, s36, v24
	v_fma_f32 v24, v4, s37, v24
	v_fma_f32 v24, v3, s38, v24
	v_fma_f32 v24, v2, s39, v24
	s_waitcnt lgkmcnt(0)
	s_load_dwordx8 s[24:31], s[6:7], 0xac0
	s_load_dwordx8 s[32:39], s[6:7], 0xbc0
	v_fma_f32 v18, v8, s8, v18
	v_fma_f32 v18, v6, s9, v18
	v_fma_f32 v18, v7, s10, v18
	v_fma_f32 v18, v9, s11, v18
	v_fma_f32 v18, v5, s12, v18
	v_fma_f32 v18, v4, s13, v18
	v_fma_f32 v18, v3, s14, v18
	v_fma_f32 v18, v2, s15, v18
	v_fma_f32 v17, v8, s16, v17
	v_fma_f32 v17, v6, s17, v17
	v_fma_f32 v17, v7, s18, v17
	v_fma_f32 v17, v9, s19, v17
	v_fma_f32 v17, v5, s20, v17
	v_fma_f32 v17, v4, s21, v17
	v_fma_f32 v17, v3, s22, v17
	v_fma_f32 v17, v2, s23, v17
	s_waitcnt lgkmcnt(0)
	s_load_dwordx8 s[8:15], s[6:7], 0xcc0
	s_load_dwordx8 s[16:23], s[6:7], 0xdc0
	v_fma_f32 v14, v8, s24, v14
	v_fma_f32 v14, v6, s25, v14
	v_fma_f32 v14, v7, s26, v14
	v_fma_f32 v14, v9, s27, v14
	v_fma_f32 v14, v5, s28, v14
	v_fma_f32 v14, v4, s29, v14
	v_fma_f32 v14, v3, s30, v14
	v_fma_f32 v13, v8, s32, v13
	v_fma_f32 v13, v6, s33, v13
	v_fma_f32 v13, v7, s34, v13
	v_fma_f32 v13, v9, s35, v13
	v_fma_f32 v13, v5, s36, v13
	v_fma_f32 v13, v4, s37, v13
	s_waitcnt lgkmcnt(0)
	s_load_dwordx8 s[24:31], s[6:7], 0xec0
	s_load_dwordx8 s[32:39], s[6:7], 0xfc0
	v_fma_f32 v12, v8, s8, v12
	v_fma_f32 v12, v6, s9, v12
	v_fma_f32 v12, v7, s10, v12
	v_fma_f32 v12, v9, s11, v12
	v_fma_f32 v12, v5, s12, v12
	v_fma_f32 v12, v4, s13, v12
	v_fma_f32 v12, v3, s14, v12
	v_fma_f32 v31, v2, s15, v12
	v_fma_f32 v11, v8, s16, v11
	v_fma_f32 v11, v6, s17, v11
	v_fma_f32 v11, v7, s18, v11
	v_fma_f32 v11, v9, s19, v11
	v_fma_f32 v11, v5, s20, v11
	v_fma_f32 v11, v4, s21, v11
	v_fma_f32 v11, v3, s22, v11
	v_fma_f32 v32, v2, s23, v11
	s_waitcnt lgkmcnt(0)
        s_load_dword s13, s[6:7], 0x6d8
        s_load_dword s57, s[6:7], 0xbd8
        s_load_dword s12, s[6:7], 0x5dc
        s_load_dword s54, s[6:7], 0xadc
        s_load_dwordx4 s[8:11], s[6:7], 0xe0
        s_load_dword s14, s[6:7], 0x6dc
        s_load_dword s58, s[6:7], 0xbdc
	v_fma_f32 v10, v8, s24, v10
	v_fma_f32 v10, v6, s25, v10
	v_fma_f32 v10, v7, s26, v10
	v_fma_f32 v10, v9, s27, v10
	v_fma_f32 v10, v5, s28, v10
	v_fma_f32 v10, v4, s29, v10
	v_fma_f32 v10, v3, s30, v10
	v_fma_f32 v36, v2, s31, v10
        v_fma_f32 v8, v8, s32, v25
        v_fma_f32 v6, v6, s33, v8
        v_fma_f32 v6, v7, s34, v6
        v_fma_f32 v6, v9, s35, v6
        v_fma_f32 v5, v5, s36, v6
        s_waitcnt lgkmcnt(0)
        s_load_dword s5, s[6:7], 0xf0
        s_load_dwordx8 s[16:23], s[6:7], 0x1e0
        s_load_dwordx8 s[24:31], s[6:7], 0x4e0
        v_fma_f32 v26, v3, s13, v26
        v_fma_f32 v13, v3, s57, v13
        v_fma_f32 v27, v2, s12, v27
        v_fma_f32 v26, v2, s14, v26
        v_fma_f32 v14, v2, s54, v14
        v_fma_f32 v13, v2, s58, v13
        v_fma_f32 v4, v4, s37, v5
        v_fma_f32 v3, v3, s38, v4
        v_fma_f32 v25, v2, s39, v3
        s_waitcnt vmcnt(3)
        v_fma_f32 v2, v21, s8, v15
        v_fma_f32 v2, v22, s9, v2
        v_fma_f32 v2, v23, s10, v2
        v_fma_f32 v2, v33, s11, v2
	s_waitcnt lgkmcnt(0)
	s_load_dwordx8 s[8:15], s[6:7], 0x5e0
	s_load_dwordx8 s[32:39], s[6:7], 0x6e0
	v_fma_f32 v2, v34, s5, v2
        v_fma_f32 v3, v21, s16, v16
        v_fma_f32 v3, v22, s17, v3
        v_fma_f32 v3, v23, s18, v3
        v_fma_f32 v3, v33, s19, v3
        v_fma_f32 v3, v34, s20, v3
        s_waitcnt vmcnt(0)
        v_fma_f32 v3, v35, s21, v3
        v_fma_f32 v3, v19, s22, v3
        v_fma_f32 v3, v20, s23, v3
	v_fma_f32 v6, v21, s24, v28
	v_fma_f32 v6, v22, s25, v6
	v_fma_f32 v6, v23, s26, v6
	v_fma_f32 v6, v33, s27, v6
	v_fma_f32 v6, v34, s28, v6
        v_fma_f32 v6, v35, s29, v6
        v_fma_f32 v6, v19, s30, v6
        v_fma_f32 v6, v20, s31, v6
	s_waitcnt lgkmcnt(0)
	s_load_dwordx8 s[16:23], s[6:7], 0x7e0
	s_load_dwordx8 s[24:31], s[6:7], 0x9e0
	v_fma_f32 v7, v21, s8, v27
	v_fma_f32 v7, v22, s9, v7
        v_fma_f32 v7, v23, s10, v7
        v_fma_f32 v7, v33, s11, v7
        v_fma_f32 v7, v34, s12, v7
        v_fma_f32 v7, v35, s13, v7
        v_fma_f32 v7, v19, s14, v7
	v_fma_f32 v8, v21, s32, v26
	v_fma_f32 v8, v22, s33, v8
	v_fma_f32 v8, v23, s34, v8
        v_fma_f32 v8, v33, s35, v8
        v_fma_f32 v8, v34, s36, v8
        v_fma_f32 v8, v35, s37, v8
	s_waitcnt lgkmcnt(0)
	s_load_dwordx8 s[8:15], s[6:7], 0xae0
	s_load_dwordx8 s[32:39], s[6:7], 0xbe0
	v_fma_f32 v9, v21, s16, v24
        v_fma_f32 v9, v22, s17, v9
        v_fma_f32 v9, v23, s18, v9
        v_fma_f32 v9, v33, s19, v9
        v_fma_f32 v9, v34, s20, v9
        v_fma_f32 v11, v21, s24, v17
        v_fma_f32 v11, v22, s25, v11
        v_fma_f32 v11, v23, s26, v11
        v_fma_f32 v11, v33, s27, v11
        v_fma_f32 v11, v34, s28, v11
        v_fma_f32 v11, v35, s29, v11
        v_fma_f32 v11, v19, s30, v11
        v_fma_f32 v11, v20, s31, v11
	s_waitcnt lgkmcnt(0)
	s_load_dwordx8 s[16:23], s[6:7], 0xce0
	s_load_dwordx8 s[24:31], s[6:7], 0xee0
	v_fma_f32 v12, v21, s8, v14
	v_fma_f32 v12, v22, s9, v12
        v_fma_f32 v12, v23, s10, v12
        v_fma_f32 v12, v33, s11, v12
        v_fma_f32 v12, v34, s12, v12
        v_fma_f32 v12, v35, s13, v12
        v_fma_f32 v12, v19, s14, v12
	v_fma_f32 v13, v21, s32, v13
	v_fma_f32 v13, v22, s33, v13
        v_fma_f32 v13, v23, s34, v13
        v_fma_f32 v13, v33, s35, v13
        v_fma_f32 v13, v34, s36, v13
        v_fma_f32 v13, v35, s37, v13
        s_waitcnt lgkmcnt(0)
	s_load_dwordx8 s[8:15], s[6:7], 0xfe0
        v_fma_f32 v14, v21, s16, v31
        v_fma_f32 v14, v22, s17, v14
        v_fma_f32 v14, v23, s18, v14
        v_fma_f32 v14, v33, s19, v14
        v_fma_f32 v14, v34, s20, v14
        v_fma_f32 v16, v21, s24, v36
        v_fma_f32 v16, v22, s25, v16
        v_fma_f32 v16, v23, s26, v16
        v_fma_f32 v16, v33, s27, v16
        v_fma_f32 v16, v34, s28, v16
	v_fma_f32 v16, v35, s29, v16
	s_waitcnt lgkmcnt(0)
        s_load_dwordx8 s[16:23], s[6:7], 0x2e0
        s_load_dwordx8 s[24:31], s[6:7], 0x3e0
        s_load_dwordx2 s[40:41], s[6:7], 0x7f4
        s_load_dwordx4 s[44:47], s[6:7], 0x8e0
        s_load_dwordx4 s[48:51], s[6:7], 0x8f0
        s_load_dwordx2 s[60:61], s[6:7], 0xcf4
        s_load_dwordx8 s[64:71], s[6:7], 0xde0
	s_load_dwordx2 s[0:1], s[6:7], 0xef8
        v_fma_f32 v17, v21, s8, v25
        v_fma_f32 v17, v22, s9, v17
        v_fma_f32 v17, v23, s10, v17
        v_fma_f32 v17, v33, s11, v17
        v_fma_f32 v17, v34, s12, v17
        v_fma_f32 v17, v35, s13, v17
        v_fma_f32 v17, v19, s14, v17
	s_waitcnt lgkmcnt(0)
	s_load_dwordx4 s[12:15], s[6:7], 0xf4
        s_load_dword s37, s[6:7], 0x6f8
        s_load_dword s57, s[6:7], 0xbf8
	v_fma_f32 v10, v21, s44, v18
	v_mov_b32_e32 v18, 0xc40
	v_fma_f32 v16, v19, s0, v16
	v_fma_f32 v4, v21, s16, v30
	v_fma_f32 v5, v21, s24, v29
	v_fma_f32 v15, v21, s64, v32
	v_mad_i32_i24 v0, s4, v18, v0
	v_readlane_b32 s0, v42, 2
	v_fma_f32 v4, v22, s17, v4
	v_fma_f32 v5, v22, s25, v5
	v_fma_f32 v10, v22, s45, v10
	v_fma_f32 v15, v22, s65, v15
	v_fma_f32 v16, v20, s1, v16
	v_add_i32_e32 v0, vcc, v0, v1
	v_mov_b32_e32 v1, 0
	v_fma_f32 v4, v23, s18, v4
	v_fma_f32 v5, v23, s26, v5
	v_fma_f32 v10, v23, s46, v10
	v_fma_f32 v15, v23, s66, v15
	v_readlane_b32 s1, v42, 3
	v_lshlrev_b64 v[0:1], 2, v[0:1]
	v_fma_f32 v4, v33, s19, v4
	v_fma_f32 v5, v33, s27, v5
	v_fma_f32 v10, v33, s47, v10
	v_fma_f32 v15, v33, s67, v15
	v_add_i32_e32 v0, vcc, s0, v0
	v_mov_b32_e32 v18, s1
	v_addc_u32_e32 v1, vcc, v18, v1, vcc
	s_waitcnt lgkmcnt(0)
	v_fma_f32 v2, v35, s12, v2
	v_fma_f32 v4, v34, s20, v4
	v_fma_f32 v5, v34, s28, v5
	v_fma_f32 v10, v34, s48, v10
	v_fma_f32 v15, v34, s68, v15
	v_fma_f32 v2, v19, s13, v2
	v_fma_f32 v4, v35, s21, v4
	v_fma_f32 v5, v35, s29, v5
	v_fma_f32 v9, v35, s40, v9
	v_fma_f32 v10, v35, s49, v10
	v_fma_f32 v14, v35, s60, v14
	v_fma_f32 v15, v35, s69, v15
	v_add_i32_e32 v18, vcc, 0x3100, v0
	v_fma_f32 v4, v19, s22, v4
	v_fma_f32 v5, v19, s30, v5
	v_fma_f32 v2, v20, s14, v2
	v_fma_f32 v8, v19, s37, v8
	v_fma_f32 v9, v19, s41, v9
	v_fma_f32 v10, v19, s50, v10
	v_fma_f32 v13, v19, s57, v13
	v_fma_f32 v14, v19, s61, v14
	v_fma_f32 v15, v19, s70, v15
        s_load_dword s34, s[6:7], 0x5fc
        s_load_dword s38, s[6:7], 0x6fc
        s_load_dword s42, s[6:7], 0x7fc
        s_load_dword s50, s[6:7], 0x8fc
        s_load_dword s54, s[6:7], 0xafc
        s_load_dword s58, s[6:7], 0xbfc
        s_load_dword s62, s[6:7], 0xcfc
        s_load_dword s70, s[6:7], 0xdfc
        s_load_dword s6, s[6:7], 0xffc
	v_addc_u32_e32 v19, vcc, 0, v1, vcc
	flat_store_dword v[0:1], v2
	v_add_i32_e32 v2, vcc, 0x6200, v0
	flat_store_dword v[18:19], v3
	v_fma_f32 v4, v20, s23, v4
	v_addc_u32_e32 v3, vcc, 0, v1, vcc
	flat_store_dword v[2:3], v4
	v_add_i32_e32 v2, vcc, 0x9300, v0
	v_fma_f32 v5, v20, s31, v5
	v_addc_u32_e32 v3, vcc, 0, v1, vcc
	flat_store_dword v[2:3], v5
	v_add_i32_e32 v2, vcc, 0xc400, v0
	v_addc_u32_e32 v3, vcc, 0, v1, vcc
	flat_store_dword v[2:3], v6
	v_add_i32_e32 v2, vcc, 0xf500, v0
	s_waitcnt lgkmcnt(0)
	v_fma_f32 v7, v20, s34, v7
	v_addc_u32_e32 v3, vcc, 0, v1, vcc
	flat_store_dword v[2:3], v7
	v_add_i32_e32 v2, vcc, 0x12600, v0
	v_fma_f32 v8, v20, s38, v8
	v_addc_u32_e32 v3, vcc, 0, v1, vcc
	flat_store_dword v[2:3], v8
	v_add_i32_e32 v2, vcc, 0x15700, v0
	v_fma_f32 v9, v20, s42, v9
	v_addc_u32_e32 v3, vcc, 0, v1, vcc
	flat_store_dword v[2:3], v9
	v_add_i32_e32 v2, vcc, 0x18800, v0
	v_fma_f32 v10, v20, s50, v10
	v_addc_u32_e32 v3, vcc, 0, v1, vcc
	flat_store_dword v[2:3], v10
	v_add_i32_e32 v2, vcc, 0x1b900, v0
	v_addc_u32_e32 v3, vcc, 0, v1, vcc
	flat_store_dword v[2:3], v11
	v_add_i32_e32 v2, vcc, 0x1ea00, v0
	v_fma_f32 v12, v20, s54, v12
	v_addc_u32_e32 v3, vcc, 0, v1, vcc
	flat_store_dword v[2:3], v12
	v_add_i32_e32 v2, vcc, 0x21b00, v0
	v_fma_f32 v13, v20, s58, v13
	v_addc_u32_e32 v3, vcc, 0, v1, vcc
	flat_store_dword v[2:3], v13
	v_add_i32_e32 v2, vcc, 0x24c00, v0
	v_fma_f32 v14, v20, s62, v14
	v_addc_u32_e32 v3, vcc, 0, v1, vcc
	flat_store_dword v[2:3], v14
	v_add_i32_e32 v2, vcc, 0x27d00, v0
	v_fma_f32 v15, v20, s70, v15
	v_addc_u32_e32 v3, vcc, 0, v1, vcc
	flat_store_dword v[2:3], v15
	v_add_i32_e32 v2, vcc, 0x2ae00, v0
	v_addc_u32_e32 v3, vcc, 0, v1, vcc
	v_add_i32_e32 v0, vcc, 0x2df00, v0
	v_fma_f32 v17, v20, s6, v17
	v_addc_u32_e32 v1, vcc, 0, v1, vcc
	flat_store_dword v[2:3], v16
	flat_store_dword v[0:1], v17
	v_readlane_b32 s0, v42, 0
	v_readlane_b32 s1, v42, 1
BB0_5:                                  ; %Flow
	s_or_b64 exec, exec, s[0:1]
	s_endpgm
.Lfunc_end0:
	.size	MIOpenConv1x1, .Lfunc_end0-MIOpenConv1x1
                                        ; -- End function
	.section	.AMDGPU.csdata
; Kernel info:
; codeLenInByte = 8184
; NumSgprs: 98
; NumVgprs: 43
; ScratchSize: 0
; codeLenInByte = 8184
; NumSgprs: 98
; NumVgprs: 43
; FloatMode: 240
; IeeeMode: 1
; ScratchSize: 0
; LDSByteSize: 0 bytes/workgroup (compile time only)
; SGPRBlocks: 12
; VGPRBlocks: 10
; NumSGPRsForWavesPerEU: 98
; NumVGPRsForWavesPerEU: 43
; ReservedVGPRFirst: 0
; ReservedVGPRCount: 0
; COMPUTE_PGM_RSRC2:USER_SGPR: 6
; COMPUTE_PGM_RSRC2:TRAP_HANDLER: 1
; COMPUTE_PGM_RSRC2:TGID_X_EN: 1
; COMPUTE_PGM_RSRC2:TGID_Y_EN: 0
; COMPUTE_PGM_RSRC2:TGID_Z_EN: 0
; COMPUTE_PGM_RSRC2:TIDIG_COMP_CNT: 0

	.ident	"clang version 4.0 "
	.section	".note.GNU-stack"
	.amdgpu_code_object_metadata
---
Version:         [ 1, 0 ]
Kernels:         
  - Name:            MIOpenConv1x1
    Language:        OpenCL C
    LanguageVersion: [ 1, 2 ]
    Attrs:           
      ReqdWorkGroupSize: [ 64, 1, 1 ]
    Args:            
      - Size:            8
        Align:           8
        ValueKind:       GlobalBuffer
        ValueType:       F32
        AccQual:         ReadOnly
        AddrSpaceQual:   Global
        IsConst:         true
        IsRestrict:      true
        Name:            in_ptr
        TypeName:        'float*'
      - Size:            8
        Align:           8
        ValueKind:       GlobalBuffer
        ValueType:       F32
        AccQual:         ReadOnly
        AddrSpaceQual:   Constant
        IsConst:         true
        IsRestrict:      true
        Name:            wei_ptr
        TypeName:        'float*'
      - Size:            8
        Align:           8
        ValueKind:       GlobalBuffer
        ValueType:       F32
        AccQual:         Default
        AddrSpaceQual:   Global
        IsRestrict:      true
        Name:            out_ptr
        TypeName:        'float*'
      - Size:            8
        Align:           8
        ValueKind:       HiddenGlobalOffsetX
        ValueType:       I64
      - Size:            8
        Align:           8
        ValueKind:       HiddenGlobalOffsetY
        ValueType:       I64
      - Size:            8
        Align:           8
        ValueKind:       HiddenGlobalOffsetZ
        ValueType:       I64
    CodeProps:       
      KernargSegmentSize: 56
      WavefrontNumSGPRs: 98
      WorkitemNumVGPRs: 43
      KernargSegmentAlign: 4
      GroupSegmentAlign: 4
      PrivateSegmentAlign: 4
      WavefrontSize:   6
...
	.end_amdgpu_code_object_metadata
