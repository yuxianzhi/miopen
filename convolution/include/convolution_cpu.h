#ifndef __CONVOLUTION_CPU_H__
#define __CONVOLUTION_CPU_H__
#include "hip_fundamental.h"
#include "tensor.h"


//#define DEBUG

// Convolution on CPU
// data format: NCHW
// support:
//      same convolution
//      valid convolution
//      dialation convolution
double convolution_cpu(Tensor input, 
                       Tensor weight, 
                       Tensor output,
                       Tensor stride,
                       int *padding,
                       FLOAT padding_val,
                       FLOAT *bias,
                       int* dialation_factor_option)
{
    //std::cout << input.data << std::endl;
    // dialation factor
    int dialation_factor[2];
    if (dialation_factor_option == NULL)
    {
        dialation_factor[0] = 1;
        dialation_factor[1] = 1;
    }
    else
    {
        dialation_factor[0] = dialation_factor_option[0];
        dialation_factor[1] = dialation_factor_option[1];
    }

    int input_start_h, input_start_w, input_end_h, input_end_w;
    int weight_radius_h = (weight.h - 1) / 2;
    int weight_radius_w = (weight.w - 1) / 2;
    int weight_dialation_radius_h = weight_radius_h*dialation_factor[0];
    int weight_dialation_radius_w = weight_radius_w*dialation_factor[1];

    // padding
    if(padding != NULL){
        input_start_h = -1 * padding[0] + weight_dialation_radius_h;
        input_start_w = -1 * padding[1] + weight_dialation_radius_w;
        input_end_h = input.h + padding[0] - weight_dialation_radius_h;
        input_end_w = input.w + padding[1] - weight_dialation_radius_w;
    }
    else{
        input_start_h = weight_dialation_radius_h;
        input_start_w = weight_dialation_radius_w;
        input_end_h = input.h - weight_dialation_radius_h;
        input_end_w = input.w - weight_dialation_radius_w;
    }
    
    // bias
    bool do_bias = false;
    if (bias != NULL)
        do_bias = true;

    int INPUT_CHANNEL_STRIDE = input.h * input.w;
    int INPUT_BATCH_STRIDE = input.c * INPUT_CHANNEL_STRIDE;
    int OUTPUT_CHANNEL_STRIDE = output.h * output.w;
    int OUTPUT_BATCH_STRIDE = output.c * OUTPUT_CHANNEL_STRIDE;
    int WEIGHT_IN_CHANNEL_STRIDE = weight.h * weight.w;
    int WEIGHT_OUT_CHANNEL_STRIDE = weight.c * WEIGHT_IN_CHANNEL_STRIDE;
    cpu_time_tic();
#ifdef DEBUG
    int OPS = 0;
#endif
    for (int n=0; n<output.n; n++){
        for (int c=0; c<output.c; c++){
            for(int hh=input_start_h,h=0; hh<input_end_h; hh+=stride.h,h++){
                for(int ww=input_start_w,w=0; ww<input_end_w; ww+=stride.w,w++){
                    FLOAT val = 0;
                    for (int cc=0; cc<input.c; cc++){
                        for (int i=0,ii=hh-weight_dialation_radius_h; i<weight.h; i++,ii+=dialation_factor[0]){
                            for (int j=0,jj=ww-weight_dialation_radius_w; j<weight.w; j++,jj+=dialation_factor[1]){
                               FLOAT input_val;
                               if (ii<0 || jj<0 || ii>=input.h || jj>=input.w)
                                   input_val = padding_val;
#ifdef DEBUG
                                   if(padding == NULL)
                                       std::cerr << "[ERROR] CPU Convolution Padding!!!!!" << std::endl;
#endif
                               else
                                   input_val = input.data[n*INPUT_BATCH_STRIDE + cc*INPUT_CHANNEL_STRIDE + ii*input.w + jj];
                               FLOAT weight_val = weight.data[c*WEIGHT_OUT_CHANNEL_STRIDE + cc*WEIGHT_IN_CHANNEL_STRIDE + i*weight.w + j];
                               
                               //if(n==0 && c==0 && h==0 && w==54)
                               //    std::cout << "I_C: " << cc << "  W_H: " << i << "  W_W: " << j << "  I_H: " << ii << "  I_W: " << jj << "  value(" << input_val << "," << weight_val << ")" << std::endl;
                               val += input_val * weight_val;
#ifdef DEBUG
                               OPS += 2;
#endif
                            } // end for j
                        } // end for i
                    } // end for cc(input channel)
                    if (do_bias)
                        val += bias[h*output.w + w];
                    if (h >= output.h || w >= output.w)
                    {
                        std::cerr << input_start_h << "  " << input_end_h << std::endl;
                        std::cerr << "[ERROR] CPU Convolution Output out of bound!!!!! (" <<output.h<<","<<output.w<<")   (" << h << "," << w << ")" << std::endl;
                    }
                    output.data[n*OUTPUT_BATCH_STRIDE + c*OUTPUT_CHANNEL_STRIDE + h*output.w + w] = val;
                } // end for w(output w)
            } // end for h(output h)
        } // end for c(output channel)
    } // end for n(batch)
    double convolution_cpu_time = cpu_time_toc();
#ifdef DEBUG
    std::cerr << "[ERROR] CPU Convolution OPS: " << OPS << std::endl;
#endif
    return convolution_cpu_time;
}
#endif
