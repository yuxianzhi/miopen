#ifndef __TENSOR_H__
#define __TENSOR_H__
#include "hip_fundamental.h"

#define FLOAT float

typedef struct {
    int n;
    int c;
    int h;
    int w;
    FLOAT *data;
} Tensor;

inline Tensor cpuTensor(int n, int c, int h, int w, bool do_alloc, bool do_random){
    Tensor cpuTensor;
    cpuTensor.n = n;
    cpuTensor.c = c;
    cpuTensor.h = h;
    cpuTensor.w = w;
    int length = cpuTensor.n*cpuTensor.c*cpuTensor.h*cpuTensor.w;
    int size = sizeof(FLOAT)*length;
    if (do_alloc)
    {
        cpuTensor.data = (FLOAT *)cpuData(size);
        cpuDataRandom<FLOAT>(cpuTensor.data, length, do_random);
    }
    else
        cpuTensor.data = NULL;
    return cpuTensor;
}

inline Tensor cpuTensor(Tensor cpuTensor){
    Tensor tensor;
    tensor.n = cpuTensor.n;
    tensor.c = cpuTensor.c;
    tensor.h = cpuTensor.h;
    tensor.w = cpuTensor.w;
    int size = sizeof(FLOAT)*tensor.n*tensor.c*tensor.h*tensor.w;
    tensor.data = (FLOAT *)cpuData(size);
    return cpuTensor;
}

inline void cpuTensorFree(Tensor tensor){
    cpuDataFree(tensor.data);
}

inline Tensor gpuTensor(int n, int c, int h, int w, bool do_alloc, bool do_random){
    Tensor gpuTensor;
    gpuTensor.n = n;
    gpuTensor.c = c;
    gpuTensor.h = h;
    gpuTensor.w = w;
    int length = gpuTensor.n*gpuTensor.c*gpuTensor.h*gpuTensor.w;
    int size = sizeof(FLOAT)*length;
    if (do_alloc)
    {
        gpuTensor.data = (FLOAT *)gpuData(size);
        gpuDataRandom<FLOAT>(gpuTensor.data, length, do_random);
    }
    else
        gpuTensor.data = NULL;
    return gpuTensor;
}

inline Tensor gpuTensor(Tensor cpuTensor, bool do_copy=true){
    Tensor gpuTensor;
    gpuTensor.n = cpuTensor.n;
    gpuTensor.c = cpuTensor.c;
    gpuTensor.h = cpuTensor.h;
    gpuTensor.w = cpuTensor.w;
    int length = gpuTensor.n*gpuTensor.c*gpuTensor.h*gpuTensor.w;
    int size = sizeof(FLOAT)*length;
    gpuTensor.data = (FLOAT *)gpuData(size);
    if (do_copy)
        cpu2gpu(cpuTensor.data, gpuTensor.data, size);
    else
        gpuTensor.data = (FLOAT *)gpuData(size);
    return gpuTensor;
}

inline Tensor gpuTensor_const(Tensor cpuTensor){
    Tensor gpuTensor;
    gpuTensor.n = cpuTensor.n;
    gpuTensor.c = cpuTensor.c;
    gpuTensor.h = cpuTensor.h;
    gpuTensor.w = cpuTensor.w;
    int size = sizeof(FLOAT)*gpuTensor.n*gpuTensor.c*gpuTensor.h*gpuTensor.w;
    gpuTensor.data = (FLOAT *)gpuData(size);
    cpu2gpu_const(cpuTensor.data, gpuTensor.data, size);
    return gpuTensor;
}


inline void gpuTensorFree(Tensor tensor){
    gpuDataFree(tensor.data);
}


inline bool cpuTensor_cpuTensor_verify(Tensor a, Tensor b)
{
    bool result = true;
    result &= (a.n == b.n);
    result &= (a.c == b.c);
    result &= (a.h == b.h);
    result &= (a.w == b.w);
    size_t N = a.n * a.c * a.h * a.w;
    result &= cpu_cpu_verify(a.data, b.data, N);
    return result;
}
inline bool cpuTensor_gpuTensor_verify(Tensor cpuTensor, Tensor gpuTensor)
{
    bool result = true;
    result &= (cpuTensor.n == gpuTensor.n);
    result &= (cpuTensor.c == gpuTensor.c);
    result &= (cpuTensor.h == gpuTensor.h);
    result &= (cpuTensor.w == gpuTensor.w);
    size_t N = cpuTensor.n * cpuTensor.c * cpuTensor.h * cpuTensor.w;
    result &= cpu_gpu_verify(cpuTensor.data, gpuTensor.data, N);
    return result;
}




// ========= Convolution =========
inline double conv_TFLOPS(Tensor input, Tensor weight, Tensor output, double time)
{
    double OPS = 2.0 * output.n * output.c * output.h * output.w * weight.c * weight.h * weight.w;
    //std::cout << (int)OPS << std::endl;
    return TFLOPS(OPS, time);
}
#endif
