#include "hip/hip_runtime.h"


__constant__ int array[1024];

__global__ void kernel1(hipLaunchParm lp, int *d_dst) {
   int tId = hipThreadIdx_x + hipBlockIdx_x * hipBlockDim_x;
   d_dst[tId] = array[tId];
}

__global__ void kernel2(hipLaunchParm lp, int *d_dst, int *d_src) {
   int tId = hipThreadIdx_x + hipBlockIdx_x * hipBlockDim_x;
   d_dst[tId] = d_src[tId];
}

int main(int argc, char **argv) {
   int *d_array;
   int *d_src;
   hipMalloc((void**)&d_array, sizeof(int) * 1024);
   hipMalloc((void**)&d_src, sizeof(int) * 1024);

   int *test = new int[1024];
   memset(test, 0, sizeof(int) * 1024);

   for (int i = 0; i < 1024; i++) {
     test[i] = 100;
   }

   hipMemcpyToSymbol(array, test, sizeof(int) * 1024);

   hipLaunchKernel(kernel1,   /* compute kernel*/
                dim3(1), dim3(1024), 0/*dynamic shared*/, 0/*stream*/,     /* launch config*/
                d_array);

   hipMemcpy(d_src, test, sizeof(int) * 1024, hipMemcpyHostToDevice);
   hipLaunchKernel(kernel2,   /* compute kernel*/
                dim3(1), dim3(1024), 0/*dynamic shared*/, 0/*stream*/,     /* launch config*/
                d_array, d_src);


   free(test);
   hipFree(d_array);
   hipFree(d_src);

   return 0;
}


