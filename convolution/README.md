## Convolution and Matrix Mutiply

AMD releases the [ROCM](https://github.com/RadeonOpenCompute/ROCm) platform  last year and develops a lot of [deep learning softwares](https://github.com/RadeonOpenCompute/) on it to promote the deep learning system on AMD platform.  

Here we will benchmark AMD [MIOpen](https://github.com/ROCmSoftwarePlatform/MIOpen) library performance on ```AMD Vega10 GPU```. We mainly take interests in convolution layer and full connection layer performance in popular ```CNN``` networks(alexnet, resnet), include forward algorithm and backward algorithm performance. We test the performance of direct comvolution and gemm convolution ```TFLOPS``` performance using ```MIOpen``` library, and we also test the performance of full connect layer matrix multiply performance using ```hipblas``` library. Because the limits of ```MIOpen``` implementation of direct convolution and the memory problem of gemm convolution(AMD wants to find a algorithm to do convolution and does't care which algorithm to convolution at present. ```GEMM```, `Direct`, `Winograd` or `FFT` ),  we can not get some test data, but it doesn't affect we have a global view of AMD ```MIOpen``` library performance.   

### Configure

#### Hardware Configure
| Label | Value |
| :------:| :--------:|
| CPU |  E5-2680 v3 @ 2.50GHz 12 cores|
| Memory | 32GB |
| GPU| AMD Vega10|

#### Software Configure

| Label | Value |
| :------:| :--------:|
| ROCM |  [git latest version](https://github.com/RadeonOpenCompute/ROCm)|
| MIOpen |  [git latest version](https://github.com/ROCmSoftwarePlatform/MIOpen)|
| MIOpengemm|  [git latest version](https://github.com/ROCmSoftwarePlatform/MIOpenGEMM)|
| hipblas |  [git latest version](https://github.com/ROCmSoftwarePlatform/hipBLAS)|
| rocBlas |  [git latest version](https://github.com/ROCmSoftwarePlatform/rocBLAS)|

### Algorithm Complexity

#### Convolution Forward

* problem: output(N, C, H, W) = conv(input(N, C, H, W), filter(N, C, H, W), padding(u, v), stride(p, q))

    complexity: O(2 * output_N * output_C * output_H * output_W * input_C * filter_H * filter_W)

#### Convolution Backward

* backward data problem: dinput = conv(doutput(N,C,H,W) +filter(N, C, H, W), padding(u, v), stride(p, q) )
     backward data complexity: O(2 * doutput_N * doutput_C * doutput_H * doutput_W * dinput_C * filter_H * filter_W)

* backward weight problem: dfilter = conv(doutput(N,C,H,W) + input(N, C, H, W), padding(u, v), stride(p, q) )
     backward weight complexity: O(2 * doutput_N * doutput_C * doutput_H * doutput_W * input_C * dfilter_H * dfilter_W)

#### Matrix Multiply

problem: C(M, K) =  A(M, N) * B(N, K)

complexity: O(2 * M * N * K)

### <span id="Results">Our Tunning Results</span>

Our performance improved 5%~10% compared to AMD official MIOpen deep learning library.

* Alexnet Forward

| Convolution Layer        | 11x11           | 5x5             | 3x3                     | 3x3                     | 3x3                     |
| -------------------------| ----------------|-----------------|-------------------------|-------------------------|-------------------------|
| Input Map                | (128,3,224,224) | (128,64,27,27)  | (128,192,13,13)         | (128,384,13,13)         | (128,256,13,13)         |
| Output Map               | (128,64,55,55)  | (128,192,27,27) | (128,384,13,13)         | (128,256,13,13)         | (128,256,13,13)         |
| Original Performance     | 4.35 TFLOPS     | 5.84 TFLOPS     | no direct implementation| no direct implementation| no direct implementation|
| Optimization Performance | 4.70 TFLOPS     | 6.19 TFLOPS     | no                      | no                      | no                      |

* Resnet Forward

| Convolution Layer        | 7x7             | 1x1           |......|
| -------------------------|-----------------|---------------|------|
| Input Map                | (16,3,224,224)  | (16,64,56,56) |......|
| Output Map               | (16,64,112,112) | (16,64,56,56) |......|
| Original Performance     | 4.71 TFLOPS     | 3.35 TFLOPS   |......|
| Optimization Performance | 4.76 TFLOPS     | 3.54 TFLOPS   |......|


### Performance

#### Alexnet Forward
![](img/alexnet_forward.png) 

#### Alexnet Backward
![](img/alexnet_backward.png) 

#### Resnet Forward
![](img/resnet_forward.png) 

#### Resnet Backward
![](img/resnet_backward.png) 

#### Layerwise Forward
![](img/layerwise_forward.png) 

#### Layerwise Backward
![](img/layerwise_backward.png) 

 
