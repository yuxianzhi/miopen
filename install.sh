#!/bin/bash

MIOPEN_HOME=/home/yxz/MIOpen

mkdir build
mkdir build/opencl
mkdir install
mkdir install/opencl

cd build/opencl
cmake -DMIOPEN_BACKEND=OpenCL -DCMAKE_INSTALL_PREFIX=${MIOPEN_HOME}/install/opencl ../..
make -j33
make install
make tests
export LD_LIBRARY_PATH=${MIOPEN_HOME}/install/opencl/lib:$LD_LIBRARY_PATH
cp bin/test_conv ${MIOPEN_HOME}/install/opencl/bin/

${MIOPEN_HOME}/install/opencl/bin/test_conv --all
